/*
 * Copyright © 2020 Hedzr Yeh.
 */

package rediser

import (
	"errors"
	"fmt"
	"github.com/mediocregopher/radix/v3"
	"github.com/mediocregopher/radix/v3/resp/resp2"
	"log"
	"math/rand"
	"time"
)

func a() {
	// this is a ConnFunc which will set up a connection which is authenticated
	// and has a 1 minute timeout on all operations
	customConnFunc := func(network, addr string) (radix.Conn, error) {
		return radix.Dial(network, addr,
			radix.DialTimeout(1*time.Minute),
			radix.DialAuthPass("mySuperSecretPassword"),
		)
	}

	// this cluster will use the ClientFunc to create a pool to each node in the
	// cluster. The pools also use our customConnFunc, but have more connections
	poolFunc := func(network, addr string) (radix.Client, error) {
		return radix.NewPool(network, addr, 100, radix.PoolConnFunc(customConnFunc))
	}

	// // this pool will use our ConnFunc for all connections it creates
	// client, err := radix.NewPool("tcp", "127.0.0.1:6379", 10, radix.PoolConnFunc(customConnFunc))

	client, err := radix.NewCluster([]string{"127.0.0.1:6379"}, radix.ClusterPoolFunc(poolFunc))
	if err != nil {
		// handle error'
		return
	}

	// https://godoc.org/github.com/mediocregopher/radix

	defer client.Close()

	var redisErr resp2.Error
	err = client.Do(radix.Cmd(nil, "AUTH", "wrong password"))
	if errors.As(err, &redisErr) {
		log.Printf("redis error returned: %s", redisErr.E)
	}

	// Have PersistentPubSub pick a random cluster node everytime it wants to
	// make a new connection. If the node fails PersistentPubSub will
	// automatically pick a new node to connect to.
	ps := radix.PersistentPubSub("", "", func(string, string) (radix.Conn, error) {
		topo := client.Topo()
		node := topo[rand.Intn(len(topo))]
		return radix.Dial("tcp", node.Addr)
	})

	// Use the PubSubConn as normal.
	msgCh := make(chan radix.PubSubMessage)
	err = ps.Subscribe(msgCh, "myChannel")
	for msg := range msgCh {
		log.Printf("publish to channel %q received: %q", msg.Channel, msg.Message)
	}

	err = client.Do(radix.Cmd(nil, "SET", "foo", "someval"))

	var fooVal string
	err = client.Do(radix.Cmd(&fooVal, "GET", "foo"))

	var fooValB []byte
	err = client.Do(radix.Cmd(&fooValB, "GET", "foo"))

	var barI int
	err = client.Do(radix.Cmd(&barI, "INCR", "bar"))

	var bazEls []string
	err = client.Do(radix.Cmd(&bazEls, "LRANGE", "baz", "0", "-1"))

	var buzMap map[string]string
	err = client.Do(radix.Cmd(&buzMap, "HGETALL", "buz"))

	// var fooVal string
	p := radix.Pipeline(
		radix.FlatCmd(nil, "SET", "foo", 1),
		radix.Cmd(&fooVal, "GET", "foo"),
	)
	if err := client.Do(p); err != nil {
		panic(err)
	}
	fmt.Printf("fooVal: %q\n", fooVal)
}
