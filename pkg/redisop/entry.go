/*
 * Copyright © 2020 Hedzr Yeh.
 */

package redisop

import (
	"github.com/sirupsen/logrus"
	"gitlab.com/hedzr/mqttlib/pkg/tool/vxconf"
	"os"
)

// 不支持多个redis配置分别启动为多个hub后台，仅允许单例模型
//

type RedisOpOpt func(hub *hubStruct)

func IsStarted() bool {
	return hub.IsStarted()
}

func Start(opts ...RedisOpOpt) {
	if hub.IsStarted() {
		return
	}

	logrus.Debugf("    Starting Redis huber...")
	for _, opt := range opts {
		opt(&hub)
	}
	hub.start()
}

func StartWithConfig(config *Config) {
	hub.startWith(config)
}

func Stop() {
	logrus.Debugf("    Stopping Redis huber...")
	// cache.HashDel(fmt.Sprintf("%s:instances", cli_common.AppName), id.GetInstanceId())
	// cache.DelZone()
	// redis cluster
	hub.stop()
}

func loadDefaultConfig(prefix string) (config *Config) {
	config = new(Config)
	_ = vxconf.LoadSectionTo(prefix, config)

	if s := os.Getenv("CACHE_ADDR"); len(s) > 0 {
		config.Peers = s
	}

	logrus.Debugf("      [cache] redis: %v", config.Peers)
	return
}

// WithPrefixKey specify the prefix key of cmdr options store.
//
// For prefixKey="mqtt.server.storage.redis", the config structure might be:
//
//     app:
//       mqtt:
//         server:
//           storage:
//             what: redis
//             redis:
//               devel:
//                 peers: localhost:6379
//                 readonly-route: true         # read-write separately
//                 enable-cluster:              # for redis cluster, it must be true
//               devel-95:
//                 peers: 192.168.0.95:6379
//                 user:
//                 pass:
//                 db:
//                 dial-timeout:
//                 read-timeout:
//                 write-timeout:
//                 readonly-route:
//                 enable-cluster:
//
//
func WithPrefixKey(prefixKey string) RedisOpOpt {
	return func(hub *hubStruct) {
		hub.prefixKey = prefixKey
	}
}

// func WithConfig(config *Config) RedisOpOpt {
//
// }
