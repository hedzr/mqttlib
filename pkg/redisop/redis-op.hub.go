/*
 * Copyright © 2020 Hedzr Yeh.
 */

package redisop

import (
	"github.com/go-redis/redis"
	"github.com/sirupsen/logrus"
	"strings"
	"sync/atomic"
	"time"
)

type (
	hubStruct struct {
		clients   map[redis.Cmdable]bool
		rw        redis.Cmdable
		rd        redis.Cmdable
		exitCh    chan bool
		exited    int32
		prefixKey string
		reading   chan *kvpair
		writing   chan *kvpair
		// broadcast  chan []byte
		// register   chan *redis.ClusterClient
		// unregister chan *redis.ClusterClient
	}

	kvpair struct {
		key, value string
	}

	Config struct {
		Peers         string        `yaml:"peers"`
		Username      string        `yaml:"user"`
		Password      string        `yaml:"pass"`
		Db            int           `yaml:"db"`
		ReadonlyRoute bool          `yaml:"readonly-route"`
		DialTimeout   time.Duration `yaml:"dial-timeout"`
		ReadTimeout   time.Duration `yaml:"read-timeout"`
		WriteTimeout  time.Duration `yaml:"write-timeout"`
		EnableCluster bool          `yaml:"enable-cluster"`
	}
)

var (
	hub = hubStruct{
		clients: make(map[redis.Cmdable]bool),
		rw:      nil,
		rd:      nil,
		// exitCh:    make(chan bool),
		exited:    0,
		prefixKey: "server.pub.deps.redis",
		reading:   make(chan *kvpair),
		writing:   make(chan *kvpair),
		// broadcast:  make(chan []byte),
		// register:   make(chan *redis.ClusterClient),
		// unregister: make(chan *redis.ClusterClient),
	}
)

func (h *hubStruct) IsStarted() bool {
	return h.rw != nil
}

func (h *hubStruct) start() {
	config := loadDefaultConfig(h.prefixKey)
	h.startWith(config)
}

func (h *hubStruct) startWith(config *Config) {

	if h.rw != nil {
		return
	}

	// for debugging only, to simulate inserting automatically
	// go h.runSimulator()

	if config.DialTimeout == 0 {
		config.DialTimeout = 10 * time.Second
	}
	if config.ReadTimeout == 0 {
		config.ReadTimeout = 20 * time.Second
	}
	if config.WriteTimeout == 0 {
		config.WriteTimeout = 30 * time.Second
	}

	if config.EnableCluster {
		client := redis.NewClusterClient(&redis.ClusterOptions{
			Addrs:          strings.Split(config.Peers, ","),
			RouteByLatency: config.ReadonlyRoute,
			DialTimeout:    config.DialTimeout,
			ReadTimeout:    config.ReadTimeout,
			WriteTimeout:   config.WriteTimeout,
			Password:       config.Password,
		})
		h.clients[client] = true
		h.rd = client

		clientW := redis.NewClusterClient(&redis.ClusterOptions{
			Addrs:        strings.Split(config.Peers, ","),
			ReadOnly:     false,
			DialTimeout:  config.DialTimeout,
			ReadTimeout:  config.ReadTimeout,
			WriteTimeout: config.WriteTimeout,
			Password:     config.Password,
		})
		h.clients[clientW] = true
		h.rw = clientW
	} else {
		// peers := vxconf.GetStringSliceR("server.pub.deps.redis.peers", nil)
		client := redis.NewClient(&redis.Options{
			Addr:         config.Peers,
			DialTimeout:  config.DialTimeout,
			ReadTimeout:  config.ReadTimeout,
			WriteTimeout: config.WriteTimeout,
			Password:     config.Password,
		})
		h.clients[client] = true
		h.rd = client

		clientW := redis.NewClient(&redis.Options{
			Addr:         config.Peers,
			DialTimeout:  config.DialTimeout,
			ReadTimeout:  config.ReadTimeout,
			WriteTimeout: config.WriteTimeout,
			Password:     config.Password,
		})
		h.clients[clientW] = true
		h.rw = clientW
	}

	if atomic.CompareAndSwapInt32(&h.exited, 1, 0) {
		logrus.Warn("h.exited must be 0 (zero) at this time")
		return
	}

	if h.exitCh == nil {
		h.exitCh = make(chan bool)
	}
	go h.run(h.exitCh)
}

func (h *hubStruct) stop() {
	if atomic.CompareAndSwapInt32(&h.exited, 0, 1) {
		ch := h.exitCh
		h.exitCh = make(chan bool)
		close(ch)
		// h.exitCh = nil
	}

	for k := range hub.clients {
		if cc, ok := k.(*redis.ClusterClient); ok {
			cc.Close()
		} else if bc, ok := k.(*redis.Client); ok {
			bc.Close() // it will break the for loop in ws_hello()
		}
	}
	h.clients = make(map[redis.Cmdable]bool)
	h.rw = nil
	h.rd = nil
	// h.exitCh = make(chan bool)
}

func (h *hubStruct) run(done chan bool) {
	ticker := time.NewTicker(120 * time.Second)
	defer func() {
		ticker.Stop()
		logrus.Debugf("chat hub exiting. (WebSocket message processing service)")
	}()

	for {
		select {
		case tm := <-ticker.C:
			logrus.Debugf("refreshing clients at %v", tm)
			// uid := rand.Intn(math.MaxInt32)
			// did := strconv.Itoa(rand.Intn(32))
			// zid := rand.Intn(32)
			// PutUserHash(uint64(uid), did, int2str(zid))

		case <-done:
			return

			// case client := <-h.register:
			// 	h.clients[client] = true
			// 	size := int(unsafe.Sizeof(*client))
			// 	logrus.Debugf("=== new client in. %v clients x %v bytes. %s", hub.clients, size, client.userAgent) // and log it for debugging
			//
			// case client := <-h.unregister:
			// 	if _, ok := h.clients[client]; ok {
			// 		logrus.Println("=== the client leaved.", hub.clients) // and log it for debugging
			// 		delete(h.clients, client)
			// 		close(client.textSend)
			// 	}

		}
	}
}
