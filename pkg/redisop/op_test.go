/*
 * Copyright © 2020 Hedzr Yeh.
 */

package redisop_test

import (
	"fmt"
	"github.com/go-redis/redis"
	"gitlab.com/hedzr/mqttlib/pkg/redisop"
	"reflect"
	"strconv"
	"testing"
	"time"
)

const (
	addr    = "127.0.0.1:26379"
	cluster = false
	// addr    = "127.0.0.1:6379"
	// cluster = true
)

func setupRedis() func() {
	redisop.StartWithConfig(&redisop.Config{
		Peers:         addr,
		EnableCluster: cluster,
	})
	return func() {
		redisop.Stop()
	}
}

func TestConn(t *testing.T) {
	defer setupRedis()()

	redisop.Put("r:v:o:k", 1)
}

func TestPublish(t *testing.T) {
	defer setupRedis()()
	done := make(chan struct{})
	redisop.Publish("mychannel", "hello budy!\n")
	go func() {
		pubsub := redisop.Subscribe("mychannel")
		msg, _ := pubsub.Receive()
		fmt.Println("Receive from channel:", msg)
		done <- struct{}{}
	}()

	<-done
}

func TestPipeline(t *testing.T) {
	defer setupRedis()()
	err := redisop.PipelineExec(func(pl redis.Pipeliner) (err error) {
		pl.Set("pipe", 0, 0)
		pl.Incr("pipe")
		pl.Incr("pipe")
		pl.Incr("pipe")
		return
	})
	if err != nil {
		t.Fatal(err)
	}
}

// Any formats any value as a string.
func Any(value interface{}) string {
	return formatAtom(reflect.ValueOf(value))
}

// formatAtom formats a value without inspecting its internal structure.
func formatAtom(v reflect.Value) string {
	switch v.Kind() {
	case reflect.Invalid:
		return "invalid"
	case reflect.Int, reflect.Int8, reflect.Int16,
		reflect.Int32, reflect.Int64:
		return strconv.FormatInt(v.Int(), 10)
	case reflect.Uint, reflect.Uint8, reflect.Uint16,
		reflect.Uint32, reflect.Uint64, reflect.Uintptr:
		return strconv.FormatUint(v.Uint(), 10)
		// ...floating-point and complex cases omitted for brevity...
	case reflect.Bool:
		return strconv.FormatBool(v.Bool())
	case reflect.String:
		return strconv.Quote(v.String())
	case reflect.Chan, reflect.Func, reflect.Ptr, reflect.Slice, reflect.Map:
		return v.Type().String() + " 0x" +
			strconv.FormatUint(uint64(v.Pointer()), 16)
	default: // reflect.Array, reflect.Struct, reflect.Interface
		return v.Type().String() + " value"
	}
}

func TestReflectTypes(t *testing.T) {
	var x int64 = 1
	var d time.Duration = 1 * time.Nanosecond
	fmt.Println(Any(x))                  // "1"
	fmt.Println(Any(d))                  // "1"
	fmt.Println(Any([]int64{x}))         // "[]int64 0x8202b87b0"
	fmt.Println(Any([]time.Duration{d})) // "[]time.Duration 0x8202b87e0"
}
