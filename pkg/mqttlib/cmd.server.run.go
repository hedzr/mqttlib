/*
 * Copyright © 2020 Hedzr Yeh.
 */

package mqttlib

import (
	"github.com/hedzr/cmdr"
	"gitlab.com/hedzr/mqttlib/pkg/mqttlib/auth"
	"gitlab.com/hedzr/mqttlib/pkg/mqttlib/mqtt"
	"gitlab.com/hedzr/mqttlib/pkg/mqttlib/server"
)

func mqttServerRun(cmd *cmdr.Command, args []string) (err error) {
	err = server.Run(
		server.WithMaxVersion(reqVersionStr),
		server.WithMaxProtocolLevel(testReqVersion()),
		server.WithMaxQoS(mqtt.QoS2),

		server.WithStateContextOpts(mqtt.WithNoWildMatchFilter(cmdr.GetBoolR("mqtt.server.no-wildcard-filter"))),
		server.WithSessionStoreNoSysStats(cmdr.GetBoolR("mqtt.server.no-sys-stats")),
		server.WithSessionStoreNoSysStatsLog(cmdr.GetBoolR("mqtt.server.no-sys-stats-log")),
		server.WithSessionStoreResetStorage(cmdr.GetBoolR("mqtt.server.reset-storage")),
		server.WithStrictClientId(!cmdr.GetBoolR("mqtt.server.no-strict-client-id")),
		server.WithStrictProtocolName(!cmdr.GetBoolR("mqtt.server.no-strict-protocol-name")),
		// server.WithResetStorage(cmdr.GetBoolR("mqtt.server.reset-storage")),
		server.WithTracingEnabled(cmdr.GetBoolR("trace", false)),

		server.WithTLSConfig("mqtt.server.tls", "mqtt.server"),
		server.WithMQTTSNMode(cmdr.GetBoolR("mqtt.server.mqttsn")),
		server.WithMQTTOverWebSocket(cmdr.GetBoolR("mqtt.server.websocket")),

		server.WithStateContextOpts(
			mqtt.WithAuthenticator(auth.NewDefaultTigerLaohuAuthenticator(newFileCredLoader())),
		),
	)
	return
}
