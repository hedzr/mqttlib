/*
 * Copyright © 2020 Hedzr Yeh.
 */

package ws

func New() *WsF {
	return &WsF{}
}

type WsF struct {
}

func (s *WsF) Start() (err error) {
	return
}

func (s *WsF) Stop() (err error) {
	return
}
