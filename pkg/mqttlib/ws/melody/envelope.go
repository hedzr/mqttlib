/*
 * Copyright © 2020 Hedzr Yeh.
 */

package melody

type envelope struct {
	t      int
	msg    []byte
	filter filterFunc
}
