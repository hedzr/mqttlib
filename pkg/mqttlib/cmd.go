/*
 * Copyright © 2020 Hedzr Yeh.
 */

package mqttlib

import (
	"fmt"
	"github.com/hedzr/cmdr"
	"github.com/sirupsen/logrus"
	"gitlab.com/hedzr/mqttlib/pkg/mqttlib/client"
	"gitlab.com/hedzr/mqttlib/pkg/mqttlib/mqtt"
	"gitlab.com/hedzr/mqttlib/pkg/mqttlib/server"
	"gitlab.com/hedzr/mqttlib/pkg/mqttlib/tls"
	"time"
)

// var reqVersion mqtt.ProtocolLevel = mqtt.ProtocolLevelForV311
var reqVersion mqtt.ProtocolLevel = mqtt.ProtocolLevelForV50
var reqVersionStr = "5.0"

func testReqVersion() mqtt.ProtocolLevel {
	return reqVersion
}

func testReqVersionStr() string {
	return reqVersionStr
}

func FindServer() server.ServerCntr {
	return server.GetServer()
}

func AttachToCmdr(mqttOptCmd cmdr.OptCmd) {

	mqttOptCmd.NewFlagV(false, "v3", "v3").Description("enable MQTT V3.x").Group("MQTT Version").ToggleGroup("MQTT Version").Action(func(cmd *cmdr.Command, args []string) (err error) {
		reqVersion = mqtt.ProtocolLevelForV311
		reqVersionStr = "3.1"
		return
	})
	mqttOptCmd.NewFlagV(false, "v311", "v311").Description("enable MQTT V3.1.1").Group("MQTT Version").ToggleGroup("MQTT Version").Action(func(cmd *cmdr.Command, args []string) (err error) {
		reqVersion = mqtt.ProtocolLevelForV311
		reqVersionStr = "3.1.1"
		return
	})
	mqttOptCmd.NewFlagV(true, "v5", "v5").Description("enable MQTT V5.x").Group("MQTT Version").ToggleGroup("MQTT Version").Action(func(cmd *cmdr.Command, args []string) (err error) {
		reqVersion = mqtt.ProtocolLevelForV50
		reqVersionStr = "5.0"
		return
	})

	serverCmd := mqttOptCmd.NewSubCommand("server", "s", "mqtt-server").
		Description("test mqtt server", "test mqtt server,\nverbose long descriptions here.").
		Group("Test").
		Action(mqttServerRun)

	serverSubCommands(serverCmd)

	clientCmd := mqttOptCmd.NewSubCommand("client", "c", "mqtt-client").
		Description("test mqtt client", "test mqtt client,\nverbose long descriptions here.").
		Group("Test")

	clientSubCommands(clientCmd)

	certCmd := mqttOptCmd.NewSubCommand("cert").
		Description("certification tool (such as create-ca, create-cert, ...)", "certification tool (such as create-ca, create-cert, ...)\nverbose long descriptions here.").
		Group("Tool")

	certSubCommands(certCmd)
}

func certSubCommands(certOptCmd cmdr.OptCmd) {
	caCmd := certOptCmd.NewSubCommand("ca", "ca").
		Description("certification tool (such as create-ca, create-cert, ...)", "certification tool (such as create-ca, create-cert, ...)\nverbose long descriptions here.").
		Group("CA")

	caCreateCmd := caCmd.NewSubCommand("create", "c").
		Description("create NEW CA certificates").
		Group("Tool").
		Action(tls.CaCreate)
	logrus.Trace(caCreateCmd)

	// certCmd := certOptCmd.NewSubCommand().
	// 	Titles("", "cert").
	// 	Description("certification tool (such as create-ca, create-cert, ...)", "certification tool (such as create-ca, create-cert, ...)\nverbose long descriptions here.").
	// 	Group("Tool")

	certCreateCmd := certOptCmd.NewSubCommand("create", "c").
		Description("create NEW certificates").
		Group("Tool").
		Action(tls.CertCreate)
	logrus.Trace(certCreateCmd)

	certCreateCmd.NewFlagV([]string{}, "hostnames", "hns").
		Description("Comma-separated hostnames and IPs to generate a certificate for")
	certCreateCmd.NewFlagV("", "start-date", "sd", "from", "valid-from").
		Description("Creation date formatted as Jan 1 15:04:05 2011 (default now)")
	certCreateCmd.NewFlagV(365*10*24*time.Hour, "valid-for", "d", "duration").
		Description("Duration that certificate is valid for")
}

func serverSubCommands(serverCmd cmdr.OptCmd) {
	serverCmd.NewFlagV(":1883", "addr", "a", "address").
		Description("address, (host and/or port)").
		Group("").
		Placeholder("HOST:PORT")
	serverCmd.NewFlagV(1883, "port", "p").
		Description("MQTT port (optional)").
		Placeholder("PORT").
		Group("").
		Action(func(cmd *cmdr.Command, args []string) (err error) {
			cmdr.Set("mqtt.server.ports.default", cmdr.GetIntR("mqtt.server.port"))
			return
		})

	serverCmd.NewFlagV(false, "no-wildcard-filter", "nwf").
		Description("no wildcard topic filter support").
		OnSet(func(keyPath string, value interface{}) {
			logrus.Debugf("%%-> -> OnSet(%q, %v) <-", keyPath, value)
		}).
		Group("Switches")
	serverCmd.NewFlagV(false, "no-sys-stats", "nss").
		Description("without $SYS statistics").
		OnSet(func(keyPath string, value interface{}) {
			logrus.Debugf("%%-> -> OnSet(%q, %v) <-", keyPath, value)
		}).
		Group("Switches")
	serverCmd.NewFlagV(false, "no-sys-stats-log", "nssl").
		Description("without $SYS statistics logging printing").
		OnSet(func(keyPath string, value interface{}) {
			logrus.Debugf("%%-> -> OnSet(%q, %v) <-", keyPath, value)
		}).
		Group("Switches")
	serverCmd.NewFlagV(false, "reset-storage", "rs").
		Description("clear the persistent storage").
		OnSet(func(keyPath string, value interface{}) {
			logrus.Debugf("%%-> -> OnSet(%q, %v) <-", keyPath, value)
		}).
		Group("Switches")
	serverCmd.NewFlagV(false, "no-strict-client-id", "nsci").
		Description("don't verify client id strictly").
		// Examples("use `-sci-` or `--strict-client-id-` to disable the flag").
		OnSet(func(keyPath string, value interface{}) {
			logrus.Debugf("%%-> -> OnSet(%q, %v) <-", keyPath, value)
		}).
		Group("Switches")
	serverCmd.NewFlagV(false, "no-strict-protocol-name", "nspn").
		Description("don't verify protocol name in CONNECT packet strictly").
		OnSet(func(keyPath string, value interface{}) {
			logrus.Debugf("%%-> -> OnSet(%q, %v) <-", keyPath, value)
		}).
		Group("Switches")

	serverCmd.NewFlagV("", "cafile", "ca").
		Description("CA cert path (.cer,.crt,.pem) if it's standalone").
		Group("TLS").
		Placeholder("PATH")
	serverCmd.NewFlagV("", "cert").
		Description("server public-cert path (.cer,.crt,.pem)").
		Group("TLS").
		Placeholder("PATH")
	serverCmd.NewFlagV("", "key").
		Description("server private-key path (.cer,.crt,.pem)").
		Group("TLS").
		Placeholder("PATH")
	serverCmd.NewFlagV(false, "client-auth").
		Description("enable client cert authentication").
		Group("TLS")
	serverCmd.NewFlagV(2, "tls-version", "tv", "tls-ver").
		Description("tls-version: 0,1,2,3").
		Group("TLS")
	serverCmd.NewFlagV(8883, "tls-port", "sp").
		Description("MQTT over TLS port").
		Placeholder("PORT").
		Group("TLS").
		Action(func(cmd *cmdr.Command, args []string) (err error) {
			cmdr.Set("mqtt.server.ports.tls", cmdr.GetIntR("mqtt.server.tls-port"))
			return
		})

	serverCmd.NewFlagV(false, "mqttsn", "sn").
		Description("enable MQTTSN mode (with 1883/udp)").
		Group("zzz0.MQTTSN")
	serverCmd.NewFlagV(1884, "sn-port", "snp").
		Description("MQTTSN udp port").
		Placeholder("PORT").
		Group("zzz0.MQTTSN").
		Action(func(cmd *cmdr.Command, args []string) (err error) {
			cmdr.Set("mqtt.server.ports.sn", cmdr.GetIntR("mqtt.server.sn-port"))
			return
		})
	serverCmd.NewFlagV(false, "websocket", "ws", "web-socket").
		Description("enable MQTT over WebSocket mode (with 443/tcp)").
		Group("zz00.WebSocket")
	serverCmd.NewFlagV(443, "ws-port", "wsp").
		Description("MQTT over WebSocket port").
		Placeholder("PORT").
		Group("zz00.WebSocket").
		Action(func(cmd *cmdr.Command, args []string) (err error) {
			cmdr.Set("mqtt.server.ports.websocket", cmdr.GetIntR("mqtt.server.ws-port"))
			return
		})
	serverCmd.NewFlagV("8k", "max-msg-size", "mms").
		Description("Max message size for WebSocket (Valid formats: 2k, 2kb, 2kB, 2KB. Suffixes: k, m, g, t, p, e)").
		Placeholder("SIZE").
		Group("zz00.WebSocket").
		Action(func(cmd *cmdr.Command, args []string) (err error) {
			z := cmdr.GetKibibytesR("mqtt.server.max-msg-size")
			cmdr.Set("mqtt.server.websocket.max-message-size", z)
			fmt.Printf("\n\nmax-msg-size <= %v (%v)\n\n\n", z, cmdr.GetStringR("mqtt.server.max-msg-size"))
			return
		})

	serverCmd.NewFlagV(false, "dry-run", "dr", "dryrun").
		Description("In dry-run mode, arguments will be parsed, tcp listener will not be stared.").
		Group("zzz1.Dry Run")
}

func clientSubCommands(clientCmd cmdr.OptCmd) {
	// clientCmd.NewFlagV(1).
	// 	Titles("sp", "subpub").
	// 	Description("must sub or pub?").
	// 	Placeholder("PUBSUB")

	// clientCmd.NewFlagV(client.Topic).
	// 	Titles("tpc", "topic").
	// 	Description("topic should be made").
	// 	Group("").
	// 	Placeholder("TOPIC")

	clientCmd.NewFlagV(client.Broker, "broker", "b").
		Description("broker address, = -h").
		Group("").
		Placeholder("BROKER")

	clientCmd.NewFlagV(client.Broker, "host", "h").
		Description("broker address, = -b").
		Group("").
		Placeholder("HOST[:PORT]")

	clientCmd.NewFlagV(client.Parallel, "parallel", "p").
		Description("publish how many goroutines").
		Placeholder("PARALLEL").
		Group("Test")

	// clientCmd.NewFlagV(client.Times).
	// 	Titles("t", "times").
	// 	Description("publish times").
	// 	Placeholder("TIMES").
	// 	Group("Test")

	clientCmd.NewFlagV(time.Duration(0), "sleep").
		Description("sleep time between each sending").
		Group("Test")

	clientCmd.NewFlagV(0, "qos", "qos").
		Description("qos (= 0,1,2)").
		Group("MQTT QoS")

	clientCmd.NewFlagV("", "cafile", "ca").
		Description("CA cert path (.cer,.crt,.pem)").
		Group("TLS").
		Placeholder("PATH")
	clientCmd.NewFlagV("", "cert").
		Description("client public-cert path for dual auth (.cer,.crt,.pem)").
		Group("TLS").
		Placeholder("PATH")
	clientCmd.NewFlagV("", "key").
		Description("client private-key path for dual auth (.cer,.crt,.pem)").
		Group("TLS").
		Placeholder("PATH")
	clientCmd.NewFlagV(false, "client-auth").
		Description("enable client cert authentication").
		Group("TLS")
	clientCmd.NewFlagV(2, "tls-version").
		Description("tls-version: 0,1,2,3").
		Group("TLS")

	pubCommands(clientCmd)
	subCommands(clientCmd)
}

func pubCommands(clientCmd cmdr.OptCmd) {
	pubCmd := clientCmd.NewSubCommand("pub", "p", "publish").
		Description("test mqtt client", "test mqtt client,\nverbose long descriptions here.").
		Group("Test").
		Action(func(cmd *cmdr.Command, args []string) (err error) {
			client.PubRun(testReqVersion())
			return
		})

	pubCmd.NewFlagV("", "topic", "t", "topic-name").
		Description("topic").
		Group("")

	pubCmd.NewFlagV("", "payload", "p", "msg").
		Description("message payload").
		Group("0001.Payload")

	pubCmd.NewFlagV("", "file", "f", "input-file").
		Description("treat the file content as msg body line by line").
		Group("0001.Payload")

	pubCmd.NewFlagV(false, "clean-session", "cs").
		Description("clean or keep session, default is keep session").
		Group("Session")

	pubCmd.NewFlagV("", "client-id", "cid").
		Description("the client id. default is $mqtt.client.pub.persist-client-id").
		Group("Session")

	pubCmd.NewFlagV(false, "retain", "r").
		Description("retain flag").
		Group("")

	// pubCmd.NewFlagV(0).
	// 	Titles("", "qos").
	// 	Description("qos (= 0,1,2)", "")

	pubCmd.NewFlagV(1, "repeat", "rpt").
		Description("repeat times.").
		Group("Auto Repeat")

	pubCmd.NewFlagV(1, "start-number", "sn").
		Description("the start number of publishing messages.").
		Group("Auto Repeat")

	pubCmd.NewFlagV(false, "rand", "rnd", "random").
		Description("append random string at the end of message text").
		Group("Auto Repeat")

	pubCmd.NewFlagV(0, "min", "min", "min-length").
		Description("random string minimal length, eg. 4096").
		Placeholder("MIN").
		Group("Auto Repeat")

	pubCmd.NewFlagV(32768, "max", "max", "max-length").
		Description("random string maximal length, eg. 8192").
		Placeholder("MAX").
		Group("Auto Repeat")

	attachWillMsgOptions(pubCmd)
}

func subCommands(clientCmd cmdr.OptCmd) {
	subCmd := clientCmd.NewSubCommand("sub", "s", "subscribe").
		Description("test mqtt client", "test mqtt client,\nverbose long descriptions here.").
		Group("Test").
		Action(func(cmd *cmdr.Command, args []string) (err error) {
			client.SubRun(testReqVersion())
			return
		})

	subCmd.NewFlagV([]string{"#"}, "topic-filter", "t", "tf").
		Description("topic filters with comma sep, eg: 'a/b/c,d/+'").
		Group("")

	// subCmd.NewFlagV(0).
	// 	Titles("", "qos").
	// 	Description("qos (= 0,1,2)").
	// 	Group("")

	//

	// subCmd.NewFlagV("").
	// 	Titles("wf", "will-file", "will-input-file").
	// 	Description("treat the file content as msg body line by line").
	// 	Group("")

	attachWillMsgOptions(subCmd)
}

func attachWillMsgOptions(oc cmdr.OptCmd) {

	oc.NewFlagV("", "will-topic", "wt", "will-topic-name").
		Titles("wt", "will-topic", "will-topic-name").
		Description("will topic").
		Group("Will Msg")

	oc.NewFlagV("", "will-payload", "wp", "will-msg").
		Description("will message").
		Group("Will Msg")

	oc.NewFlagV(false, "will-retain", "wr").
		Description("will retain flag").
		Group("Will Msg")

	oc.NewFlagV(0, "will-qos", "wq").
		Description("will qos (= 0,1,2)").
		Group("Will Msg")

}
