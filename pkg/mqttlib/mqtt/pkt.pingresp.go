/*
 * Copyright © 2020 Hedzr Yeh.
 */

package mqtt

import (
	"bytes"
	"io"
)

// ....................................................

func NewPingrespBuilder() PacketBuilder {
	return &pingrespBuilder{}
}

type pingrespBuilder struct {
	data []byte
}

func (b *pingrespBuilder) IsResending() bool {
	return false
}

func (b *pingrespBuilder) RequestResend() {
}

func (b *pingrespBuilder) NextPktId(ctx *StateContext) uint16 {
	return 0
}

func (b *pingrespBuilder) ReportType() ReportType {
	return PINGRESP
}

func (b *pingrespBuilder) Build(ctx *StateContext, pkg *Pkg) (err error) {
	bb := bytes.NewBuffer([]byte{0xd0, 0x00})
	b.data = bb.Bytes()
	return
}

func (b *pingrespBuilder) WriteTo(w io.Writer) (n int, err error) {
	n, err = w.Write(b.Bytes())
	return
}

func (b *pingrespBuilder) Bytes() (bytes []byte) {
	return b.data
}

func (b *pingrespBuilder) AsString() (str string) {
	return
}

// ....................................................
