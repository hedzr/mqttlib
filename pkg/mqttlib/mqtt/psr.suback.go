/*
 * Copyright © 2020 Hedzr Yeh.
 */

package mqtt

// ....................................................

func NewSubackParser(to State) PacketParser {
	return &subackParser{base: newBase("subackParser"), to: to}
}

type subackParser struct {
	base
	to State
}

func (p *subackParser) GetToState() State {
	return p.to
}

func (p *subackParser) CreateVH() VariantHeader {
	return newSubackVH()
}

func (p *subackParser) CreatePayload() Payload {
	return newSubackPayload()
}

func (p *subackParser) OnAction(ctx *StateContext, pkg *Pkg) (err error) {
	if vh, ok := pkg.VH.(*subackVH); ok {
		if payload, ok := pkg.Payload.(*subackPayload); ok {
			p.Debug("       %s: id=%v, vh=%v, payload=%v", pkg.ReportType, pkg.PacketIdentifier, vh, payload)

			// unsubscribed
		}
	}
	return
}

func (p *subackParser) OnError(ctx *StateContext, pkg *Pkg, reason error) (err error) {
	err = reason
	return
}

func newSubackVH() *subackVH {
	return &subackVH{}
}

type subackVH struct{}

func (s *subackVH) Apply(ctx *StateContext, pkg *Pkg, data []byte, pos int) (newPos int, err error) {
	newPos = pos // Apply() must return newPos to pointer the updated new position
	pkg.VH = s   // Apply() must assign itself into pkg.VH
	return
}

type subackPayload struct {
}

func newSubackPayload() *subackPayload {
	return &subackPayload{}
}

func (s *subackPayload) Apply(ctx *StateContext, pkg *Pkg, data []byte, pos int) (newPos int, err error) {
	newPos = ctx.clientSubsHolder.ApplyRetCodes(ctx, pkg.PacketIdentifier, data, pos)
	// newPos = pos    // Apply() must return newPos to pointer the updated new position
	pkg.Payload = s // Apply() must be assign itself into pkg.Payload
	return
}
