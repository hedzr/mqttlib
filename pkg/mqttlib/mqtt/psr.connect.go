/*
 * Copyright © 2020 Hedzr Yeh.
 */

package mqtt

import (
	"gitlab.com/hedzr/mqttlib/pkg/mqttlib/errors"
	"time"
)

// ....................................................

func NewConnectParser(to State, strictClientId bool) PacketParser {
	return &connectParser{base: newBase("connectParser"), to: to, strictClientId: strictClientId}
}

type connectParser struct {
	base
	to             State
	strictClientId bool
}

func (p *connectParser) GetToState() State {
	return p.to
}

func (p *connectParser) CreateVH() VariantHeader {
	return newConnectVH()
}

func (p *connectParser) CreatePayload() Payload {
	return newConnectPayload(p.strictClientId)
}

func (p *connectParser) OnAction(ctx *StateContext, pkg *Pkg) (err error) {
	if timeout := time.Now().UTC().Sub(ctx.TimeOfConnected); timeout > ctx.ConnectTimeout {
		err = errors.ErrConnectTimeout
		return
	}

	if vh, ok := pkg.VH.(*ConnectVH); ok {
		if payload, ok := pkg.Payload.(*ConnectPayload); ok {
			p.Debug("       %s: id=%v, vh=%v, payload=%+v", pkg.ReportType, pkg.PacketIdentifier, vh, payload)

			err = ctx.collectConnectParam(pkg)

			// vh.ProtocolLevel will be checked in builder.Build()

			// 3.2.0-1
			var opts []ConnackBuilderOpt
			if ctx.ConnectParam.ClientIdCreated {
				opts = append(opts, WithConnack50AssignedClientId(ctx.ConnectParam.ClientId))
			}
			var builder PacketBuilder = newConnackBuilder(opts...)
			if err = builder.Build(ctx, pkg); err != nil {
				p.Wrong(err, "can't build CONNACK packet")
				return
			}

			// ctx.startMessageDispatchLoop
			// ctx.startKeepAliveStateMonitoring

			if _, err = ctx.Write(builder.Bytes()); err != nil {
				p.Wrong(err, "can't send package")
				return
			} else {
				p.Debug("    ▲ MQTT.%s % x (%v)", builder.ReportType(), builder.Bytes(), ctx.ConnectParam.ClientId)
			}
		}
	}
	return
}

func (p *connectParser) OnError(ctx *StateContext, pkg *Pkg, reason error) (err error) {
	err = reason
	return
}

func newConnectVH() *ConnectVH {
	return &ConnectVH{}
}

func newConnectPayload(strictClientId bool) *ConnectPayload {
	return &ConnectPayload{StrictClientId: strictClientId}
}
