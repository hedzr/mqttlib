/*
 * Copyright © 2020 Hedzr Yeh.
 */

package mqtt

import "gitlab.com/hedzr/mqttlib/pkg/logger"

type base struct{ logger.Base }

func newBase(tag string) base {
	return base{Base: logger.Base{Tag: tag}}
}

// WithCtx could cause race exception. for example: when two or more
// clients incoming with CONNECT packets, now connectParser parse them
// ok and print logging info via base.WithCtx(ctx).Debug(...), here
// is the race point.
// Another scene is the multiple clients send pingreq packets.
func (p *base) WithCtx(ctx *StateContext) *logger.Base {
	return p.With("ClientId", ctx.ConnectParam.ClientId)
}
