/*
 * Copyright © 2020 Hedzr Yeh.
 */

package mqtt

import (
	"bytes"
	"encoding/gob"
	"fmt"
	"github.com/sirupsen/logrus"
	"gitlab.com/hedzr/mqttlib/pkg/mqttlib/mqtt/codec"
	"strconv"
	"sync"
	"sync/atomic"
)

func newSubinfo(clientId string, qos QoSType, pi uint16, pl ProtocolLevel) *Subinfo {
	s := &Subinfo{
		// clientIds: map[string]*qp{clientId: {qos, pi}},
	}
	s.add(clientId, pl, qos, pi)
	return s
}

type Subinfo struct {
	// clientIds      map[string]*qp // k: clientId, v: requested QoS
	clientIds      sync.Map // k: clientId, v: *qp
	clientCount    int64
	retainMessages []Pubinfo // just for retain messages
	allMessages    []Pubinfo // for testing, we cached any publishing requests
	rwm            sync.RWMutex
	// sharedName     string
}

func (s Subinfo) String() string {
	var buf bytes.Buffer
	buf.WriteRune('{')
	buf.WriteString("clientIds:[")
	s.clientIds.Range(func(key, value interface{}) bool {
		buf.WriteString(fmt.Sprintf("%q(%v),", key, value))
		return true
	})
	buf.WriteString("],")

	buf.WriteString("clientCount:")
	buf.WriteString(strconv.FormatInt(s.clientCount, 10))
	buf.WriteRune(',')

	buf.WriteString("retainMessages:[")
	for _, x := range s.retainMessages {
		buf.WriteString(fmt.Sprint(x))
	}
	buf.WriteString("],")

	buf.WriteString("allMessages:[")
	for _, x := range s.allMessages {
		buf.WriteString(fmt.Sprint(x))
	}
	buf.WriteString("],")

	buf.WriteRune('}')
	return buf.String()
}

func (s Subinfo) MarshalBinary() (ret []byte, err error) {
	var b bytes.Buffer
	enc := gob.NewEncoder(&b)

	err = s.saveMap(enc, s.clientIds)
	if err != nil {
		logrus.Debugf("error: %v", err)
		return
	}
	err = enc.Encode(s.retainMessages)
	if err != nil {
		logrus.Debugf("error: %v", err)
		return
	}
	err = enc.Encode(s.allMessages)
	if err != nil {
		logrus.Debugf("error: %v", err)
		return
	}
	ret = b.Bytes()
	return
}

func (s *Subinfo) UnmarshalBinary(data []byte) (err error) {
	b := bytes.NewBuffer(data)
	// _, err = fmt.Fscanln(b, &s.qos, &s.packetIdentifier)
	dec1 := gob.NewDecoder(b)

	var count int64
	count, err = s.loadMap(dec1, s.clientIds, func(dec codec.Decoder) (i interface{}, e error) {
		var key string
		e = dec.Decode(&key)
		i = key
		return
	}, func(dec codec.Decoder) (i interface{}, e error) {
		var value *qp
		e = dec.Decode(&value)
		i = value
		return
	})
	if err != nil {
		logrus.Debugf("error loadMap: %v", err)
		return
	}

	s.clientCount = count

	err = dec1.Decode(&s.retainMessages)
	if err != nil {
		logrus.Debugf("error retainMessages: %v", err)
		return
	}
	err = dec1.Decode(&s.allMessages)
	if err != nil {
		logrus.Debugf("error allMessages: %v", err)
		return
	}
	return
}

func (s *Subinfo) saveMap(enc codec.Encoder, m sync.Map) (err error) {
	count := int64(0)
	m.Range(func(key, value interface{}) bool {
		count++
		return true
	})

	err = enc.Encode(count)
	if err != nil {
		logrus.Debugf("saveMap error: %v", err)
		return
	}

	m.Range(func(key, value interface{}) bool {
		err = enc.Encode(key)
		if err != nil {
			logrus.Debugf("saveMap error: %v", err)
			return false
		}
		if sess, ok := value.(codec.Serializable); ok {
			err = sess.Save(enc)
			if err != nil {
				logrus.Debugf("saveMap error: %v", err)
				return false
			}
		} else {
			err = enc.Encode(value)
			if err != nil {
				logrus.Debugf("saveMap error: %v", err)
				return false
			}
		}
		return true
	})
	return
}

func (s *Subinfo) loadMap(dec codec.Decoder, m sync.Map, decodeKey, decodeValue func(dec codec.Decoder) (interface{}, error)) (count int64, err error) {
	err = dec.Decode(&count)
	if err != nil {
		logrus.Debugf("loadMap error: %v", err)
		return
	}

	for i := int64(0); i < count; i++ {
		var key, value interface{}
		key, err = decodeKey(dec)
		if err != nil {
			logrus.Debugf("loadMap error: %v", err)
			return
		}
		value, err = decodeValue(dec)
		if err != nil {
			logrus.Debugf("loadMap error: %v", err)
			return
		}
		m.Store(key, value)
	}
	return
}

func (s *Subinfo) add(clientId string, protocolLevel ProtocolLevel, qos QoSType, pi uint16) {
	s.clientIds.Store(clientId, newQP(protocolLevel, qos, pi))
	atomic.AddInt64(&s.clientCount, 1)
	// s.rwm.Lock()
	// defer s.rwm.Unlock()
	// s.clientIds[clientId] = &qp{qos, pi}
}

func (s *Subinfo) remove(clientId string) (ok bool) {
	if _, ok = s.clientIds.Load(clientId); ok {
		s.clientIds.Delete(clientId)
		atomic.AddInt64(&s.clientCount, -1)
	}
	// s.rwm.Lock()
	// defer s.rwm.Unlock()
	// delete(s.clientIds, clientId)
	return
}

func (s *Subinfo) get(clientId string) (ret *qp) {
	v, ok := s.clientIds.Load(clientId)
	if ok {
		ret = v.(*qp)
	}
	return
}

func (s *Subinfo) count() (count int64) {
	count = atomic.LoadInt64(&s.clientCount)
	return
}

func (s *Subinfo) exists(clientId string) (exists bool) {
	_, exists = s.clientIds.Load(clientId)
	return
}

func (s *Subinfo) forEach(itf func(key, value interface{}) bool) {
	s.clientIds.Range(itf)
}

func (s *Subinfo) addAsSentClient(clientId string, qp *qp) {
	// only one element in retainMessages, see also pubsubStore.pbulishVxSi,
	// see also: 3.3.1.3 (3.3.1-3), 3.3.1-7， 3.3.1-10, 3.3.1-11
	for i := range s.retainMessages {
		s.retainMessages[i].sentClients[clientId] = qp
		break
	}
}

// NOTE that duplicates the map only, but *qp was reused.
func (s *Subinfo) duplicateClients() map[string]*qp {
	m := make(map[string]*qp)
	s.clientIds.Range(func(key, value interface{}) bool {
		m[key.(string)] = value.(*qp)
		return true
	})

	// s.rwm.RLock()
	// defer s.rwm.RUnlock()
	//
	// m := make(map[string]*qp)
	// for k, v := range s.clientIds {
	// 	m[k] = &qp{v.qos, v.packetIdentifier}
	// }
	return m
}

func (s *Subinfo) clearMessages() {
	s.retainMessages = nil
	s.allMessages = nil
}

func (s *Subinfo) appendMessage(pi Pubinfo, retain bool) {
	s.retainMessages = append(s.retainMessages, pi)
	if retain {
		s.allMessages = append(s.allMessages, pi)
	}
}
