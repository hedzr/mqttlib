/*
 * Copyright © 2020 Hedzr Yeh.
 */

package mqtt

type hookerImpl struct {
	base
}

func newDefaultHooker() Hooker {
	return &hookerImpl{base: newBase("hooker.impl")}
}
