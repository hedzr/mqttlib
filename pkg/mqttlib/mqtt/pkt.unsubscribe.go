/*
 * Copyright © 2020 Hedzr Yeh.
 */

package mqtt

import (
	"bytes"
	"gitlab.com/hedzr/mqttlib/pkg/mqttlib/mqtt/codec"
	"io"
)

// ....................................................

func NewUnsubscribeBuilder(opts ...UnsubscribeBuilderOpt) PacketBuilder {
	builder := &unsubBuilder{
		BasePacketBuilder: newBasePacketBuilder("unsubBuilder"),
		vh50ext:           *newvh50ext(),
		QoSMain:           QoS1,
	}
	for _, opt := range opts {
		opt(builder)
	}
	return builder
}

func WithUnsubPacketIdentifier(id uint16) UnsubscribeBuilderOpt {
	return func(builder *unsubBuilder) {
		builder.PacketIdentifer = id
	}
}

func WithUnsubTopicFilter(topicFilter string) UnsubscribeBuilderOpt {
	return func(builder *unsubBuilder) {
		builder.filters = append(builder.filters, SubBlock{TopicFilter: topicFilter})
	}
}

func WithUnsub50UserProperties(props map[string]string) UnsubscribeBuilderOpt {
	return func(builder *unsubBuilder) {
		for k, v := range props {
			builder.userProperties[k] = v
		}
	}
}

type UnsubscribeBuilderOpt func(*unsubBuilder)

type unsubBuilder struct {
	BasePacketBuilder
	vh50ext

	filters []SubBlock
	// PacketIdentifier uint16

	DUP     bool
	RETAIN  bool
	QoSMain QoSType

	b1   byte
	data []byte
	str  string
	n    int
}

func (b *unsubBuilder) IsResending() bool {
	return b.DUP
}

func (b *unsubBuilder) RequestResend() {
	b.DUP = true
}

func (b *unsubBuilder) ReportType() ReportType {
	return UNSUBSCRIBE
}

func (b *unsubBuilder) Bytes() (bytes []byte) {
	return b.data[:b.n]
}

func (b *unsubBuilder) AsString() (str string) {
	str = b.str
	return
}

func (b *unsubBuilder) WriteTo(w io.Writer) (n int, err error) {
	n, err = w.Write(b.Bytes())
	return
}

func (b *unsubBuilder) Build(ctx *StateContext, pkg *Pkg) (err error) {
	bb := bytes.NewBuffer([]byte{})
	bs := bytes.NewBuffer([]byte{})

	b.b1 = b.firstByte(b.ReportType(), b.DUP, b.RETAIN, b.QoSMain)
	// b.b1 = byte(b.ReportType() << 4)
	// if b.DUP {
	// 	b.b1 |= 0x08
	// }
	// if b.RETAIN {
	// 	b.b1 |= 0x01
	// }
	// b.b1 |= byte(b.QoSMain) << 1

	for i := 0; i < 5; i++ {
		err = bb.WriteByte(b.b1)
		if err != nil {
			return
		}
	}

	// vh

	if b.PacketIdentifer <= 0 {
		b.PacketIdentifer = ctx.Borrow()
	}
	err = codec.WriteBEUint16(bb, b.PacketIdentifer)
	if err != nil {
		return
	}

	if ctx.ConnectParam.ProtocolLevel >= ProtocolLevelForV50 {
		var buf bytes.Buffer
		if buf, err = b.buildv5(ctx, pkg); err != nil {
			return
		}
		bb.Write(buf.Bytes())
	}

	// payload

	// err = mqtt.WriteBEUint16(bb, uint16(len(b.filters)))
	// if err != nil {
	// 	return
	// }

	for _, sb := range b.filters {
		bs.WriteString(sb.TopicFilter)
		bs.WriteString(", ")
		err = codec.WriteString(bb, sb.TopicFilter)
		if err != nil {
			return
		}
		// err = bb.WriteByte(byte(sb.RequestQoS))
		// if err != nil {
		// 	return
		// }
	}

	l := bb.Len() - 5
	b.data = bb.Bytes()
	bl := codec.EncodeIntToBuf(l, b.data, 1)
	copy(b.data[bl+1:], b.data[5:])
	b.n = l + 1 + bl
	b.str = bs.String()
	return
}

func (b *unsubBuilder) buildv5(ctx *StateContext, pkg *Pkg) (buf bytes.Buffer, err error) {
	// for MQTT v5.0

	// construct the properties buffer at first

	var propertiesBuf bytes.Buffer
	propertiesBuf, err = b.WriteWithOrder(0x1f, 0x26)
	if err != nil {
		return
	}

	var length int
	var buf8 []byte = make([]byte, 8)
	length = codec.EncodeIntToBuf(propertiesBuf.Len(), buf8, 0)

	// vh
	buf.Write(buf8[:length])         // properties length
	buf.Write(propertiesBuf.Bytes()) // properties buffer
	return
}
