/*
 * Copyright © 2020 Hedzr Yeh.
 */

package mqtt

import (
	"testing"
	"time"
)

// 0000   20 32 00 00 2f 22 00 0a 12 00 29 61 75 74 6f 2d
// 0010   37 44 35 42 43 33 41 42 2d 41 43 39 30 2d 37 43
// 0020   45 35 2d 36 32 31 36 2d 30 35 32 31 37 43 45 42
// 0030   43 41 39 34
// 203200002f22000a1200296175746f2d37443542433341422d414339302d374345352d363231362d303532313743454243413934

var (
	connectV5S01 = []byte("\x10\x10\x00\x04\x4d\x51\x54\x54\x05\x02\x00\x3c\x03\x21\x00\x14\x00\x00")
	connackV5S01 = []byte("\x20\x32\x00\x00\x2f\x22\x00\x0a\x12\x00\x29\x61\x75\x74\x6f\x2d" +
		"\x37\x44\x35\x42\x43\x33\x41\x42\x2d\x41\x43\x39\x30\x2d\x37\x43" +
		"\x45\x35\x2d\x36\x32\x31\x36\x2d\x30\x35\x32\x31\x37\x43\x45\x42" +
		"\x43\x41\x39\x34")
	connackV5S02 = []byte("\x20\x26\x00\x00\x23\x12\x00\x17\x72\x66\x42\x64\x35\x36\x74\x69" +
		"\x32\x53\x4d\x74\x59\x76\x53\x67\x44\x35\x78\x41\x56\x30\x59\x13" +
		"\x00\x78\x24\x02\x28\x01\x2a\x01")
)

const noOnAction = true

func toPkg(sr StateRegistry, ctx *StateContext, rt ReportType, srcData []byte) (pkg *Pkg) {
	pkg = &Pkg{
		ReceivedTime:     time.Now().UTC(),
		HdrLen:           2,
		Length:           len(srcData) - 2,
		ReportType:       rt,
		DUP:              false,
		RETAIN:           false,
		QoS:              0,
		Flags:            0,
		PacketIdentifier: 0,
		VH:               nil,
		Payload:          nil,
		Data:             srcData[2:],
	}
	return
}

func TestPktConnackV5S01(t *testing.T) {
	srServer := NewDefaultClientStateRegistry(false)
	// srClient := NewDefaultClientStateRegistry(false)

	ctx := newMqttContext(ModeBroker, "1", nil, nil, WithMaxVersion("5.0"))

	pkg := toPkg(srServer, ctx, CONNECT, connectV5S01)

	var to, err = srServer.Advance(nil, ctx, pkg, noOnAction)
	if err != nil {
		t.Fatal(err)
	} else {
		t.Logf("TO: %v, VH:%v, PAYLOAD: %+v", to, pkg.VH, pkg.Payload)
	}

	// since we ignore PacketParser.OnAction, we MUST collect connect params manually.
	err = ctx.collectConnectParam(pkg)

	var opts []ConnackBuilderOpt
	if ctx.ConnectParam.ClientIdCreated {
		opts = append(opts, WithConnack50AssignedClientId(ctx.ConnectParam.ClientId))
	}
	var builder PacketBuilder = newConnackBuilder(opts...)
	if err := builder.Build(ctx, pkg); err != nil {
		t.Fatal(err, "can't build CONNACK packet")
		return
	}
	t.Logf("% x", builder.Bytes())
}

func testPsrConnackV5(t *testing.T, data []byte) {
	sr := NewDefaultClientStateRegistry(false)

	// psr := NewConnackParser(CONNECTED)
	ctx := newMqttContext(ModeBroker, "1", nil, nil, WithMaxVersion("5.0"))
	pkg := toPkg(sr, ctx, CONNACK, data)

	to, err := sr.Parse(ctx, pkg, noOnAction)
	// to, err := sr.Advance(nil, ctx, pkg)

	if err != nil {
		t.Fatal(err)
	} else {
		t.Logf("TO: %v, VH:%v, PAYLOAD: %+v", to, pkg.VH, pkg.Payload)
	}
}

func TestPsrConnackV5S01(t *testing.T) {
	testPsrConnackV5(t, connackV5S01)
}

func TestPsrConnackV5S02(t *testing.T) {
	testPsrConnackV5(t, connackV5S02)
}
