/*
 * Copyright © 2020 Hedzr Yeh.
 */

package mqtt

import (
	"gitlab.com/hedzr/mqttlib/pkg/mqttlib/errors"
	"gitlab.com/hedzr/mqttlib/pkg/mqttlib/mqtt/codec"
)

// ....................................................

// NewPubackParser
// for QoS 1
func NewPubackParser(to State) PacketParser {
	return &pubackParser{base: newBase("pubackParser"), to: to}
}

type pubackParser struct {
	base
	to State
}

func (p *pubackParser) GetToState() State {
	return p.to
}

func (p *pubackParser) CreateVH() VariantHeader {
	return newPubackVH()
}

func (p *pubackParser) CreatePayload() Payload {
	return newPubackPayload()
}

func (p *pubackParser) OnAction(ctx *StateContext, pkg *Pkg) (err error) {
	if vh, ok := pkg.VH.(*PubackVH); ok {
		if payload, ok := pkg.Payload.(*pubackPayload); ok {
			p.Debug("       %s: id=%v, vh=%v, payload=%v", pkg.ReportType, pkg.PacketIdentifier, vh, payload)

			ctx.Return(vh.PacketIdentifier)
		}
	}
	return
}

func (p *pubackParser) OnError(ctx *StateContext, pkg *Pkg, reason error) (err error) {
	err = reason
	return
}

func newPubackVH() *PubackVH {
	return &PubackVH{}
}

type PubackVH struct{ PacketIdentifier uint16 }

func (s *PubackVH) Apply(ctx *StateContext, pkg *Pkg, data []byte, pos int) (newPos int, err error) {
	if pos >= len(data) {
		err = errors.ErrCodePacketCorrupt.New("wrong %v pkt (vh+payload): %v", PUBACK, data)
		return
	}

	s.PacketIdentifier, pos, err = codec.ReadBEUint16(data, pos)
	if err != nil {
		return
	}

	newPos = pos // Apply() must return newPos to pointer the updated new position
	pkg.VH = s   // Apply() must assign itself into pkg.VH
	return
}

func newPubackPayload() *pubackPayload {
	return &pubackPayload{}
}

type pubackPayload struct {
}

func (s *pubackPayload) Apply(ctx *StateContext, pkg *Pkg, data []byte, pos int) (newPos int, err error) {
	newPos = pos    // Apply() must return newPos to pointer the updated new position
	pkg.Payload = s // Apply() must be assign itself into pkg.Payload
	return
}
