/*
 * Copyright © 2020 Hedzr Yeh.
 */

package mqtt

import "sync/atomic"

// Message is the base structure of MQTT communicating packet data block
type Message struct {
	QoS    QoSType
	Retain bool
	Topic  string
	Body   []byte
}

type ControlMessage struct {
	Message
}

type DataMessage struct {
	UniqueId          int64
	PackageIdentifier uint16
	Message
}

type WillMessage struct {
	Message
}

func NewWillMessage(willQoS QoSType, willRetainFlag bool, willTopic string, willMessage []byte) *WillMessage {
	mess := &WillMessage{Message: Message{QoS: willQoS, Retain: willRetainFlag, Topic: willTopic, Body: willMessage}}
	return mess
}

func NewDataMessage(willQoS QoSType, willRetainFlag bool, packageIdentifier uint16, willTopic string, willMessage []byte) *DataMessage {
	mess := &DataMessage{Message: Message{QoS: willQoS, Retain: willRetainFlag, Topic: willTopic, Body: willMessage}}
	messIdSerials++
	mess.UniqueId = nextUniqueId()
	mess.PackageIdentifier = packageIdentifier
	return mess
}

func nextUniqueId() int64 {
	id := atomic.LoadInt64(&messIdSerials)
	newId := id + 1
	if swapped := atomic.CompareAndSwapInt64(&messIdSerials, id, newId); swapped {
		return newId
	}
	return INVALID_UNIQUE_ID
}

const INVALID_UNIQUE_ID int64 = -1

var messIdSerials int64
