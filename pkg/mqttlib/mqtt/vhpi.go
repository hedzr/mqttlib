/*
 * Copyright © 2020 Hedzr Yeh.
 */

package mqtt

import (
	"github.com/sirupsen/logrus"
	"gitlab.com/hedzr/mqttlib/pkg/mqttlib/errors"
	"gitlab.com/hedzr/mqttlib/pkg/mqttlib/mqtt/codec"
)

var noVariantHeaderPI = map[ReportType]bool{
	CONNECT:    true,
	CONNACK:    true,
	PINGREQ:    true,
	PINGRESP:   true,
	DISCONNECT: true,
	PUBLISH:    true, // publishParser will parse PI itself
	PUBACK:     true, // pubackParser will parse PI itself
	PUBREC:     true, // pubrecParser will parse PI itself
	PUBREL:     true, // pubrelParser will parse PI itself
	PUBCOMP:    true, // pubcompParser will parse PI itself
}

// doTestAndReadVHPI parse the packet identifier field at beginning of VH.
// doTestAndReadVHPI don't parse the field in PUBLISH packet since it is
// not at the first field of PUBLISH VH.
func doTestAndReadVHPI(pkg *Pkg, data []byte, pos int) (newPos int, exists bool, err error) {
	newPos = pos

	if _, ok := noVariantHeaderPI[pkg.ReportType]; ok {
		return
	}
	// if pkg.ReportType == PUBLISH && pkg.QoS == 0 {
	// 	return
	// }

	pkg.PacketIdentifier, newPos, err = codec.ReadBEUint16(data, pos)
	if err != nil {
		err = errors.ErrCodePacketCorrupt.New("can't read requested Packet Identifier word(uint16)")
		return
	}

	logrus.Debugf("       [VHPI] identifier = %v", pkg.PacketIdentifier)
	exists = true
	return
}
