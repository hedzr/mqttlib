/*
 * Copyright © 2020 Hedzr Yeh.
 */

package mqtt

import (
	"gitlab.com/hedzr/mqttlib/pkg/mqttlib/errors"
	"gitlab.com/hedzr/mqttlib/pkg/mqttlib/mqtt/codec"
)

// ....................................................

// NewPubrecParser
// for QoS 2
func NewPubrecParser(to State) PacketParser {
	return &pubrecParser{base: newBase("pubrecParser"), to: to}
}

type pubrecParser struct {
	base
	to State
}

func (p *pubrecParser) GetToState() State {
	return p.to
}

func (p *pubrecParser) CreateVH() VariantHeader {
	return newPubrecVH()
}

func (p *pubrecParser) CreatePayload() Payload {
	return newPubrecPayload()
}

func (p *pubrecParser) OnAction(ctx *StateContext, pkg *Pkg) (err error) {
	if vh, ok := pkg.VH.(*pubrecVH); ok {
		if payload, ok := pkg.Payload.(*pubrecPayload); ok {
			p.Debug("       %s: id=%v, vh=%v, payload=%v", pkg.ReportType, pkg.PacketIdentifier, vh, payload)

			if ctx.IsClientMode() {
				switch pkg.QoS {
				case 2, 1, 0:
					err = p.sendPubRel(ctx, pkg)
					if err == nil {
						//
					}
				}
			}
		}
	}
	return
}

func (p *pubrecParser) OnError(ctx *StateContext, pkg *Pkg, reason error) (err error) {
	err = reason
	return
}

// sendPubRel for qos 2 step 2, send back to server
func (p *pubrecParser) sendPubRel(ctx *StateContext, pkg *Pkg) (err error) {
	if !ctx.CanWrite() {
		return // when there are no writer in ctx or receiving publish at client, it's no need to send ack
	}

	if vh, ok := pkg.VH.(*pubrecVH); ok {
		if _, ok := pkg.Payload.(*pubrecPayload); ok {
			p.Debug("       sendPubRel: pktid=%v, DUP=%v, QoS=%v, RETAIN=%v", vh.id, pkg.DUP, pkg.QoS, pkg.RETAIN)

			switch pkg.QoS {
			case 2, 1, 0:
				var builder PacketBuilder
				builder = NewPub22relBuilderFromPkg(vh.id)

				if err = builder.Build(ctx, pkg); err != nil {
					p.Wrong(err, "can't build PUBREL packet")
					return
				}

				if _, err = ctx.Write(builder.Bytes()); err != nil {
					p.Wrong(err, "can't send PUBREL packet")
					return
				} else {
					p.Trace("    ▲ MQTT.%s %v", builder.ReportType(), builder.Bytes())
				}
			}
		}
	}
	return
}

func newPubrecVH() *pubrecVH {
	return &pubrecVH{}
}

type pubrecVH struct{ id uint16 }

func (s *pubrecVH) Apply(ctx *StateContext, pkg *Pkg, data []byte, pos int) (newPos int, err error) {
	if pos >= len(data) {
		err = errors.ErrCodePacketCorrupt.New("wrong %v pkt (vh+payload): %v", PUBACK, data)
		return
	}

	s.id, pos, err = codec.ReadBEUint16(data, pos)
	if err != nil {
		return
	}

	newPos = pos // Apply() must return newPos to pointer the updated new position
	pkg.VH = s   // Apply() must assign itself into pkg.VH
	return
}

func newPubrecPayload() *pubrecPayload {
	return &pubrecPayload{}
}

type pubrecPayload struct {
}

func (s *pubrecPayload) Apply(ctx *StateContext, pkg *Pkg, data []byte, pos int) (newPos int, err error) {
	newPos = pos    // Apply() must return newPos to pointer the updated new position
	pkg.Payload = s // Apply() must be assign itself into pkg.Payload
	return
}
