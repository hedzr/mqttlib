/*
 * Copyright © 2020 Hedzr Yeh.
 */

package mqtt

import (
	"bytes"
	"encoding/gob"
	"github.com/sirupsen/logrus"
	"gitlab.com/hedzr/mqttlib/pkg/mqttlib/errors"
	"gitlab.com/hedzr/mqttlib/pkg/mqttlib/mqtt/pktidgen"
	"time"
)

type Session interface {
	Close() (err error)

	// // AddMessage adds a msg into session storage, which will be published to subscribers.
	// // Based on the qos type, i mean all messages with qos 1 or 2, its
	// // will be persisted into session storage, reloaded every time the
	// // mqtt server restarted, or the client disconnect with the mqtt
	// // server.
	// // If RETAIN is true, this message will be persisted too anyway.
	// AddMessage(QoS QoSType, RETAIN bool, packageIdentifier uint16, topic string, message []byte)
	// RemoveMessage(id int64)
	// CleanMessage(topic string)
	// CleanAllMessages()
	// HasMessage(id int64) bool
	// HasMessageByTopic(topic string) bool
	// GetMessage(id int64) (QoS QoSType, RETAIN bool, packageIdentifier uint16, topic string, message []byte)
	// GetMessageByTopic(topic string) (uid int64, QoS QoSType, RETAIN bool, packageIdentifier uint16, message []byte)

	SetCleanSessionFlag(b bool)
	GetCleanSessionFlag() bool

	// Write write data to the connection associated with this session, in the context
	Write(data []byte) (n int, err error)

	// LinkTo relink the new context 'ctx' to this session
	LinkTo(ctx *StateContext)

	// KeepAlive reset the internal keep-alive timer so that refresh it
	KeepAlive()

	// Subscribe() (err error)
	// Unsubscribe() (err error)

	// Publish(ctx *StateContext, pkg *Pkg) (err error)
}

type session struct {
	willMessages    []WillMessage
	messages        map[int64]*DataMessage // the retain messages, or all of which contains qos 1 or 2
	messagesByTopic map[string]map[int64]*DataMessage

	cleanSession bool
	clientId     string
	ctx          *StateContext
	ConnectParam *ConnectParam
	// subscribers  map[string]*sub // wrong

	exitCh         chan struct{}
	running        bool
	timerKeepAlive *time.Timer
	base
}

func (s session) MarshalBinary() (ret []byte, err error) {
	var b bytes.Buffer
	enc := gob.NewEncoder(&b)
	// _, err = fmt.Fprintln(&b, s.qos, s.packetIdentifier)

	err = enc.Encode(s.willMessages)
	if err == nil {
		err = enc.Encode(s.messages)
		if err == nil {
			err = enc.Encode(s.messagesByTopic)
			if err == nil {
				err = enc.Encode(s.cleanSession)
				if err == nil {
					err = enc.Encode(s.clientId)
					if err == nil {
						err = enc.Encode(s.ConnectParam)
					}
				}
			}
		}
	}

	if err != nil {
		logrus.Debugf("error: %v", err)
	}
	ret = b.Bytes()
	return
}

func (s *session) UnmarshalBinary(data []byte) (err error) {
	b := bytes.NewBuffer(data)
	dec := gob.NewDecoder(b)
	// _, err = fmt.Fscanln(b, &s.qos, &s.packetIdentifier)

	err = dec.Decode(&s.willMessages)
	if err == nil {
		err = dec.Decode(&s.messages)
		if err == nil {
			err = dec.Decode(&s.messagesByTopic)
			if err == nil {
				err = dec.Decode(&s.cleanSession)
				if err == nil {
					err = dec.Decode(&s.clientId)
					if err == nil {
						err = dec.Decode(&s.ConnectParam)
					}
				}
			}
		}
	}
	if err != nil {
		logrus.Debugf("error: %v", err)
	}
	return
}

// func (s *session) Save(enc codec.Encoder) (err error) {
// 	err = enc.Encode(s.cleanSession)
// 	if err != nil {
// 		logrus.Debugf("error: %v", err)
// 		return
// 	}
// 	err = enc.Encode(s.clientId)
// 	if err != nil {
// 		logrus.Debugf("error: %v", err)
// 		return
// 	}
// 	err = enc.Encode(s.willMessages)
// 	if err != nil {
// 		logrus.Debugf("error: %v", err)
// 		return
// 	}
// 	err = enc.Encode(s.messages)
// 	if err != nil {
// 		logrus.Debugf("error: %v", err)
// 		return
// 	}
// 	err = enc.Encode(s.messagesByTopic)
// 	if err != nil {
// 		logrus.Debugf("error: %v", err)
// 		return
// 	}
// 	return
// }
//
// func (s *session) Load(dec codec.Decoder) (err error) {
// 	// need restored:
// 	// ctx, exitCh, timerKeepAlive, tag
// 	err = dec.Decode(&s.cleanSession)
// 	if err != nil {
// 		logrus.Debugf("error: %v", err)
// 		return
// 	}
// 	err = dec.Decode(&s.clientId)
// 	if err != nil {
// 		logrus.Debugf("error: %v", err)
// 		return
// 	}
// 	err = dec.Decode(&s.willMessages)
// 	if err != nil {
// 		logrus.Debugf("error: %v", err)
// 		return
// 	}
// 	err = dec.Decode(&s.messages)
// 	if err != nil {
// 		logrus.Debugf("error: %v", err)
// 		return
// 	}
// 	err = dec.Decode(&s.messagesByTopic)
// 	if err != nil {
// 		logrus.Debugf("error: %v", err)
// 		return
// 	}
// 	return
// }

func newSession(ctx *StateContext, clientId string, cleanSession bool) Session {
	sess := &session{
		ctx:             ctx,
		ConnectParam:    ctx.ConnectParam,
		clientId:        clientId,
		cleanSession:    cleanSession,
		messages:        make(map[int64]*DataMessage),
		messagesByTopic: make(map[string]map[int64]*DataMessage),
		// subscribers:  make(map[string]*sub),

		exitCh: make(chan struct{}),
		base:   newBase("session." + clientId),
	}

	go sess.runLoop()

	return sess
}

func (s *session) LinkTo(ctx *StateContext) {
	if s.ctx == ctx {
		return
	} else if s.ctx != nil {
		_ = s.Close()
	}

	s.ctx = ctx
	s.ConnectParam = ctx.ConnectParam
	if s.running {
		return
	}
	go s.runLoop()
}

func (s *session) KeepAlive() {
	if s.ConnectParam.KeepAlivedSeconds > 0 {
		// 1.5x (see also 3.1.2.10)
		s.timerKeepAlive.Reset(time.Duration(s.ConnectParam.KeepAlivedSeconds*3/2+5) * time.Second)
	} else {
		s.timerKeepAlive.Reset(InfiniteDuration)
	}
}

func (s *session) runLoop() {
	if s.ctx == nil {
		return
	}
	if s.exitCh == nil {
		s.exitCh = make(chan struct{})
	}

	s.running = true
	if s.ConnectParam.KeepAlivedSeconds > 0 {
		// 1.5x (see also 3.1.2.10)
		s.timerKeepAlive = time.NewTimer(time.Duration(s.ConnectParam.KeepAlivedSeconds*3/2+5) * time.Second)
	} else {
		s.timerKeepAlive = time.NewTimer(time.Hour * time.Duration(pktidgen.Int64MAX))
	}
	defer func() {
		s.timerKeepAlive.Stop()
		s.running = false
		s.Debug("session run loop stopped")
	}()

	s.Debug("session run loop started")
	for {
		select {
		case <-s.exitCh:
			return

		case tick := <-s.timerKeepAlive.C:
			go func() {
				s.Trace("%v", tick)
				_ = s.ctx.CloseWithReason(errors.CloseKeepAliveTimeout)
			}()
			return
		}
	}
}

func (s *session) Close() (err error) {
	if s.running && s.exitCh != nil {
		s.Trace("    ? triggering...")
		if s.exitCh != nil {
			close(s.exitCh)
		}
		s.exitCh = nil
		s.Trace("    ! triggered ok.")

		if s.cleanSession {
			// the session must be dropped if context and connection broken.
			err = s.clearAll()
		} else {
			// the session should be persisted, with the messages has QoS type 1 or 2 too.
			// some messages with QoS typo 0 can be persisted at the same time.
			err = s.persistAll()
		}
	}
	return
}

func (s *session) clearAll() (err error) {
	// 当清理会话标志被设置为 1 时，客户端和服务端的状态删除不需要是原子操作。

	// 3.1.2-6

	return
}

func (s *session) persistAll() (err error) {
	// 3.1.2-4
	// 3.1.2-5
	// 3.1.2.7 / 3.1.2-7

	// 客户端的会话状态包括：
	//
	// - 已经发送给服务端，但是还没有完成确认的 QoS 1 和 QoS 2 级别的消息
	// - 已从服务端接收，但是还没有完成确认的 QoS 2 级别的消息。
	//
	// 服务端的会话状态包括：
	//
	// - 会话是否存在，即使会话状态的其它部分都是空。
	// - 客户端的订阅信息。
	// - 已经发送给客户端，但是还没有完成确认的 QoS 1 和 QoS 2 级别的消息。
	// - 即将传输给客户端的 QoS 1 和 QoS 2 级别的消息。
	// - 已从客户端接收，但是还没有完成确认的 QoS 2 级别的消息。
	// - 可选，准备发送给客户端的 QoS 0 级别的消息。
	//
	// 保留消息不是服务端会话状态的一部分，会话终止时不能删除保留消息 [MQTT-3.1.2.7]。
	return
}

func (s *session) GetCleanSessionFlag() bool {
	return s.cleanSession
}

func (s *session) SetCleanSessionFlag(b bool) {
	s.cleanSession = b

	if s.cleanSession {
		// the session must be dropped if context and connection broken.
	} else {
		// the session should be persisted, with the messages has QoS type 1 or 2 too.
		// some messages with QoS typo 0 can be persisted at the same time.
	}
}

// func (s *session) Publish(ctx *StateContext, pkg *Pkg) (err error) {
// 	err = ctx.Publish(pkg)
// 	return
// }

func (s *session) Write(data []byte) (n int, err error) {
	if s.ctx != nil {
		n, err = s.ctx.Write(data)
	}
	return
}

// func (s *session) AddWillMessage(willQoS QoSType, willRetainFlag bool, willTopic string, willMessage []byte) {
// 	mess := WillMessage{Message: Message{QoS: willQoS, Retain: willRetainFlag, Topic: willTopic, Body: willMessage}}
// 	s.willMessages = append(s.willMessages, mess)
// }
//
// func (s *session) CleanWillMessage() {
// 	s.willMessages = nil
// }
//
// func (s *session) HasWillMessage() bool {
// 	return len(s.willMessages) > 0
// }
//
// func (s *session) GetWillMessage() (willQoS QoSType, willRetainFlag bool, willTopic string, willMessage []byte) {
// 	for _, v := range s.willMessages {
// 		willQoS = v.QoS
// 		willRetainFlag = v.Retain
// 		willTopic = v.Topic
// 		willMessage = v.Body
// 		break
// 	}
// 	return
// }

func (s *session) AddMessage(QoS QoSType, RETAIN bool, packageIdentifier uint16, topic string, message []byte) {
	if len(message) == 0 {
		for topic, v := range s.messagesByTopic {
			for id := range v {
				delete(s.messagesByTopic[topic], id)
				if _, ok := s.messages[id]; ok {
					delete(s.messages, id)
				}
			}
		}
		return
	}

	if QoS > QoSDefault || RETAIN {
		mess := NewDataMessage(QoS, RETAIN, packageIdentifier, topic, message)
		if mess.UniqueId != INVALID_UNIQUE_ID {
			s.messages[mess.UniqueId] = mess
			if v, ok := s.messagesByTopic[mess.Topic]; ok {
				if v == nil {
					s.messagesByTopic[mess.Topic] = make(map[int64]*DataMessage)
				}
				s.messagesByTopic[mess.Topic][mess.UniqueId] = mess
			} else {
				s.messagesByTopic[mess.Topic] = make(map[int64]*DataMessage)
				s.messagesByTopic[mess.Topic][mess.UniqueId] = mess
			}
		}
	}
}

func (s *session) RemoveMessage(id int64) {
	if _, ok := s.messages[id]; ok {
		delete(s.messages, id)
	}
	for topic, v := range s.messagesByTopic {
		if _, ok := v[id]; ok {
			delete(s.messagesByTopic[topic], id)
		}
	}
}

func (s *session) CleanMessage(topic string) {
	for id, v := range s.messages {
		if v.Topic == topic {
			delete(s.messages, id)
		}
	}
	if _, ok := s.messagesByTopic[topic]; ok {
		delete(s.messagesByTopic, topic)
	}
}

func (s *session) CleanAllMessages() {
	s.messages = make(map[int64]*DataMessage)
	s.messagesByTopic = make(map[string]map[int64]*DataMessage)
}

func (s *session) HasMessage(id int64) bool {
	_, ok := s.messages[id]
	return ok
}

func (s *session) HasMessageByTopic(topic string) bool {
	_, ok := s.messagesByTopic[topic]
	return ok
}

func (s *session) GetMessage(id int64) (QoS QoSType, RETAIN bool, packageIdentifier uint16, topic string, message []byte) {
	if x, ok := s.messages[id]; ok {
		QoS, RETAIN, packageIdentifier, topic, message = x.QoS, x.Retain, x.PackageIdentifier, x.Topic, x.Body
	}
	return
}

func (s *session) GetMessageByTopic(topic string) (uid int64, QoS QoSType, RETAIN bool, packageIdentifier uint16, message []byte) {
	if m, ok := s.messagesByTopic[topic]; ok {
		for k, x := range m {
			uid, QoS, RETAIN, packageIdentifier, topic, message = k, x.QoS, x.Retain, x.PackageIdentifier, x.Topic, x.Body
			break
		}
	}
	return
}
