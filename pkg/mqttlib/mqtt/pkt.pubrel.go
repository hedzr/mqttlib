/*
 * Copyright © 2020 Hedzr Yeh.
 */

package mqtt

import (
	"bytes"
	"gitlab.com/hedzr/mqttlib/pkg/mqttlib/mqtt/codec"
	"io"
)

func NewPub22relBuilderFromPkg(packetIdentifier uint16) PacketBuilder {
	return &pubrelBuilder{
		vh50ext:    *newvh50ext(),
		identifier: packetIdentifier,
	}
}

type pubrelBuilder struct {
	vh50ext
	identifier uint16
	data       []byte
}

func (b *pubrelBuilder) IsResending() bool {
	return false
}

func (b *pubrelBuilder) RequestResend() {
}

func (b *pubrelBuilder) NextPktId(ctx *StateContext) uint16 {
	return 0
}

func (b *pubrelBuilder) ReportType() ReportType {
	return PUBREL
}

// Build to build the packet
//
func (b *pubrelBuilder) Build(ctx *StateContext, pkg *Pkg) (err error) {
	if ctx.ConnectParam.ProtocolLevel >= ProtocolLevelForV50 {
		err = b.Buildv5(ctx, pkg, 0x62)
		return
	}

	bb := bytes.NewBuffer([]byte{0x62, 0x02})

	err = codec.WriteBEUint16(bb, b.identifier)
	if err != nil {
		return
	}

	b.data = bb.Bytes()
	return
}

func (b *pubrelBuilder) Buildv5(ctx *StateContext, pkg *Pkg, firstByte byte) (err error) {
	var vhBuf bytes.Buffer
	vhBuf, err = b.buildv5(ctx, pkg)
	if err != nil {
		return
	}

	var bb bytes.Buffer
	bb.WriteByte(firstByte)
	_, err = codec.EncodeIntToW(&bb, vhBuf.Len())
	if err != nil {
		return
	}

	_, err = bb.Write(vhBuf.Bytes())
	if err != nil {
		return
	}

	b.data = bb.Bytes()
	return
}

func (b *pubrelBuilder) buildv5(ctx *StateContext, pkg *Pkg) (buf bytes.Buffer, err error) {
	// for MQTT v5.0

	// construct the properties buffer at first

	var propertiesBuf bytes.Buffer
	propertiesBuf, err = b.WriteAs()
	if err != nil {
		return
	}

	var length int
	var buf8 []byte = make([]byte, 8)
	length = codec.EncodeIntToBuf(propertiesBuf.Len(), buf8, 0)

	// vh
	buf.Write(buf8[:length])         // properties length
	buf.Write(propertiesBuf.Bytes()) // properties buffer
	return
}

func (b *pubrelBuilder) bizPublish(ctx *StateContext, pkg *Pkg, bb *bytes.Buffer) (err error) {
	err = ctx.bizPublish(pkg)
	return
}

func (b *pubrelBuilder) WriteTo(w io.Writer) (n int, err error) {
	n, err = w.Write(b.Bytes())
	return
}

func (b *pubrelBuilder) Bytes() (bytes []byte) {
	return b.data
}

func (b *pubrelBuilder) AsString() (str string) {
	return
}
