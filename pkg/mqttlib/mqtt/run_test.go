/*
 * Copyright © 2020 Hedzr Yeh.
 */

package mqtt_test

import (
	"bytes"
	"gitlab.com/hedzr/mqttlib/pkg/mqttlib/mqtt/codec"
	"testing"
)

func TestDecodeInt(t *testing.T) {
	// run_test.go:10: 4206 -> [238 32]
	// run_test.go:11: 4240 -> [144 33]
	t.Logf("%v -> %v", 4206, codec.EncodeInt(4206))
	t.Logf("%v -> %v", 4240, codec.EncodeInt(4240))

	testEncodeIntOne(t, []byte{0}, 0)
	testEncodeIntOne(t, []byte{1}, 1)
	testEncodeIntOne(t, []byte{0x40}, 64)
	testEncodeIntOne(t, []byte{193, 2}, 321)
	testEncodeIntOne(t, []byte{0x80, 0x01}, 128)
	testEncodeIntOne(t, []byte{127}, 127)
	testEncodeIntOne(t, []byte{0xff, 0x7f}, 16383)
	testEncodeIntOne(t, []byte{0x80, 0x80, 0x01}, 16384)
	testEncodeIntOne(t, []byte{0xff, 0xff, 0x7f}, 2097151)
	testEncodeIntOne(t, []byte{0x80, 0x80, 0x80, 0x01}, 2097152)
	testEncodeIntOne(t, []byte{0xff, 0xff, 0xff, 0x7f}, 268435455)
	testEncodeIntOne(t, []byte{0x80, 0x80, 0x80, 0x80, 0x01}, 268435456)

	testDecodeIntOne(t, []byte{0}, 0)
	testDecodeIntOne(t, []byte{1}, 1)
	testDecodeIntOne(t, []byte{0x40}, 64)
	testDecodeIntOne(t, []byte{193, 2}, 321)
	testDecodeIntOne(t, []byte{0x80, 0x01}, 128)
	testDecodeIntOne(t, []byte{127}, 127)
	testDecodeIntOne(t, []byte{0xff, 0x7f}, 16383)
	testDecodeIntOne(t, []byte{0x80, 0x80, 0x01}, 16384)
	testDecodeIntOne(t, []byte{0xff, 0xff, 0x7f}, 2097151)
	testDecodeIntOne(t, []byte{0x80, 0x80, 0x80, 0x01}, 2097152)
	testDecodeIntOne(t, []byte{0xff, 0xff, 0xff, 0x7f}, 268435455)
	testDecodeIntOne(t, []byte{0x80, 0x80, 0x80, 0x80, 0x01}, 268435456)
	// testDecodeIntOne(t, []byte{0xff, 0xff, 0xff, 0xff, 0x7f}, 268435456)

}

func testDecodeIntOne(t *testing.T, buf []byte, expect int) {
	if v, eat := codec.DecodeInt(buf); v != expect {
		t.Fatalf("for DecodeInt(%v), expect %v, but got %v and eat %v", buf, expect, v, eat)
	}
	if v, eat := codec.DecodeInt64(buf); v != int64(expect) {
		t.Fatalf("for DecodeInt(%v), expect %v, but got %v and eat %v", buf, expect, v, eat)
	}
	if v, eat := codec.DecodeUint64(buf); v != uint64(expect) {
		t.Fatalf("for DecodeInt(%v), expect %v, but got %v and eat %v", buf, expect, v, eat)
	}
}

func testEncodeIntOne(t *testing.T, expect []byte, value int) {
	if v := codec.EncodeInt(value); bytes.Compare(v, expect) != 0 {
		t.Fatalf("for EncodeInt(%v), expect %v, but got %v", value, expect, v)
	}
	if v := codec.EncodeInt64(int64(value)); bytes.Compare(v, expect) != 0 {
		t.Fatalf("for EncodeInt(%v), expect %v, but got %v", value, expect, v)
	}
	if v := codec.EncodeUint64(uint64(value)); bytes.Compare(v, expect) != 0 {
		t.Fatalf("for EncodeInt(%v), expect %v, but got %v", value, expect, v)
	}
}
