/*
 * Copyright © 2020 Hedzr Yeh.
 */

package mqtt

import (
	"bytes"
	"gitlab.com/hedzr/mqttlib/pkg/mqttlib/errors"
	"gitlab.com/hedzr/mqttlib/pkg/mqttlib/mqtt/codec"
	"io"
)

// ....................................................

func NewSubackBuilder(vh *subscribeVH, payload *subscribePayload, id uint16) PacketBuilder {
	return &subackBuilder{
		vh50ext: *newvh50ext(),
		vh:      vh, payload: payload, identifier: id,
	}
}

// type SubackRetcode int
//
// const (
// 	SubackOK SubackRetcode = iota
// 	subackQoS1
// 	subackQoS2
// 	subackFailure = iota + 0x80
// )

type subackBuilder struct {
	vh50ext

	vh         *subscribeVH
	payload    *subscribePayload
	identifier uint16
	// retCode    []SubackRetcode

	b1   byte
	data []byte
	n    int
}

func (b *subackBuilder) IsResending() bool {
	return false
}

func (b *subackBuilder) RequestResend() {
}

func (b *subackBuilder) NextPktId(ctx *StateContext) uint16 {
	return 0
}

func (b *subackBuilder) ReportType() ReportType {
	return SUBACK
}

func (b *subackBuilder) Build(ctx *StateContext, pkg *Pkg) (err error) {
	// bb := bytes.NewBuffer([]byte{0x90, 0x00})
	//
	// if err = codec.WriteBEUint16(bb, b.identifier); err != nil {
	// 	err = errors.ErrNumBadIO.New("can't write identifier field")
	// 	return
	// }
	//
	// for _, rc := range b.retCode {
	// 	if err = codec.WriteBEUint16(bb, uint16(rc)); err != nil {
	// 		err = errors.ErrNumBadIO.New("can't write retCode")
	// 		return
	// 	}
	// }
	//
	// b.data = bb.Bytes()

	bb := bytes.NewBuffer([]byte{})

	b.b1 = byte(b.ReportType() << 4)
	// if b.DUP {
	// 	b.b1 |= 0x08
	// }
	// if b.RETAIN {
	// 	b.b1 |= 0x01
	// }
	// b.b1 |= byte(b.QoS) << 1

	for i := 0; i < 5; i++ {
		err = bb.WriteByte(b.b1)
		if err != nil {
			return
		}
	}

	if err = codec.WriteBEUint16(bb, b.identifier); err != nil {
		err = errors.ErrCodeBadIO.New("can't write identifier field")
		return
	}

	for _, tf := range b.payload.TopicFilters {
		// if err = codec.WriteBEUint16(bb, uint16(rc)); err != nil {
		// 	err = errors.ErrCodeBadIO.New("can't write retCode")
		// 	return
		// }
		bb.WriteByte(byte(tf.RetCode))
	}

	if ctx.ConnectParam.ProtocolLevel >= ProtocolLevelForV50 {
		var buf bytes.Buffer
		if buf, err = b.buildv5(ctx, pkg); err != nil {
			return
		}
		bb.Write(buf.Bytes())
	}

	l := bb.Len() - 5
	b.data = bb.Bytes()
	bl := codec.EncodeIntToBuf(l, b.data, 1)
	copy(b.data[bl+1:], b.data[5:])
	b.n = l + 1 + bl

	return
}

func (b *subackBuilder) buildv5(ctx *StateContext, pkg *Pkg) (buf bytes.Buffer, err error) {
	// for MQTT v5.0

	// construct the properties buffer at first

	var propertiesBuf bytes.Buffer
	propertiesBuf, err = b.WriteAs()
	if err != nil {
		return
	}

	var length int
	var buf8 []byte = make([]byte, 8)
	length = codec.EncodeIntToBuf(propertiesBuf.Len(), buf8, 0)

	// vh
	buf.Write(buf8[:length])         // properties length
	buf.Write(propertiesBuf.Bytes()) // properties buffer
	return
}

func (b *subackBuilder) WriteTo(w io.Writer) (n int, err error) {
	n, err = w.Write(b.Bytes())
	return
}

func (b *subackBuilder) Bytes() (bytes []byte) {
	return b.data[:b.n]
}

func (b *subackBuilder) AsString() (str string) {
	return
}
