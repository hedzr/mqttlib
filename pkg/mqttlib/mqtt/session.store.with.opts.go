/*
 * Copyright © 2020 Hedzr Yeh.
 */

package mqtt

import "time"

// func WithDefaultSessionStoreSavingFile(filepath string) DefaultSessionStoreOpt {
// 	return func(store *defaultSessionStore) {
// 		store.filepath = filepath
// 	}
// }

func WithDefaultSessionStoreSavingPeriod(d time.Duration) DefaultSessionStoreOpt {
	return func(store *defaultSessionStore) {
		store.gobSavingDuration = d
	}
}

func WithDefaultSessionStoreSavingEnabled(persist bool) DefaultSessionStoreOpt {
	return func(store *defaultSessionStore) {
		store.gobPersist = persist
	}
}

func WithDefaultSessionStoreNoSysStats(b bool) DefaultSessionStoreOpt {
	return func(store *defaultSessionStore) {
		store.noSysStats = b
	}
}

func WithDefaultSessionStoreNoSysStatsLog(b bool) DefaultSessionStoreOpt {
	return func(store *defaultSessionStore) {
		store.noSysStatsLog = b
	}
}

func WithDefaultSessionStoreResetStorage(b bool) DefaultSessionStoreOpt {
	return func(store *defaultSessionStore) {
		store.resetStorage = b
	}
}

func WithDefaultSessionStorePathRW(rw PathRW) DefaultSessionStoreOpt {
	return func(store *defaultSessionStore) {
		store.pathRW = rw
	}
}
