/*
 * Copyright © 2020 Hedzr Yeh.
 */

package mqtt

import (
	"fmt"
	"github.com/hedzr/cmdr"
	"github.com/hedzr/cmdr/conf"
	"github.com/sirupsen/logrus"
	"path"
	"strconv"
	"strings"
	"sync"
	"sync/atomic"
	"time"
)

type DefaultSessionStoreOpt func(store *defaultSessionStore)

// NewDefaultSessionStore create or return the unique session store instance
func NewDefaultSessionStore(opts ...DefaultSessionStoreOpt) SessionStore {
	onceDefaultSessionStore_.Do(func() {
		defaultSessionStore_ = &defaultSessionStore{
			// filepath:       "",
			// clients:     make(map[string]Session),
			base:              newBase("session.store"),
			upTime:            time.Now().UTC(),
			sysPublishCh:      make(chan *publishBuilder, 256),
			sysPublishPeriod:  60 * time.Second,
			stvBrokerMessages: make(map[string]*int64v),
			//
			pubsubStore:       newPubsubStore(),
			done:              make(chan struct{}),
			gobSavingDuration: time.Second * 60,
			gobPersist:        false,
		}

		for _, part := range []string{"received", "sent", "publish/dropped", "publish/received", "publish/sent", "retained/count"} {
			keyPath := path.Join("broker/messages", part)
			defaultSessionStore_.stvBrokerMessages[keyPath] = &int64v{}
		}

		for _, part := range []string{"bytes/received", "bytes/sent"} {
			keyPath := path.Join("broker/load", part)
			defaultSessionStore_.stvBrokerMessages[keyPath] = &int64v{}
		}

		for _, opt := range opts {
			opt(defaultSessionStore_)
		}

		if defaultSessionStore_.pathRW != nil {
			loadDefaultSessionStore(defaultSessionStore_)
			go defaultSessionStore_.run()
		}
	})

	return defaultSessionStore_
}

//

var defaultSessionStore_ *defaultSessionStore
var onceDefaultSessionStore_ sync.Once

type defaultSessionStore struct {
	base
	// rwm         sync.RWMutex
	// clients     map[string]Session
	clients       sync.Map     // key: clientId string, value: Session
	pubsubStore   *pubsubStore // holds the qos 1 and 2 msgs, will msgs, and retained msgs
	connected     int64
	disconnected  int64
	maxinum       int64
	total         int64
	noSysStats    bool
	noSysStatsLog bool
	resetStorage  bool

	gobSavingDuration time.Duration
	gobPersist        bool

	pathRW PathRW

	done                     chan struct{}
	sysPublishCh             chan *publishBuilder
	internalPublisherContext *StateContext
	sysPublishPeriod         time.Duration
	upTime                   time.Time
	stvBrokerMessages        map[string]*int64v // stats values for broker/messages
}

type int64v struct {
	value int64
}

func (s *defaultSessionStore) EnableSysStats(enabled bool) {
	s.noSysStats = !enabled
}

func (s *defaultSessionStore) SysStatsEnabled() bool {
	return !s.noSysStats
}

func (s *defaultSessionStore) EnableSysStatsLog(enabled bool) {
	s.noSysStatsLog = !enabled
}

func (s *defaultSessionStore) SysStatsLogEnabled() bool {
	return !s.noSysStatsLog
}

func (s *defaultSessionStore) EnableResetStorage(enabled bool) {
	s.resetStorage = enabled
}

func (s *defaultSessionStore) ResetStorageEnabled() bool {
	return s.resetStorage
}

func (s *defaultSessionStore) dumpSessions() (ret string) {
	var cc []string
	s.clients.Range(func(key, value interface{}) bool {
		cc = append(cc, key.(string))
		return true
	})
	return fmt.Sprint(cc)
}

func (s *defaultSessionStore) Stop() {
	if s.pathRW != nil {
		s.pathRW.Stop()
	}
}

func (s *defaultSessionStore) run() {
	debugTicker := time.NewTicker(30 * time.Second)

	savingTicker := time.NewTicker(s.gobSavingDuration)
	sysPubTicker := time.NewTicker(s.sysPublishPeriod)

	upTimeTicker := time.NewTicker(13 * time.Second) // 11,13,17,19s

	defer func() {
		upTimeTicker.Stop()
		savingTicker.Stop()
		sysPubTicker.Stop()
		s.Debug("defaultSessionStore run loop terminated.")
		debugTicker.Stop()
		s.Stop()
	}()

	var lastPubs = make(map[string]*publishBuilder)

	for {
		select {
		case <-upTimeTicker.C:
			s.publishUpTimeSeconds()

		case tick := <-savingTicker.C:
			// logrus.Debugf("tick %v, dur %v, saving default session store...", tick, s.gobSavingDuration)
			saveDefaultSessionStore(s, tick)

		case pb := <-s.sysPublishCh:
			lastPubs[pb.topicName] = pb
		case tick := <-sysPubTicker.C:
			for _, pb := range lastPubs {
				s.doSysPublish(pb, tick)
			}
			lastPubs = make(map[string]*publishBuilder)

		case <-debugTicker.C:
			// just for debugging
			if cmdr.InDebugging() || cmdr.GetDebugMode() {
				if cmdr.GetBoolR("mqtt.server.debug.dump-sessions") {
					s.Debug("active sessions: %v", s.dumpSessions())
				}
				if cmdr.GetBoolR("mqtt.server.debug.dump-subscriptions") {
					s.Debug("  subscriptions: %v", s.pubsubStore.dump())
				}
			}
		case <-s.done:
			return
		}
	}
}

func (s *defaultSessionStore) publishUpTimeSeconds() {
	d := time.Now().Sub(s.upTime)
	s.SysPublishFloat64("broker/uptime", d.Seconds())
}

func (s *defaultSessionStore) BeforeServerListening() {
	s.SysPublishString("broker/version", conf.Version)
	s.publishUpTimeSeconds()
}

func (s *defaultSessionStore) Close() {
	if s.done != nil {
		if s.gobPersist {
			saveDefaultSessionStore(s, time.Now().UTC())
		}
		close(s.done)
		s.done = nil
	}
}

func (s *defaultSessionStore) Has(key string) bool {
	// s.rwm.RLock()
	// _, ok := s.clients[key]
	// s.rwm.RUnlock()
	_, ok := s.clients.Load(key)
	return ok
}

func (s *defaultSessionStore) Get(key string, defaultValue ...Session) Session {
	// s.rwm.RLock()
	// defer s.rwm.RUnlock()
	// if v, ok := s.clients[key]; ok {
	// 	return v
	// }
	if v, ok := s.clients.Load(key); ok {
		if vv, ok := v.(Session); ok {
			return vv
		}
	}
	if len(defaultValue) > 0 {
		return defaultValue[0]
	}
	return nil
}

func (s *defaultSessionStore) Set(key string, value Session) (err error) {
	s.clients.Store(key, value)
	atomic.AddInt64(&s.connected, 1)
	s.total++

	// s.rwm.Lock()
	// s.clients[key] = value
	// s.rwm.Unlock()
	s.Printf("this session (%v) was added", key)
	s.SysPublish("broker/clients/connected", []byte(strconv.FormatInt(s.connected, 10)))
	s.SysPublish("broker/clients/total", []byte(strconv.FormatInt(s.total, 10)))
	if s.connected > s.maxinum {
		s.maxinum = s.connected
		s.SysPublish("broker/clients/maximum", []byte(strconv.FormatInt(s.maxinum, 10)))
	}
	return
}

func (s *defaultSessionStore) Unset(key string) (err error) {
	if v, ok := s.clients.Load(key); ok {
		if vv, ok := v.(Session); ok {
			if vv.GetCleanSessionFlag() == false {
				s.Printf("dont' remove this session (%v) from store, if the clean-session flag wasn't set", key)
				return
			}
		}
	}

	s.clients.Delete(key)
	atomic.AddInt64(&s.connected, -1)
	s.disconnected++

	if delta := s.pubsubStore.RemoveSubscriber(key); delta < 0 {
		s.SysPublishInt64("broker/subscriptions/count", s.pubsubStore.subscribesCount)
	}

	// s.rwm.Lock()
	// defer s.rwm.Unlock()
	// if v, ok := s.clients[key]; ok {
	// 	if v.GetCleanSessionFlag() == false {
	// 		s.Printf("dont' remove this session (%v) from store, if the clean-session flag wasn't set", key)
	// 		return
	// 	}
	// }
	// delete(s.clients, key)
	s.Printf("this session (%v) was removed", key)
	s.SysPublish("broker/clients/connected", []byte(strconv.FormatInt(s.connected, 10)))
	s.SysPublish("broker/clients/disconnected", []byte(strconv.FormatInt(s.disconnected, 10)))
	return
}

func (s *defaultSessionStore) AddWillMessage(clientId string, willQoS QoSType, willRetainFlag bool, willTopic string, willMessage []byte) {
	s.pubsubStore.AddWillMessage(clientId, willQoS, willRetainFlag, willTopic, willMessage)
}

func (s *defaultSessionStore) HasWillMessage(clientId string) bool {
	return s.pubsubStore.HasWillMessage(clientId)
}

// SendWillMessage do publish the will msg to all clients on will topic, and
// erase the will msg if not retained
// sendPaylaodVx should be nil so that a default sender can dispatch the
// payload to all subscribers, see also
// defaultSessionStore.sendPayloadToClient
func (s *defaultSessionStore) SendWillMessage(ctx *StateContext, clientId string, sendPayloadVx SendPayloadVx) (err error) {
	if sendPayloadVx == nil {
		sendPayloadVx = s.sendPayloadToClient
	}
	err = s.pubsubStore.SendWillMessage(ctx, clientId, sendPayloadVx)
	return
}

func (s *defaultSessionStore) Publish(ctx *StateContext, pkg *Pkg) (err error) {
	// s.rwm.Lock()
	// defer s.rwm.Unlock()

	if vh, ok := pkg.VH.(*PublishVH); ok {
		if payload, ok := pkg.Payload.(*PublishPayload); ok {
			s.Debug("       DOING PUBLISH: id=%v, DUP=%v, %v, RETAIN=%v, vh=%+v, payload: %+v bytes", pkg.PacketIdentifier, pkg.DUP, pkg.QoS, pkg.RETAIN, *vh, len(payload.Data))

			// 3.3.1-5
			newPktId := ctx.Borrow()
			const shallWeNeedTransmitRETAIN = false
			builder := NewPublishBuilder(vh.Topic, payload.Data, WithPubQoS(false, pkg.QoS, shallWeNeedTransmitRETAIN), WithPubPacketIdentifier(newPktId))

			// ctx.session.AddMessage(pkg.QoS, pkg.RETAIN, newPktId, vh.Topic, payload.Data)

			if pkg.DUP {
				// might be a resending request
			}

			// ctx.ClientId is requesting to publish a payload to the path 'Topic'

			// 3.3.1-5
			err = s.pubsubStore.DispatchPublishMessage(ctx, pkg.RETAIN, builder, s.sendPayloadToClient)
			// err = s.pubsubStore.Publish(ctx, false, pkg.QoS, pkg.RETAIN,
			// 	vh.Topic, newPktId, payload.Data,
			// 	s.sendPayloadForSub)
		}
	}
	return
}

func (s *defaultSessionStore) StoreQos1or2Message(ctx *StateContext, pkg *Pkg) (err error) {
	if vh, ok := pkg.VH.(*PublishVH); ok {
		if payload, ok := pkg.Payload.(*PublishPayload); ok {
			newPktId := ctx.Borrow()
			const shallWeNeedTransmitRETAIN = false
			builder := NewPublishBuilder(vh.Topic, payload.Data, WithPubQoS(false, 0, shallWeNeedTransmitRETAIN), WithPubPacketIdentifier(newPktId))

			// ctx.session.AddMessage(pkg.QoS, pkg.RETAIN, newPktId, vh.Topic, payload.Data)

			if pkg.DUP {
				// might be a resending request
			}

			s.pubsubStore.StoreAsTask(vh.PacketIdentifier, &PubSubTask{PacketIdentifier: newPktId, SentTime: time.Now().UTC(), SendingTask: SendingTask{Builder: builder}})
		}
	}
	return
}

func (s *defaultSessionStore) PublishByPktId(ctx *StateContext, pktId uint16) (err error) {
	if task := s.pubsubStore.GetTaskById(pktId); task != nil {
		err = s.PublishByBuilder(ctx, task.Builder)
	}
	return
}

// IncMessages updates statistics data under $SYS/broker/messages/...
// The known suffixes must be:
// "received", "sent", "publish/dropped", "publish/received", "publish/sent", "retained/count",
func (s *defaultSessionStore) IncMessages(suffix string, delta int64) {
	keyPath := path.Join("broker/messages", suffix)
	if v, ok := s.stvBrokerMessages[keyPath]; ok {
		atomic.AddInt64(&v.value, delta)
		s.SysPublishInt64(keyPath, v.value)
	} else {
		logrus.Fatalf("unknown suffix for $SYS/broker/messages/...: %v", suffix)
	}
}

// IncLoad updates statistics data under $%SYS/broker/load/...
// The known suffixes could be:
// "bytes/received", "bytes/sent", ...
func (s *defaultSessionStore) IncLoad(suffix string, delta int) {
	keyPath := path.Join("broker/load", suffix)
	if v, ok := s.stvBrokerMessages[keyPath]; ok {
		d := int64(delta)
		atomic.AddInt64(&v.value, d)
		s.SysPublishInt64(keyPath, v.value)
	} else {
		logrus.Warnf("unknown suffix for $SYS/broker/load/...: %v", suffix)
	}
}

func (s *defaultSessionStore) SysPublishUint64(topicParts string, payload uint64, opts ...PublishBuilderOpt) {
	s.SysPublish(topicParts, []byte(fmt.Sprint(payload)))
}

func (s *defaultSessionStore) SysPublishInt64(topicParts string, payload int64, opts ...PublishBuilderOpt) {
	s.SysPublish(topicParts, []byte(fmt.Sprint(payload)))
}

func (s *defaultSessionStore) SysPublishFloat64(topicParts string, payload float64, opts ...PublishBuilderOpt) {
	s.SysPublish(topicParts, []byte(fmt.Sprint(payload)))
}

func (s *defaultSessionStore) SysPublishString(topicParts string, payload string, opts ...PublishBuilderOpt) {
	s.SysPublish(topicParts, []byte(payload))
}

func (s *defaultSessionStore) SysPublish(topicParts string, payload []byte, opts ...PublishBuilderOpt) {
	if s.internalPublisherContext == nil {
		s.internalPublisherContext = NewMqttServerContext("$sys", nil, nil)
	}

	if s.noSysStats {
		return
	}

	builder := NewPublishBuilder(path.Join("$SYS", topicParts), payload, opts...).(*publishBuilder)
	s.sysPublishCh <- builder
	return
}

func (s *defaultSessionStore) doSysPublish(builder *publishBuilder, tick time.Time) {
	if s.noSysStats {
		return
	}

	if err := s.pubsubStore.DispatchNoLimit(s.internalPublisherContext, true, builder, s.sendPayloadToClient); err != nil {
		s.Wrong(err, "publish error")
	}
}

func (s *defaultSessionStore) PublishByBuilder(ctx *StateContext, builder PacketBuilder) (err error) {
	// s.rwm.Lock()
	// defer s.rwm.Unlock()
	// publish a message (or will message) as retain or unretain one. 3.1.2-16, 3.1.2-17
	err = s.pubsubStore.DispatchPublishMessage(ctx, false, builder, s.sendPayloadToClient)
	return
}

func (s *defaultSessionStore) Subscribe(ctx *StateContext, clientId string, topicFilter TopicFilter, packetIdentifier uint16) (invoked QoSType, err error) {
	var delta int
	invoked, delta, err = s.pubsubStore.Subscribe(ctx, clientId, topicFilter, packetIdentifier, s.sendPayloadForSub)
	if err == nil && delta > 0 {
		s.SysPublishInt64("broker/subscriptions/count", s.pubsubStore.subscribesCount)
	}
	return
}

func (s *defaultSessionStore) Unsubscribe(ctx *StateContext, clientId, topicFilter string) (err error) {
	var delta int
	delta, err = s.pubsubStore.Unsubscribe(ctx, clientId, topicFilter)
	if err == nil && delta < 0 {
		s.SysPublishInt64("broker/subscriptions/count", s.pubsubStore.subscribesCount)
	}
	return
}

func (s *defaultSessionStore) sendPayloadToClient(ctx *StateContext, clientId string, builder PacketBuilder, qp *qp) (err error) {
	s.clients.Range(func(key, value interface{}) bool {
		if sess, ok := value.(Session); ok && key.(string) == clientId {
			// todo send with qpPacketQoS (3.3.4-1)
			// todo make this sender do WriteTo in a goroutine or pool

			bdr := builder.(*publishBuilder)
			bdr.protocolLevel = qp.wantedProtocolLevel

			str := builder.AsString()
			if strings.Contains(str, "[$SYS/") {
				s.Trace("▲ %v %v -> ('%s') (qp: pi=%v, %v)", builder.ReportType(), str, clientId, qp.packetIdentifier, qp.qos)
			} else {
				s.Debug("▲ %v %v -> ('%s') (qp: pi=%v, %v)", builder.ReportType(), str, clientId, qp.packetIdentifier, qp.qos)
			}

			// var nw int
			// nw, err = sess.Write(builder.Bytes())
			// nw, err = sess.Write([]byte{})
			_, err = builder.WriteTo(sess)
			// if nw != 0 {
			// 	//
			// }
			// if err == nil {
			// 	// save the alived client for retained message publishing
			// }
		}
		return true
	})
	return
}

// sendPayloadForSub will sent the packet back to the owner of ctx, that is the client requesting subscribing.
func (s *defaultSessionStore) sendPayloadForSub(ctx *StateContext, clientId string, qos QoSType, path string, payload []byte, packetIdentifier uint16) (err error) {
	s.Trace("▲ %v %v ('%s') -> %v (%v)", path, payload, string(payload), clientId, qos)
	if v, ok := s.clients.Load(clientId); ok {
		if sess, ok := v.(Session); ok {
			builder := NewPublishBuilder(path, payload,
				WithPubProtocolLevel(ctx.ConnectParam.ProtocolLevel),
				WithPubQoS(false, qos, false),
				WithPubPacketIdentifier(packetIdentifier))
			if err = builder.Build(ctx, nil); err != nil {
				return
			}
			var nw int
			// nw, err = sess.Write(builder.Bytes())
			nw, err = builder.WriteTo(sess)
			// nw, err = sess.Write([]byte{})
			if nw != 0 {
				//
			}
		}
	}
	return
}
