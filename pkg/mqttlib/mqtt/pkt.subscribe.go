/*
 * Copyright © 2020 Hedzr Yeh.
 */

package mqtt

import (
	"bytes"
	"gitlab.com/hedzr/mqttlib/pkg/mqttlib/mqtt/codec"
	"io"
)

// ....................................................

func NewSubscribeBuilder(verbose bool, opts ...SubscribeBuilderOpt) PacketBuilder {
	builder := &subBuilder{
		BasePacketBuilder: newBasePacketBuilder("subBuilder"),
		vh50ext:           *newvh50ext(),
		verbose:           verbose,
		QoSMain:           QoS1, // lo-byte = 0x20
	}
	for _, opt := range opts {
		opt(builder)
	}
	return builder
}

func WithSubHolder(holder SubHolder) SubscribeBuilderOpt {
	return func(builder *subBuilder) {
		builder.holder = holder
	}
}

func WithSubPacketIdentifier(id uint16) SubscribeBuilderOpt {
	return func(builder *subBuilder) {
		builder.PacketIdentifer = id
	}
}

func WithSubTopicFilter(topicFilter string, qos QoSType) SubscribeBuilderOpt {
	return func(builder *subBuilder) {
		builder.filters = append(builder.filters, SubBlock{TopicFilter: topicFilter, RequestQoS: qos})
	}
}

func WithSubTopicFilters(topicFilters []string, qos QoSType) SubscribeBuilderOpt {
	return func(builder *subBuilder) {
		for _, tf := range topicFilters {
			builder.filters = append(builder.filters, SubBlock{TopicFilter: tf, RequestQoS: qos})
		}
	}
}

func WithSub50UserProperties(props map[string]string) SubscribeBuilderOpt {
	return func(builder *subBuilder) {
		for k, v := range props {
			builder.userProperties[k] = v
		}
	}
}

type SubscribeBuilderOpt func(*subBuilder)

type SubHolder interface {
	Add(packetIdentifier uint16, filters []SubBlock) (err error)
	ApplyRetCodes(ctx *StateContext, packetIdentifier uint16, data []byte, pos int) (newPos int)
}

type subBuilder struct {
	BasePacketBuilder
	vh50ext

	verbose bool
	filters []SubBlock
	// PacketIdentifier uint16

	DUP     bool
	RETAIN  bool
	QoSMain QoSType

	holder SubHolder

	b1   byte
	data []byte
	n    int
	str  string
}

func (b *subBuilder) IsResending() bool {
	return b.DUP
}

func (b *subBuilder) RequestResend() {
	b.DUP = true
}

func (b *subBuilder) ReportType() ReportType {
	return SUBSCRIBE
}

func (b *subBuilder) Bytes() (bytes []byte) {
	return b.data[:b.n]
}

func (b *subBuilder) AsString() (str string) {
	str = b.str
	return
}

func (b *subBuilder) WriteTo(w io.Writer) (n int, err error) {
	n, err = w.Write(b.Bytes())
	return
}

func (b *subBuilder) Build(ctx *StateContext, pkg *Pkg) (err error) {
	bb := bytes.NewBuffer([]byte{})
	bs := bytes.NewBuffer([]byte{})

	b.b1 = b.firstByte(b.ReportType(), b.DUP, b.RETAIN, b.QoSMain)
	// b.b1 = byte(b.ReportType() << 4)
	// if b.DUP {
	// 	b.b1 |= 0x08
	// }
	// if b.RETAIN {
	// 	b.b1 |= 0x01
	// }
	// b.b1 |= byte(b.QoSMain) << 1

	for i := 0; i < 5; i++ {
		err = bb.WriteByte(b.b1)
		if err != nil {
			return
		}
	}

	// vh

	if b.PacketIdentifer <= 0 {
		b.PacketIdentifer = ctx.Borrow()
	}
	err = codec.WriteBEUint16(bb, b.PacketIdentifer)
	if err != nil {
		return
	}

	if ctx.ConnectParam.ProtocolLevel >= ProtocolLevelForV50 {
		var buf bytes.Buffer
		if buf, err = b.buildv5(ctx, pkg); err != nil {
			return
		}
		bb.Write(buf.Bytes())
	}

	// payload

	// err = mqtt.WriteBEUint16(bb, uint16(len(b.filters)))
	// if err != nil {
	// 	return
	// }

	for _, sb := range b.filters {
		bs.WriteString(sb.TopicFilter)
		bs.WriteString(", ")
		err = codec.WriteString(bb, sb.TopicFilter)
		if err != nil {
			return
		}
		err = bb.WriteByte(byte(sb.RequestQoS))
		if err != nil {
			return
		}
	}

	if b.holder == nil {
		b.holder = ctx.clientSubsHolder
	}
	if b.holder != nil {
		if b.PacketIdentifer <= 0 {
			b.PacketIdentifer = ctx.Borrow()
		}
		err = b.holder.Add(b.PacketIdentifer, b.filters)
		if err != nil {
			return
		}
	}

	l := bb.Len() - 5
	b.data = bb.Bytes()
	bl := codec.EncodeIntToBuf(l, b.data, 1)
	copy(b.data[bl+1:], b.data[5:])
	b.n = l + 1 + bl
	b.str = bs.String()
	return
}

func (b *subBuilder) buildv5(ctx *StateContext, pkg *Pkg) (buf bytes.Buffer, err error) {
	// for MQTT v5.0

	// construct the properties buffer at first

	var propertiesBuf bytes.Buffer
	propertiesBuf, err = b.WriteWithOrder(0x0b, 0x26)
	if err != nil {
		return
	}

	var length int
	var buf8 []byte = make([]byte, 8)
	length = codec.EncodeIntToBuf(propertiesBuf.Len(), buf8, 0)

	// vh
	// b.buf.WriteByte(byte(b.reason))    // close reason byte
	buf.Write(buf8[:length])         // properties length
	buf.Write(propertiesBuf.Bytes()) // properties buffer
	return
}
