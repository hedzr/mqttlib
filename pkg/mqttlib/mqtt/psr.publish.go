/*
 * Copyright © 2020 Hedzr Yeh.
 */

package mqtt

// ....................................................

func NewPublishParser(to State) PacketParser {
	return &publishBuilder{BasePacketBuilder: newBasePacketBuilder("publishParser")}
}
