/*
 * Copyright © 2020 Hedzr Yeh.
 */

package mqtt

import (
	"github.com/sirupsen/logrus"
	"gitlab.com/hedzr/mqttlib/pkg/mqttlib/errors"
)

type ClientSubsHolder struct {
	// filters          []SubBlock
	// PacketIdentifier uint16
	Subs map[uint16][]SubBlock
}

func newClientSubsHolder() *ClientSubsHolder {
	return &ClientSubsHolder{
		Subs: make(map[uint16][]SubBlock),
	}
}

func (s *ClientSubsHolder) Add(packetIdentifier uint16, filters []SubBlock) (err error) {
	if v, ok := s.Subs[packetIdentifier]; ok {
		err = errors.ErrCodePacketIdentifierExisted.New("%+v", v)
	} else {
		s.Subs[packetIdentifier] = filters
	}
	return
}

func (s *ClientSubsHolder) ApplyRetCodes(ctx *StateContext, packetIdentifier uint16, data []byte, pos int) (newPos int) {
	if filters, ok := s.Subs[packetIdentifier]; ok {
		var subOk []int
		for i, v := range filters {
			v.RetCode = QoSType(data[pos])
			if v.RetCode >= 0 && v.RetCode <= QoS2 {
				subOk = append(subOk, i)
			}
			pos++
		}

		if len(subOk) > 0 {
			for ix := len(subOk) - 1; ix >= 0; ix-- {
				i := subOk[ix]
				filters = removeSubBlockItemByIndex(filters, i)
			}

			if len(filters) > 0 {
				s.Subs[packetIdentifier] = filters
			} else {
				delete(s.Subs, packetIdentifier)
				_ = ctx.Return(packetIdentifier)
			}
		}

		newPos = pos
	} else {
		logrus.Warnf("missed ctx.clientSubsHolder.Subs[packetIdentifier(%v)], ", packetIdentifier)
	}
	return
}

func removeSubBlockItemByIndex(a []SubBlock, i int) []SubBlock {
	if i >= 0 && i < len(a) {
		r := append(a[0:i], a[i+1:]...)
		return r
	}
	return a
}
