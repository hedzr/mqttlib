/*
 * Copyright © 2020 Hedzr Yeh.
 */

package mqtt

import (
	"bytes"
	"gitlab.com/hedzr/mqttlib/pkg/mqttlib/errors"
	"gitlab.com/hedzr/mqttlib/pkg/mqttlib/mqtt/codec"
	"io"
)

// ....................................................

// if b.sessionExpiryInterval >= 0 {
// if err == nil && len(b.reasonString) > 0 {
// if err == nil && len(b.userProperties) > 0 {
// if err == nil && len(b.serverReferenceId) > 0 {
func newConnackBuilder(opts ...ConnackBuilderOpt) PacketBuilder {
	bdr := &connackBuilder{
		vh50ext: *newvh50ext(),
	}

	for _, opt := range opts {
		opt(bdr)
	}
	return bdr
}

func WithConnack50SessionExpiryInterval(interval int32) ConnackBuilderOpt {
	return func(builder *connackBuilder) {
		builder.sessionExpiryInterval = interval
	}
}

func WithConnack50AssignedClientId(s string) ConnackBuilderOpt {
	return func(builder *connackBuilder) {
		builder.assignedClientId = s
	}
}

// WithConnack50ServerKeepAlive sets a seconds.
// optional, default value will be initialized in newvh50ext()
func WithConnack50ServerKeepAlive(interval uint16) ConnackBuilderOpt {
	return func(builder *connackBuilder) {
		builder.serverKeepAlive = interval
	}
}

func WithConnack50Authentication(method string, data []byte) ConnackBuilderOpt {
	return func(builder *connackBuilder) {
		builder.authenticationMethod = method
		builder.authenticationData = data
	}
}

func WithConnack50ResponseInfo(info string) ConnackBuilderOpt {
	return func(builder *connackBuilder) {
		builder.responseInfo = info
	}
}

func WithConnack50ConnackReason(reason ConnackRetcode, reasonString string) ConnackBuilderOpt {
	return func(builder *connackBuilder) {
		builder.reason = reason
		builder.reasonString = reasonString
	}
}

func WithConnack50ReceiveMaximum(i uint16) ConnackBuilderOpt {
	return func(builder *connackBuilder) {
		builder.receiveMaximum = i
	}
}

func WithConnack50TopicAliasMaximum(i uint16) ConnackBuilderOpt {
	return func(builder *connackBuilder) {
		builder.topicAliasMaximum = i
	}
}

func WithConnack50MaximumQoS(qos QoSType) ConnackBuilderOpt {
	return func(builder *connackBuilder) {
		builder.maximumQoS = byte(qos)
	}
}

func WithConnack50RetainAvailable(i bool) ConnackBuilderOpt {
	return func(builder *connackBuilder) {
		builder.retainAvailable = i
	}
}

func WithConnack50UserProperties(props map[string]string) ConnackBuilderOpt {
	return func(builder *connackBuilder) {
		for k, v := range props {
			builder.userProperties[k] = v
		}
	}
}

func WithConnack50MaximumPacketSize(i int32) ConnackBuilderOpt {
	return func(builder *connackBuilder) {
		builder.maximumPacketSize = i
	}
}

//
// optional, default value will be initialized in newvh50ext()
func WithConnack50WildcardSubsAvailable(i bool) ConnackBuilderOpt {
	return func(builder *connackBuilder) {
		builder.wildcardSubsAvailable = i
	}
}

//
func WithConnack50SubsIdAvailable(i bool) ConnackBuilderOpt {
	return func(builder *connackBuilder) {
		builder.subsIdAvailable = i
	}
}

//
// optional, default value will be initialized in newvh50ext()
func WithConnack50SharedSubsAvailable(i bool) ConnackBuilderOpt {
	return func(builder *connackBuilder) {
		builder.sharedSubsAvailable = i
	}
}

type connackBuilder struct {
	sessionCreated bool
	reason         ConnackRetcode
	vh50ext

	buf bytes.Buffer
}

type ConnackBuilderOpt func(*connackBuilder)

type ConnackRetcode int

const (
	ConnackOK                               ConnackRetcode = iota
	ConnackUnspecError                      ConnackRetcode = 0x80
	ConnackPacketInvalid                    ConnackRetcode = 0x81
	ConnackProtocolError                    ConnackRetcode = 0x82
	ConnackServerUnacceptable               ConnackRetcode = 0x83
	connackRejectUnsupportedProtocolVersion ConnackRetcode = 0x84
	connackRejectMisformClientIdentifier    ConnackRetcode = 0x85
	connackInvalidUsernameOrPassword        ConnackRetcode = 0x86
	connackUnauthorized                     ConnackRetcode = 0x87
	connackRejectUnavailable                ConnackRetcode = 0x88
	ConnackServerBusy                       ConnackRetcode = 0x89
	ConnackForbidden                        ConnackRetcode = 0x8a
	ConnackUnknownAuthScheme                ConnackRetcode = 0x8c
	ConnackInvalidTopicName                 ConnackRetcode = 0x90
	ConnackPacketTooLarge                   ConnackRetcode = 0x95
	ConnackQuoteFull                        ConnackRetcode = 0x97
	ConnackInvalidPayloadFormat             ConnackRetcode = 0x99
	ConnackUnsupportedRetain                ConnackRetcode = 0x9a
	ConnackUnsupportedQoS                   ConnackRetcode = 0x9b
	ConnackServerUseAnother                 ConnackRetcode = 0x9c
	ConnackServerMovedPermanant             ConnackRetcode = 0x9d
	ConnackRateOverflow                     ConnackRetcode = 0x9f
)

func (b *connackBuilder) IsResending() bool {
	return false
}

func (b *connackBuilder) RequestResend() {
}

func (b *connackBuilder) NextPktId(ctx *StateContext) uint16 {
	return 0
}

func (b *connackBuilder) ReportType() ReportType {
	return CONNACK
}

func (b *connackBuilder) WriteTo(w io.Writer) (n int, err error) {
	n, err = w.Write(b.Bytes())
	return
}

func (b *connackBuilder) Bytes() (bytes []byte) {
	// return b.data[:b.n]
	return b.buf.Bytes()
}

func (b *connackBuilder) AsString() (str string) {
	str = b.buf.String()
	return
}

func (b *connackBuilder) Build(ctx *StateContext, pkg *Pkg) (err error) {
	if ctx.ConnectParam.ProtocolLevel >= ProtocolLevelForV50 {
		b.buf, err = b.buildv5(ctx, pkg)
	} else {
		// bb := bytes.NewBuffer([]byte{0x20, 0x02})
		// bb := bytes.NewBuffer([]byte{})

		b1 := byte(b.ReportType() << 4)
		// if b.DUP {
		// 	b.b1 |= 0x08
		// }
		// if b.RETAIN {
		// 	b.b1 |= 0x01
		// }
		// b.b1 |= byte(b.QoS) << 1

		for i := 0; i < 5; i++ {
			err = b.buf.WriteByte(b1)
			if err != nil {
				return
			}
		}

		if len(ctx.ConnectParam.Username) > 0 && len(ctx.ConnectParam.Password) > 0 {
			if ctx.authEnabled {
				if !ctx.authenticator.Authenticate(ctx.ConnectParam.ClientId, ctx.ConnectParam.Username, ctx.ConnectParam.Password) {
					err = errors.ErrUnauthorized
				}
			}
			if err != nil {
				// sent CONNACK with non-zero number
				b.reason = connackInvalidUsernameOrPassword
			}
		}

		if ctx.ConnectParam.RetCode == 0x02 {
			b.reason = connackRejectMisformClientIdentifier
		}

		if err != nil {
			return
		}

		if ctx.ConnectParam.ProtocolLevel > ctx.maxProtocolLevel {
			err = errors.ErrUnsupportedProtocolLevel
			b.reason = connackRejectUnsupportedProtocolVersion
			return
		}

		if ctx.ConnectParam.CleanSessionFlag {
			// clean exist session
			// assumed sessionPresent bit to zero
			b.buf.WriteByte(0) // 3.1.4-4 // 3.2.2-1
		} else {
			if b.sessionCreated {
				b.buf.WriteByte(0x01) // 3.2.2-2
			} else {
				b.buf.WriteByte(0) // 3.1.4-4 // 3.2.2-3
			}
		}

		_, err = ctx.CreateSessionForThisClient(pkg)
		if err != nil {
			return // 3.1.4-5
		}

		b.buf.WriteByte(byte(b.reason)) // 3.2.2-3 // 3.2.2-5 // 3.2.2-6

		// TODO 3.2.2-4

		// b.data = bb.Bytes()

		l := b.buf.Len() - 5
		data := b.buf.Bytes()
		bl := codec.EncodeIntToBuf(l, data, 1)
		copy(data[bl+1:], data[5:])
		n := l + 1 + bl
		b.buf.Truncate(n)
	}
	return
}

func (b *connackBuilder) buildv5(ctx *StateContext, pkg *Pkg) (buf bytes.Buffer, err error) {
	// for MQTT v5.0

	// construct the properties buffer at first

	// b.maximumQoS = byte(ctx.maxQoS)
	b.topicAliasMaximum = topicAliasMaximum
	if ctx.ConnectParam.KeepAlivedSeconds < b.serverKeepAlive {
		b.serverKeepAlive = 0xffff
	}

	var propertiesBuf bytes.Buffer
	propertiesBuf, err = b.WriteWithOrder(0x11, 0x21, 0x24, 0x25, 0x27, 0x12, 0x22, 0x1f, 0x26, 0x28, 0x29, 0x2a, 0x13, 0x1a, 0x1c, 0x15, 0x16)
	if err != nil {
		return
	}

	var length int
	var buf8 []byte = make([]byte, 8)
	length = codec.EncodeIntToBuf(propertiesBuf.Len(), buf8, 0)

	// write the whole packet now

	b1 := byte(b.ReportType() << 4)
	buf.WriteByte(b1)                                                 // 0xe0
	_, err = codec.EncodeIntToW(&buf, propertiesBuf.Len()+length+1+1) // remains length = prop-len + len-of-encoeded-prop-len + 1 byte reason + 1 byte acknowledge flag

	//
	// vh

	if len(ctx.ConnectParam.Username) > 0 && len(ctx.ConnectParam.Password) > 0 {
		if ctx.authEnabled {
			if !ctx.authenticator.Authenticate(ctx.ConnectParam.ClientId, ctx.ConnectParam.Username, ctx.ConnectParam.Password) {
				err = errors.ErrUnauthorized
			}
		}
		if err != nil {
			// sent CONNACK with non-zero number
			b.reason = connackInvalidUsernameOrPassword
		}
	}

	if ctx.ConnectParam.RetCode == 0x02 {
		b.reason = connackRejectMisformClientIdentifier
	}

	if err != nil {
		return
	}

	if ctx.ConnectParam.ProtocolLevel > ctx.maxProtocolLevel {
		err = errors.ErrUnsupportedProtocolLevel
		b.reason = connackRejectUnsupportedProtocolVersion
		return
	}

	if ctx.ConnectParam.CleanSessionFlag {
		// clean exist session
		// assumed sessionPresent bit to zero
		buf.WriteByte(0) // 3.1.4-4 // 3.2.2-1
	} else {
		if b.sessionCreated {
			buf.WriteByte(0x01) // 3.2.2-2
		} else {
			buf.WriteByte(0) // 3.1.4-4 // 3.2.2-3
		}
	}

	_, err = ctx.CreateSessionForThisClient(pkg)
	if err != nil {
		return // 3.1.4-5
	}

	buf.WriteByte(byte(b.reason)) // close reason byte

	buf.Write(buf8[:length])         // properties length
	buf.Write(propertiesBuf.Bytes()) // properties buffer
	return
}

// TODO rewriting Build() & buildv5() for building connack packet
