/*
 * Copyright © 2020 Hedzr Yeh.
 */

package mqtt

import (
	"gitlab.com/hedzr/mqttlib/pkg/mqttlib/auth"
	"io"
	"net"
	"time"
)

// NewMqttClientContext create StateContext for Client
func NewMqttClientContext(clientId string, conn net.Conn, opts ...StateContextOpt) *StateContext {
	return newMqttContext(ModeClient, clientId, conn, conn, opts...)
}

func NewMqttTestContext(clientId string, wr io.Writer, closer io.Closer, opts ...StateContextOpt) *StateContext {
	return newMqttContext(ModeBroker, clientId, wr, closer, opts...)
}

func NewMqttServerContext(clientId string, wr io.Writer, closer io.Closer, opts ...StateContextOpt) *StateContext {
	return newMqttContext(ModeBroker, clientId, wr, closer, opts...)
}

type StateContextOpt func(ctx *StateContext)

func WithSessionStore(ss SessionStore) StateContextOpt {
	return func(ctx *StateContext) {
		ctx.sessionStore = ss
	}
}

func WithStateRegistry(ss StateRegistry) StateContextOpt {
	return func(ctx *StateContext) {
		ctx.stateRegistry = ss
	}
}

func WithAuthenticator(ss auth.Authenticator) StateContextOpt {
	return func(ctx *StateContext) {
		ctx.authenticator = ss
		ctx.authEnabled = true
	}
}

func WithAuthEnabled(b bool) StateContextOpt {
	return func(ctx *StateContext) {
		ctx.authEnabled = b
	}
}

func WithRiskControl(ss RiskControl) StateContextOpt {
	return func(ctx *StateContext) {
		ctx.riskControl = ss
	}
}

func WithDefaultRiskControl(b bool) StateContextOpt {
	return func(ctx *StateContext) {
		ctx.riskControl = NewDefaultRiskControl(b)
	}
}

func WithConnectedTimestamp(t time.Time) StateContextOpt {
	return func(ctx *StateContext) {
		ctx.TimeOfConnected = t
	}
}

func WithStrictClientId(b bool) StateContextOpt {
	return func(ctx *StateContext) {
		ctx.strictClientId = b
	}
}

func WithStrictProtocolName(b bool) StateContextOpt {
	return func(ctx *StateContext) {
		ctx.strictProtocolName = b
	}
}

func WithNoWildMatchFilter(b bool) StateContextOpt {
	return func(ctx *StateContext) {
		ctx.noWildMatchFilter = b
	}
}

func WithMaxQoS(maxQoS QoSType) StateContextOpt {
	return func(ctx *StateContext) {
		ctx.maxQoS = maxQoS
	}
}

func WithMaxProtocolLevel(level ProtocolLevel) StateContextOpt {
	return func(ctx *StateContext) {
		ctx.maxProtocolLevel = level
		switch level {
		case ProtocolLevelForV311:
			ctx.maxVersion = "3.1.1"
		case ProtocolLevelForV50:
			ctx.maxVersion = "5.0"
		}
	}
}

func WithMaxVersion(v string) StateContextOpt {
	return func(ctx *StateContext) {
		ctx.maxVersion = v
		switch v {
		case "3.0", "3.1", "mqttv3", "mqttv3.1":
			ctx.maxProtocolLevel = ProtocolLevelForV311
			ctx.maxQoS = 1
		case "3.1.1":
			ctx.maxProtocolLevel = ProtocolLevelForV311
			ctx.maxQoS = 1
		case "5.0", "mqttv5", "mqttv5.0":
			ctx.maxProtocolLevel = ProtocolLevelForV50
			ctx.maxQoS = 1
		}
	}
}

func WithConnectTimeout(timeout time.Duration) StateContextOpt {
	return func(ctx *StateContext) {
		ctx.ConnectTimeout = timeout
	}
}

func WithTimeOfConnected(tm time.Time) StateContextOpt {
	return func(ctx *StateContext) {
		ctx.TimeOfConnected = tm
	}
}

// func WithDefaultSessionStoreOpts(opts ...DefaultSessionStoreOpt) StateContextOpt {
// 	return func(ctx *StateContext) {
// 		ctx.defaultSessionStoreOpts = append(ctx.defaultSessionStoreOpts, opts...)
// 	}
// }
