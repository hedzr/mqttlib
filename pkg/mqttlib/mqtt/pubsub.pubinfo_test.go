/*
 * Copyright © 2020 Hedzr Yeh.
 */

package mqtt

import (
	"bytes"
	"encoding/gob"
	"github.com/hedzr/logex"
	"testing"
	"time"
)

func TestPubinfo_MarshalBinary(t *testing.T) {
	defer logex.CaptureLog(t).Release()

	si := &Pubinfo{
		data:             []byte{1, 2, 3},
		publishingQoS:    2,
		sentClients:      map[string]*qp{"a": {5, 6, ProtocolLevelForV50}},
		published:        1,
		publisher:        "x",
		packetIdentifier: 7,
		timestamp:        time.Now().UTC(),
	}
	si2 := &Pubinfo{}
	testPubInfoEncAndDec(t, si, si2)

	si = &Pubinfo{}
	si2 = &Pubinfo{}
	testPubInfoEncAndDec(t, si, si2)
}

func testPubInfoEncAndDec(t *testing.T, vIn, vOut *Pubinfo) {
	var err error
	var b bytes.Buffer
	enc := gob.NewEncoder(&b)
	dec := gob.NewDecoder(&b)

	err = enc.Encode(vIn)
	if err != nil {
		t.Error(err)
	}
	t.Logf("buffer: % x", b.Bytes())
	err = dec.Decode(&vOut)
	if err != nil {
		t.Error(err)
	}
	t.Logf("vOut: %v", vOut)
}

func TestSubinfo_MarshalBinary(t *testing.T) {
	defer logex.CaptureLog(t).Release()

	si := &Subinfo{}
	si2 := &Subinfo{}
	testSubInfoEncAndDec(t, si, si2)

	si = &Subinfo{}
	si2 = &Subinfo{}
	si.add("1", ProtocolLevelForV50, 2, 3)
	si.appendMessage(Pubinfo{
		data:             []byte{1, 2, 3},
		publishingQoS:    2,
		sentClients:      map[string]*qp{"a": {5, 6, ProtocolLevelForV50}},
		published:        1,
		publisher:        "xzx",
		packetIdentifier: 7,
		timestamp:        time.Now().UTC(),
		// data:             nil,
		// publishingQoS:    0,
		// sentClients:      nil,
		// published:        0,
		// publisher:        "",
		// packetIdentifier: 0,
		// timestamp:        time.Now().UTC(),
	}, true)
	testSubInfoEncAndDec(t, si, si2)
}

func testSubInfoEncAndDec(t *testing.T, vIn, vOut *Subinfo) {
	var err error
	var b bytes.Buffer
	enc := gob.NewEncoder(&b)
	dec := gob.NewDecoder(&b)

	err = enc.Encode(vIn)
	if err != nil {
		t.Error(err)
	}
	t.Logf("buffer: % x", b.Bytes())
	err = dec.Decode(&vOut)
	if err != nil {
		t.Error(err)
	}
	t.Logf("vOut: %v", vOut)
}
