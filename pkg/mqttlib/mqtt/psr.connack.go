/*
 * Copyright © 2020 Hedzr Yeh.
 */

package mqtt

// ....................................................

func NewConnackParser(to State) PacketParser {
	return &connackParser{base: newBase("connackParser"), to: to}
}

// ....................................................

// ....................................................

type connackParser struct {
	base
	to State
}

func (p *connackParser) GetToState() State {
	return p.to
}

func (p *connackParser) CreateVH() VariantHeader {
	return newConnackVH()
}

func (p *connackParser) CreatePayload() Payload {
	return nil
}

func (p *connackParser) OnAction(ctx *StateContext, pkg *Pkg) (err error) {
	return
}

func (p *connackParser) OnError(ctx *StateContext, pkg *Pkg, reason error) (err error) {
	err = reason
	return
}

func newConnackVH() *ConnackVH {
	return &ConnackVH{
		vh50ext: *newvh50ext(),
	}
}

type ConnackVH struct {
	vh50ext
	SessionPresent bool
	ReasonCode     ConnackRetcode
	// ConnectAcknowledgeFlag bool
}

func (s *ConnackVH) Apply(ctx *StateContext, pkg *Pkg, data []byte, pos int) (newPos int, err error) {
	if data[pos] != 0 {
		s.SessionPresent = true
	}

	s.ReasonCode = ConnackRetcode(data[pos+1])

	if ctx.ConnectParam.ProtocolLevel >= ProtocolLevelForV50 {
		newPos, err = s.vh50ext.Apply(ctx, pkg, data, pos+2)
	} else {
		newPos = pos + 2 // Apply() must return newPos to pointer the updated new position
	}
	pkg.VH = s // Apply() must assign itself into pkg.VH
	return
}
