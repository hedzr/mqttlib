/*
 * Copyright © 2020 Hedzr Yeh.
 */

package mqtt

import (
	"bytes"
	"gitlab.com/hedzr/mqttlib/pkg/mqttlib/errors"
	"gitlab.com/hedzr/mqttlib/pkg/mqttlib/mqtt/codec"
	"io"
)

func NewDisconnectBuilder(opts ...Disconnect50BuilderOpt) PacketBuilder {
	builder := &disconnectBuilder{
		vh50ext: *newvh50ext(),
		// sessionExpiryInterval: -1,
		// userProperties:        make(map[string]string),
	}
	for _, opt := range opts {
		opt(builder)
	}
	return builder
}

func WithDisconnect50Reason(reason errors.CloseReason, reasonString string) Disconnect50BuilderOpt {
	return func(builder *disconnectBuilder) {
		builder.reason = reason
		builder.reasonString = reasonString
	}
}

func WithDisconnect50ReasonServerMoved(serverReference string) Disconnect50BuilderOpt {
	return func(builder *disconnectBuilder) {
		builder.reason = errors.CloseServerMovedPermanent
		builder.reasonString = "server moved permanent"
		if len(serverReference) > 0 {
			builder.serverReferenceId = serverReference
		}
	}
}

func WithDisconnect50ReasonUseOtherServer(serverReference string) Disconnect50BuilderOpt {
	return func(builder *disconnectBuilder) {
		builder.reason = errors.CloseServerMovedPermanent
		builder.reasonString = "use other server (temporarily), pls"
		if len(serverReference) > 0 {
			builder.serverReferenceId = serverReference
		}
	}
}

func WithDisconnect50SessionExpiryInterval(seconds int) Disconnect50BuilderOpt {
	return func(builder *disconnectBuilder) {
		builder.sessionExpiryInterval = int32(seconds)
	}
}

// WithDisconnect50UserProperties sets the user's properties
// todo, in the use properties, duplicated keys are allowed, see also 3.14.2.2.5
func WithDisconnect50UserProperties(props map[string]string) Disconnect50BuilderOpt {
	return func(builder *disconnectBuilder) {
		for k, v := range props {
			builder.userProperties[k] = v
		}
	}
}

type Disconnect50BuilderOpt func(builder *disconnectBuilder)

type disconnectBuilder struct {
	buf    bytes.Buffer
	reason errors.CloseReason
	vh50ext
	// propertiesLength      int
	// sessionExpiryInterval int32 // in seconds
	// reasonString          string
	// userProperties        map[string]string
	// serverReferenceId     string
}

func (b *disconnectBuilder) IsResending() bool {
	return false
}

func (b *disconnectBuilder) RequestResend() {
}

func (b *disconnectBuilder) NextPktId(ctx *StateContext) uint16 {
	return 0
}

func (b *disconnectBuilder) ReportType() ReportType {
	return DISCONNECT
}

func (b *disconnectBuilder) WriteTo(w io.Writer) (n int, err error) {
	n, err = w.Write(b.Bytes())
	return
}

var disconnectPacketHdr = []byte{0xe0, 0x00}

func (b *disconnectBuilder) Bytes() (bytes []byte) {
	// return disconnectPacketHdr
	return b.buf.Bytes()
}

func (b *disconnectBuilder) AsString() (str string) {
	str = b.buf.String()
	return
}

func (b *disconnectBuilder) Build(ctx *StateContext, pkg *Pkg) (err error) {
	if ctx.ConnectParam.ProtocolLevel >= ProtocolLevelForV50 {
		// for MQTT v5.0
		err = b.buildv5(ctx, pkg)
	} else {
		b.buf.Write(disconnectPacketHdr)
	}
	return
}

func (b *disconnectBuilder) buildv5(ctx *StateContext, pkg *Pkg) (err error) {
	// for MQTT v5.0

	// construct the properties buffer at first

	var propertiesBuf bytes.Buffer
	propertiesBuf, err = b.WriteWithOrder(0x11, 0x1c, 0x1f, 0x26)
	if err != nil {
		return
	}

	var length int
	var buf8 []byte = make([]byte, 8)
	length = codec.EncodeIntToBuf(propertiesBuf.Len(), buf8, 0)

	// write the whole packet now

	b.buf.WriteByte(disconnectPacketHdr[0])                           // 0xe0
	_, err = codec.EncodeIntToW(&b.buf, propertiesBuf.Len()+length+1) // remains length = prop-len + len-of-encoeded-prop-len + 1 byte reason

	// vh
	b.buf.WriteByte(byte(b.reason))    // close reason byte
	b.buf.Write(buf8[:length])         // properties length
	b.buf.Write(propertiesBuf.Bytes()) // properties buffer
	return
}
