/*
 * Copyright © 2020 Hedzr Yeh.
 */

package mqtt

import (
	"gitlab.com/hedzr/mqttlib/pkg/mqttlib/errors"
	"gitlab.com/hedzr/mqttlib/pkg/mqttlib/mqtt/codec"
)

// ....................................................

func NewUnsubscribeParser(to State) PacketParser {
	return &unsubscribeParser{base: newBase("unsubscribeParser"), to: to}
}

type unsubscribeParser struct {
	base
	to State
}

func (p *unsubscribeParser) GetToState() State {
	return p.to
}

func (p *unsubscribeParser) CreateVH() VariantHeader {
	return newUnsubscribeVH()
}

func (p *unsubscribeParser) CreatePayload() Payload {
	return newUnsubscribePayload()
}

func (p *unsubscribeParser) OnAction(ctx *StateContext, pkg *Pkg) (err error) {
	if vh, ok := pkg.VH.(*unsubscribeVH); ok {
		if payload, ok := pkg.Payload.(*unsubscribePayload); ok {
			p.Debug("       %s: id=%v, vh=%v, payload=%v", pkg.ReportType, pkg.PacketIdentifier, vh, payload.TopicFilters)

			var maxQoS = UnsubackOK
			var retCodes []UnsubackRetcode
			for range payload.TopicFilters { // 3.10.3-1
				retCodes = append(retCodes, maxQoS)
			}

			// unsubscribe 3.10.4-4, 3.10.4-5, 3.10.4-6

			var builder PacketBuilder = NewUnsubackBuilder(
				WithUnsubackIdentifier(pkg.PacketIdentifier),
				WithUnsubackRetCodes(retCodes))
			if err = builder.Build(ctx, pkg); err != nil {
				p.Wrong(err, "can't build UNSUBACK packet")
				return
			}

			if _, err = ctx.Write(builder.Bytes()); err != nil {
				p.Wrong(err, "can't send package")
				return
			} else {
				p.Debug("    ▲ MQTT.%s %v", builder.ReportType(), builder.Bytes())
			}
		}
	}
	return
}

func (p *unsubscribeParser) OnError(ctx *StateContext, pkg *Pkg, reason error) (err error) {
	err = reason
	return
}

func newUnsubscribeVH() *unsubscribeVH {
	return &unsubscribeVH{
		vh50ext: *newvh50ext(),
	}
}

type unsubscribeVH struct {
	vh50ext
}

func (s *unsubscribeVH) Apply(ctx *StateContext, pkg *Pkg, data []byte, pos int) (newPos int, err error) {
	if pkg.QoS != 1 {
		// 3.10.1-1
		err = errors.ErrCodePacketIllegal.New("in sub/unsub packet header, lo-byte must be 0,0,1,0")
		return
	}

	if ctx.ConnectParam.ProtocolLevel >= ProtocolLevelForV50 {
		newPos, err = s.vh50ext.Apply(ctx, pkg, data, pos)
	} else {
		newPos = pos // Apply() must return newPos to pointer the updated new position
	}

	pkg.VH = s // Apply() must assign itself into pkg.VH
	return
}

type unsubscribePayload struct {
	TopicFilters []TopicFilter
	// WillTopic   string
	// WillMessage string
	// Username    string
	// Password    string
}

func newUnsubscribePayload() *unsubscribePayload {
	return &unsubscribePayload{}
}

func (s *unsubscribePayload) Apply(ctx *StateContext, pkg *Pkg, data []byte, pos int) (newPos int, err error) {
	// var count uint16
	// count, pos, err = ReadBEUint16(data, pos)
	// if err != nil {
	// 	err = newErrorN(ErrCodePacketCorrupt, "can't read topic filter counter field")
	// 	return
	// }
	length := len(data)
	for pos < length {
		var tf TopicFilter
		tf.Filter, pos, err = codec.ReadString(data, pos)
		if err != nil {
			err = errors.ErrCodePacketCorrupt.New("can't read topic filter string field")
			return
		}

		if pos < length {
			var b = data[pos]
			if (b & 0xfc) != 0 {
				err = errors.ErrCodePacketCorrupt.New("requested QoS byte '%v' not ok: hi-order bit 7-2 must be 0", b)
				return
			}
			tf.RequestedQoS = QoSType(b)
			if tf.RequestedQoS == 3 {
				err = errors.ErrCodePacketCorrupt.New("requested QoS byte '%v' not ok: QoS must be 0,1, or 2", tf.RequestedQoS)
				return
			}
			pos++
		} else {
			err = errors.ErrCodePacketCorrupt.New("can't read requested QoS byte")
			return
		}

		s.TopicFilters = append(s.TopicFilters, tf)
	}

	if len(s.TopicFilters) == 0 {
		err = errors.ErrCodePacketIllegal.New("No topic filter, see 3.10.3-2") // 3.10.3-2
	}

	newPos = pos    // Apply() must return newPos to pointer the updated new position
	pkg.Payload = s // Apply() must be assign itself into pkg.Payload
	return
}
