/*
 * Copyright © 2020 Hedzr Yeh.
 */

package mqtt

import (
	"encoding/gob"
	"github.com/hedzr/cmdr"
	"github.com/sirupsen/logrus"
	"os"
	"path"
	"time"
)

// NewPathRWGob creates a new gob PathReadWriter [PathRW] instance
func NewPathRWGob(opts ...PathRWOpt) PathRW {
	rw := &gobPathRW{}
	for _, opt := range opts {
		opt(rw)
	}
	return rw
}

func WithPathRWGobDataDir(dataDir string) PathRWOpt {
	return func(rw PathRW) {
		if g, ok := rw.(*gobPathRW); ok {
			g.dataDir = dataDir
			g.filepath = path.Join(dataDir, "session.store.ser")
		}
	}
}

type gobPathRW struct {
	dataDir  string
	filepath string
}

func (rw *gobPathRW) Start(opts ...PathRWOpt) {
	if len(rw.dataDir) > 0 {
		return // don't re-start but the caller need to invoke Start one more times with its chances
	}
	for _, opt := range opts {
		opt(rw)
	}
}

func (rw *gobPathRW) Stop() {
}

func (rw *gobPathRW) Load(dst *defaultSessionStore) (err error) {
	logrus.Debugf("loading session store from gob file: %v", rw.filepath)
	if cmdr.FileExists(rw.filepath) {
		var f *os.File
		// var err error
		if f, err = os.Open(rw.filepath); err != nil {
			logrus.Errorf("can't open %v: %v", rw.filepath, err)
			return
		}
		defer f.Close()

		dec := gob.NewDecoder(f)
		if err = dec.Decode(&dst); err != nil {
			logrus.Errorf("load default session store from %v failed: %v", rw.filepath, err)
		} else {
			logrus.Debug("    loaded")
		}
	}
	return
}

func (rw *gobPathRW) Save(src *defaultSessionStore, tick time.Time) (err error) {
	// var err error
	var f *os.File

	// filepath := fmt.Sprintf("%s/session.store.ser", cmdr.GetStringR("mqtt.server.storage.gob.devel.data-dir"))
	// logrus.Debugf("reading %v...", filepath)
	logrus.Debugf("saving session store to gob file: %v", rw.filepath)

	err = cmdr.EnsureDir(path.Dir(rw.filepath))
	if f, err = os.Create(rw.filepath); err != nil {
		logrus.Errorf("can't open %v: %v", rw.filepath, err)
		return
	}
	defer func() {
		e := f.Close()
		if err != nil || e != nil {
			err = os.Remove(rw.filepath)
		}
	}()

	enc := gob.NewEncoder(f)
	if err = enc.Encode(src); err != nil {
		logrus.Errorf("save default session store into %v failed: %v", rw.filepath, err)
	} else {
		logrus.Debugf("    session store saved at %v", tick)
	}

	return
}

func (rw *gobPathRW) Get(pathKey string, defaultValue ...interface{}) (ret interface{}, err error) {
	return
}

func (rw *gobPathRW) Set(pathKey string, value interface{}) (err error) {
	return
}
