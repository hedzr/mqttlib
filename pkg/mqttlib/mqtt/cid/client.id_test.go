/*
 * Copyright © 2020 Hedzr Yeh.
 */

package cid

import "testing"

func TestCreateNewClientId(t *testing.T) {
	cid := CreateNewClientId()
	err := VerifyClientId(cid)
	if err != nil {
		t.Fatal(err)
	}
}
