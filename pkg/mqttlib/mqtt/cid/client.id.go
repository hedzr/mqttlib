/*
 * Copyright © 2020 Hedzr Yeh.
 */

package cid

import (
	"gitlab.com/hedzr/mqttlib/pkg/mqttlib/errors"
	"math/rand"
	"strings"
)

func VerifyClientId(s string) (err error) {
	if len(s) > 23 || len(s) < 1 {
		err = errors.ErrCodeMqttBadClientId.New("illegal length (valid length in 1..23)")
		return
	}
	for _, ch := range s {
		if !strings.ContainsRune(validClientIdCharset, ch) {
			err = errors.ErrCodeMqttBadClientId.New("illegal character '%v'", ch)
			return
		}
	}
	return
}

// createNewClientId create a random client id
// NOTE todo to prevent the conflcts on creating new client id with random string algorithm
func CreateNewClientId() (clientId string) {
	var length = 23 // 16 // 23
	var buf = make([]rune, length)
	vcc := []rune(validClientIdCharset)
	vcclen := len(vcc)
	for i := 0; i < length; i++ {
		pos := rand.Intn(vcclen)
		buf[i] = vcc[pos]
	}
	clientId = string(buf)
	return
}

const validClientIdCharset = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"
