/*
 * Copyright © 2020 Hedzr Yeh.
 */

package mqtt

func NewConnectBuilder(opts ...ConnectBuilderOpt) PacketBuilder {
	builder := &connectBuilder{
		DUP: false, QoS: QoSDefault, RETAIN: false,
		ProtocolLevel: ProtocolLevelForV311, ConnectFlag: 0, KeepAlivedSeconds: 60,
	}
	for _, opt := range opts {
		opt(builder)
	}
	return builder
}

func WithConnectQoS(DUP bool, qos QoSType, RETAIN bool) ConnectBuilderOpt {
	return func(builder *connectBuilder) {
		builder.DUP = DUP
		builder.QoS = qos
		builder.RETAIN = RETAIN
	}
}

func WithConnectClientId(id string) ConnectBuilderOpt {
	return func(builder *connectBuilder) {
		builder.ClientId = id
	}
}

func WithConnectUsername(username string) ConnectBuilderOpt {
	return func(builder *connectBuilder) {
		builder.ConnectFlag |= 0x80
		builder.Username = username
	}
}

func WithConnectPassword(password string) ConnectBuilderOpt {
	return func(builder *connectBuilder) {
		builder.ConnectFlag |= 0x40
		builder.Password = password
	}
}

func WithConnectWillMsg(willRetain bool, willQoS QoSType, willTopic string, willMessage []byte) ConnectBuilderOpt {
	return func(builder *connectBuilder) {
		if willRetain {
			builder.ConnectFlag |= 0x20
		} else {
			builder.ConnectFlag &= ^byte(0x20)
		}
		builder.ConnectFlag |= 0x04
		builder.ConnectFlag |= byte(willQoS) << 3
		builder.willTopic = willTopic
		builder.willMessage = willMessage
	}
}

func WithConnectCleanSession(cleanSession bool) ConnectBuilderOpt {
	return func(builder *connectBuilder) {
		if cleanSession {
			builder.ConnectFlag |= 0x02
		} else {
			builder.ConnectFlag &= ^byte(0x02)
		}
		builder.cleanSessionSet = true
	}
}

func WithConnectKeepAliveSeconds(seconds uint16) ConnectBuilderOpt {
	return func(builder *connectBuilder) {
		builder.KeepAlivedSeconds = seconds
		builder.keepAliveSet = true
	}
}

func WithConnectKeepAliveSecondsIfNotSet(seconds uint16) ConnectBuilderOpt {
	return func(builder *connectBuilder) {
		if !builder.keepAliveSet {
			builder.KeepAlivedSeconds = seconds
			builder.keepAliveSet = true
		}
	}
}

func WithConnectCleanSessionIfNotSet(cleanSession bool) ConnectBuilderOpt {
	return func(builder *connectBuilder) {
		if !builder.cleanSessionSet {
			if cleanSession {
				builder.ConnectFlag |= 0x02
			} else {
				builder.ConnectFlag &= ^byte(0x02)
			}
			builder.cleanSessionSet = true
		}
	}
}
