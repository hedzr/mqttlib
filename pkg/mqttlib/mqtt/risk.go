/*
 * Copyright © 2020 Hedzr Yeh.
 */

package mqtt

type RiskControl interface {
	Disable(disable bool)
	IsEnabled() bool
}

//
// - [x] 重复的连接请求
// - 重复的身份验证请求
// - 连接的异常终止
// - 主题扫描（请求发送或订阅大量主题）
// - 发送无法送达的消息（没有订阅者的主题）
// - 客户端连接但是不发送数据

// TODO The advanced risk control: flow/band-width/rate control based on white list or black list

type riskControl struct {
	enabled    bool
	blackLists []string
	whiteLists []string
}

func (r *riskControl) Disable(disable bool) {
	r.enabled = !disable
}

func (r *riskControl) IsEnabled() bool {
	return r.enabled
}

func NewDefaultRiskControl(enabled bool) RiskControl {
	return &riskControl{enabled: enabled}
}
