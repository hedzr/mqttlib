/*
 * Copyright © 2020 Hedzr Yeh.
 */

package mqtt

import (
	"bufio"
	"fmt"
	"github.com/sirupsen/logrus"
	"gitlab.com/hedzr/mqttlib/pkg/mqttlib/errors"
	"gitlab.com/hedzr/mqttlib/pkg/mqttlib/mqtt/cid"
	"gitlab.com/hedzr/mqttlib/pkg/mqttlib/mqtt/codec"
)

type ConnectParam struct {
	ProtocolLevel     ProtocolLevel
	KeepAlivedSeconds uint16 // in seconds
	CleanSessionFlag  bool

	// WillFlag          bool
	WillQoS        QoSType
	WillRetainFlag bool
	// PasswordFlag      bool
	// UsernameFlag      bool

	// ClientId may be in or out property
	// For client, it's initialized by NewClient and NewMqttClientContext
	ClientId        string
	ClientIdCreated bool
	WillTopic       string
	WillMessage     []byte

	Username string
	Password string

	RetCode int // in this field, the connect packet errors will be kept and sent to client later
}

func (s *ConnectParam) MigrateFrom(vh1 VariantHeader, payload1 Payload) {
	if vh, ok := vh1.(*ConnectVH); ok {
		if payload, ok := payload1.(*ConnectPayload); ok {
			s.ProtocolLevel = vh.ProtocolLevel
			s.KeepAlivedSeconds = vh.KeepAlivedSeconds
			s.CleanSessionFlag = vh.CleanSessionFlag
			s.WillQoS = vh.WillQoS
			s.WillRetainFlag = vh.WillRetainFlag
			if vh.WillFlag {
				s.WillTopic = payload.WillTopic
				s.WillMessage = payload.WillMessage
			}
			if vh.PasswordFlag {
				s.Password = payload.Password
			}
			if vh.UsernameFlag {
				s.Username = payload.Username
			}
			s.RetCode = payload.RetCode
			s.ClientId = payload.ClientId
			s.ClientIdCreated = payload.ClientIdCreated
		}
	}
}

type ConnectVH struct {
	vh50ext

	protocolName  []byte
	ProtocolLevel ProtocolLevel
	ConnectFlag   byte
	// keepAlived    uint16

	KeepAlivedSeconds uint16 // in seconds
	CleanSessionFlag  bool   // Clean Start Flag for v5.0+
	WillFlag          bool
	WillQoS           QoSType
	WillRetainFlag    bool
	PasswordFlag      bool
	UsernameFlag      bool
}

func (s *ConnectVH) expectR(r *bufio.Reader, sample []byte) (err error) {
	var b byte
	for i, b1 := range sample {
		b, err = r.ReadByte()
		if err != nil {
			return
		}
		if b != b1 {
			err = fmt.Errorf("expect %v but read %v (at index %v of sample %v)", b1, b, i, sample)
			return
		}
	}
	return
}

func (s *ConnectVH) expect(data []byte, pos int, sample []byte) (err error) {
	var b, b1 byte
	var i, j int
	for i, j = pos, 0; i < len(data) && j < len(sample); i, j = i+1, j+1 {
		b, b1 = data[i], sample[j]
		if b != b1 {
			err = fmt.Errorf("expect %v but read %v (at index %v of sample %v)", b1, b, i, sample)
			return
		}
	}
	return
}

func (s *ConnectVH) Apply(ctx *StateContext, pkg *Pkg, data []byte, pos int) (newPos int, err error) {
	if ctx.strictProtocolName {
		if err = s.expect(data, pos, defaultConnectVH.protocolName); err != nil {
			return // 3.1.2-1
		}
		s.protocolName = defaultConnectVH.protocolName[2:]
		pos += len(defaultConnectVH.protocolName)
	} else {
		l, np, e := codec.ReadBEUint16(data, pos)
		if e != nil {
			err = e
			return
		}
		pos = np
		s.protocolName = data[pos : pos+int(l)]
		pos = pos + int(l)
	}
	if pos >= len(data) {
		err = errors.ErrOverflow
		return
	}

	s.ProtocolLevel = ProtocolLevel(data[pos])
	pos++
	if pos >= len(data) {
		err = errors.ErrOverflow
		return
	}

	s.ConnectFlag = data[pos]
	pos++
	if pos >= len(data) {
		err = errors.ErrOverflow
		return
	}

	if (s.ConnectFlag & 0x01) != 0 {
		err = errors.ErrCodePacketIllegal.New("Reserved flag in first byte must not be non-zero: byte = %v", s.ConnectFlag)
		return // 3.1.2-3
	}

	if (s.ConnectFlag & 0x02) != 0 {
		s.CleanSessionFlag = true
	}
	if (s.ConnectFlag & 0x04) != 0 {
		s.WillFlag = true
	}
	if s.WillFlag {
		if (s.ConnectFlag & 0x18) != 0 {
			s.WillQoS = QoSType((s.ConnectFlag >> 3) & 0x3)
			if s.WillQoS == 3 {
				// 3.1.2-14
				err = errors.ErrCodePacketCorrupt.New("unexpect qos value %v", s.WillQoS)
				return
			}
		}
		if (s.ConnectFlag & 0x20) != 0 { // 3.1.2-11
			s.WillRetainFlag = true
		}
	} else {
		//  3.1.2-13

		if (s.ConnectFlag & 0x20) != 0 { // 3.1.2-15
			err = errors.ErrCodePacketCorrupt.New("unexpect will retain true when will flag not set")
			return
		}
	}
	if (s.ConnectFlag & 0x40) != 0 {
		s.PasswordFlag = true
	}
	if (s.ConnectFlag & 0x80) != 0 {
		s.UsernameFlag = true
	}

	s.KeepAlivedSeconds, newPos, err = codec.ReadBEUint16(data, pos)

	if s.ProtocolLevel >= ProtocolLevelForV50 {
		newPos, err = s.vh50ext.Apply(ctx, pkg, data, newPos)
	}

	// NOTE newPos = pos // Apply() must return newPos to pointer the updated new position
	pkg.VH = s // Apply() must assign itself into pkg.VH
	return
}

func (s *ConnectVH) read(r *bufio.Reader) (err error) {
	// var n int
	// io.ReadFull(r, s.protocolName)
	// n,err=r.Read(s.protocolName)
	if err = s.expectR(r, defaultConnectVH.protocolName); err != nil {
		return
	}

	var lvl byte
	lvl, err = r.ReadByte()
	s.ProtocolLevel = ProtocolLevel(lvl)
	if err != nil {
		return
	}

	s.ConnectFlag, err = r.ReadByte()
	if err != nil {
		return
	}
	if (s.ConnectFlag & 0x02) != 0 {
		s.CleanSessionFlag = true
	}
	if (s.ConnectFlag & 0x04) != 0 {
		s.WillFlag = true
	}
	if (s.ConnectFlag & 0x18) != 0 {
		s.WillQoS = QoSType((s.ConnectFlag >> 3) & 0x3)
	}
	if (s.ConnectFlag & 0x20) != 0 {
		s.WillRetainFlag = true
	}
	if (s.ConnectFlag & 0x40) != 0 {
		s.PasswordFlag = true
	}
	if (s.ConnectFlag & 0x80) != 0 {
		s.UsernameFlag = true
	}

	s.KeepAlivedSeconds, err = codec.ReadBEUint16R(r)
	return
}

type ConnectPayload struct {
	ClientId        string
	ClientIdCreated bool
	WillTopic       string
	WillMessage     []byte
	Username        string
	Password        string

	RetCode int // in this field, the connect packet errors will be kept and sent to client later

	StrictClientId bool // should we verify the user's client id with strict rules?
}

// Apply to parse incoming 'data' from 'pos' postion.
// 3.1.3-1
// Apply return false if payload or vh read error occured, 3.1.4-1, so that caller will fail the process flow and close client connection later.
func (s *ConnectPayload) Apply(ctx *StateContext, pkg *Pkg, data []byte, pos int) (newPos int, err error) {
	// 3.1.3-2, 3.1.3-3, 3.1.3-4, 3.1.3-5
	s.ClientId, pos, err = codec.ReadString(data, pos)
	if err != nil {
		err = errors.ErrCodeMqttBadClientId.NewE(err, "can't read client id")
		s.RetCode = 0x02 // 3.1.3-9
		return
	}

	if len(s.ClientId) == 0 {
		// 3.1.3-6
		s.ClientId = cid.CreateNewClientId()
		s.ClientIdCreated = true
		logrus.Debugf("empty client identifier, new one created: %v", s.ClientId)
		if vh, ok := pkg.VH.(*ConnectVH); ok {
			if vh.CleanSessionFlag == false {
				// 3.1.3-7
				err = errors.ErrCodeMqttBadClientId.New("clean-session must be 1 if has a zero client id")
				s.RetCode = 0x02 // 3.1.3-8
				return
			}
		}
	} else if s.StrictClientId {
		if err = cid.VerifyClientId(s.ClientId); err != nil {
			s.RetCode = 0x02 // 3.1.3-9
			return
		}
	}

	if vh, ok := pkg.VH.(*ConnectVH); ok {
		if vh.ProtocolLevel > ctx.maxProtocolLevel {
			err = errors.ErrCodePacketIllegal.New("Unsupported protocol level %v", vh.ProtocolLevel)
			s.RetCode = 0x01 // 3.1.2-2
			return
		}

		// 3.1.2-9
		if vh.WillFlag {
			// 3.1.3-10
			s.WillTopic, pos, err = codec.ReadString(data, pos)
			if err != nil {
				return
			}
			//
			s.WillMessage, pos, err = codec.ReadBytes(data, pos)
			if err != nil {
				return
			}
		}
		if vh.UsernameFlag {
			s.Username, pos, err = codec.ReadString(data, pos)
			if err != nil {
				return
			}
		} else {
			// 3.1.2-18, 3.1.2-19
		}
		if vh.PasswordFlag {
			// 3.1.3-11
			s.Password, pos, err = codec.ReadString(data, pos)
			if err != nil {
				return
			}
			if len(s.Username) == 0 {
				// 3.1.2-22
			}
		} else {
			// 3.1.2-20, 3.1.2-21
		}

		newPos = pos    // Apply() must return newPos to pointer the updated new position
		pkg.Payload = s // Apply() must be assign itself into pkg.Payload
	} else {
		err = errors.New("wrong variant header, expect connectVH, got: %+v", pkg.VH)
	}
	return
}

var defaultConnectVH = &ConnectVH{
	protocolName: []byte{0, 4, 77, 81, 84, 84},
	// ProtocolLevel: 0,
}
