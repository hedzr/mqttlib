/*
 * Copyright © 2020 Hedzr Yeh.
 */

package mqtt

import (
	"bytes"
	"gitlab.com/hedzr/mqttlib/pkg/mqttlib/errors"
	"gitlab.com/hedzr/mqttlib/pkg/mqttlib/mqtt/cid"
	"testing"
)

func TestTopicFiltersMatching(t *testing.T) {
	pss := newPubsubStore()

	for _, lit := range []struct {
		tf, tn string
		expect bool
		errno  errors.Code
	}{
		{"#", "$SYS/a/bv", false, errors.NoError},
		{"+/a/bv", "$SYS/a/bv", false, errors.NoError},
		{"$SYS/a/bv/+", "$SYS/a/bv/c", true, errors.NoError},
		{"$SYS/a/#", "$SYS/a/bv/c", true, errors.NoError},
		{"$SYS/#", "$SYS/a/bv/c", true, errors.NoError},
		{"$SYS/#", "$SYS/", true, errors.NoError},
		{"$SYS/monitor/+", "$SYS/monitor/Clients", true, errors.NoError},

		{"sport/tennis/#", "a/tennis/#", false, errors.ErrCodeBadTopicName},
		{"sport/tennis/#", "a/tennis/+", false, errors.ErrCodeBadTopicName},
		{"sport/tennis/#", "a/ten#nis/a", false, errors.ErrCodeBadTopicName},
		{"sport/tennis/#", "a/ten+nis/b", false, errors.ErrCodeBadTopicName},
		{"sport/tennis#", "a/tennis/", true, errors.ErrCodeBadTopicFilter},
		{"sport/tennis/#/ranking", "a/tennis/", false, errors.ErrCodeBadTopicFilter},
		{"sport/tennis+", "a/tennis/", true, errors.ErrCodeBadTopicFilter},

		{"+/tennis/#", "a/tennis/", true, errors.NoError},
		{"+/tennis/#", "a/tennis/1/2/3", true, errors.NoError},
		{"sport/#", "sport", true, errors.NoError},

		{"", "", false, errors.NoError},
		{"+/+", "/finance", true, errors.NoError},
		{"/+", "/finance", true, errors.NoError},
		{"+", "/finance", false, errors.NoError},
		{"+/tennis/#", "a/tennis/1/2/3", true, errors.NoError},
		{"+/tennis/#", "a/tennis/1", true, errors.NoError},
		{"+/tennis/#", "a/tennis/", true, errors.NoError},
		{"+/tennis/#", "a/tennis", true, errors.NoError},
		{"+/tennis/#", "a/b/tennis", false, errors.NoError},
		{"+/tennis/#", "a//tennis", false, errors.NoError},
		{"sport/+", "sport//p1", false, errors.NoError},
		{"sport/+", "sport//", false, errors.NoError},
		{"sport/+", "sport/", true, errors.NoError},
		{"sport/+", "sport/p2", true, errors.NoError},
		{"sport/+", "sport", false, errors.NoError},
		{"sport/#", "sport//p1", true, errors.NoError},
		{"sport/#", "sport//", true, errors.NoError},
		{"sport/#", "sport/", true, errors.NoError},
		{"sport/#", "sport", true, errors.NoError},
	} {
		matched, err := pss.match(lit.tf, lit.tn, nil)
		if err != nil {
			if errors.IsAny(err, errors.NoError, lit.errno) {
				continue
			}
			t.Errorf("matching error: %v", err)
		} else if matched != lit.expect {
			t.Fatalf("ERROR: expect '%v' matching '%v' returns %v, but got %v", lit.tn, lit.tf, lit.expect, matched)
		}
	}
}

type testCloser struct {
}

func (t *testCloser) Close() error {
	return nil
}

const testClientId = "test-1234567890"

func TestListTopics(t *testing.T) {
	pss := newPubsubStore()

	writer := bytes.NewBufferString("")
	closer := &testCloser{}

	ctx := NewMqttTestContext(testClientId, writer, closer)
	defer ctx.CloseWithReason(errors.CloseNormal)
	ctx.saveAllMessages = true // to test the topics tree

	for _, lit := range []struct {
		errno            errors.Code
		DUP              bool
		qos              QoSType
		RETAIN           bool
		topicName        string
		packetIdentifier uint16
		data             []byte
	}{
		// {false, QoSDefault, false, "sport/", 2, []byte{1, 2, 3,}},
		{errors.NoError, false, QoSDefault, false, "sport/air", 2, []byte{1, 2, 3}},
		{errors.NoError, false, QoSDefault, false, "sport/archery/field", 2, []byte{1, 2, 3}},
		{errors.NoError, false, QoSDefault, false, "sport/archery/flight", 2, []byte{1, 2, 3}},
		{errors.NoError, false, QoSDefault, false, "sport/archery/gungdo", 2, []byte{1, 2, 3}},
		{errors.NoError, false, QoSDefault, false, "sport/archery/indoor", 2, []byte{1, 2, 3}},
		{errors.NoError, false, QoSDefault, false, "sport/archery/Kyūdō", 2, []byte{1, 2, 3}},
		{errors.NoError, false, QoSDefault, true, "sport/archery/popinjay", 2, []byte{1, 2, 3}},
		{errors.NoError, false, QoSDefault, false, "sport/archery/target", 2, []byte{1, 2, 3}},
		// {NoError, false, QoSDefault, false, "sport/archery", 2, []byte{1, 2, 3,}},
		{errors.NoError, false, QoSDefault, false, "sport/ball-over-net", 2, []byte{1, 2, 3}},
		{errors.NoError, false, QoSDefault, false, "sport/basketball", 2, []byte{1, 2, 3}},
		{errors.NoError, false, QoSDefault, false, "sport/bat-and-ball", 2, []byte{1, 2, 3}},
		{errors.NoError, false, QoSDefault, true, "sport", 2, []byte{89, 89, 89}},
		{errors.NoError, false, QoSDefault, false, "tech", 2, []byte{1, 2, 3}},
		{errors.NoError, false, QoSDefault, false, "sci", 2, []byte{1, 2, 3}},
	} {
		builder := NewPublishBuilder(lit.topicName, lit.data, WithPubQoS(lit.DUP, lit.qos, lit.RETAIN), WithPubPacketIdentifier(lit.packetIdentifier))
		err := pss.DispatchNoLimit(ctx, lit.RETAIN, builder, testSendPayloadVx(t, testClientId))
		if err != nil {
			if errors.IsAny(err, errors.NoError, lit.errno) {
				continue
			}
			t.Errorf("publish error: %v", err)
		}
	}

	for _, lit := range []struct {
		cid, tf          string
		qos              QoSType
		packetIdentifier uint16
		errno            errors.Code
	}{
		{"", "#", QoSDefault, 2, errors.NoError},
		{"", "sport/#", QoSDefault, 2, errors.NoError},
		{"", "sport/archery/#", QoSDefault, 2, errors.NoError},
	} {
		if len(lit.cid) == 0 {
			lit.cid = cid.CreateNewClientId()
		}
		invoked, delta, err := pss.subscribe(ctx, lit.cid, TopicFilter{Filter: lit.tf, RequestedQoS: lit.qos}, lit.packetIdentifier, testSendPayload(t, lit.tf))
		if err != nil {
			if errors.IsAny(err, errors.NoError, lit.errno) {
				continue
			}
			t.Errorf("subscribe error: %v", err)
		} else if invoked < lit.qos {
			t.Logf("subscribe warn: invoked qos (%v) is lower than requested (%v). // delta=%v", invoked, lit.qos, delta)
		}
	}

	t.Log(pss.dump())

}

func testSendPayloadVx(t *testing.T, sub string) SendPayloadVx {
	return func(ctx *StateContext, clientId string, builder PacketBuilder, qp *qp) (err error) {
		t.Logf("[%v] ▲ %v -> %v (%v)", sub, builder.(*publishBuilder).topicName, clientId, *qp)
		return
	}
}

func testSendPayload(t *testing.T, sub string) SendPayload {
	return func(ctx *StateContext, clientId string, qos QoSType, path string, payload []byte, packetIdentifier uint16) (err error) {
		t.Logf("[%v] ▲ %v %v -> %v (%v)", sub, path, payload, clientId, qos)
		return
	}
}

func TestTopicFiltersCRUD(t *testing.T) {
	pss := newPubsubStore()

	writer := bytes.NewBufferString("")
	closer := &testCloser{}

	ctx := NewMqttTestContext(testClientId, writer, closer)
	defer ctx.CloseWithReason(errors.CloseNormal)

	for _, lit := range []struct {
		cid, tf          string
		qos              QoSType
		packetIdentifier uint16
		errno            errors.Code
	}{
		{"", "sport/#", QoSDefault, 2, errors.NoError},
	} {
		if len(lit.cid) == 0 {
			lit.cid = cid.CreateNewClientId()
		}
		invoked, delta, err := pss.subscribe(ctx, lit.cid, TopicFilter{Filter: lit.tf, RequestedQoS: lit.qos}, lit.packetIdentifier, testSendPayload(t, lit.tf))
		if err != nil {
			if errors.IsAny(err, errors.NoError, lit.errno) {
				continue
			}
			t.Errorf("subscribing error: %v", err)
		} else if invoked < lit.qos {
			t.Logf("subscribe warn: invoked qos (%v) is lower than requested (%v). // delta=%v", invoked, lit.qos, delta)
		}
	}
}
