/*
 * Copyright © 2020 Hedzr Yeh.
 */

package mqtt

import (
	"github.com/sirupsen/logrus"
	"gitlab.com/hedzr/mqttlib/pkg/mqttlib/auth"
	"gitlab.com/hedzr/mqttlib/pkg/mqttlib/errors"
	"gitlab.com/hedzr/mqttlib/pkg/mqttlib/mqtt/pktidgen"
	"io"
	"sync/atomic"
	"time"
)

const DefaultConnectTimeout = 60 * time.Second

type StateContext struct {
	base

	TimeOfConnected time.Time
	ConnectTimeout  time.Duration

	maxVersion         string
	maxProtocolLevel   ProtocolLevel
	maxQoS             QoSType
	noWildMatchFilter  bool
	authEnabled        bool
	strictClientId     bool
	strictProtocolName bool

	State    State
	uxHooker Hooker
	session  Session
	// ClientId     string
	ConnectParam *ConnectParam

	writer      io.Writer
	closer      io.Closer
	closed      int32
	closeReason errors.CloseReason

	authenticator          auth.Authenticator
	sessionStore           SessionStore
	stateRegistry          StateRegistry
	riskControl            RiskControl
	store                  Store
	packetIdentifierHolder *pktidgen.PacketIdentifierHolder
	clientSubsHolder       *ClientSubsHolder
	// riskControlEnabled bool

	saveAllMessages bool
	mode            Mode // server, client, or broker
	normalClose     bool // 3.1.2-10 // 3.1.2-8 // 3.1.2-10 // 3.1.2-12

	// defaultSessionStoreOpts []DefaultSessionStoreOpt
}

type Mode int

const (
	ModeClient Mode = iota
	ModeBroker
	// ModeBroker
)

func newMqttContext(mode Mode, clientId string, wr io.Writer, closer io.Closer, opts ...StateContextOpt) *StateContext {

	c := &StateContext{
		base: newBase("mqtt.ctx"),
		mode: mode,

		TimeOfConnected: time.Now().UTC(),
		ConnectTimeout:  DefaultConnectTimeout,

		maxVersion:         "3.1.1",
		maxProtocolLevel:   ProtocolLevelForV311,
		maxQoS:             2,
		noWildMatchFilter:  false,
		authEnabled:        false,
		strictClientId:     false,
		strictProtocolName: true,

		State:    IDLE,
		uxHooker: newDefaultHooker(),
		// session:
		// ClientId: clientId,
		ConnectParam: &ConnectParam{
			ClientId: clientId,
		},

		writer: wr,
		closer: closer,
		closed: 0,

		packetIdentifierHolder: pktidgen.NewPacketIdentifierHolder(),
		clientSubsHolder:       newClientSubsHolder(),
		// riskControlEnabled: enableRiskControl,
	}

	for _, opt := range opts {
		opt(c)
	}

	if c.sessionStore == nil {
		c.sessionStore = NewDefaultSessionStore()
	}
	if c.stateRegistry == nil {
		c.stateRegistry = NewDefaultStateRegistry(c.strictClientId)
	}
	if c.authenticator == nil {
		c.authenticator = auth.NewDefaultTigerLaohuAuthenticator()
	}
	if c.riskControl == nil {
		c.riskControl = NewDefaultRiskControl(false)
	}

	// maximumQoS = c.maxQoS
	return c
}

func (s *StateContext) CanWrite() bool {
	return s.writer != nil
}

func (s *StateContext) MaxVersion() string {
	return s.maxVersion
}

func (s *StateContext) MaxProtocolLevel() ProtocolLevel {
	return s.maxProtocolLevel
}

func (s *StateContext) MaxQoS() QoSType {
	return s.maxQoS
}

func (s *StateContext) IsClientMode() bool {
	return s.mode == ModeClient
}

func (s *StateContext) IsNotClientMode() bool {
	return s.mode != ModeClient
}

func (s *StateContext) IsServer() bool {
	return s.mode == ModeBroker
}

func (s *StateContext) IsNotServer() bool {
	return s.mode != ModeBroker
}

// func (s *StateContext) IsBroker() bool {
// 	return s.mode == ModeBroker
// }
//
// func (s *StateContext) IsNotBroker() bool {
// 	return s.mode != ModeBroker
// }

func (s *StateContext) IsNormalClosed() bool {
	return s.normalClose
}

func (s *StateContext) IsClose() bool {
	var i int32
	atomic.LoadInt32(&i)
	return i != 0
}

func (s *StateContext) CloseWithReason(reason errors.CloseReason) (err error) {
	s.closeReason = reason
	s.Info("♦︎♦︎ ctx closing with reason %v", reason)
	err = s.close()
	return
}

func (s *StateContext) close() (err error) {
	if atomic.CompareAndSwapInt32(&s.closed, 0, 1) {

		if e := s.closer.Close(); e != nil {
			err = errors.ErrCodeErrors.NewE(e, "can't close connection")
		}

		if s.session != nil {

			// 3.1.4-3

			// 3.1.2-8
			if !s.normalClose && s.sessionStore.HasWillMessage(s.ConnectParam.ClientId) { // 3.1.2-10 // 3.1.2-12
				// if non-normalizing closing, send the will message if exists
				if e := s.sessionStore.SendWillMessage(s, s.ConnectParam.ClientId, nil); e != nil {
					err = errors.ErrCodeErrors.NewE(e, "can't publish the will message")
				}
			}

			// close itself
			if s.session != nil {
				if e := s.session.Close(); e != nil {
					err = errors.ErrCodeErrors.NewE(e, "can't close session")
				}
				s.session = nil
			}

			if s.ConnectParam != nil {
				// remove it from session store, unsubscribe anything if necessary
				if e := s.sessionStore.Unset(s.ConnectParam.ClientId); e != nil {
					err = errors.ErrCodeErrors.NewE(e, "can't unset session from session store")
				} else {
					logrus.Debugf("session %q removed", s.ConnectParam.ClientId)
				}
			}
		}
	}
	return
}

func (s *StateContext) collectConnectParam(pkg *Pkg) (err error) {
	if s.ConnectParam == nil {
		s.ConnectParam = &ConnectParam{}
	}
	s.ConnectParam.MigrateFrom(pkg.VH, pkg.Payload)
	if s.ConnectParam.ProtocolLevel < s.maxProtocolLevel {
		s.maxProtocolLevel = s.ConnectParam.ProtocolLevel
	}
	return
}

func (ctx *StateContext) Parse(pkg *Pkg) (to State, err error) {
	to, err = ctx.stateRegistry.Parse(ctx, pkg, false)
	return
}

// Write to write bytes back to remote client.
// mqttReader implements Writer interface too.
func (s *StateContext) Write(p []byte) (nn int, err error) {
	if sp, ok := s.sessionStore.(SysPublisher); ok {
		if s.sessionStore.SysStatsEnabled() {
			sp.IncMessages("sent", 1)
			sp.IncLoad("bytes/sent", len(p))
			if s.sessionStore.SysStatsLogEnabled() {
				logrus.Debugf("  ------> $SYS/broker/messages/sent++, $SYS/broker/load/bytes/sent+%d", len(p))
			}
		}
	}
	nn, err = s.writer.Write(p)
	if err == nil {
		if wr, ok := s.writer.(interface{ Flush() error }); ok {
			err = wr.Flush()
		}
	}
	return
}

func (s *StateContext) Advance(pkg *Pkg) (next State, err error) {
	if sp, ok := s.sessionStore.(SysPublisher); ok {
		if s.sessionStore.SysStatsEnabled() {
			sp.IncMessages("received", 1)
			sp.IncLoad("bytes/received", pkg.HdrLen+pkg.Length)
			if s.sessionStore.SysStatsLogEnabled() {
				logrus.Debugf("  ------> $SYS/broker/messages/received + 1, $SYS/broker/load/bytes/received + %d", pkg.HdrLen+pkg.Length)
			}
		}
	}
	next, err = s.stateRegistry.Advance(s, s, pkg, false)
	if err != nil {
		// s.Wrong(err, "ERROR OCCURS, closing")
		ex := errors.Unwrap(err)
		logrus.Debugf("ex: %+v", ex)
		logrus.Debugf("err: %+v", err)
		var e *errors.MqttError
		if errors.As(err, &e) {
			err = e.Attach(s.CloseWithReason(e.GetReason()))
		}
	}
	return
}

func (s *StateContext) CreateSessionForThisClient(pkg *Pkg) (sess Session, err error) {

	if s.IsClose() {
		err = errors.ErrCodeMqttStateWrong.New("can't create session on closed context")
		return
	}

	logrus.Debugf("session %q creating: max %v, max %v, max v%v", s.ConnectParam.ClientId, s.maxProtocolLevel, s.maxQoS, s.maxVersion)

	if !s.sessionStore.Has(s.ConnectParam.ClientId) {
		// create new session
		if err = s.sessionStore.Set(s.ConnectParam.ClientId, newSession(s, s.ConnectParam.ClientId, s.ConnectParam.CleanSessionFlag)); err != nil {
			return
		}
	} else {
		// 3.1.4-2: server must do disconnect from the old client if exists (and reconnect to this session)
		s.Debug("session '%s' EXISTED, connect and reuse with it", s.ConnectParam.ClientId)
	}

	sess = s.sessionStore.Get(s.ConnectParam.ClientId)
	sess.SetCleanSessionFlag(s.ConnectParam.CleanSessionFlag)
	s.session = sess

	sess.LinkTo(s)

	if len(s.ConnectParam.WillTopic) > 0 { // && s.ConnectParam.WillRetainFlag {
		s.sessionStore.AddWillMessage(s.ConnectParam.ClientId, s.ConnectParam.WillQoS, s.ConnectParam.WillRetainFlag, s.ConnectParam.WillTopic, s.ConnectParam.WillMessage)
	}

	return
}

func (s *StateContext) Borrow() (pktId uint16) {
	pktId = s.packetIdentifierHolder.Borrow()
	return
}

func (s *StateContext) Return(pktId uint16) (err error) {
	s.packetIdentifierHolder.Return(pktId)
	return
}

func (s *StateContext) bizPublish(pkg *Pkg) (err error) {
	err = s.sessionStore.Publish(s, pkg)
	return
}

func (s *StateContext) storeAsTask(pkg *Pkg) (err error) {
	err = s.sessionStore.StoreQos1or2Message(s, pkg)
	return
}

func (s *StateContext) bizPublishByPktId(pktId uint16) (err error) {
	err = s.sessionStore.PublishByPktId(s, pktId)
	return
}
