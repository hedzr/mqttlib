/*
 * Copyright © 2020 Hedzr Yeh.
 */

package mqtt

import (
	"bytes"
	"gitlab.com/hedzr/mqttlib/pkg/mqttlib/mqtt/codec"
	"io"
)

func NewPub21recBuilderFromPkg(pkg *Pkg) PacketBuilder {
	if vh, ok := pkg.VH.(*PublishVH); ok {
		if payload, ok := pkg.Payload.(*PublishPayload); ok {
			return &pubrecBuilder{
				vh50ext:    *newvh50ext(),
				vh:         vh,
				payload:    payload,
				identifier: vh.PacketIdentifier,
			}
		}
	}
	return nil
}

type pubrecBuilder struct {
	vh50ext
	vh         *PublishVH
	payload    *PublishPayload
	identifier uint16
	data       []byte
}

func (b *pubrecBuilder) IsResending() bool {
	return false
}

func (b *pubrecBuilder) RequestResend() {
}

func (b *pubrecBuilder) NextPktId(ctx *StateContext) uint16 {
	return 0
}

func (b *pubrecBuilder) ReportType() ReportType {
	return PUBREC
}

// Build to build the packet
// when PUBLISH pkt received, PUBACK will be send back (QoS 1).
func (b *pubrecBuilder) Build(ctx *StateContext, pkg *Pkg) (err error) {
	if ctx.ConnectParam.ProtocolLevel >= ProtocolLevelForV50 {
		err = b.Buildv5(ctx, pkg, 0x52)
		return
	}

	bb := bytes.NewBuffer([]byte{0x50, 0x02})

	err = codec.WriteBEUint16(bb, b.identifier)
	if err != nil {
		return
	}

	b.data = bb.Bytes()
	return
}

func (b *pubrecBuilder) Buildv5(ctx *StateContext, pkg *Pkg, firstByte byte) (err error) {
	var vhBuf bytes.Buffer
	vhBuf, err = b.buildv5(ctx, pkg)
	if err != nil {
		return
	}

	var bb bytes.Buffer
	bb.WriteByte(firstByte)
	_, err = codec.EncodeIntToW(&bb, vhBuf.Len())
	if err != nil {
		return
	}

	_, err = bb.Write(vhBuf.Bytes())
	if err != nil {
		return
	}

	b.data = bb.Bytes()
	return
}

func (b *pubrecBuilder) buildv5(ctx *StateContext, pkg *Pkg) (buf bytes.Buffer, err error) {
	// for MQTT v5.0

	// construct the properties buffer at first

	var propertiesBuf bytes.Buffer
	propertiesBuf, err = b.WriteAs()
	if err != nil {
		return
	}

	var length int
	var buf8 []byte = make([]byte, 8)
	length = codec.EncodeIntToBuf(propertiesBuf.Len(), buf8, 0)

	// vh
	buf.Write(buf8[:length])         // properties length
	buf.Write(propertiesBuf.Bytes()) // properties buffer
	return
}

func (b *pubrecBuilder) bizPublish(ctx *StateContext, pkg *Pkg, bb *bytes.Buffer) (err error) {
	err = ctx.bizPublish(pkg)
	return
}

func (b *pubrecBuilder) WriteTo(w io.Writer) (n int, err error) {
	n, err = w.Write(b.Bytes())
	return
}

func (b *pubrecBuilder) Bytes() (bytes []byte) {
	return b.data
}

func (b *pubrecBuilder) AsString() (str string) {
	return
}
