/*
 * Copyright © 2020 Hedzr Yeh.
 */

package math

const MaxUint = ^uint(0)
const MinUint = 0

const MaxUint16 = ^uint16(0)
const MinUint16 = 0

const MaxUint32 = ^uint32(0)
const MinUint32 = 0

const MaxUint64 = ^uint64(0)
const MinUint64 = 0

const MaxInt = int(MaxUint >> 1)
const MinInt = -MaxInt - 1

const IntMAX = int(^uint(0) >> 1)
const Int64MAX = int64(2) ^ 64 - 1

const MaxInt16 = int(MaxUint16 >> 1)
const MinInt16 = -MaxInt16 - 1

const MaxInt32 = int(MaxUint32 >> 1)
const MinInt32 = -MaxInt32 - 1

const MaxInt64 = int(MaxUint64 >> 1)
const MinInt64 = -MaxInt64 - 1
