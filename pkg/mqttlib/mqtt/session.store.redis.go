/*
 * Copyright © 2020 Hedzr Yeh.
 */

package mqtt

import (
	"github.com/hedzr/cmdr"
	"github.com/hedzr/errors"
	"github.com/mediocregopher/radix/v3"
	"github.com/sirupsen/logrus"
	"gitlab.com/hedzr/mqttlib/pkg/redisop"
	"reflect"
	"strings"
	"sync"
	"time"
)

// NewPathRWGob creates a new gob PathReadWriter [PathRW] instance
func NewPathRWRedis(opts ...PathRWOpt) PathRW {
	rw := &redisPathRW{}
	for _, opt := range opts {
		opt(rw)
	}
	return rw
}

// func WithPathRWRedisConfig(config *redis.Config) PathRWOpt {
// 	return func(rw PathRW) {
// 		if g, ok := rw.(*redisPathRW); ok {
// 			g.config = config
// 		}
// 	}
// }

func WithPathRWRedisPrefix(prefix string) PathRWOpt {
	return func(rw PathRW) {
		if g, ok := rw.(*redisPathRW); ok {
			// kp := fmt.Sprintf("mqtt.server.storage.redis.%v", vxconf.RunModeExt())
			// redis.WithPrefixKey("mqtt.server.storage.redis")
			opt := redisop.WithPrefixKey(prefix)
			g.redisOpts = append(g.redisOpts, opt)
		}
	}
}

func WithPathRWRedisOpts(opts ...redisop.RedisOpOpt) PathRWOpt {
	return func(rw PathRW) {
		if g, ok := rw.(*redisPathRW); ok {
			g.redisOpts = append(g.redisOpts, opts...)
		}
	}
}

type redisPathRW struct {
	// config    *redis.Config
	redisOpts  []redisop.RedisOpOpt
	client     radix.Client
	dialer     func(opts ...radix.DialOpt) radix.Conn
	psConn     radix.PubSubConn
	psChannels []string
	exitCh     chan struct{}
	msgCh      chan radix.PubSubMessage
}

func (r *redisPathRW) Start(opts ...PathRWOpt) {
	if r.exitCh != nil {
		return
	}

	for _, opt := range opts {
		opt(r)
	}

	// redisop.Start(r.redisOpts...)
	var config Config
	if err := cmdr.GetSectionFrom("mqtt.server.storage.redis.devel", &config); err != nil {
		logrus.Fatalf("[mqttlib.redis] load config failed. %v", err)
	}

	r.connectTo(&config)

	r.exitCh = make(chan struct{})
	go r.run()
}

func (r *redisPathRW) Stop() {
	if r.psConn != nil {
		if err := r.psConn.Close(); err != nil {
			logrus.Errorf("[mqttlib.redis] redis close persistent pub-sub connection failed: %v", err)
		}
		r.psConn = nil
	}
	if r.msgCh != nil {
		close(r.msgCh)
		r.msgCh = nil
	}
	if r.exitCh != nil {
		close(r.exitCh)
		r.exitCh = nil
	}
	if r.client != nil {
		if err := r.client.Close(); err != nil {
			logrus.Errorf("[mqttlib.redis] redis close failed: %v", err)
		}
		r.client = nil
	}
}

type Config struct {
	Peers         string        `yaml:"peers"`
	Username      string        `yaml:"user"`
	Password      string        `yaml:"pass"`
	Db            int           `yaml:"db"`
	ReadonlyRoute bool          `yaml:"readonly-route"`
	DialTimeout   time.Duration `yaml:"dial-timeout"`
	ReadTimeout   time.Duration `yaml:"read-timeout"`
	WriteTimeout  time.Duration `yaml:"write-timeout"`
	EnableCluster bool          `yaml:"enable-cluster"`
	PoolSize      int           `yaml:"pool-size"`
	SubsChannels  []string      `yaml:"subs-channels"`
}

func (r *redisPathRW) connectTo(config *Config) {
	if config.PoolSize < 1 {
		config.PoolSize = 50
	}
	if config.DialTimeout == 0 {
		config.DialTimeout = 30 * time.Second
	}
	if config.ReadTimeout == 0 {
		config.ReadTimeout = 15 * time.Second
	}
	if config.WriteTimeout == 0 {
		config.WriteTimeout = 30 * time.Second
	}
	if config.SubsChannels == nil {
		config.SubsChannels = strings.Split("mqtool.$sys.notify,mqtool.$sys.notify.*,mqtool.news,mqtool.news.*,mqtool.notify,mqtool.notify.*", ",")
	}

	dialOpts := []radix.DialOpt{radix.DialTimeout(config.DialTimeout)}
	if len(config.Password) > 0 {
		dialOpts = append(dialOpts, radix.DialAuthPass(config.Password))
	}

	// this is a ConnFunc which will set up a connection which is authenticated
	// and has a 1 minute timeout on all operations
	customConnFunc := func(network, addr string) (radix.Conn, error) {
		return radix.Dial(network, addr, dialOpts...)
	}

	// this cluster will use the ClientFunc to create a pool to each node in the
	// cluster. The pools also use our customConnFunc, but have more connections
	poolFunc := func(network, addr string) (radix.Client, error) {
		return radix.NewPool(network, addr, config.PoolSize, radix.PoolConnFunc(customConnFunc))
	}

	if config.EnableCluster || strings.Contains(config.Peers, ",") {

		client, err := radix.NewCluster(strings.Split(config.Peers, ","), radix.ClusterPoolFunc(poolFunc))
		if err != nil {
			logrus.WithError(err).Errorf("[mqttlib.redis] can't connect to redis server %q via radix", config.Peers)
			return
		}
		r.dialer = func(opts ...radix.DialOpt) radix.Conn {
			// todo need more tests about persistPubSub under redis cluster mode
			c, err := radix.Dial("tcp", config.Peers, opts...)
			if err != nil {
				logrus.WithError(err).Errorf("[mqttlib.redis] can't connect to redis server %q", config.Peers)
				return nil
			}
			r.initSubscribe()
			return c
		}

		// var redisErr resp2.Error
		// err = client.Do(radix.Cmd(nil, "AUTH", config.Password))
		// if errors.As(err, &redisErr) {
		// 	log.Printf("redis error returned: %s", redisErr.E)
		// }

		r.client = client
	} else {
		// this pool will use our ConnFunc for all connections it creates
		client, err := radix.NewPool("tcp", config.Peers, 10, radix.PoolConnFunc(customConnFunc))
		if err != nil {
			logrus.WithError(err).Errorf("[mqttlib.redis] can't connect to redis server %q via radix", config.Peers)
			return
		}
		r.dialer = func(opts ...radix.DialOpt) radix.Conn {
			c, err := radix.Dial("tcp", config.Peers, opts...)
			if err != nil {
				logrus.WithError(err).Errorf("[mqttlib.redis] can't connect to redis server %q", config.Peers)
				return nil
			}
			r.initSubscribe()
			return c
		}

		r.client = client
	}

	r.psChannels = config.SubsChannels
}

func (r *redisPathRW) run() {
	onceTimer := time.NewTimer(10 * time.Second)
	defer func() {
		onceTimer.Stop()
	}()

	for {
		select {
		case <-r.exitCh:
			return
		case <-onceTimer.C:
			r.doTestAndInit()

		case msg := <-r.msgCh:
			r.onPubMsg(&msg)
		}
	}
}

// func (r *redisPathRW) dial(opts ...radix.DialOpt) radix.Conn {
// 	c, err := radix.Dial("tcp", "127.0.0.1:6379", opts...)
// 	if err != nil {
// 		panic(err)
// 	}
// 	return c
// }

func (r *redisPathRW) initSubscribe() {
	// // Have PersistentPubSub pick a random cluster node everytime it wants to
	// // make a new connection. If the node fails PersistentPubSub will
	// // automatically pick a new node to connect to.
	// ps := radix.PersistentPubSub("", "", func(string, string) (radix.Conn, error) {
	// 	topo := r.client.(*radix.Cluster).Topo()
	// 	node := topo[rand.Intn(len(topo))]
	// 	return radix.Dial("tcp", node.Addr)
	// })
	//
	// // Use the PubSubConn as normal.
	// r.msgCh = make(chan radix.PubSubMessage)
	// err := ps.Subscribe(r.msgCh, "myChannel")
	// if err != nil {
	// 	logrus.Errorf("can't subscribe from redis channel. %v", err)
	// }
	// // for msg := range msgCh {
	// // 	log.Printf("publish to channel %q received: %q", msg.Channel, msg.Message)
	// // }

	var err error
	var errNope = errors.New("nope")
	var attempts int
	connFn := func(_, _ string) (radix.Conn, error) {
		attempts++
		if attempts%3 != 0 {
			return nil, errNope
		}
		return r.dialer(), nil
	}

	r.psConn, err = radix.PersistentPubSubWithOpts("", "",
		radix.PersistentPubSubConnFunc(connFn),
		radix.PersistentPubSubAbortAfter(3))
	if err != nil {
		logrus.WithError(err).Error("[mqttlib.redis] can't create persistent pub-sub with opts")
		return
	} else if err = r.psConn.Ping(); err != nil {
		logrus.WithError(err).Error("[mqttlib.redis] can't create persistent pub-sub with opts (# ping ...)")
		return
	}

	// Use the PubSubConn as normal.
	r.msgCh = make(chan radix.PubSubMessage)
	err = r.psConn.PSubscribe(r.msgCh, r.psChannels...)
	if err != nil {
		logrus.Errorf("[mqttlib.redis] can't subscribe from redis channel. %v", err)
	} else {
		logrus.Debugf("[mqttlib.redis] subscribed at channel %q", r.psChannels)
	}
	// for msg := range msgCh {
	// 	log.Printf("publish to channel %q received: %q", msg.Channel, msg.Message)
	// }
}

func (r *redisPathRW) onPubMsg(msg *radix.PubSubMessage) {
	logrus.Printf("[mqttlib.redis] publish to channel %q received: %q", msg.Channel, msg.Message)
}

func (r *redisPathRW) doTestAndInit() {
	if r.client != nil {
		err := r.client.Do(radix.Cmd(nil, "SET", "foo", "someval"))
		if err != nil {
			logrus.Errorf("[mqttlib.redis] redis do failed: %v", err)
		}
	}
}

func (r *redisPathRW) Load(dst *defaultSessionStore) (err error) {
	logrus.Debugf("[mqttlib.redis] loading session store from redis: %v", r.client)
	dec := newDecoder(r)
	if err = dec.Decode(&dst); err != nil {
		logrus.Errorf("[mqttlib.redis] failed to load: %v", err)
	} else {
		logrus.Debug("[mqttlib.redis]     loaded")
	}
	return
}

func (r *redisPathRW) Save(src *defaultSessionStore, tick time.Time) (err error) {
	logrus.Debugf("[mqttlib.redis] saving session store to redis")
	// enc := newEncoder(r)
	// if err = enc.Encode(src); err != nil {
	// 	logrus.Errorf("[mqttlib.redis] failed to save: %v", err)
	// } else {
	// 	logrus.Debugf("[mqttlib.redis]     session store saved at %v", tick)
	// }
	return
}

func (r *redisPathRW) Get(pathKey string, defaultValue ...interface{}) (ret interface{}, err error) {
	logrus.Debugf("[mqttlib.redis] Get(%q) ==> %v", pathKey, ret)
	return
}

func (r *redisPathRW) Set(pathKey string, value interface{}) (err error) {
	logrus.Debugf("[mqttlib.redis] Set(%q, %v)", pathKey, value)
	return
}

type decoder struct {
	r     *redisPathRW
	err   error
	mutex sync.Mutex
}

func newDecoder(r *redisPathRW) *decoder {
	return &decoder{r: r}
}

// Decode reads the next value from the input stream and stores
// it in the data represented by the empty interface value.
// If e is nil, the value will be discarded. Otherwise,
// the value underlying e must be a pointer to the
// correct type for the next data item received.
// If the input is at EOF, Decode returns io.EOF and
// does not modify e.
func (dec *decoder) Decode(e interface{}) error {
	if e == nil {
		return dec.DecodeValue(reflect.Value{})
	}
	value := reflect.ValueOf(e)
	// If e represents a value as opposed to a pointer, the answer won't
	// get back to the caller. Make sure it's a pointer.
	if value.Type().Kind() != reflect.Ptr {
		dec.err = errors.New("redisob: attempt to decode into a non-pointer")
		return dec.err
	}
	return dec.DecodeValue(value)
}

// DecodeValue reads the next value from the input stream.
// If v is the zero reflect.Value (v.Kind() == Invalid), DecodeValue discards the value.
// Otherwise, it stores the value into v. In that case, v must represent
// a non-nil pointer to data or be an assignable reflect.Value (v.CanSet())
// If the input is at EOF, DecodeValue returns io.EOF and
// does not modify v.
func (dec *decoder) DecodeValue(v reflect.Value) error {
	if v.IsValid() {
		if v.Kind() == reflect.Ptr && !v.IsNil() {
			// That's okay, we'll store through the pointer.
		} else if !v.CanSet() {
			return errors.New("redisob: DecodeValue of unassignable value")
		}
	}
	// Make sure we're single-threaded through here.
	dec.mutex.Lock()
	defer dec.mutex.Unlock()

	dec.err = nil
	// dec.buf.Reset() // In case data lingers from previous invocation.
	// id := dec.decodeTypeSequence(false)
	// if dec.err == nil {
	// 	dec.decodeValue(id, v)
	// }
	return dec.err
}
