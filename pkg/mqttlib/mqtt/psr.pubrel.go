/*
 * Copyright © 2020 Hedzr Yeh.
 */

package mqtt

import (
	"gitlab.com/hedzr/mqttlib/pkg/mqttlib/errors"
	"gitlab.com/hedzr/mqttlib/pkg/mqttlib/mqtt/codec"
)

// ....................................................

// NewPubrelParser
// for QoS 2
func NewPubrelParser(to State) PacketParser {
	return &pubrelParser{base: newBase("pubrelParser"), to: to}
}

type pubrelParser struct {
	base
	to State
}

func (p *pubrelParser) GetToState() State {
	return p.to
}

func (p *pubrelParser) CreateVH() VariantHeader {
	return newPubrelVH()
}

func (p *pubrelParser) CreatePayload() Payload {
	return newPubrelPayload()
}

func (p *pubrelParser) OnAction(ctx *StateContext, pkg *Pkg) (err error) {
	if vh, ok := pkg.VH.(*pubrelVH); ok {
		if payload, ok := pkg.Payload.(*pubrelPayload); ok {
			p.Debug("       %s: id=%v, vh=%v, payload=%v", pkg.ReportType, vh.id, vh, payload)

			if ctx.IsNotClientMode() {
				switch pkg.QoS {
				case 1, 2:
					err = p.sendPubComp(ctx, pkg)
					if err == nil {
						pkg.QoS = 0
						err = ctx.bizPublishByPktId(vh.id)
					}
				}
			}
		}
	}
	return
}

func (p *pubrelParser) OnError(ctx *StateContext, pkg *Pkg, reason error) (err error) {
	err = reason
	return
}

// sendPubComp for qos 2 step 3, send back to client
func (p *pubrelParser) sendPubComp(ctx *StateContext, pkg *Pkg) (err error) {
	if !ctx.CanWrite() {
		return // when there are no writer in ctx or receiving publish at client, it's no need to send ack
	}

	if vh, ok := pkg.VH.(*pubrelVH); ok {
		if _, ok := pkg.Payload.(*pubrelPayload); ok {
			p.Debug("       sendPubComp: pktid=%v, DUP=%v, QoS=%v, RETAIN=%v", vh.id, pkg.DUP, pkg.QoS, pkg.RETAIN)

			if pkg.QoS > 0 {
				var builder PacketBuilder
				builder = NewPub23compBuilderFromPkg(vh.id)

				if err = builder.Build(ctx, pkg); err != nil {
					p.Wrong(err, "can't build PUBCOMP packet")
					return
				}

				if _, err = ctx.Write(builder.Bytes()); err != nil {
					p.Wrong(err, "can't send PUBCOMP packet")
					return
				} else {
					p.Trace("    ▲ MQTT.%s %v", builder.ReportType(), builder.Bytes())
				}
			}
		}
	}
	return
}

func newPubrelVH() *pubrelVH {
	return &pubrelVH{}
}

type pubrelVH struct{ id uint16 }

func (s *pubrelVH) Apply(ctx *StateContext, pkg *Pkg, data []byte, pos int) (newPos int, err error) {
	if pos >= len(data) {
		err = errors.ErrCodePacketCorrupt.New("wrong %v pkt (vh+payload): %v", PUBREL, data)
		return
	}

	s.id, pos, err = codec.ReadBEUint16(data, pos)
	if err != nil {
		return
	}

	newPos = pos // Apply() must return newPos to pointer the updated new position
	pkg.VH = s   // Apply() must assign itself into pkg.VH
	return
}

func newPubrelPayload() *pubrelPayload {
	return &pubrelPayload{}
}

type pubrelPayload struct {
}

func (s *pubrelPayload) Apply(ctx *StateContext, pkg *Pkg, data []byte, pos int) (newPos int, err error) {
	newPos = pos    // Apply() must return newPos to pointer the updated new position
	pkg.Payload = s // Apply() must be assign itself into pkg.Payload
	return
}
