/*
 * Copyright © 2020 Hedzr Yeh.
 */

package mqtt

import (
	"bytes"
	"encoding/gob"
	"github.com/sirupsen/logrus"
	"gitlab.com/hedzr/mqttlib/pkg/mqttlib/mqtt/codec"
	"sync"
)

func (s defaultSessionStore) MarshalBinary() (ret []byte, err error) {
	var b bytes.Buffer
	enc := gob.NewEncoder(&b)
	// _, err = fmt.Fprintln(&b, s.qos, s.packetIdentifier)

	filepath := "/"

	err = s.saveMap(enc, s.clients)
	if err == nil {
		err = enc.Encode(s.pubsubStore)
		if err == nil {
			err = enc.Encode(filepath)
			if err == nil {
				err = enc.Encode(s.gobSavingDuration)
				if err == nil {
					err = enc.Encode(s.gobPersist)
					if err == nil {
						err = enc.Encode(s.connected)
						if err == nil {
							err = enc.Encode(s.disconnected)
							if err == nil {
								err = enc.Encode(s.maxinum)
								if err == nil {
									err = enc.Encode(s.total)
									if err == nil {
										err = enc.Encode(s.noSysStats)
										if err == nil {
											err = enc.Encode(s.noSysStatsLog)
										}
									}
								}
							}
						}
					}
				}
			}
		}
	}

	if err != nil {
		logrus.Debugf("error: %v", err)
	}
	ret = b.Bytes()
	return
}

func (s *defaultSessionStore) UnmarshalBinary(data []byte) (err error) {
	b := bytes.NewBuffer(data)
	dec := gob.NewDecoder(b)
	// _, err = fmt.Fscanln(b, &s.qos, &s.packetIdentifier)

	err = s.loadMap(dec, s.clients, func(dec codec.Decoder) (i interface{}, e error) {
		var key string
		e = dec.Decode(&key)
		i = key
		return
	}, func(dec codec.Decoder) (i interface{}, e error) {
		var value *session
		e = dec.Decode(&value)
		i = value
		return
	})

	var filepath string
	if err == nil {
		err = dec.Decode(&s.pubsubStore)
		if err == nil {
			err = dec.Decode(&filepath)
			if err == nil {
				err = dec.Decode(&s.gobSavingDuration)
				if err == nil {
					err = dec.Decode(&s.gobPersist)
					if err == nil {
						err = dec.Decode(&s.connected)
						if err == nil {
							err = dec.Decode(&s.disconnected)
							if err == nil {
								err = dec.Decode(&s.maxinum)
								if err == nil {
									err = dec.Decode(&s.total)
									if err == nil {
										err = dec.Decode(&s.noSysStats)
										if err == nil {
											err = dec.Decode(&s.noSysStatsLog)
										}
									}
								}
							}
						}
					}
				}
			}
		}
	}

	if err != nil {
		logrus.Debugf("error: %v", err)
	}
	return
}

func (s *defaultSessionStore) saveMap(enc codec.Encoder, m sync.Map) (err error) {
	count := 0
	m.Range(func(key, value interface{}) bool {
		count++
		return true
	})
	err = enc.Encode(count)
	if err != nil {
		logrus.Debugf("saveMap error: %v", err)
		return
	}
	m.Range(func(key, value interface{}) bool {
		err = enc.Encode(key)
		if err != nil {
			logrus.Debugf("saveMap error: %v", err)
			return false
		}
		if sess, ok := value.(codec.Serializable); ok {
			err = sess.Save(enc)
			if err != nil {
				logrus.Debugf("saveMap error: %v", err)
				return false
			}
		} else {
			err = enc.Encode(value)
			if err != nil {
				logrus.Debugf("saveMap error: %v", err)
				return false
			}
		}
		return true
	})
	return
}

func (s *defaultSessionStore) loadMap(dec codec.Decoder, m sync.Map, decodeKey, decodeValue func(dec codec.Decoder) (interface{}, error)) (err error) {
	var count int
	err = dec.Decode(&count)
	if err != nil {
		logrus.Debugf("loadMap error: %v", err)
		return
	}

	for i := 0; i < count; i++ {
		var key, value interface{}
		key, err = decodeKey(dec)
		if err != nil {
			logrus.Debugf("loadMap error: %v", err)
			return
		}
		value, err = decodeValue(dec)
		if err != nil {
			logrus.Debugf("loadMap error: %v", err)
			return
		}
		m.Store(key, value)
	}
	return
}
