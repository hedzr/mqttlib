/*
 * Copyright © 2020 Hedzr Yeh.
 */

package pktidgen

import (
	"gitlab.com/hedzr/mqttlib/pkg/mqttlib/mqtt/math"
	"math/rand"
	"sync"
)

const (
	Uint16MAX  = math.MaxUint16
	uint16MAX1 = Uint16MAX - 1
	// intMAX  = int64(2) ^ 64 - 1
	IntMAX   = int(^uint(0) >> 1)
	Int64MAX = int64(2) ^ 64 - 1
)

func NewPacketIdentifierHolder() *PacketIdentifierHolder {
	return &PacketIdentifierHolder{
		// base:  mqtt.newBase("packet.identifier.holder"),
		using: make(map[uint16]bool),
	}
}

type PacketIdentifierHolder struct {
	// mqtt.base
	using map[uint16]bool
	rwm   sync.RWMutex
}

func (p *PacketIdentifierHolder) Return(id uint16) {
	p.rwm.RLock()
	_, ok := p.using[id]
	p.rwm.RUnlock()
	if ok {
		p.rwm.Lock()
		defer p.rwm.Unlock()
		p.using[id] = false
	}
}

func (p *PacketIdentifierHolder) Borrow() uint16 {
	var id uint16
keepGoing:
	id = uint16(rand.Int31n(int32(uint16MAX1)))
	id++
	p.rwm.RLock()
	b, ok := p.using[id]
	p.rwm.RUnlock()
	if ok {
		if !b {
			p.rwm.Lock()
			p.using[id] = true
			p.rwm.Unlock()
			return id
		}
		goto keepGoing
	}

	p.rwm.Lock()
	p.using[id] = true
	p.rwm.Unlock()
	return id
}
