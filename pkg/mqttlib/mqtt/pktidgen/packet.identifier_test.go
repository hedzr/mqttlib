/*
 * Copyright © 2020 Hedzr Yeh.
 */

package pktidgen

import "testing"

func TestNewPacketIdentifierHolder(t *testing.T) {
	pih := NewPacketIdentifierHolder()

	pkid := pih.Borrow()
	pih.Return(pkid)
}
