/*
 * Copyright © 2020 Hedzr Yeh.
 */

package mqtt

import (
	"bytes"
	"encoding/gob"
	"fmt"
	"github.com/sirupsen/logrus"
	"gitlab.com/hedzr/mqttlib/pkg/mqttlib/errors"
	"gitlab.com/hedzr/mqttlib/pkg/mqttlib/mqtt/codec"
	"math/rand"
	"strings"
	"sync"
	"sync/atomic"
	"time"
)

type SendPayload func(ctx *StateContext, clientId string, qos QoSType, path string, payload []byte, packetIdentifier uint16) (err error)
type SendPayloadVx func(ctx *StateContext, clientId string, builder PacketBuilder, qp *qp) (err error)

type pubsubStore struct {
	// subinfoTreeMap map[string]interface{} // k: parts of topic fullPath, v: *Subinfo or sub-map
	base
	rwm             sync.RWMutex
	Root            *pubsubNode // hierarchy subs info
	subscribes      sync.Map    // the subscribers: type is map[string]*Subinfo - k: topic filter fullPath
	subscribesCount int64       // count of ...
	messages        sync.Map    // the important message with qos 1 or 2: type is map[uint16]*PubSubTask - k: packet identifier
	willMessages    sync.Map    // type is map[clientId.string]*WillMessage
}

func newPubsubStore() *pubsubStore {
	return &pubsubStore{
		base: newBase("pubsubStore"),
		Root: newPubsubNode(),
		// subinfoTreeMap: make(map[string]interface{}),
		// subscribes: make(map[string]*Subinfo),
		// messages:   make(map[uint16]*PubSubTask),
	}
}

func (s *pubsubStore) StoreAsTask(pktId uint16, task *PubSubTask) {
	// s.rwm.Lock()
	// s.messages[pktId] = task
	// s.rwm.Unlock()
	s.messages.Store(pktId, task)
}

func (s *pubsubStore) GetTaskById(pktId uint16) (task *PubSubTask) {
	v, ok := s.messages.Load(pktId)
	if ok {
		task, ok = v.(*PubSubTask)
	}
	return
	// s.rwm.RLock()
	// v, ok := s.messages[pktId]
	// s.rwm.RUnlock()
	// if ok {
	// 	return v
	// }
	// return nil
}

func (s *pubsubStore) dump() string {
	var buf = bytes.NewBufferString("pubsubStore{\nTree: ")
	// buf.WriteString(fmt.Sprint("  ", s.subinfoTreeMap))
	s.dumptree(buf, s.Root, 0)
	buf.WriteString("Subscriptions: \n")
	s.subscribes.Range(func(key, value interface{}) bool {
		buf.WriteString(fmt.Sprintf("  - '%v': %v", key, *value.(*Subinfo)))
		buf.WriteString(",\n")
		return true
	})
	// for path, si := range s.subscribes {
	// 	buf.WriteString(fmt.Sprintf("  - '%v': %v", path, *si))
	// 	buf.WriteString(",\n")
	// }
	buf.WriteString("}\n")
	return buf.String()
}

func (s *pubsubStore) dumptree(buf *bytes.Buffer, parent *pubsubNode, lvl int) {
	pre := strings.Repeat("  ", lvl)
	var str string
	if parent.Subscriptions != nil {
		str = fmt.Sprintf("%s%+v\n", pre, *parent.Subscriptions)
		buf.WriteString(str)
	} else {
		buf.WriteString("\n")
	}
	for k, v := range parent.Children {
		str = fmt.Sprintf("%s  - %v", pre, k)
		buf.WriteString(str)
		if v != nil {
			s.dumptree(buf, v, lvl+1)
		}
	}
}

func (s *pubsubStore) saveMap(enc codec.Encoder, m sync.Map) (err error) {
	count := int64(0)
	m.Range(func(key, value interface{}) bool {
		count++
		return true
	})

	err = enc.Encode(count)
	if err != nil {
		logrus.Debugf("saveMap error: %v", err)
		return
	}

	m.Range(func(key, value interface{}) bool {
		err = enc.Encode(key)
		if err != nil {
			logrus.Debugf("saveMap error: %v", err)
			return false
		}
		if sess, ok := value.(codec.Serializable); ok {
			err = sess.Save(enc)
			if err != nil {
				logrus.Debugf("saveMap error: %v", err)
				return false
			}
		} else {
			err = enc.Encode(value)
			if err != nil {
				logrus.Debugf("saveMap error: %v", err)
				return false
			}
		}
		return true
	})
	return
}

func (s *pubsubStore) loadMap(dec codec.Decoder, m sync.Map, decodeKey, decodeValue func(dec codec.Decoder) (interface{}, error)) (count int64, err error) {
	err = dec.Decode(&count)
	if err != nil {
		logrus.Debugf("loadMap error: %v", err)
		return
	}

	for i := int64(0); i < count; i++ {
		var key, value interface{}
		key, err = decodeKey(dec)
		if err != nil {
			logrus.Debugf("loadMap error: %v", err)
			return
		}
		value, err = decodeValue(dec)
		if err != nil {
			logrus.Debugf("loadMap error: %v", err)
			return
		}
		m.Store(key, value)
	}
	return
}

func (s pubsubStore) MarshalBinary() (ret []byte, err error) {
	var b bytes.Buffer
	enc := gob.NewEncoder(&b)
	// _, err = fmt.Fprintln(&b, s.qos, s.packetIdentifier)

	err = enc.Encode(s.Root)
	if err == nil {
		err = s.saveMap(enc, s.subscribes)
		if err == nil {
			err = s.saveMap(enc, s.messages)
			if err == nil {
				err = s.saveMap(enc, s.willMessages)
			}
		}
	}

	if err != nil {
		logrus.Debugf("error: %v", err)
	}
	ret = b.Bytes()
	return
}

func (s *pubsubStore) UnmarshalBinary(data []byte) (err error) {
	b := bytes.NewBuffer(data)
	dec := gob.NewDecoder(b)
	// _, err = fmt.Fscanln(b, &s.qos, &s.packetIdentifier)

	var count int64
	err = dec.Decode(&s.Root)
	if err == nil {
		count, err = s.loadMap(dec, s.subscribes, func(dec codec.Decoder) (i interface{}, e error) {
			var key string
			e = dec.Decode(&key)
			i = key
			return
		}, func(dec codec.Decoder) (i interface{}, e error) {
			var value *Subinfo
			e = dec.Decode(&value)
			i = value
			return
		})

		if err == nil {
			s.subscribesCount = count

			if err == nil {
				count, err = s.loadMap(dec, s.messages, func(dec codec.Decoder) (i interface{}, e error) {
					var key uint16
					e = dec.Decode(&key)
					i = key
					return
				}, func(dec codec.Decoder) (i interface{}, e error) {
					var value *PubSubTask
					e = dec.Decode(&value)
					i = value
					return
				})
				if err == nil {
					count, err = s.loadMap(dec, s.willMessages, func(dec codec.Decoder) (i interface{}, e error) {
						var key string
						e = dec.Decode(&key)
						i = key
						return
					}, func(dec codec.Decoder) (i interface{}, e error) {
						var value *WillMessage
						e = dec.Decode(&value)
						i = value
						return
					})
				}
			}
		}
	}

	if err != nil {
		logrus.Debugf("error: %v", err)
	}
	return
}

// func (s *pubsubStore) Save(enc codec.Encoder) (err error) {
// 	err = s.saveMap(enc, s.subscribes)
// 	if err != nil {
// 		logrus.Debugf("error: %v", err)
// 		return
// 	}
// 	err = s.saveMap(enc, s.messages)
// 	if err != nil {
// 		logrus.Debugf("error: %v", err)
// 		return
// 	}
// 	err = s.saveMap(enc, s.willMessages)
// 	if err != nil {
// 		logrus.Debugf("error: %v", err)
// 		return
// 	}
//
// 	s.rwm.RLock()
// 	defer s.rwm.RUnlock()
// 	err = s.Root.Save(enc)
// 	if err != nil {
// 		logrus.Debugf("error: %v", err)
// 		return
// 	}
// 	return
// }
//
// func (s *pubsubStore) Load(dec codec.Decoder) (err error) {
// 	// need restored:
// 	// tag,
// 	err = s.loadMap(dec, s.subscribes, func() interface{} {
// 		return new(Pubinfo)
// 	})
// 	if err != nil {
// 		logrus.Debugf("error: %v", err)
// 		return
// 	}
// 	err = s.loadMap(dec, s.messages, func() interface{} {
// 		return new(PubSubTask)
// 	})
// 	if err != nil {
// 		logrus.Debugf("error: %v", err)
// 		return
// 	}
// 	err = s.loadMap(dec, s.willMessages, func() interface{} {
// 		return new(WillMessage)
// 	})
// 	if err != nil {
// 		logrus.Debugf("error: %v", err)
// 		return
// 	}
//
// 	s.rwm.Lock()
// 	defer s.rwm.Unlock()
// 	err = s.Root.Load(dec)
// 	if err != nil {
// 		logrus.Debugf("error: %v", err)
// 		return
// 	}
// 	return
// }

// #, +, $
func (s *pubsubStore) Subscribe(ctx *StateContext, clientId string, topicFilter TopicFilter, packetIdentifier uint16, sendPayload SendPayload) (invoked QoSType, delta int, err error) {
	// if strings.HasPrefix(topicFilter, "$") {
	// 	err = newErrorN(ErrNumErrors, "prefix '$' of a topicFilter is invalid")
	// 	return
	// }
	invoked, delta, err = s.subscribe(ctx, clientId, topicFilter, packetIdentifier, sendPayload)
	if err != nil {
		logrus.Errorf("Subscribe failed: %+v", err)
	}
	return
}

// delta: the subscriptions increments number
func (s *pubsubStore) subscribe(ctx *StateContext, clientId string, topicFilter TopicFilter, packetIdentifier uint16, sendPayload SendPayload) (invoked QoSType, delta int, err error) {
	vn := newSubinfo(clientId, topicFilter.RequestedQoS, packetIdentifier, ctx.ConnectParam.ProtocolLevel)
	// topicFilter.Filter 以 $shared/SharedName/topicFilter 开头时，表示这是共享订阅
	// 注意此时 topicFilter.shared 也为 true。
	if v, loaded := s.subscribes.LoadOrStore(topicFilter.Filter, vn); loaded {
		if si, ok := v.(*Subinfo); ok {
			// see also 3.8.4-3
			//   如果服务端收到一个 SUBSCRIBE 报文，报文的主题过滤器与一个现存订阅的
			//   主题过滤器相同，那么必须 使用新的订阅彻底替换现存的订阅。新订阅的主
			//   题过滤器和之前订阅的相同，但是它的最大 QoS 值可以不 同。与这个主题
			//   过滤器匹配的任何现存的保留消息必须被重发，但是发布流程不能中断。
			// 然而，上面的规范未有明确针对的是具体的一个Client还是所有Clients，因此
			// 这里选择保留一个订阅客户端的列表，仅对相同的客户端进行替换。
			// 我们的策略是：相同客户端在同一主题过滤器下仅保留最新订阅，陈旧的订阅自
			// 动地被覆盖掉；不同的客户端在同一过滤器下的订阅关系允许并存。
			si.add(clientId, ctx.ConnectParam.ProtocolLevel, topicFilter.RequestedQoS, packetIdentifier)
			atomic.AddInt64(&s.subscribesCount, 1)
			delta = 1
			invoked = topicFilter.RequestedQoS
		}
	} else {
		atomic.AddInt64(&s.subscribesCount, 1)
		delta = 1
		invoked = topicFilter.RequestedQoS
	}

	if delta == 1 {
		s.Debug("         %s ✎☩ %+v", clientId, topicFilter)
	}

	// check the retained messages

	if sendPayload == nil {
		sendPayload = s.defaultSendPayload
	}

	// 3.3.1-6
	err = s.subscribeSearchRetained(ctx, s.Root, clientId, topicFilter, packetIdentifier, sendPayload)

	var clients []struct {
		key string
		wm  *WillMessage
	}
	// search the retained will message too
	s.willMessages.Range(func(key, value interface{}) bool {
		if wm, ok := value.(*WillMessage); ok && wm.Retain {
			tfParts := strings.Split(topicFilter.Filter, "/")
			if matched, e := s.match(topicFilter.Filter, wm.Topic, tfParts); e != nil {
				s.Wrong(e, "loop on willMessages failed")
				return false
			} else if matched {
				if topicFilter.Shared {
					// 如果是共享订阅的过滤器，只要有一个订阅者发送成功，那就不再发送给其余的订阅者了
					clients = append(clients, struct {
						key string
						wm  *WillMessage
					}{key: key.(string), wm: wm})
				} else {
					if e := sendPayload(ctx, clientId, topicFilter.RequestedQoS, wm.Topic, wm.Body, packetIdentifier); e != nil {
						s.Wrong(e, "sendWillMessage for clientId %q failed, topic=%q", key.(string), wm.Topic)
						// return false // don't break s.willMessages.Range()
					} else {
						// todo record the sent clients for retained will messages

					}
				}
			}
		}
		return true
	})

	// 如果是共享订阅的过滤器，只要有一个订阅者发送成功，那就不再发送给其余的订阅者了
	if len(clients) > 0 {
		i := rand.Intn(len(clients))
		wm := clients[i].wm
		if e := sendPayload(ctx, clientId, topicFilter.RequestedQoS, wm.Topic, wm.Body, packetIdentifier); e != nil {
			s.Wrong(e, "sendWillMessage for clientId %q failed, topic=%q", clients[i].key, wm.Topic)
			// return false // don't break s.willMessages.Range()
		} else {
			// todo record the sent clients for retained will messages

		}
	}
	return
}

func (s *pubsubStore) subscribeSearchRetained(ctx *StateContext, parent *pubsubNode, clientId string, topicFilter TopicFilter, packetIdentifier uint16, sendPayload SendPayload) (err error) {
	tfParts := strings.Split(topicFilter.Filter, "/")

	s.rwm.RLock()
	defer s.rwm.RUnlock()
	err = s.subscribeSearchRetainedX(ctx, "", nil, tfParts, s.Root, clientId, topicFilter, packetIdentifier, sendPayload)
	return
}

func (s *pubsubStore) subscribeSearchRetainedX(ctx *StateContext, pathPart string, pathes, remains []string, parent *pubsubNode, clientId string, topicFilter TopicFilter, packetIdentifier uint16, sendPayload SendPayload) (err error) {
	if len(remains) == 0 {
		return
	}

	var newPathes []string
	var matched bool
	name, newNames := remains[0], remains[1:]
	if name == "#" {
		if len(newNames) > 0 {
			err = errors.ErrCodeBadTopicFilter.New("'#' can only be put at the last part of a topic filter string")
			return
		}
		matched = true
		newNames = remains
	}
	newPathes = append(pathes, pathPart)

	errHolder := errors.ErrCodeErrors.New("OK")

	for k, v := range parent.Children {
		if len(pathes) == 0 && matched && strings.HasPrefix(k, "$") {
			continue // subscriber subs '#', for-loop looping $SYS
		}
		if !matched {
			switch name {
			case "+":
				if len(newNames) > 0 {
					_ = errHolder.Attach(s.subscribeSearchRetainedX(ctx, k, newPathes, newNames, v, clientId, topicFilter, packetIdentifier, sendPayload))
				} else {
					// matched, check the retain messages
				}
			default:
				if k == name {
					if len(newNames) > 0 {
						newPathes = append(pathes, k)
						_ = errHolder.Attach(s.subscribeSearchRetainedX(ctx, k, newPathes, newNames, v, clientId, topicFilter, packetIdentifier, sendPayload))
					} else {
						matched = true
					}
				}
			}
		} else {
			newPathes = append(pathes, k)
			_ = errHolder.Attach(s.subscribeSearchRetainedX(ctx, k, newPathes, newNames, v, clientId, topicFilter, packetIdentifier, sendPayload))
		}

		if matched {
			// matched, check the retain messages
			if v.Subscriptions != nil {
				for _, rm := range v.Subscriptions.retainMessages {
					if rm.published == 0 {
						if _, sent := rm.sentClients[clientId]; !sent {
							rm.sentClients[clientId] = newQP(ctx.ConnectParam.ProtocolLevel, topicFilter.RequestedQoS, packetIdentifier)
							_ = errHolder.Attach(sendPayload(ctx, clientId, topicFilter.RequestedQoS, strings.Join(newPathes[:len(newPathes)-1], "/"), rm.data, packetIdentifier))
						}
					}
				}
			}
		}
	}

	if matched && parent.Subscriptions != nil {
		// 对于 retained messages，没有共享订阅的说法，在这里总是发送给所有订阅者
		subInfo := parent.Subscriptions
		for _, rm := range subInfo.retainMessages {
			if rm.published == 0 {
				if _, sent := rm.sentClients[clientId]; !sent {
					qp := newQP(ctx.ConnectParam.ProtocolLevel, topicFilter.RequestedQoS, packetIdentifier)
					rm.sentClients[clientId] = qp
					if e := sendPayload(ctx, clientId, topicFilter.RequestedQoS, strings.Join(newPathes[:len(newPathes)-1], "/"), rm.data, packetIdentifier); e != nil {
						if errors.IsMqttCaredNetError(e) {
							errors.Attach(err, e)
							s.Wrong(e, "sendPayload error for target client %q", clientId)
						}
					} else {
						subInfo.addAsSentClient(clientId, qp)
					}
				}
			}
		}
	}

	if !errors.HasAttachedErrors(errHolder) {
		err = errHolder.Msg("subscribe failed")
	}
	return
}

func (s *pubsubStore) RemoveSubscriber(clientId string) (delta int) {
	s.subscribes.Range(func(key, value interface{}) bool {
		if si, ok := value.(*Subinfo); ok {
			if si.remove(clientId) {
				atomic.AddInt64(&s.subscribesCount, -1)
				delta--
			}
		}
		return true
	})

	s.rwm.RLock()
	defer s.rwm.RUnlock()
	s.loopTreeNodes(s.Root, 0, func(node *pubsubNode, si *Subinfo) {
		si.remove(clientId)
	})
	return
}

func (s *pubsubStore) loopTreeNodes(parent *pubsubNode, level int, fn func(node *pubsubNode, si *Subinfo)) {
	if parent.Subscriptions != nil {
		fn(parent, parent.Subscriptions)
	}
	for _, node := range parent.Children {
		s.loopTreeNodes(node, level+1, fn)
	}
}

func (s *pubsubStore) Unsubscribe(ctx *StateContext, clientId, topicFilter string) (delta int, err error) {
	if v, ok := s.subscribes.Load(topicFilter); ok {
		if si, ok := v.(*Subinfo); ok {
			if si.remove(clientId) { // // 3.10.4-1
				atomic.AddInt64(&s.subscribesCount, -1)
				delta = -1
				// if si.count() == 0 {
				// 	s.subscribes.Delete(topicFilter)
				// }
			}
		}
		//    3.10.4-2
		//    3.10.4-3
	}
	// s.rwm.RLock()
	// if si, ok := s.subscribes[topicFilter]; ok {
	// 	s.rwm.RUnlock()
	// 	si.remove(clientId) // // 3.10.4-1
	// 	// to be checked:
	// 	//    3.10.4-2
	// 	//    3.10.4-3
	// } else {
	// 	s.rwm.RUnlock()
	// 	// 3.10.4-1
	// }
	return
}

func (s *pubsubStore) defaultSendPayload(ctx *StateContext, clientId string, qos QoSType, path string, payload []byte, packetIdentifier uint16) (err error) {
	logrus.Debugf("▲ %v %v -> %v (%v)", path, payload, clientId, qos)
	return
}

func (s *pubsubStore) defaultSendPayloadVx(ctx *StateContext, clientId string, builder PacketBuilder, qp *qp) (err error) {
	// logrus.Debugf("▲ %v %v -> %v (%v)", path, payload, clientId, qos)
	return
}

func (s *pubsubStore) DispatchPublishMessage(ctx *StateContext, retain bool, builder PacketBuilder, sendPayloadToClient SendPayloadVx) (err error) {
	if bdr, ok := builder.(*publishBuilder); ok {
		if strings.HasPrefix(bdr.topicName, "$") {
			err = errors.ErrCodeErrors.New("prefix '$' of a topicName ('%v') is invalid in publishing", bdr.topicName)
			return
		}

		err = s.DispatchNoLimit(ctx, retain, bdr, sendPayloadToClient)
	}
	return
}

// DispatchNoLimit filter the incoming public packet with the topic filters of subscribers
func (s *pubsubStore) DispatchNoLimit(ctx *StateContext, retain bool, packetBuilder PacketBuilder, sendPayloadToClient SendPayloadVx) (err error) {
	var builder *publishBuilder
	var ok, sysPub, sharedPub bool

	if builder, ok = packetBuilder.(*publishBuilder); !ok {
		return
	}
	if sendPayloadToClient == nil {
		sendPayloadToClient = s.defaultSendPayloadVx
	}

	if err = builder.Build(ctx, nil); err != nil {
		s.Wrong(err, "->")
		return
	}

	err = errors.New("sendPayload error")
	sysPub = strings.HasPrefix(builder.topicName, "$")
	sharedPub = strings.HasPrefix(builder.topicName, "$shared/")
	// if !sysPub {
	// 	logrus.Debugf("")
	// }
	matchedSubInfos := s.searcher(builder, err.(*errors.MqttError))
	if !sysPub {
		// for debugging view
		logrus.Tracef("       DispatchNoLimit.searcher: src-ctx = %s, topic = %s, targets: %v", ctx.ConnectParam.ClientId, builder.topicName, matchedSubInfos)
	}

	type Target struct {
		cid     string
		qp      *qp
		subInfo *Subinfo
	}
	var targets []Target

	// send to the living subscribers
	// todo 3.3.5-1, 3.3.5-2
	for _, subInfo := range matchedSubInfos {
		subInfo.forEach(func(key, value interface{}) bool {
			cid, ok1 := key.(string)
			qp, ok2 := value.(*qp)
			if ok1 && ok2 {
				if sysPub {
					if sharedPub {
						targets = append(targets, Target{cid: cid, qp: qp, subInfo: subInfo})
						return true
					}
					logrus.Tracef("          - : src-ctx = %s, sys-publish to %s (%+v)", ctx.ConnectParam.ClientId, cid, qp)
				} else {
					logrus.Tracef("          - : src-ctx = %s, publish to %s (%+v)", ctx.ConnectParam.ClientId, cid, qp)
				}
				// 3.3.2-3 note that 'matched' is true here, when we sending-payload (this means sending the publish packet)
				// 3.3.4-1 note that qp.qos == the qos of the client requesting publish packet
				if e := sendPayloadToClient(ctx, cid, builder, qp); e != nil {
					if errors.IsMqttCaredNetError(e) {
						errors.Attach(err, e)
						s.Wrong(e, "sendPayload error for target client %q", cid)
						// _ = err.(*errors.MqttError).Attach(e)
					}
				} else if retain {
					// save the alived client for retained message publishing
					subInfo.addAsSentClient(cid, qp)
				}
				return true
			}
			return false
		})
		// for cid, qp := range v.clientIds {
		// 	// 3.3.2-3 note that 'matched' is true here, when we sending-payload (this means sending the publish packet)
		// 	// 3.3.4-1 note that qp.qos == the qos of the client requesting publish packet
		// 	if e := sendPayloadToClient(ctx, cid, builder, qp.packetIdentifier, qp.qos); e != nil {
		// 		if errors.IsMqttCaredNetError(e) {
		// 			_ = errors.Attach(err, e)
		// 			logrus.Errorf("sendPayload error: %v", e)
		// 		}
		// 	}
		// }
	}

	if sharedPub && len(targets) > 0 {
		i := rand.Intn(len(targets))
		if e := sendPayloadToClient(ctx, targets[i].cid, builder, targets[i].qp); e != nil {
			if errors.IsMqttCaredNetError(e) {
				errors.Attach(err, e)
				s.Wrong(e, "sendPayload error for target client %q", targets[i].cid)
				// _ = err.(*errors.MqttError).Attach(e)
			}
		} else if retain {
			// save the alived client for retained message publishing
			targets[i].subInfo.addAsSentClient(targets[i].cid, targets[i].qp)
		}
	}

	// retain it if necessary
	if retain || ctx.saveAllMessages {
		names := strings.Split(builder.topicName, "/")
		// if e := s.publish(nil, names, s.Root, ctx, builder.DUP, builder.QoS, builder.RETAIN, builder.PacketIdentifier, builder.topicName, builder.msg); e != nil {
		if e := s.publishVx(nil, names, s.Root, ctx, builder, retain); e != nil {
			errors.Attach(err, e)
			logrus.Errorf("publish error: %v", e)
		}
		// err = s.publish(nil, names, s.subinfoTreeMap, ctx, DUP, qos, RETAIN, packetIdentifier, topicName, data)
	}

	if !errors.HasAttachedErrors(err) {
		err = nil
	}
	return
}

func (s *pubsubStore) searcher(builder *publishBuilder, errorHolder *errors.MqttError) (matchedSubInfos []*Subinfo) {
	s.subscribes.Range(func(key, value interface{}) bool {
		if topicFilter, ok := key.(string); ok {
			if v, ok := value.(*Subinfo); ok {
				if matched, e := s.match(topicFilter, builder.topicName, nil); e == nil && matched {
					matchedSubInfos, ok = uniqueAppend(matchedSubInfos, v)
					// logrus.Debugf("MATCHED: client '%v' publishing at topic '%v', and clients %v subscribed on '%v'", ctx.ClientId, topicName, v.clientIds, topicFilter)
				} else if e != nil {
					_ = errorHolder.Attach(e)
					logrus.Errorf("publish error: %v", e)
				}
			}
		}
		return true
	})

	// s.rwm.RLock()
	// defer s.rwm.RUnlock()
	//
	// // filter by the live subscribers
	// for topicFilter, v := range s.subscribes {
	// 	if matched, e := s.match(topicFilter, builder.topicName, nil); e == nil && matched {
	// 		matchedSubInfos = append(matchedSubInfos, v)
	// 		// logrus.Debugf("MATCHED: client '%v' publishing at topic '%v', and clients %v subscribed on '%v'", ctx.ClientId, topicName, v.clientIds, topicFilter)
	// 	} else if e != nil {
	// 		errorHolder.Add(e)
	// 		logrus.Errorf("publish error: %v", e)
	// 	}
	// }
	return
}

func uniqueAppend(matchedSubInfos []*Subinfo, v *Subinfo) (a []*Subinfo, found bool) {
	for _, x := range matchedSubInfos {
		if x == v {
			found = true
			a = matchedSubInfos
			return
		}
	}
	a = append(matchedSubInfos, v)
	return
}

func (s *pubsubStore) publishVx(pathes, remains []string, parent *pubsubNode, ctx *StateContext, bdr *publishBuilder, retain bool) (err error) {
	if len(remains) == 0 {
		err = errors.ErrCodeErrors.New("can't publish, topicName '%v' cannot be found or pathed", strings.Join(remains, "/"))
		return
	}

	name, newNames := remains[0], remains[1:]
	pathes = append(pathes, name)

	if _, ok := parent.Children[name]; !ok {
		parent.Children[name] = newPubsubNode()
	}

	v, _ := parent.Children[name]
	if len(newNames) > 0 {
		err = s.publishVx(pathes, newNames, v, ctx, bdr, retain)
	} else {
		v.Subscriptions = newSubinfo(ctx.ConnectParam.ClientId, bdr.QoS, bdr.PacketIdentifer, ctx.ConnectParam.ProtocolLevel)
		err = s.publishVxSi(pathes, v.Subscriptions, ctx, bdr, retain)
	}

	return
}

func (s *pubsubStore) publishVxSi(matchedParts []string, si *Subinfo, ctx *StateContext, bdr *publishBuilder, retain bool) (err error) {
	var published int32
	if !bdr.RETAIN {
		published = 1
	}

	if retain {
		si.retainMessages = nil // 3.3.1.3 (3.3.1-3), 3.3.1-7， 3.3.1-10, 3.3.1-11
		if len(bdr.msg) > 0 {
			si.retainMessages = append(si.retainMessages, Pubinfo{
				data:          bdr.msg,
				publishingQoS: bdr.QoS,
				sentClients:   si.duplicateClients(),
				publisher:     ctx.ConnectParam.ClientId,
				published:     0,
				timestamp:     time.Now().UTC(),
			})
		}
	} // else 3.3.1-12

	if ctx.saveAllMessages {
		si.allMessages = append(si.allMessages, Pubinfo{
			data: bdr.msg, publishingQoS: bdr.QoS,
			sentClients: nil,
			publisher:   ctx.ConnectParam.ClientId,
			published:   published,
			timestamp:   time.Now().UTC(),
		})
	}

	return
}

// func (s *pubsubStore) PublishNeverUsed(ctx *StateContext, DUP bool, qos QoSType, RETAIN bool, topicName string, packetIdentifier uint16, data []byte, sendPayload SendPayload) (err error) {
//
// 	if strings.HasPrefix(topicName, "$") {
// 		err = errors.ErrNumErrors.New("prefix '$' of a topicName is invalid in publishing")
// 		return
// 	}
//
// 	err = s.PublishNeverUsedNoLimit(ctx, DUP, qos, RETAIN, topicName, packetIdentifier, data, sendPayload)
// 	return
//
// }
//
// func (s *pubsubStore) PublishNeverUsedNoLimit(ctx *StateContext, DUP bool, qos QoSType, RETAIN bool, topicName string, packetIdentifier uint16, data []byte, sendPayload SendPayload) (err error) {
// 	if sendPayload == nil {
// 		sendPayload = s.defaultSendPayload
// 	}
//
// 	err = errors.ErrNumErrors.NewE(nil, "sendPayload error")
// 	s.subscribes.Range(func(key, value interface{}) bool {
// 		if topicFilter, ok := key.(string); ok {
// 			if v, ok := value.(*Subinfo); ok {
// 				if matched, e := s.match(topicFilter, topicName, nil); e == nil && matched {
// 					// logrus.Debugf("MATCHED: client '%v' publishing at topic '%v', and clients %v subscribed on '%v'", ctx.ClientId, topicName, v.clientIds, topicFilter)
// 					for cid, qp := range v.clientIds {
// 						// 3.3.2-3 noted matched is true here, when we sending-payload (this means sending the publish packet)
// 						if e = sendPayload(ctx, cid, qp.qos, topicName, data, qp.packetIdentifier); e != nil {
// 							if errors.IsMqttCaredNetError(e) {
// 								_ = errors.Attach(err, e)
// 								logrus.Errorf("         ✗ sendPayload error (%v): %v", matched, e)
// 							} else {
// 								s.Debug("         ? ignored sendPayload matter for '%v' (qp=%v): %v", cid, qp, e)
// 							}
// 						} else {
// 							s.Debug("         ✓ publish sent to '%v' (qp=%v)", cid, qp)
// 						}
// 					}
// 				} else if e != nil {
// 					err.(*errors.MqttError).Add(e)
// 					logrus.Errorf("publish error: %v", e)
// 				}
// 			}
// 		}
// 		return true
// 	})
//
// 	// s.rwm.RLock()
// 	// defer s.rwm.RUnlock()
// 	//
// 	// err = errors.ErrNumErrors.NewE(nil, "sendPayload error")
// 	// // var matched bool
// 	// for topicFilter, v := range s.subscribes {
// 	// 	if matched, e := s.match(topicFilter, topicName, nil); e == nil && matched {
// 	// 		// logrus.Debugf("MATCHED: client '%v' publishing at topic '%v', and clients %v subscribed on '%v'", ctx.ClientId, topicName, v.clientIds, topicFilter)
// 	// 		for cid, qp := range v.clientIds {
// 	// 			// 3.3.2-3 noted matched is true here, when we sending-payload (this means sending the publish packet)
// 	// 			if e = sendPayload(ctx, cid, qp.qos, topicName, data, qp.packetIdentifier); e != nil {
// 	// 				if errors.IsMqttCaredNetError(e) {
// 	// 					_ = errors.Attach(err, e)
// 	// 					logrus.Errorf("         ✗ sendPayload error (%v): %v", matched, e)
// 	// 				} else {
// 	// 					s.Debug("         ? ignored sendPayload matter for '%v' (qp=%v): %v", cid, qp, e)
// 	// 				}
// 	// 			} else {
// 	// 				s.Debug("         ✓ publish sent to '%v' (qp=%v)", cid, qp)
// 	// 			}
// 	// 		}
// 	// 	} else if e != nil {
// 	// 		err.(*errors.MqttError).Add(e)
// 	// 		logrus.Errorf("publish error: %v", e)
// 	// 	}
// 	// }
//
// 	if RETAIN || ctx.saveAllMessages {
// 		names := strings.Split(topicName, "/")
// 		if e := s.publish(nil, names, s.Root, ctx, DUP, qos, RETAIN, packetIdentifier, topicName, data); e != nil {
// 			_ = errors.Attach(err, e)
// 			logrus.Errorf("publish error: %v", e)
// 		}
// 		// err = s.publish(nil, names, s.subinfoTreeMap, ctx, DUP, qos, RETAIN, packetIdentifier, topicName, data)
// 	}
//
// 	if err.(*errors.MqttError).NotCannedError() {
// 		err = nil
// 	}
// 	return
// }
//
// func (s *pubsubStore) publish(pathes, remains []string, parent *pubsubNode, ctx *StateContext, DUP bool, qos QoSType, RETAIN bool, packetIdentifier uint16, topicName string, data []byte) (err error) {
// 	if len(remains) == 0 {
// 		err = errors.ErrNumErrors.New("can't publish, topicName '%v' cannot be found or pathed", strings.Join(remains, "/"))
// 		return
// 	}
//
// 	name, newNames := remains[0], remains[1:]
// 	pathes = append(pathes, name)
//
// 	if _, ok := parent.Children[name]; !ok {
// 		parent.Children[name] = newPubsubNode()
// 	}
//
// 	v, _ := parent.Children[name]
// 	if len(newNames) > 0 {
// 		err = s.publish(pathes, newNames, v, ctx, DUP, qos, RETAIN, packetIdentifier, topicName, data)
// 	} else {
// 		v.Subscriptions = newSubinfo(ctx.ConnectParam.ClientId, qos, packetIdentifier)
// 		err = s.publishSi(pathes, v.Subscriptions, ctx, DUP, qos, RETAIN, packetIdentifier, topicName, data)
// 	}
//
// 	return
// }
//
// func (s *pubsubStore) publishSi(matchedParts []string, si *Subinfo, ctx *StateContext, DUP bool, qos QoSType, RETAIN bool, packetIdentifier uint16, topicName string, data []byte) (err error) {
// 	var published int32
// 	if !RETAIN {
// 		published = 1
// 	}
//
// 	if RETAIN {
// 		si.retainMessages = nil // 3.3.1.3
// 		if len(data) > 0 {
// 			si.retainMessages = append(si.retainMessages, &Pubinfo{
// 				data: data, publishingQoS: qos, sentClients: si.duplicateClients(),
// 				publisher: ctx.ConnectParam.ClientId,
// 				published: 0, timestamp: time.Now().UTC(),
// 			})
// 		}
// 	}
//
// 	if ctx.saveAllMessages {
// 		si.allMessages = append(si.allMessages, &Pubinfo{
// 			data: data, publishingQoS: qos, sentClients: nil,
// 			publisher: ctx.ConnectParam.ClientId,
// 			published: published, timestamp: time.Now().UTC(),
// 		})
// 	}
//
// 	//
// 	return
// }
//
// func (s *pubsubStore) publish(pathes, remains []string, tree map[string]interface{}, ctx *StateContext, DUP bool, qos QoSType, RETAIN bool, packetIdentifier uint16, topicName string, data []byte) (err error) {
// 	if len(remains) == 0 {
// 		err = newErrorN(ErrNumErrors, "can't publish, topicName '%v' cannot be found or pathed", strings.Join(remains, "/"))
// 		return
// 	}
//
// 	name, newNames := remains[0], remains[1:]
// 	pathes = append(pathes, name)
//
// 	if m, ok := tree[name]; ok {
// 		if len(newNames) == 0 {
// 			if mm, ok := m.(map[string]interface{}); ok {
// 				err = s.publish(pathes, newNames, mm, ctx, DUP, qos, RETAIN, packetIdentifier, topicName, data)
// 				if err != nil {
// 					return
// 				}
// 			} else if si, ok := m.(*Subinfo); ok {
// 				err = s.publishSi(pathes, si, ctx, DUP, qos, RETAIN, packetIdentifier, topicName, data)
// 			} else {
// 				err = newErrorN(ErrNumErrors, "unknown nested type m: %+v", m)
// 			}
// 		} else {
// 			if mm, ok := m.(map[string]interface{}); ok {
// 				err = s.publish(pathes, newNames, mm, ctx, DUP, qos, RETAIN, packetIdentifier, topicName, data)
// 				if err != nil {
// 					return
// 				}
// 			} else {
// 				err = newErrorN(ErrNumErrors, "unknown nested type m: %+v", m)
// 			}
// 		}
// 	} else {
// 		if len(newNames) == 0 {
// 			tree[name] = newSubinfo(ctx.ClientId, qos)
// 			if m, ok := tree[name]; ok {
// 				if si, ok := m.(*Subinfo); ok {
// 					err = s.publishSi(pathes, si, ctx, DUP, qos, RETAIN, packetIdentifier, topicName, data)
// 				}
// 			}
// 		} else {
// 			tree[name] = make(map[string]interface{})
// 			if m, ok := tree[name]; ok {
// 				if mm, ok := m.(map[string]interface{}); ok {
// 					err = s.publish(pathes, newNames, mm, ctx, DUP, qos, RETAIN, packetIdentifier, topicName, data)
// 					if err != nil {
// 						return
// 					}
// 				}
// 			}
// 		}
// 		//	err = newErrorN(ErrNumErrors, "can't match '...%v'", strings.Join(remains, "/"))
// 	}
// 	return
// }
//
// func (s *pubsubStore) publishSi(matchedParts []string, si *Subinfo, ctx *StateContext, DUP bool, qos QoSType, RETAIN bool, packetIdentifier uint16, topicName string, data []byte) (err error) {
// 	var published int32
// 	if !RETAIN {
// 		published = 1
// 	}
//
// 	if RETAIN {
// 		si.retainMessages = nil // 3.3.1.3
// 		si.retainMessages = append(si.retainMessages, &Pubinfo{
// 			data: data, publishingQoS: qos, sentClients: si.duplicateClients(),
// 			publisher: ctx.ClientId,
// 			published: 0, timestamp: time.Now().UTC(),
// 		})
// 	}
//
// 	si.allMessages = append(si.allMessages, &Pubinfo{
// 		data: data, publishingQoS: qos, sentClients: nil,
// 		publisher: ctx.ClientId,
// 		published: published, timestamp: time.Now().UTC(),
// 	})
//
// 	//
// 	return
// }

func (s *pubsubStore) match(topicFilter, topicName string, matchedParts []string) (matched bool, err error) {
	if len(topicFilter) == 0 || len(topicName) == 0 {
		return
	}

	topicFilterParts := strings.Split(topicFilter, "/")
	topicNameParts := strings.Split(topicName, "/")
	for _, s := range topicFilterParts {
		if strings.ContainsAny(s, "#+") && s != "#" && s != "+" {
			err = errors.ErrCodeBadTopicFilter.New("'#', '+' must be used as the full part of a topic filter string, such as 'sport/#'; wrong examples: 'sport#', 'sport/+', ...")
			return
		}
	}
	for _, s := range topicNameParts {
		if strings.ContainsAny(s, "#+") {
			err = errors.ErrCodeBadTopicName.New("'#', '+' cannot be allowed in a topic name")
			return
		}
	}

	matched, err = s.matchRC(topicFilterParts, topicNameParts, topicFilter, topicName, matchedParts)
	return
}

func (s *pubsubStore) matchRC(tfParts, tnParts []string, topicFilter, topicName string, matchedParts []string) (matched bool, err error) {
	la, lb := len(tfParts), len(tnParts)
	if la == 2 && tfParts[1] == "#" {
		if lb >= 1 && tfParts[0] == tnParts[0] {
			if lb == 1 {
				matched = true
				return
			}
			matched, err = s.matchRC(tfParts, append(tnParts[0:1], tnParts[2:]...), topicFilter, topicName, matchedParts)
			return
		}
	}

	a, b := la == 0, lb == 0
	if a && b {
		return true, nil
	} else if a || b {
		return false, nil
	}

	if tfParts[0] == tnParts[0] {
		matched, err = s.matchRC(tfParts[1:], tnParts[1:], topicFilter, topicName, matchedParts)
	} else {
		switch tfParts[0] {
		case "#":
			if la > 1 {
				err = errors.ErrCodeBadTopicFilter.New("'#' must be the last part of a topic filter string, such as 'sport/#'; wrong example: 'sport/#/test', 'sport/test#', ...")
				return
			}
			if la == 1 && lb == 1 {
				matched = true
			} else if !strings.HasPrefix(tnParts[0], "$") {
				matched, err = s.matchRC(tfParts, tnParts[1:], topicFilter, topicName, matchedParts)
			}
		case "+":
			if !strings.HasPrefix(tnParts[0], "$") {
				matched, err = s.matchRC(tfParts[1:], tnParts[1:], topicFilter, topicName, matchedParts)
			}
		default:
			return // not matched
		}
	}
	return
}

func (s *pubsubStore) AddWillMessage(clientId string, willQoS QoSType, willRetainFlag bool, willTopic string, willMessage []byte) {
	topic := willTopic
	if !strings.HasPrefix(topic, "$") && strings.Contains(topic, "$") {
		topic = strings.ReplaceAll(topic, "$cid", clientId)
	}
	s.willMessages.LoadOrStore(clientId, &WillMessage{Message: Message{QoS: willQoS, Retain: willRetainFlag, Topic: topic, Body: willMessage}})
}

func (s *pubsubStore) HasWillMessage(clientId string) (exists bool) {
	_, exists = s.willMessages.Load(clientId)
	return
}

func (s *pubsubStore) SendWillMessage(ctx *StateContext, clientId string, sendPayloadVx SendPayloadVx) (err error) {
	if v, ok := s.willMessages.Load(clientId); ok {
		if wm, ok := v.(*WillMessage); ok {
			builder := NewPublishBuilder(wm.Topic, wm.Body, WithPubQoS(false, wm.QoS, wm.Retain))
			// NOTE that never retain a will msg into retainMessages store
			err = s.DispatchNoLimit(ctx, false, builder, sendPayloadVx)
			if err != nil {
				err = errors.ErrCodeErrors.NewE(err, "can't publish the will messsage")
				// need retry the sending action
			} else {
				if !wm.Retain {
					s.willMessages.Delete(clientId)
				}
			}
		}
	}
	return
}
