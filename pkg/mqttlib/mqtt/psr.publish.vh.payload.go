/*
 * Copyright © 2020 Hedzr Yeh.
 */

package mqtt

import (
	"gitlab.com/hedzr/mqttlib/pkg/mqttlib/errors"
	"gitlab.com/hedzr/mqttlib/pkg/mqttlib/mqtt/codec"
	"strings"
)

type PublishVH struct {
	vh50ext
	// DUP, QoS, RETAIN
	Topic            string
	PacketIdentifier uint16 // unless QoS in 1 or 2
}

func (s *PublishVH) Apply(ctx *StateContext, pkg *Pkg, data []byte, pos int) (newPos int, err error) {
	// 3.3.2-1
	s.Topic, pos, err = codec.ReadString(data, pos)
	if err != nil {
		err = errors.ErrCodePacketCorrupt.NewE(err, "[PublishVH] can't read Topic field")
		return
	}
	if strings.ContainsAny(s.Topic, "#+") { // 3.3.2-2
		err = errors.ErrCodeBadTopicName.New("[PublishVH] Topic can't contains # or +")
		return
	}

	if pkg.QoS > 0 {
		s.PacketIdentifier, pos, err = codec.ReadBEUint16(data, pos)
		if err != nil {
			err = errors.ErrCodePacketCorrupt.NewE(err, "[PublishVH] can't read PacketIdentifier field")
			return
		}
	}

	if ctx.ConnectParam.ProtocolLevel >= ProtocolLevelForV50 {
		newPos, err = s.vh50ext.Apply(ctx, pkg, data, pos)
	} else {
		newPos = pos
	}
	// newPos = pos // Apply() must return newPos to pointer the updated new position
	pkg.VH = s // Apply() must assign itself into pkg.VH
	return
}

type PublishPayload struct {
	Data []byte // by app
}

func (s *PublishPayload) Apply(ctx *StateContext, pkg *Pkg, data []byte, pos int) (newPos int, err error) {
	s.Data = data[pos:] // the whole of remains bytes would be treat as the valid payload block
	newPos = len(data)  // Apply() must return newPos to pointer the updated new position
	pkg.Payload = s     // Apply() must be assign itself into pkg.Payload
	return
}
