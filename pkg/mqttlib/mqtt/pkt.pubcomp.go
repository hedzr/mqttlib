/*
 * Copyright © 2020 Hedzr Yeh.
 */

package mqtt

import (
	"bytes"
	"gitlab.com/hedzr/mqttlib/pkg/mqttlib/mqtt/codec"
	"io"
)

func NewPub23compBuilderFromPkg(packetIdentifier uint16) PacketBuilder {
	return &pubcompBuilder{
		vh50ext:    *newvh50ext(),
		identifier: packetIdentifier,
	}
}

type pubcompBuilder struct {
	vh50ext
	identifier uint16
	data       []byte
}

func (b *pubcompBuilder) IsResending() bool {
	return false
}

func (b *pubcompBuilder) RequestResend() {
}

func (b *pubcompBuilder) NextPktId(ctx *StateContext) uint16 {
	return 0
}

func (b *pubcompBuilder) ReportType() ReportType {
	return PUBCOMP
}

// Build to build the packet
//
func (b *pubcompBuilder) Build(ctx *StateContext, pkg *Pkg) (err error) {
	if ctx.ConnectParam.ProtocolLevel >= ProtocolLevelForV50 {
		err = b.Buildv5(ctx, pkg, 0x70)
		return
	}

	bb := bytes.NewBuffer([]byte{0x70, 0x02})

	err = codec.WriteBEUint16(bb, b.identifier)
	if err != nil {
		return
	}

	b.data = bb.Bytes()
	return
}

func (b *pubcompBuilder) Buildv5(ctx *StateContext, pkg *Pkg, firstByte byte) (err error) {
	var vhBuf bytes.Buffer
	vhBuf, err = b.buildv5(ctx, pkg)
	if err != nil {
		return
	}

	var bb bytes.Buffer
	bb.WriteByte(firstByte)
	_, err = codec.EncodeIntToW(&bb, vhBuf.Len())
	if err != nil {
		return
	}

	_, err = bb.Write(vhBuf.Bytes())
	if err != nil {
		return
	}

	b.data = bb.Bytes()
	return
}

func (b *pubcompBuilder) buildv5(ctx *StateContext, pkg *Pkg) (buf bytes.Buffer, err error) {
	// for MQTT v5.0

	// construct the properties buffer at first

	var propertiesBuf bytes.Buffer
	propertiesBuf, err = b.WriteAs()
	if err != nil {
		return
	}

	var length int
	var buf8 []byte = make([]byte, 8)
	length = codec.EncodeIntToBuf(propertiesBuf.Len(), buf8, 0)

	// vh
	buf.Write(buf8[:length])         // properties length
	buf.Write(propertiesBuf.Bytes()) // properties buffer
	return
}

func (b *pubcompBuilder) bizPublish(ctx *StateContext, pkg *Pkg, bb *bytes.Buffer) (err error) {
	err = ctx.bizPublish(pkg)
	return
}

func (b *pubcompBuilder) WriteTo(w io.Writer) (n int, err error) {
	n, err = w.Write(b.Bytes())
	return
}

func (b *pubcompBuilder) Bytes() (bytes []byte) {
	return b.data
}

func (b *pubcompBuilder) AsString() (str string) {
	return
}
