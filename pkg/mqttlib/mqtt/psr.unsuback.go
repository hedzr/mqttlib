/*
 * Copyright © 2020 Hedzr Yeh.
 */

package mqtt

// ....................................................

func NewUnsubackParser(to State) PacketParser {
	return &unsubackParser{base: newBase("unsubackParser"), to: to}
}

type unsubackParser struct {
	base
	to State
}

func (p *unsubackParser) GetToState() State {
	return p.to
}

func (p *unsubackParser) CreateVH() VariantHeader {
	return newUnsubackVH()
}

func (p *unsubackParser) CreatePayload() Payload {
	return newUnsubackPayload()
}

func (p *unsubackParser) OnAction(ctx *StateContext, pkg *Pkg) (err error) {
	if vh, ok := pkg.VH.(*unsubackVH); ok {
		if payload, ok := pkg.Payload.(*unsubackPayload); ok {
			p.Debug("       %s: id=%v, vh=%v, payload=%v", pkg.ReportType, pkg.PacketIdentifier, vh, payload)

			// unsubscribed
		}
	}
	return
}

func (p *unsubackParser) OnError(ctx *StateContext, pkg *Pkg, reason error) (err error) {
	err = reason
	return
}

func newUnsubackVH() *unsubackVH {
	return &unsubackVH{}
}

type unsubackVH struct{}

func (s *unsubackVH) Apply(ctx *StateContext, pkg *Pkg, data []byte, pos int) (newPos int, err error) {
	newPos = pos // Apply() must return newPos to pointer the updated new position
	pkg.VH = s   // Apply() must assign itself into pkg.VH
	return
}

type unsubackPayload struct {
}

func newUnsubackPayload() *unsubackPayload {
	return &unsubackPayload{}
}

func (s *unsubackPayload) Apply(ctx *StateContext, pkg *Pkg, data []byte, pos int) (newPos int, err error) {
	newPos = pos    // Apply() must return newPos to pointer the updated new position
	pkg.Payload = s // Apply() must be assign itself into pkg.Payload
	return
}
