/*
 * Copyright © 2020 Hedzr Yeh.
 */

package mqtt

import (
	"github.com/sirupsen/logrus"
	"time"
)

// PathRW presents a PathReadWriter
type PathRW interface {
	Start(opts ...PathRWOpt)
	Stop()

	Load(dst *defaultSessionStore) (err error)
	Save(src *defaultSessionStore, tick time.Time) (err error)

	Get(pathKey string, defaultValue ...interface{}) (ret interface{}, err error)
	Set(pathKey string, value interface{}) (err error)
}

type PathRWOpt func(rw PathRW)

// func initWithGobType() PathRW {
// 	kp := fmt.Sprintf("mqtt.server.storage.gob.%v.data-dir", vxconf.RunModeExt())
// 	return NewPathRWGob(WithPathRWGobDataDir(cmdr.GetStringR(kp, "/var/lib/$APPNAME/gob-data")))
// }

func loadDefaultSessionStore(store *defaultSessionStore) {
	if store.pathRW == nil {
		// store.pathRW = initWithGobType()
		return
	}

	store.pathRW.Start()

	if store.resetStorage {
		return
	}

	if err := store.pathRW.Load(store); err != nil {
		logrus.Errorf("can't load session store: %v", err)
	}
}

func saveDefaultSessionStore(store *defaultSessionStore, tick time.Time) {
	if store.pathRW == nil {
		// store.pathRW = initWithGobType()
		return
	}

	if err := store.pathRW.Save(store, time.Now().UTC()); err != nil {
		logrus.Errorf("can't load session store: %v", err)
	}

	// store.pathRW.Stop()
}
