/*
 * Copyright © 2020 Hedzr Yeh.
 */

package mqtt

import (
	"gitlab.com/hedzr/mqttlib/pkg/mqttlib/errors"
	"io"
	"time"
)

// const InfiniteDuration = time.Duration(math.MaxUint16) * time.Second
// InfiniteDuration is equal maxDuration
const InfiniteDuration time.Duration = 1<<63 - 1

type StateRegistry interface {
	AddSynonyms(from State, synonymy ...State)
	AddState(from State, parsers *PacketParserMap)
	// in testing mode, noOnAction should be true to ignore invoking PacketParser.OnAction
	Advance(out io.Writer, ctx *StateContext, pkg *Pkg, noOnAction bool) (to State, err error)
	Parsers() map[ReportType]PacketParser
	// in testing mode, noOnAction should be true to ignore invoking PacketParser.OnAction
	Parse(ctx *StateContext, pkg *Pkg, noOnAction bool) (to State, err error)
}

type Pkg struct {
	ReceivedTime time.Time
	// StateContext

	HdrLen     int
	Length     int
	ReportType ReportType
	DUP        bool
	RETAIN     bool
	QoS        QoSType
	Flags      byte

	PacketIdentifier uint16
	VH               VariantHeader
	Payload          Payload

	// Data includes mqtt.VariantHeader + mqtt.Data.
	Data []byte
}

type PacketParserMap struct {
	ToList               map[ReportType]PacketParser
	ToStateUnconditional State
	// vh      func() VariantHeader
	// payload func() Payload
	// StepAction
}

type PacketBuilder interface {
	ReportType() ReportType
	Build(ctx *StateContext, pkg *Pkg) (err error)
	Bytes() (bytes []byte)
	AsString() (str string)
	WriteTo(w io.Writer) (n int, err error)
	// NextPktId generate the packet identifier and store it, and return it too
	NextPktId(ctx *StateContext) uint16
	RequestResend()
	IsResending() bool
}

type VariantHeader interface {
	Applyable
}

type Payload interface {
	Applyable
}

type Contextual interface {
	GetContext() *StateContext
}

type ClientIdReadable interface {
	GetClientId() string
}

type SubBlock struct {
	TopicFilter string
	RequestQoS  QoSType
	Verbose     bool
	RetCode     QoSType
}

type PubBlock struct {
	TopicName string
	Payload   []byte
}

type SendingTask struct {
	Builder PacketBuilder
}

type PubSubTask struct {
	SendingTask
	PacketIdentifier uint16
	SentTime         time.Time
	OnSent           func(task *PubSubTask, data []byte)
}

func (pt *PubSubTask) RequestResend() *PubSubTask {
	if pt.Builder != nil {
		pt.Builder.RequestResend()
	}
	return pt
}

func (pt *PubSubTask) IsResending() bool {
	if pt.Builder != nil {
		return pt.Builder.IsResending()
	}
	return false
}

type Hooker interface {
}

type Applyable interface {
	Apply(ctx *StateContext, pkg *Pkg, data []byte, pos int) (newPos int, err error)
}

type TopicFilter struct {
	Filter            string
	Shared            bool
	SharedName        string
	RequestedQoS      QoSType
	NoLocal           bool // v5.0+
	RetainAsPublished bool // v5.0+
	RetainHandling    int  // v5.0+, see also ch. 3.8.3.1
	RetCode           errors.CloseReason
}

type Store interface {
	Has(key string) bool
	Get(key string, defaultValue ...interface{}) interface{}
	Set(key string, value interface{}) (err error)
}

type IdStore interface {
	Has(key uint16) bool
	Get(key uint16, defaultValue ...interface{}) interface{}
	Set(key uint16, value interface{}) (err error)
}

type SessionStore interface {
	// Close the session store if server stopping, it will flush all sessions to persistent storage.
	Close()

	BeforeServerListening()

	// Has tests the existence of a session via its clientId key
	Has(key string) bool
	Get(key string, defaultValue ...Session) Session
	Set(key string, value Session) (err error)
	Unset(key string) (err error)

	AddWillMessage(clientId string, willQoS QoSType, willRetainFlag bool, willTopic string, willMessage []byte)
	HasWillMessage(clientId string) bool
	// SendWillMessage do publish the will msg to all clients on will topic, and
	// erase the will msg if not retained
	// sendPaylaodVx should be nil so that a default sender can dispatch the
	// payload to all subscribers, see also
	// defaultSessionStore.sendPayloadToClient
	SendWillMessage(ctx *StateContext, clientId string, sendPayloadVx SendPayloadVx) (err error)

	StoreQos1or2Message(ctx *StateContext, pkg *Pkg) (err error)
	PublishByPktId(ctx *StateContext, pktId uint16) (err error)

	PublishByBuilder(ctx *StateContext, builder PacketBuilder) (err error)

	Publish(ctx *StateContext, pkg *Pkg) (err error)
	Subscribe(ctx *StateContext, clientId string, topicFilter TopicFilter, packetIdentifier uint16) (invoked QoSType, err error)
	Unsubscribe(ctx *StateContext, clientId, topicFilter string) (err error)

	EnableSysStats(enabled bool)
	SysStatsEnabled() bool
	EnableSysStatsLog(enabled bool)
	SysStatsLogEnabled() bool
	EnableResetStorage(enabled bool)
	ResetStorageEnabled() bool
}

type SysPublisher interface {
	SysPublish(topicParts string, payload []byte, opts ...PublishBuilderOpt)
	SysPublishString(topicParts string, payload string, opts ...PublishBuilderOpt)
	SysPublishInt64(topicParts string, payload int64, opts ...PublishBuilderOpt)
	SysPublishUint64(topicParts string, payload uint64, opts ...PublishBuilderOpt)
	SysPublishFloat64(topicParts string, payload float64, opts ...PublishBuilderOpt)

	// IncMessages updates statistics data under $SYS/broker/messages/...
	// The known suffixes must be:
	// "received", "sent", "publish/dropped", "publish/received", "publish/sent", "retained/count",
	IncMessages(suffix string, delta int64)

	// IncLoad updates statistics data under $%SYS/broker/load/...
	// The known suffixes could be:
	// "bytes/received", "bytes/sent", ...
	IncLoad(suffix string, delta int)
}

// type StepAction func(ctx *StateContext, pkg *Pkg) (to State, err error)
// type MatchedAction func(ctx *StateContext, pkg *Pkg, from, to State) (err error)

type PacketParser interface {
	GetToState() State
	CreateVH() VariantHeader
	CreatePayload() Payload
	OnAction(ctx *StateContext, pkg *Pkg) (err error)
	OnError(ctx *StateContext, pkg *Pkg, reason error) (err error)
}

//
//
//
