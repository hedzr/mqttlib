/*
 * Copyright © 2020 Hedzr Yeh.
 */

package mqtt

import "gitlab.com/hedzr/mqttlib/pkg/logger"

// ....................................................

func NewPingreqParser(to State) PacketParser {
	return &pingreqParser{base: newBase("pingreqParser"), to: to}
}

type pingreqParser struct {
	base
	to State
}

func (p *pingreqParser) GetToState() State {
	return p.to
}

func (p *pingreqParser) CreateVH() VariantHeader {
	return nil // no vh
}

func (p *pingreqParser) CreatePayload() Payload {
	return nil // no payload
}

// WithCtx could cause race exception, for example: when two or more
// clients incoming with CONNECT packets, now connectParser parse them
// ok and print logging info via base.WithCtx(ctx).Debug(...), here
// is the race point.
// Another scene is the multiple clients send pingreq packets.
func (p *pingreqParser) WithCtx(ctx *StateContext) *logger.Base {
	return p.With("ClientId", ctx.ConnectParam.ClientId)
}

func (p *pingreqParser) OnAction(ctx *StateContext, pkg *Pkg) (err error) {
	if pkg.VH == nil {
		if pkg.Payload == nil {

			// pinging, send pingresp packet

			if ctx.IsNotClientMode() && ctx.session != nil {

				ctx.session.KeepAlive()

				// 3.12.4-1
				var builder PacketBuilder = NewPingrespBuilder()
				if err = builder.Build(ctx, pkg); err != nil {
					p.Wrong(err, "can't build PINGACK packet")
					return
				}

				if _, err = builder.WriteTo(ctx); err != nil {
					p.Wrong(err, "can't send package")
					return
				} else {
					p.Trace("    ▲ MQTT.%s %v", builder.ReportType(), builder.Bytes())
				}
			}
		}
	}
	return nil
}

func (p *pingreqParser) OnError(ctx *StateContext, pkg *Pkg, reason error) (err error) {
	err = reason
	return
}

// ....................................................

func NewPingrespParser(to State) PacketParser {
	return &pingrespParser{base: newBase("pingrespParser"), to: to}
}

type pingrespParser struct {
	base
	to State
}

func (p *pingrespParser) GetToState() State {
	return p.to
}

func (p *pingrespParser) CreateVH() VariantHeader {
	return nil
}

func (p *pingrespParser) CreatePayload() Payload {
	return nil
}

func (p *pingrespParser) OnAction(ctx *StateContext, pkg *Pkg) (err error) {
	return
}

func (p *pingrespParser) OnError(ctx *StateContext, pkg *Pkg, reason error) (err error) {
	err = reason
	return
}
