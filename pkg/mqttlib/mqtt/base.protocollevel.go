/*
 * Copyright © 2020 Hedzr Yeh.
 */

package mqtt

// \go:generate stringer -type=CloseReason -output base.closereason_string.go
// \go:generate stringer -type=ReportType
// \go:generate ls -la ../../../
//go:generate go run ../../../ci/stringerex/stringerex.go -type=ProtocolLevel -trimprefix=ProtocolLevel -linecomment -output base.protocollevel_string.go

type ProtocolLevel byte

const (
	ProtocolLevel0       ProtocolLevel = iota // Level0
	ProtocolLevel1                            // Level1
	ProtocolLevel2                            // Level2
	ProtocolLevel3                            // Level3
	ProtocolLevelForV311                      // Level4(3.1.1)
	ProtocolLevelForV50                       // Level5(5.0)
)

func (i ProtocolLevel) Parse(s string) ProtocolLevel {
	if c, bad := _ParseProtocolLevel7Uint8(s, _ProtocolLevel_name, _ProtocolLevel_index, 0); !bad {
		return c
	}
	return ProtocolLevel0
}

func _ParseProtocolLevel7Uint8(s string, _m string, _i [7]uint8, base ProtocolLevel) (c ProtocolLevel, bad bool) {
	for t := 0; t < len(_i); t++ {
		b := _i[t]
		e := len(_m)
		if t < len(_i)-1 {
			e = int(_i[t+1])
		}
		n := _m[b:e]
		if n == s {
			return ProtocolLevel(t + int(base)), false
		}
	}
	return 0, true
}
