/*
 * Copyright © 2020 Hedzr Yeh.
 */

package mqtt

import (
	"gitlab.com/hedzr/mqttlib/pkg/mqttlib/errors"
	"io"
)

func NewDefaultStateRegistry(strictClientId bool) StateRegistry {
	if len(defaultStateRegistry_.Tag) == 0 {
		defaultStateRegistry_.Tag = "state.registry"

		defaultStateRegistry_.AddSynonyms(IDLE, WRONG_STATE, CLOSED)
		defaultStateRegistry_.AddSynonyms(CONNECTED, PUBLISHED, SUBSCRIBED, UNSUBSCRIBED)

		defaultStateRegistry_.AddState(IDLE, &PacketParserMap{
			ToList: map[ReportType]PacketParser{
				CONNECT: NewConnectParser(CONNECTED, strictClientId),
			},
		})

		defaultStateRegistry_.AddState(CONNECTED, &PacketParserMap{
			ToList: map[ReportType]PacketParser{
				CONNECT:     NewWrongPacketParser(),
				PUBLISH:     NewPublishParser(PUBLISHED), // qos 0: published, qos 1: pub1_ack, qos 2: pub2_rec
				PUBACK:      NewPubackParser(PUBLISHED),  // qos 1: received from client after pushed the publishing packet
				PUBREC:      NewPubrecParser(PUB2_COMP),  // qos 2
				PUBCOMP:     NewPubcompParser(PUBLISHED), // qos 2
				SUBSCRIBE:   NewSubscribeParser(SUBSCRIBED),
				UNSUBSCRIBE: NewUnsubscribeParser(UNSUBSCRIBED),
				PINGREQ:     NewPingreqParser(PINGING),
				DISCONNECT:  NewDisconnectParser(CLOSED),
			},
		})

		defaultStateRegistry_.AddState(PUB2_REC, &PacketParserMap{
			ToList: map[ReportType]PacketParser{
				PUBREL: NewPubrelParser(PUB2_REL), // qos 2: when PUBREL received, send back PUBCOMP
			},
		})

		defaultStateRegistry_.AddUnconditionalState(PUB1_ACK, PUBLISHED) // QoS 1: after sent back PUBACK, the publishing flow completed
		defaultStateRegistry_.AddUnconditionalState(PUB2_REL, PUBLISHED) // QoS 2: after sent back PUBREC, and waited for the PUBREL, and sent back PUBCOMP, the publishing flow completed

		defaultStateRegistry_.AddUnconditionalState(PUBLISHED, CONNECTED)
		defaultStateRegistry_.AddUnconditionalState(SUBSCRIBED, CONNECTED)
		defaultStateRegistry_.AddUnconditionalState(UNSUBSCRIBED, CONNECTED)
		defaultStateRegistry_.AddUnconditionalState(PINGING, CONNECTED)

		defaultStateRegistry_.AddUnconditionalState(CLOSED, IDLE)
		defaultStateRegistry_.AddUnconditionalState(WRONG_STATE, IDLE)

		// defaultStateRegistry_.AddParser(UNSUBSCRIBE, NewUnsubscribeParser(UNSUBSCRIBING))
		defaultStateRegistry_.AddParser(UNSUBACK, NewUnsubackParser(UNSUBSCRIBED))
		// defaultStateRegistry_.AddParser(SUBSCRIBE, NewSubscribeParser(SUBSCRIBING))
		defaultStateRegistry_.AddParser(SUBACK, NewSubackParser(SUBSCRIBED))
		// defaultStateRegistry_.AddParser(PINGREQ, NewPingreqParser(CONNECTED))
		defaultStateRegistry_.AddParser(PINGRESP, NewPingrespParser(CONNECTED))
		// defaultStateRegistry_.AddParser(PUBLISH, NewPublishParser(PUBLISHED)) // if qos>0, toState = PUBLISHING
		defaultStateRegistry_.AddParser(PUBACK, NewPubackParser(PUBLISHED))
		defaultStateRegistry_.AddParser(CONNACK, NewConnackParser(CONNECTED))

		defaultStateRegistry_.AddParser(PUBREL, NewPubrelParser(PUB2_REL))
	}
	return &defaultStateRegistry_
}

func NewDefaultClientStateRegistry(strictClientId bool) StateRegistry {
	if len(defaultStateRegistry_.Tag) == 0 {
		defaultStateRegistry_.Tag = "state.registry"

		defaultStateRegistry_.AddSynonyms(IDLE, WRONG_STATE, CLOSED)
		defaultStateRegistry_.AddSynonyms(CONNECTED, PUBLISHED, SUBSCRIBED, UNSUBSCRIBED)

		defaultStateRegistry_.AddState(IDLE, &PacketParserMap{
			ToList: map[ReportType]PacketParser{
				CONNECT: NewConnectParser(CONNECTING, strictClientId),
			},
		})

		defaultStateRegistry_.AddState(CONNECTED, &PacketParserMap{
			ToList: map[ReportType]PacketParser{
				CONNECT:     NewWrongPacketParser(),      // 3.1.0-2
				PUBLISH:     NewPublishParser(PUBLISHED), // qos 0: published, qos 1: pub1_ack, qos 2: pub2_rec
				SUBSCRIBE:   NewSubscribeParser(SUBSCRIBING),
				UNSUBSCRIBE: NewUnsubscribeParser(UNSUBSCRIBING),
				PINGREQ:     NewPingreqParser(PINGING),
				DISCONNECT:  NewDisconnectParser(CLOSED),
			},
		})

		defaultStateRegistry_.AddState(PUB1_ACK, &PacketParserMap{
			ToList: map[ReportType]PacketParser{
				PUBACK: NewPubackParser(PUBLISHED), // complete the whole QoS 1 Publishing
			},
		})

		defaultStateRegistry_.AddState(PUB2_REC, &PacketParserMap{
			ToList: map[ReportType]PacketParser{
				PUBREC: NewPubrecParser(PUB2_COMP), // send PUBREL to server and waiting for PUBCOMP
			},
		})

		defaultStateRegistry_.AddState(PUB2_COMP, &PacketParserMap{
			ToList: map[ReportType]PacketParser{
				PUBCOMP: NewPubcompParser(PUBLISHED), // complete the whole QoS 2 Publishing
			},
		})

		defaultStateRegistry_.AddUnconditionalState(PUB1_ACK, PUBLISHED)
		defaultStateRegistry_.AddUnconditionalState(PUB2_COMP, PUBLISHED)

		defaultStateRegistry_.AddUnconditionalState(PUBLISHED, CONNECTED)
		defaultStateRegistry_.AddUnconditionalState(SUBSCRIBED, CONNECTED)
		defaultStateRegistry_.AddUnconditionalState(UNSUBSCRIBED, CONNECTED)
		// defaultStateRegistry_.AddUnconditionalState(PINGING, CONNECTED)

		defaultStateRegistry_.AddUnconditionalState(CLOSED, IDLE)
		defaultStateRegistry_.AddUnconditionalState(WRONG_STATE, IDLE)

		// defaultStateRegistry_.AddParser(UNSUBSCRIBE, NewUnsubscribeParser(UNSUBSCRIBING))
		defaultStateRegistry_.AddParser(UNSUBACK, NewUnsubackParser(UNSUBSCRIBED))
		// defaultStateRegistry_.AddParser(SUBSCRIBE, NewSubscribeParser(SUBSCRIBING))
		defaultStateRegistry_.AddParser(SUBACK, NewSubackParser(SUBSCRIBED))
		// defaultStateRegistry_.AddParser(PINGREQ, NewPingreqParser(CONNECTED))
		defaultStateRegistry_.AddParser(PINGRESP, NewPingrespParser(CONNECTED))
		// defaultStateRegistry_.AddParser(PUBLISH, NewPublishParser(PUBLISHED)) // if qos>0, toState = PUBLISHING
		defaultStateRegistry_.AddParser(PUBACK, NewPubackParser(PUBLISHED))
		defaultStateRegistry_.AddParser(CONNACK, NewConnackParser(CONNECTED))

		defaultStateRegistry_.AddParser(PUBREC, NewPubrecParser(PUB2_COMP))
		defaultStateRegistry_.AddParser(PUBCOMP, NewPubcompParser(PUBLISHED))
	}
	return &defaultStateRegistry_
}

var defaultStateRegistry_ stateRegistry

type stateRegistry struct {
	base
	FromTable  map[State]*PacketParserMap
	BeginTable map[State]map[State]bool
	parsers    map[ReportType]PacketParser
}

func (sr *stateRegistry) Parsers() map[ReportType]PacketParser {
	return sr.parsers
}

func (sr *stateRegistry) AddSynonyms(from State, synonymies ...State) {
	if sr.BeginTable == nil {
		sr.BeginTable = make(map[State]map[State]bool)
	}
	if v, ok := sr.BeginTable[from]; !ok {
		sr.BeginTable[from] = make(map[State]bool)
	} else if v == nil {
		sr.BeginTable[from] = make(map[State]bool)
	}

	for _, s := range synonymies {
		sr.BeginTable[from][s] = true

		if v, ok := sr.BeginTable[s]; !ok {
			sr.BeginTable[s] = make(map[State]bool)
		} else if v == nil {
			sr.BeginTable[s] = make(map[State]bool)
		}
		sr.BeginTable[s][from] = true
	}
}

func (sr *stateRegistry) AddParser(reportType ReportType, parser PacketParser) {
	if _, ok := sr.parsers[reportType]; !ok {
		sr.parsers[reportType] = parser
	}
}

func (sr *stateRegistry) AddUnconditionalState(from State, to State) {
	sr.AddState(from, &PacketParserMap{ToStateUnconditional: to})
}

func (sr *stateRegistry) AddState(from State, parsers *PacketParserMap) {
	if sr.FromTable == nil {
		sr.FromTable = make(map[State]*PacketParserMap)
	}
	sr.FromTable[from] = parsers
	if sr.parsers == nil {
		sr.parsers = make(map[ReportType]PacketParser)
	}
	for k, v := range parsers.ToList {
		sr.parsers[k] = v
	}
}

func (sr *stateRegistry) Advance(out io.Writer, ctx *StateContext, pkg *Pkg, noOnAction bool) (to State, err error) {
	to, err = sr.advance(out, ctx, pkg, 0, noOnAction)
	if err != nil {
		sr.Wrong(err, "ERROR: ERROR: ERROR: ")
	}
	return
}

func (sr *stateRegistry) advance(out io.Writer, ctx *StateContext, pkg *Pkg, level int, noOnAction bool) (to State, err error) {
	to = IDLE
	if parser, ok := sr.FromTable[ctx.State]; ok {
		// sr.Debug("from: %v, to-list: %v", ctx.State, parser)
		to, err = WRONG_STATE, errors.ErrWrongState

		if parser.ToList != nil {
			if toP, ok := parser.ToList[pkg.ReportType]; ok {
				to, err = sr.advanceToP(toP, out, ctx, pkg, level, noOnAction)
				if err != nil {
					err = toP.OnError(ctx, pkg, err)
				}
			} else {
				err = errors.ErrCodeWrongPacketType.New("for state %v, incoming packet type %v not found, ctx.clientMode=%v", ctx.State, pkg.ReportType, ctx.IsClientMode())
			}

		} else if parser.ToStateUnconditional >= IDLE && parser.ToStateUnconditional <= MAX_STATE {
			sr.Trace("    unconditional state migrate: %v -> %v", ctx.State, parser.ToStateUnconditional)
			ctx.State = parser.ToStateUnconditional
			if level > errors.MaxNestedLoops {
				err = errors.ErrStateLoops
			} else {
				to, err = sr.advance(out, ctx, pkg, level+1, noOnAction)
			}

		} else {
			err = errors.ErrCodeWrongParserDefinition.New("wrong parser definition, %+v", parser)
		}
	} else {
		err = errors.ErrCodeWrongPrerequisiteState.New("wrong prerequisite state 'from' / not found: %v", ctx.State)
	}
	return
}

func (sr *stateRegistry) advanceToP(toP PacketParser, out io.Writer, ctx *StateContext, pkg *Pkg, level int, noOnAction bool) (to State, err error) {
	from := ctx.State
	to, err = sr.doParse(toP, ctx, pkg, noOnAction)
	sr.Debug("    state migrated (%v): %v -> %v", ctx.ConnectParam.ClientId, from, to)
	ctx.State = to
	return
}

func (sr *stateRegistry) doParse(toP PacketParser, ctx *StateContext, pkg *Pkg, noOnAction bool) (to State, err error) {
	pos := 0

	if pos, _, err = doTestAndReadVHPI(pkg, pkg.Data, pos); err != nil {
		err = errors.ErrCodePacketCorrupt.NewE(err, "[VHPI] can't read packet identifier")
		return
	}

	if vh := toP.CreateVH(); vh != nil {
		if pos, err = vh.Apply(ctx, pkg, pkg.Data, pos); err != nil {
			err = errors.ErrCodePacketCorrupt.NewE(err, "[VH] data block corrupts: %v", pkg.Data[pos:])
			return
		} else {
			sr.Trace("       [VH] parsed: %+v", pkg.VH)
		}
	}

	if payload := toP.CreatePayload(); payload != nil {
		if pos, err = payload.Apply(ctx, pkg, pkg.Data, pos); err != nil {
			err = errors.ErrCodePacketCorrupt.NewE(err, "[Playload] data block corrupts: %v", pkg.Data[pos:])
			return
		} else {
			sr.Trace("       [Payload] parsed: %+v", pkg.Payload)
		}
	}

	if pos < len(pkg.Data) {
		sr.Warn("remains payload [pos=%v]: %v | %s", pos, pkg.Data[pos:], pkg.ReportType)
	}

	if !noOnAction {
		if err = toP.OnAction(ctx, pkg); err != nil {
			err = errors.ErrCodePacketIllegal.NewE(err, "incoming packet verified, but can't be processed")
			err = toP.OnError(ctx, pkg, err)
			return
		}
	}
	to = toP.GetToState()
	return
}

func (sr *stateRegistry) Parse(ctx *StateContext, pkg *Pkg, noOnAction bool) (to State, err error) {
	if parser, ok := sr.parsers[pkg.ReportType]; ok && pkg.VH == nil && pkg.Payload == nil {
		to, err = sr.doParse(parser, ctx, pkg, noOnAction)
	}
	return
}
