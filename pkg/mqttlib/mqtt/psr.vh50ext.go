/*
 * Copyright © 2020 Hedzr Yeh.
 */

package mqtt

import (
	"bytes"
	"github.com/hedzr/cmdr"
	"gitlab.com/hedzr/mqttlib/pkg/mqttlib/errors"
	"gitlab.com/hedzr/mqttlib/pkg/mqttlib/mqtt/codec"
)

var topicAliasMaximum uint16 = 10

func newvh50ext() *vh50ext {
	s := &vh50ext{
		v50length:        0,
		propertiesLength: -1,
		userProperties:   make(map[string]string),

		payloadFormatIndicator: 0,
		messageExpiryInterval:  -1,
		sessionExpiryInterval:  -1,
		topicAlias:             0xffff,
		receiveMaximum:         0xffff,
		maximumPacketSize:      -1,
		topicAliasMaximum:      0xffff,
		serverKeepAlive:        0xffff,
		maximumQoS:             0xff,
		retainAvailable:        true,
		wildcardSubsAvailable:  true,
		subsIdAvailable:        true,
		sharedSubsAvailable:    true,
	}

	if cmdr.GetBoolR("mqtt.server.keep-alive.enabled") {
		s.serverKeepAlive = uint16(cmdr.GetDurationR("mqtt.server.keep-alive.interval").Seconds())
	}
	s.sharedSubsAvailable = cmdr.GetBoolR("mqtt.server.shared-subscription.enabled")
	s.subsIdAvailable = cmdr.GetBoolR("mqtt.server.subs-id-avaliable")
	s.wildcardSubsAvailable = !cmdr.GetBoolR("mqtt.server.no-wildcard-filter")

	return s
}

type vh50ext struct {
	v50length        int
	propertiesLength int32

	payloadFormatIndicator byte              // 0x01, publish: =0x00, unspecified format; =0x01, utf-8 string
	messageExpiryInterval  int32             // 0x02, publish:
	contentType            string            // 0x03, publish:
	responseTopic          string            // 0x08, publish:
	correlationData        []byte            // 0x09, publish:
	subscriptionIdentifier string            // 0x0b, subscribe/publish:
	sessionExpiryInterval  int32             // 0x11, disconnect/connack: in seconds
	assignedClientId       string            // 0x12, connack:
	serverKeepAlive        uint16            // 0x13, connack:
	authenticationMethod   string            // 0x15, connack:
	authenticationData     []byte            // 0x16, connack:
	responseInfo           string            // 0x1a, connack:
	serverReferenceId      string            // 0x1c, disconnect:
	reasonString           string            // 0x1f, disconnect/unsub/connack: disconnect reason, unsubscrib reason, ...
	receiveMaximum         uint16            // 0x21, connack:
	topicAliasMaximum      uint16            // 0x22, connack:
	topicAlias             uint16            // 0x23, publish:
	maximumQoS             byte              // 0x24, connack:
	retainAvailable        bool              // 0x25, connack:
	userProperties         map[string]string // 0x26, both:
	maximumPacketSize      int32             // 0x27, connack:
	wildcardSubsAvailable  bool              // 0x28, connack:
	subsIdAvailable        bool              // 0x29, connack:
	sharedSubsAvailable    bool              // 0x2a, connack:
}

func (s *vh50ext) WriteWithOrder(ids ...byte) (propertiesBuf bytes.Buffer, err error) {
	err = errors.New("can't write as properties buffer")
	for _, id := range ids {
		errors.Attach(s.write(&propertiesBuf, id))
	}
	if !errors.HasAttachedErrors(err) {
		err = nil
	}
	return
}

func (s *vh50ext) write(propertiesBuf *bytes.Buffer, id byte) (err error) {
	// propertiesBuf.WriteByte(id)
	switch id {
	case 0x01: // payload format indicator
		if s.payloadFormatIndicator > 0 {
			propertiesBuf.WriteByte(id)
			propertiesBuf.WriteByte(s.payloadFormatIndicator)
		}
	case 0x02: // message expiry interval
		if s.messageExpiryInterval > 0 {
			propertiesBuf.WriteByte(id)
			err = codec.WriteBEInt32To(propertiesBuf, s.messageExpiryInterval)
		}
	case 0x03: // content type
		if len(s.contentType) > 0 {
			propertiesBuf.WriteByte(id)
			err = codec.WriteString(propertiesBuf, s.contentType)
		}
	case 0x08: // response topic
		if len(s.responseTopic) > 0 {
			propertiesBuf.WriteByte(id)
			err = codec.WriteString(propertiesBuf, s.responseTopic)
		}
	case 0x09: // correlation data
		if len(s.correlationData) > 0 {
			propertiesBuf.WriteByte(id)
			propertiesBuf.Write(s.correlationData)
		}
	case 0x0b: // subscription identifier
		if len(s.subscriptionIdentifier) > 0 {
			propertiesBuf.WriteByte(id)
			err = codec.WriteString(propertiesBuf, s.subscriptionIdentifier)
		}
	case 0x11: // session expiry interval
		if s.sessionExpiryInterval >= 0 {
			propertiesBuf.WriteByte(id)
			err = codec.WriteBEInt32To(propertiesBuf, s.sessionExpiryInterval)
		}
	case 0x12: // assigned client identifier
		if len(s.assignedClientId) > 0 {
			propertiesBuf.WriteByte(id)
			err = codec.WriteString(propertiesBuf, s.assignedClientId)
		}
	case 0x13: // server keep alive
		if s.serverKeepAlive != 0xffff {
			propertiesBuf.WriteByte(id)
			err = codec.WriteBEUint16(propertiesBuf, s.serverKeepAlive)
		}
	case 0x15: // authentication method
		if len(s.authenticationMethod) > 0 {
			propertiesBuf.WriteByte(id)
			err = codec.WriteString(propertiesBuf, s.authenticationMethod)
		}
	case 0x16: // correlation data
		if len(s.authenticationData) > 0 {
			propertiesBuf.WriteByte(id)
			propertiesBuf.Write(s.authenticationData)
		}
	case 0x1a: // response information
		if len(s.responseInfo) > 0 {
			propertiesBuf.WriteByte(id)
			err = codec.WriteString(propertiesBuf, s.responseInfo)
		}
	case 0x1c: // server reference string
		if len(s.serverReferenceId) > 0 {
			propertiesBuf.WriteByte(id)
			err = codec.WriteString(propertiesBuf, s.serverReferenceId)
		}
	case 0x1f: // reason string
		if len(s.reasonString) > 0 {
			propertiesBuf.WriteByte(id)
			err = codec.WriteString(propertiesBuf, s.reasonString)
		}
	case 0x21: // receive maximum
		if s.receiveMaximum != 0xffff {
			propertiesBuf.WriteByte(id)
			err = codec.WriteBEUint16(propertiesBuf, s.receiveMaximum)
		}
	case 0x22: // topic aliases maximum
		if s.topicAliasMaximum != 0xffff {
			propertiesBuf.WriteByte(id)
			err = codec.WriteBEUint16(propertiesBuf, s.topicAliasMaximum)
		}
	case 0x23: // topic aliases
		if s.topicAlias != 0xffff {
			propertiesBuf.WriteByte(id)
			err = codec.WriteBEUint16(propertiesBuf, s.topicAlias)
		}
	case 0x24: // maximum qos
		if s.maximumQoS <= 2 {
			propertiesBuf.WriteByte(id)
			propertiesBuf.WriteByte(s.maximumQoS)
		}
	case 0x25: // retain avaliable
		if !s.retainAvailable {
			propertiesBuf.WriteByte(id)
			propertiesBuf.WriteByte(0)
		}
	case 0x26: // user properties
		if len(s.userProperties) > 0 {
			propertiesBuf.WriteByte(id)
			for k, v := range s.userProperties {
				err = codec.WriteString(propertiesBuf, k)
				err = codec.WriteString(propertiesBuf, v)
			}
		}
	case 0x27: // maximum packet size
		if s.maximumPacketSize >= 0 {
			propertiesBuf.WriteByte(id)
			err = codec.WriteBEInt32To(propertiesBuf, s.maximumPacketSize)
		}
	case 0x28: // wildcard subscriptions available
		if !s.wildcardSubsAvailable {
			propertiesBuf.WriteByte(id)
			propertiesBuf.WriteByte(0)
		}
	case 0x29: // subscription identifier available
		if !s.subsIdAvailable {
			propertiesBuf.WriteByte(id)
			propertiesBuf.WriteByte(0)
		}
	case 0x2a: // shared subscription available
		if !s.sharedSubsAvailable {
			propertiesBuf.WriteByte(id)
			propertiesBuf.WriteByte(0)
		}
	}

	return
}

func (s *vh50ext) WriteAs() (propertiesBuf bytes.Buffer, err error) {
	err = errors.New("can't write as properties buffer")

	if s.payloadFormatIndicator > 0 {
		propertiesBuf.WriteByte(0x01)
		propertiesBuf.WriteByte(s.payloadFormatIndicator)
	}

	if s.messageExpiryInterval > 0 {
		propertiesBuf.WriteByte(0x02)
		errors.Attach(err, codec.WriteBEInt32To(&propertiesBuf, s.messageExpiryInterval))
	}

	if len(s.contentType) > 0 {
		propertiesBuf.WriteByte(0x03)
		errors.Attach(err, codec.WriteString(&propertiesBuf, s.contentType))
	}

	if len(s.responseTopic) > 0 {
		propertiesBuf.WriteByte(0x08)
		errors.Attach(err, codec.WriteString(&propertiesBuf, s.responseTopic))
	}

	if len(s.correlationData) > 0 {
		propertiesBuf.WriteByte(0x09)
		propertiesBuf.Write(s.correlationData)
	}

	if len(s.subscriptionIdentifier) > 0 {
		propertiesBuf.WriteByte(0x0b)
		errors.Attach(err, codec.WriteString(&propertiesBuf, s.subscriptionIdentifier))
	}

	if s.sessionExpiryInterval >= 0 {
		propertiesBuf.WriteByte(0x11)
		errors.Attach(err, codec.WriteBEInt32To(&propertiesBuf, s.sessionExpiryInterval))
	}

	if len(s.assignedClientId) > 0 {
		propertiesBuf.WriteByte(0x12)
		errors.Attach(err, codec.WriteString(&propertiesBuf, s.assignedClientId))
	}

	if s.serverKeepAlive != 0xffff {
		propertiesBuf.WriteByte(0x13)
		errors.Attach(err, codec.WriteBEUint16(&propertiesBuf, s.serverKeepAlive))
	}

	if len(s.authenticationMethod) > 0 {
		propertiesBuf.WriteByte(0x15)
		errors.Attach(err, codec.WriteString(&propertiesBuf, s.authenticationMethod))
	}

	if len(s.authenticationData) > 0 {
		propertiesBuf.WriteByte(0x16)
		propertiesBuf.Write(s.authenticationData)
	}

	if len(s.responseInfo) > 0 {
		propertiesBuf.WriteByte(0x1a)
		errors.Attach(err, codec.WriteString(&propertiesBuf, s.responseInfo))
	}

	if err == nil && len(s.serverReferenceId) > 0 {
		propertiesBuf.WriteByte(0x1c)
		errors.Attach(err, codec.WriteString(&propertiesBuf, s.serverReferenceId))
	}

	if err == nil && len(s.reasonString) > 0 {
		propertiesBuf.WriteByte(0x1f)
		errors.Attach(err, codec.WriteString(&propertiesBuf, s.reasonString))
	}

	if s.receiveMaximum != 0xffff {
		propertiesBuf.WriteByte(0x21)
		errors.Attach(err, codec.WriteBEUint16(&propertiesBuf, s.receiveMaximum))
	}

	if s.topicAliasMaximum != 0xffff {
		propertiesBuf.WriteByte(0x22)
		errors.Attach(err, codec.WriteBEUint16(&propertiesBuf, s.topicAliasMaximum))
	}

	if s.topicAlias != 0xffff {
		propertiesBuf.WriteByte(0x23)
		errors.Attach(err, codec.WriteBEUint16(&propertiesBuf, s.topicAlias))
	}

	if s.maximumQoS <= 2 {
		propertiesBuf.WriteByte(0x24)
		propertiesBuf.WriteByte(s.maximumQoS)
	}

	if s.retainAvailable {
		propertiesBuf.WriteByte(0x25)
		propertiesBuf.WriteByte(1)
	}

	if err == nil && len(s.userProperties) > 0 {
		propertiesBuf.WriteByte(0x26)
		for k, v := range s.userProperties {
			errors.Attach(err, codec.WriteString(&propertiesBuf, k))
			errors.Attach(err, codec.WriteString(&propertiesBuf, v))
		}
	}

	if s.maximumPacketSize >= 0 {
		propertiesBuf.WriteByte(0x27)
		errors.Attach(err, codec.WriteBEInt32To(&propertiesBuf, s.maximumPacketSize))
	}

	if s.wildcardSubsAvailable {
		propertiesBuf.WriteByte(0x28)
		propertiesBuf.WriteByte(1)
	}

	if s.subsIdAvailable {
		propertiesBuf.WriteByte(0x29)
		propertiesBuf.WriteByte(1)
	}

	if s.sharedSubsAvailable {
		propertiesBuf.WriteByte(0x2a)
		propertiesBuf.WriteByte(1)
	}

	if !errors.HasAttachedErrors(err) {
		err = nil
	}
	return
}

func (s *vh50ext) Apply(ctx *StateContext, pkg *Pkg, data []byte, pos int) (newPos int, err error) {
	if pos < len(data) {
		if s.v50length == 0 {
			s.v50length = len(data) - pos + 1
		}

		// s.propertiesLength, pos, err = codec.ReadBEInt32(data, pos)
		var ret, eat int
		ret, eat = codec.DecodeIntFrom(data, pos)
		s.propertiesLength, pos = int32(ret), pos+eat

		if pos < len(data) && err == nil && s.propertiesLength > 0 {
			for {
				id := data[pos]
				pos++
				switch id {
				case 0x01: // payload format indicator
					if pos >= len(data) {
						err = errors.ErrOverflow
						return
					}
					s.payloadFormatIndicator = data[pos]
					pos++
				case 0x02: // message expiry interval
					if pos+4 >= len(data) {
						err = errors.ErrOverflow
						return
					}
					s.messageExpiryInterval, pos, err = codec.ReadBEInt32(data, pos)
				case 0x03: // content type
					s.contentType, pos, err = codec.ReadString(data, pos)
				case 0x08: // response topic
					s.responseTopic, pos, err = codec.ReadString(data, pos)
				case 0x09: // correlation data
					l, eat := codec.DecodeIntFrom(data, pos)
					if pos+eat+l <= len(data) {
						pos += eat
						s.correlationData = data[pos : pos+l]
						pos += l
					}
				case 0x0b: // subscription identifier
					s.subscriptionIdentifier, pos, err = codec.ReadString(data, pos)
				case 0x11: // session expiry interval
					if pos+4 >= len(data) {
						err = errors.ErrOverflow
						return
					}
					s.sessionExpiryInterval, pos, err = codec.ReadBEInt32(data, pos)
				case 0x12: // assigned client identifier
					s.assignedClientId, pos, err = codec.ReadString(data, pos)
				case 0x13: // server keep alive
					if pos+2 >= len(data) {
						err = errors.ErrOverflow
						return
					}
					s.serverKeepAlive, pos, err = codec.ReadBEUint16(data, pos)
				case 0x15: // authentication method
					s.authenticationMethod, pos, err = codec.ReadString(data, pos)
				case 0x16: // correlation data
					l, eat := codec.DecodeIntFrom(data, pos)
					if pos+eat+l <= len(data) {
						pos += eat
						s.authenticationData = data[pos : pos+l]
						pos += l
					}
				case 0x1a: // response information
					s.responseInfo, pos, err = codec.ReadString(data, pos)
				case 0x1c: // server reference string
					s.serverReferenceId, pos, err = codec.ReadString(data, pos)
				case 0x1f: // reason string
					s.reasonString, pos, err = codec.ReadString(data, pos)
				case 0x21: // receive maximum
					if pos+2 >= len(data) {
						err = errors.ErrOverflow
						return
					}
					s.receiveMaximum, pos, err = codec.ReadBEUint16(data, pos)
				case 0x22: // topic aliases maximum
					if pos+2 >= len(data) {
						err = errors.ErrOverflow
						return
					}
					s.topicAliasMaximum, pos, err = codec.ReadBEUint16(data, pos)
				case 0x23: // topic aliases
					if pos+2 >= len(data) {
						err = errors.ErrOverflow
						return
					}
					s.topicAlias, pos, err = codec.ReadBEUint16(data, pos)
				case 0x24: // maximum qos
					s.maximumQoS = data[pos]
					pos++
				case 0x25: // retain avaliable
					s.retainAvailable = data[pos] != 0
					pos++
				case 0x26: // user properties
					var k, v string
				retryReadKV:
					k, pos, err = codec.ReadString(data, pos)
					if err == nil {
						v, pos, err = codec.ReadString(data, pos)
						if err == nil {
							s.userProperties[k] = v
							if pos == len(data) {
								break
							}
							goto retryReadKV
						}
					}
				case 0x27: // maximum packet size
					if pos+4 >= len(data) {
						err = errors.ErrOverflow
						return
					}
					s.maximumPacketSize, pos, err = codec.ReadBEInt32(data, pos)
				case 0x28: // wildcard subscriptions available
					s.wildcardSubsAvailable = data[pos] != 0
					pos++
				case 0x29: // subscription identifier available
					s.subsIdAvailable = data[pos] != 0
					pos++
				case 0x2a: // shared subscription available
					s.sharedSubsAvailable = data[pos] != 0
					pos++
				}

				if err != nil {
					return
				}
				if pos == len(data) {
					break
				}
			}
		}
	}

	newPos = pos // Apply() must return newPos to pointer the updated new position
	pkg.VH = s   // Apply() must assign itself into pkg.VH
	return
}
