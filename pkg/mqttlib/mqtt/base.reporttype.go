/*
 * Copyright © 2020 Hedzr Yeh.
 */

package mqtt

const (
	Reserved0   ReportType = iota // ReportType
	CONNECT                       // ReportType = 1
	CONNACK                       // ReportType = 2
	PUBLISH                       // ReportType = 3
	PUBACK                        // ReportType = 4
	PUBREC                        // ReportType = 5
	PUBREL                        // ReportType = 6
	PUBCOMP                       // ReportType = 7
	SUBSCRIBE                     // ReportType = 8
	SUBACK                        // ReportType = 9
	UNSUBSCRIBE                   // ReportType = 10
	UNSUBACK                      // ReportType = 11
	PINGREQ                       // ReportType = 12
	PINGRESP                      // ReportType = 13
	DISCONNECT                    // ReportType = 14
	AUTH                          // ReportType = 15, since mqtt-v5.0
)

const (
	IDLE State = 1000 + iota
	CONNECTING
	CONNECTED
	PUBLISHING
	PUBLISHED
	SUBSCRIBING
	SUBSCRIBED
	UNSUBSCRIBING
	UNSUBSCRIBED
	PINGING
	CLOSED
	PUB1_ACK
	PUB2_REC
	PUB2_REL
	PUB2_COMP
	WRONG_STATE
	NEED_CLOSE_STATE
	MAX_STATE
)

var (
	ReportTypeToString = map[ReportType]string{
		Reserved0:   "Nothing",
		CONNECT:     "CONNECT",     // ReportType = 1
		CONNACK:     "CONNACK",     // ReportType = 2
		PUBLISH:     "PUBLISH",     // ReportType = 3
		PUBACK:      "PUBACK",      // ReportType = 4
		PUBREC:      "PUBREC",      // ReportType = 5
		PUBREL:      "PUBREL",      // ReportType = 6
		PUBCOMP:     "PUBCOMP",     // ReportType = 7
		SUBSCRIBE:   "SUBSCRIBE",   // ReportType = 8
		SUBACK:      "SUBACK",      // ReportType = 9
		UNSUBSCRIBE: "UNSUBSCRIBE", // ReportType = 10
		UNSUBACK:    "UNSUBACK",    // ReportType = 11
		PINGREQ:     "PINGREQ",     // ReportType = 12
		PINGRESP:    "PINGRESP",    // ReportType = 13
		DISCONNECT:  "DISCONNECT",  // ReportType = 14
		AUTH:        "AUTH",        // ReportType = 15, since mqtt-v5.0

		ReportType(IDLE):             "IDLE",
		ReportType(CONNECTING):       "CONNECTING",
		ReportType(CONNECTED):        "CONNECTED",
		ReportType(PUBLISHING):       "PUBLISHING",
		ReportType(PUBLISHED):        "PUBLISHED",
		ReportType(SUBSCRIBING):      "SUBSCRIBING",
		ReportType(SUBSCRIBED):       "SUBSCRIBED",
		ReportType(UNSUBSCRIBING):    "UNSUBSCRIBING",
		ReportType(UNSUBSCRIBED):     "UNSUBSCRIBED",
		ReportType(PINGING):          "PINGING",
		ReportType(CLOSED):           "CLOSED",
		ReportType(PUB1_ACK):         "PUB1_ACK",
		ReportType(PUB2_REC):         "PUB2_REC",
		ReportType(PUB2_REL):         "PUB2_REL",
		ReportType(PUB2_COMP):        "PUB2_COMP",
		ReportType(WRONG_STATE):      "WRONG_STATE",
		ReportType(NEED_CLOSE_STATE): "NEED_CLOSE_STATE",
		ReportType(MAX_STATE):        "MAX_STATE",
	}
	ReportTypeFromString = map[string]ReportType{
		"Nothing":     Reserved0,
		"CONNECT":     CONNECT,     // ReportType = 1
		"CONNACK":     CONNACK,     // ReportType = 2
		"PUBLISH":     PUBLISH,     // ReportType = 3
		"PUBACK":      PUBACK,      // ReportType = 4
		"PUBREC":      PUBREC,      // ReportType = 5
		"PUBREL":      PUBREL,      // ReportType = 6
		"PUBCOMP":     PUBCOMP,     // ReportType = 7
		"SUBSCRIBE":   SUBSCRIBE,   // ReportType = 8
		"SUBACK":      SUBACK,      // ReportType = 9
		"UNSUBSCRIBE": UNSUBSCRIBE, // ReportType = 10
		"UNSUBACK":    UNSUBACK,    // ReportType = 11
		"PINGREQ":     PINGREQ,     // ReportType = 12
		"PINGRESP":    PINGRESP,    // ReportType = 13
		"DISCONNECT":  DISCONNECT,  // ReportType = 14
		"AUTH":        AUTH,        // ReportType = 15, since mqtt-v5.0
	}

	// CloseReasonToString = map[CloseReason]string{
	// 	CloseNormal:           "CloseNormal",
	// 	CloseUnknownError:     "CloseUnknownError",
	// 	CloseKeepAliveTimeout: "CloseKeepAliveTimeout",
	// }
	// CloseReasonFromString = map[string]CloseReason{
	// 	"CloseNormal":           CloseNormal,
	// 	"CloseUnknownError":     CloseUnknownError,
	// 	"CloseKeepAliveTimeout": CloseKeepAliveTimeout,
	// }
)

type ReportType uint

func (n ReportType) String() string {
	if x, ok := ReportTypeToString[n]; ok {
		return x
	}
	return ReportTypeToString[Reserved0]
}

func (n ReportType) FromString(s string) ReportType {
	if x, ok := ReportTypeFromString[s]; ok {
		return x
	}
	return Reserved0
}

type State ReportType

func (s State) String() string {
	return (ReportType(s)).String()
}
