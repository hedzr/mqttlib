/*
 * Copyright © 2020 Hedzr Yeh.
 */

package mqtt

import (
	"gitlab.com/hedzr/mqttlib/pkg/mqttlib/errors"
	"gitlab.com/hedzr/mqttlib/pkg/mqttlib/mqtt/codec"
)

// ....................................................

// NewPubcompParser
// for QoS 2
func NewPubcompParser(to State) PacketParser {
	return &pubcompParser{base: newBase("pubcompParser"), to: to}
}

type pubcompParser struct {
	base
	to State
}

func (p *pubcompParser) GetToState() State {
	return p.to
}

func (p *pubcompParser) CreateVH() VariantHeader {
	return newPubcompVH()
}

func (p *pubcompParser) CreatePayload() Payload {
	return newPubcompPayload()
}

func (p *pubcompParser) OnAction(ctx *StateContext, pkg *Pkg) (err error) {
	if vh, ok := pkg.VH.(*PubCompVH); ok {
		if payload, ok := pkg.Payload.(*PubCompPayload); ok {
			p.Debug("       %s: id=%v, vh=%v, payload=%v", pkg.ReportType, pkg.PacketIdentifier, vh, payload)

			// unsubscribed
		}
	}
	return
}

func (p *pubcompParser) OnError(ctx *StateContext, pkg *Pkg, reason error) (err error) {
	err = reason
	return
}

func newPubcompVH() *PubCompVH {
	return &PubCompVH{}
}

type PubCompVH struct{ Id uint16 }

func (s *PubCompVH) Apply(ctx *StateContext, pkg *Pkg, data []byte, pos int) (newPos int, err error) {
	if pos >= len(data) {
		err = errors.ErrCodePacketCorrupt.New("wrong %v pkt (vh+payload): %v", PUBCOMP, data)
		return
	}

	s.Id, pos, err = codec.ReadBEUint16(data, pos)
	if err != nil {
		return
	}

	newPos = pos // Apply() must return newPos to pointer the updated new position
	pkg.VH = s   // Apply() must assign itself into pkg.VH
	return
}

func newPubcompPayload() *PubCompPayload {
	return &PubCompPayload{}
}

type PubCompPayload struct {
}

func (s *PubCompPayload) Apply(ctx *StateContext, pkg *Pkg, data []byte, pos int) (newPos int, err error) {
	newPos = pos    // Apply() must return newPos to pointer the updated new position
	pkg.Payload = s // Apply() must be assign itself into pkg.Payload
	return
}
