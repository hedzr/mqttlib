/*
 * Copyright © 2020 Hedzr Yeh.
 */

package mqtt

import (
	"bytes"
	"encoding/gob"
	"github.com/sirupsen/logrus"
)

func newPubsubNode() *pubsubNode {
	return &pubsubNode{
		Children: make(map[string]*pubsubNode),
	}
}

func newPubsubNodeRich() *pubsubNode {
	return &pubsubNode{
		Children:      make(map[string]*pubsubNode),
		Subscriptions: newSubinfo("", QoSDefault, 0, ProtocolLevelForV50),
	}
}

type pubsubNode struct {
	Children      map[string]*pubsubNode
	Subscriptions *Subinfo
}

func (s *pubsubNode) String() string {
	return ""
}

func (s pubsubNode) MarshalBinary() (ret []byte, err error) {
	var b bytes.Buffer
	enc := gob.NewEncoder(&b)
	// _, err = fmt.Fprintln(&b, s.qos, s.packetIdentifier)

	if s.Subscriptions != nil {
		err = enc.Encode(true)
		err = enc.Encode(s.Subscriptions)
	} else {
		err = enc.Encode(false)
	}
	if err == nil {
		err = enc.Encode(s.Children)
	}

	if err != nil {
		logrus.Debugf("error: %v", err)
	}
	ret = b.Bytes()
	return
}

func (s *pubsubNode) UnmarshalBinary(data []byte) (err error) {
	b := bytes.NewBuffer(data)
	dec := gob.NewDecoder(b)
	// _, err = fmt.Fscanln(b, &s.qos, &s.packetIdentifier)

	var isNotNil bool
	err = dec.Decode(&isNotNil)
	if isNotNil {
		err = dec.Decode(&s.Subscriptions)
	}
	if err == nil {
		err = dec.Decode(&s.Children)
	}

	if err != nil {
		logrus.Debugf("error: %v", err)
	}
	return
}

// func (s *pubsubNode) Save(enc codec.Encoder) (err error) {
// 	if s.Subscriptions == nil {
// 		err = enc.Encode(byte(1))
// 	} else {
// 		err = enc.Encode(byte(0))
// 		if err != nil {
// 			logrus.Debugf("error: %v", err)
// 			return
// 		}
// 		err = s.Subscriptions.Save(enc)
// 	}
// 	if err != nil {
// 		logrus.Debugf("error: %v", err)
// 		return
// 	}
// 	count := len(s.Children)
// 	err = enc.Encode(count)
// 	if err != nil {
// 		logrus.Debugf("error: %v", err)
// 		return
// 	}
// 	for k, v := range s.Children {
// 		err = enc.Encode(k)
// 		if err != nil {
// 			logrus.Debugf("error: %v", err)
// 			return
// 		}
// 		err = v.Save(enc)
// 		if err != nil {
// 			logrus.Debugf("error: %v", err)
// 			return
// 		}
// 	}
// 	return
// }
//
// func (s *pubsubNode) Load(dec codec.Decoder) (err error) {
// 	// need restored:
// 	// tag,
// 	var b byte
// 	err = dec.Decode(&b)
// 	if err != nil {
// 		logrus.Debugf("error: %v", err)
// 		return
// 	}
// 	if b == 0 {
// 		s.Subscriptions = &Subinfo{}
// 		err = s.Subscriptions.Load(dec)
// 	}
// 	if err != nil {
// 		logrus.Debugf("error: %v", err)
// 		return
// 	}
//
// 	var count int
// 	err = dec.Decode(&count)
// 	if err != nil {
// 		logrus.Debugf("error: %v", err)
// 		return
// 	}
// 	for i := 0; i < count; i++ {
// 		var key string
// 		var node = &pubsubNode{}
// 		err = dec.Decode(&key)
// 		if err != nil {
// 			logrus.Debugf("error: %v", err)
// 			return
// 		}
// 		err = node.Load(dec)
// 		if err != nil {
// 			logrus.Debugf("error: %v", err)
// 			return
// 		}
// 		s.Children[key] = node
// 	}
// 	return
// }
