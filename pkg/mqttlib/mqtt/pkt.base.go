/*
 * Copyright © 2020 Hedzr Yeh.
 */

package mqtt

import (
	"io"
)

type BasePacketBuilder struct {
	base
	PacketIdentifer uint16
}

func newBasePacketBuilder(tag string) BasePacketBuilder {
	return BasePacketBuilder{base: newBase(tag)}
}

func (b *BasePacketBuilder) ReportType() ReportType {
	panic("implement me")
}

func (b *BasePacketBuilder) Build(ctx *StateContext, pkg *Pkg) (err error) {
	panic("implement me")
}

func (b *BasePacketBuilder) Bytes() (bytes []byte) {
	panic("implement me")
}

func (b *BasePacketBuilder) AsString() (str string) {
	panic("implement me")
}

func (b *BasePacketBuilder) WriteTo(w io.Writer) (n int, err error) {
	n, err = w.Write(b.Bytes())
	return
}

func (b *BasePacketBuilder) NextPktId(ctx *StateContext) uint16 {
	b.PacketIdentifer = ctx.Borrow()
	return b.PacketIdentifer
}

func (b *BasePacketBuilder) RequestResend() {
}

func (b *BasePacketBuilder) IsResending() bool {
	return false
}

func (b *BasePacketBuilder) firstByte(rt ReportType, DUP, RETAIN bool, qos QoSType) (b1 byte) {
	b1 = byte(rt << 4)
	if DUP {
		b1 |= 0x08
	}
	if RETAIN {
		b1 |= 0x01
	}
	b1 |= byte(qos) << 1
	return
}
