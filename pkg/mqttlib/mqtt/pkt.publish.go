/*
 * Copyright © 2020 Hedzr Yeh.
 */

package mqtt

import (
	"bytes"
	"fmt"
	"github.com/sirupsen/logrus"
	"gitlab.com/hedzr/mqttlib/pkg/mqttlib/mqtt/codec"
	"io"
)

// ....................................................

func NewPublishBuilder(topicName string, msg []byte, opts ...PublishBuilderOpt) PacketBuilder {
	builder := &publishBuilder{
		BasePacketBuilder: newBasePacketBuilder("publishBuilder"),
		vh50ext:           *newvh50ext(),
		DUP:               false,
		QoS:               QoSDefault,
		RETAIN:            false,
		topicName:         topicName,
		msg:               msg,
		protocolLevel:     ProtocolLevelForV50, // temporary initialized
	}
	for _, opt := range opts {
		opt(builder)
	}
	return builder
}

// ....................................................

// WithPubQoS attach these flags to a publish packet builder: DUP, qos, RETAIN.
//
// For a recent publish packet, DUP must be true (3.3.1-1).
// For a QoS 0 packet, DUP must be false (3.3.1-2).
//
// For server publishing, DUP must be false except that recent it after
// the first sent failed (3.3.1-3).
//
// For server publishing, see also:
// 1. on publish packet received from a client, server will send back with
//    a publish ack packet if qos 1. In this case, refer to pkt.puback.go
//    and pubackBuilder.OnAction.
// 2. with a qos 2 publish packet received, refer to pkt.pubrec, pkt.pubrel,
//    and pub.comp.go for the further implementations.
// 3. for a qos 0 publish packet received, publishBuilder act as a
//    publishParser, so check the codes at publishBuilder.OnAction and
//    ctx.bizPublish()
func WithPubQoS(DUP bool, qos QoSType, RETAIN bool) PublishBuilderOpt {
	return func(builder *publishBuilder) {
		builder.DUP = DUP
		builder.QoS = qos
		builder.RETAIN = RETAIN
	}
}

func WithPubProtocolLevel(level ProtocolLevel) PublishBuilderOpt {
	return func(builder *publishBuilder) {
		builder.protocolLevel = level
	}
}

func WithPubPacketIdentifier(id uint16) PublishBuilderOpt {
	return func(builder *publishBuilder) {
		builder.PacketIdentifer = id
	}
}

func WithPub50payloadFormatIndicator(i byte) PublishBuilderOpt {
	return func(builder *publishBuilder) {
		builder.payloadFormatIndicator = i
	}
}

func WithPub50messageExpiryInterval(i int32) PublishBuilderOpt {
	return func(builder *publishBuilder) {
		builder.messageExpiryInterval = i
	}
}

func WithPub50contentType(i string) PublishBuilderOpt {
	return func(builder *publishBuilder) {
		builder.contentType = i
	}
}

func WithPub50responseTopic(i string) PublishBuilderOpt {
	return func(builder *publishBuilder) {
		builder.responseTopic = i
	}
}

func WithPub50correlationData(i []byte) PublishBuilderOpt {
	return func(builder *publishBuilder) {
		builder.correlationData = i
	}
}

func WithPub50subscriptionIdentifier(i string) PublishBuilderOpt {
	return func(builder *publishBuilder) {
		builder.subscriptionIdentifier = i
	}
}

func WithPub50topicAlias(i uint16) PublishBuilderOpt {
	return func(builder *publishBuilder) {
		builder.topicAlias = i
	}
}

func WithPub50UserProperties(props map[string]string) PublishBuilderOpt {
	return func(builder *publishBuilder) {
		for k, v := range props {
			builder.userProperties[k] = v
		}
	}
}

// ....................................................

type PublishBuilderOpt func(*publishBuilder)

type publishBuilder struct {
	BasePacketBuilder
	vh50ext
	// to State

	DUP    bool
	QoS    QoSType
	RETAIN bool

	topicName string
	// PacketIdentifier uint16
	msg []byte

	protocolLevel ProtocolLevel

	b1   byte
	data []byte
	n    int
}

func (b *publishBuilder) IsResending() bool {
	return b.DUP
}

func (b *publishBuilder) RequestResend() {
	b.DUP = true
}

func (b *publishBuilder) GetToState() State {
	switch b.QoS {
	case QoS1:
		return PUB1_ACK
	case QoS2:
		return PUB2_REC
	default:
		return PUBLISHED
	}
}

func (b *publishBuilder) CreateVH() VariantHeader {
	return newPublishVH()
}

func (b *publishBuilder) CreatePayload() Payload {
	return newPublishPayload()
}

func newPublishVH() *PublishVH {
	return &PublishVH{
		vh50ext: *newvh50ext(),
	}
}

func newPublishPayload() *PublishPayload {
	return &PublishPayload{}
}

func (b *publishBuilder) OnAction(ctx *StateContext, pkg *Pkg) (err error) {
	b.QoS = pkg.QoS
	b.RETAIN = pkg.RETAIN
	if ctx.IsNotClientMode() {
		if logrus.IsLevelEnabled(logrus.DebugLevel) {
			if vh, ok := pkg.VH.(*PublishVH); ok {
				if payload, ok := pkg.Payload.(*PublishPayload); ok {
					b.Debug("       PUBLISH: pktid=%v, DUP=%v, QoS=%v, RETAIN=%v, payload: %v bytes", vh.PacketIdentifier, pkg.DUP, pkg.QoS, pkg.RETAIN, len(payload.Data))
				}
			}
		}

		switch pkg.QoS {
		case 1:
			// send back puback
			err = b.sendPubAckOrRec(ctx, pkg)
			if err == nil {
				err = ctx.bizPublish(pkg)
			}
		case 2:
			err = b.sendPubAckOrRec(ctx, pkg)
			if err == nil {
				err = ctx.storeAsTask(pkg)
			}
		default:
			if ctx.IsNotClientMode() {
				// at this time, we must publish the pkt to all subscribers.
				err = ctx.bizPublish(pkg)
			}
		}
	}
	return
}

func (b *publishBuilder) OnError(ctx *StateContext, pkg *Pkg, reason error) (err error) {
	err = reason
	b.Wrong(err, "found error")
	return
}

// sendPubAckOrRec for qos 1 and qos 2 step 1, send back puback or pubrec
func (b *publishBuilder) sendPubAckOrRec(ctx *StateContext, pkg *Pkg) (err error) {
	if !ctx.CanWrite() {
		return // when there are no writer in ctx or receiving publish at client, it's no need to send ack
	}

	if vh, ok := pkg.VH.(*PublishVH); ok {
		if payload, ok := pkg.Payload.(*PublishPayload); ok {
			b.Debug("       > PUBLISH (qos 1/2): pktid=%v, DUP=%v, QoS=%v, RETAIN=%v, payload: %v bytes", vh.PacketIdentifier, pkg.DUP, pkg.QoS, pkg.RETAIN, len(payload.Data))

			if pkg.QoS == 1 || pkg.QoS == 2 {
				// publish & puback: if QoS >= 1, send PUBACK; else if QoS >= 2, send PUBREC
				var builder PacketBuilder
				if pkg.QoS == QoS1 {
					builder = NewPub10ackBuilderFromPkg(pkg)
				} else {
					builder = NewPub21recBuilderFromPkg(pkg)
				}

				if err = builder.Build(ctx, pkg); err != nil {
					b.Wrong(err, "can't build PUBACK/PUBREC packet")
					return
				}

				if _, err = ctx.Write(builder.Bytes()); err != nil {
					b.Wrong(err, "can't send PUBACK/PUBREC packet")
					return
				} else {
					b.Debug("    ▲ MQTT.%s %v", builder.ReportType(), builder.Bytes())
				}
			}
		}
	}
	return
}

func (b *publishBuilder) ReportType() ReportType {
	return PUBLISH
}

func (b *publishBuilder) Bytes() (bytes []byte) {
	return b.data[:b.n]
}

func (b *publishBuilder) WriteTo(w io.Writer) (n int, err error) {
	n, err = w.Write(b.Bytes())
	return
}

func (b *publishBuilder) AsString() (str string) {
	str = fmt.Sprintf("[%s ↙︎ %s]", b.topicName, string(b.msg))
	return
}

func (b *publishBuilder) Build(ctx *StateContext, pkg *Pkg) (err error) {
	bb := bytes.NewBuffer([]byte{})

	b.b1 = b.firstByte(b.ReportType(), b.DUP, b.RETAIN, b.QoS)
	// b.b1 = byte(b.ReportType() << 4)
	// if b.DUP {
	// 	b.b1 |= 0x08
	// }
	// if b.RETAIN {
	// 	b.b1 |= 0x01
	// }
	// b.b1 |= byte(b.QoS) << 1

	// for i := 0; i < 5; i++ {
	// 	err = bb.WriteByte(b.b1)
	// 	if err != nil {
	// 		return
	// 	}
	// }

	// vh

	var vhBuf bytes.Buffer

	err = codec.WriteString(&vhBuf, b.topicName)
	if err != nil {
		return
	}

	if b.QoS > 0 {
		if b.PacketIdentifer <= 0 {
			b.PacketIdentifer = ctx.Borrow()
		}
		err = codec.WriteBEUint16(&vhBuf, b.PacketIdentifer)
		if err != nil {
			return
		}
	}

	if b.protocolLevel >= ProtocolLevelForV50 {
		var buf bytes.Buffer
		if buf, err = b.buildv5(ctx, pkg); err != nil {
			return
		}
		vhBuf.Write(buf.Bytes())
	}

	// payload

	// err = mqtt.WriteBEUint16(bb, uint16(len(b.filters)))
	// if err != nil {
	// 	return
	// }

	bb.WriteByte(b.b1)
	l := vhBuf.Len() + len(b.msg)
	_, err = codec.EncodeIntToW(bb, l)
	bb.Write(vhBuf.Bytes())
	_, err = bb.Write(b.msg)
	if err != nil {
		return
	}

	b.data = bb.Bytes()
	b.n = bb.Len()

	logrus.Tracef("PUB.BDR: %v - len: %v, data: % x", ctx.ConnectParam.ProtocolLevel, b.n, b.data)
	return
}

func (b *publishBuilder) buildv5(ctx *StateContext, pkg *Pkg) (buf bytes.Buffer, err error) {
	// for MQTT v5.0

	// construct the properties buffer at first

	var propertiesBuf bytes.Buffer
	propertiesBuf, err = b.WriteWithOrder(0x01, 0x02, 0x03, 0x08, 0x09, 0x0b, 0x23, 0x26)
	if err != nil {
		return
	}

	var length int
	var buf8 []byte = make([]byte, 8)
	length = codec.EncodeIntToBuf(propertiesBuf.Len(), buf8, 0)

	// vh
	buf.Write(buf8[:length])         // properties length
	buf.Write(propertiesBuf.Bytes()) // properties buffer
	return
}
