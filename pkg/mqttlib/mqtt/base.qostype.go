/*
 * Copyright © 2020 Hedzr Yeh.
 */

package mqtt

const (
	QoSDefault QoSType = iota
	QoS1
	QoS2
	QoSError
	QoSFailure QoSType = 0x80 // for suback
)

type QoSType int

func (n QoSType) String() string {
	if x, ok := QoSToString[n]; ok {
		return x
	}
	return QoSToString[QoSError]
}

func (n QoSType) FromString(s string) QoSType {
	if x, ok := QoSFromString[s]; ok {
		return x
	}
	return QoSError
}

var (
	QoSToString = map[QoSType]string{
		QoSDefault: "QoSDefault",
		QoS1:       "QoS1",
		QoS2:       "QoS2",
		QoSError:   "QoSError",
		QoSFailure: "QoSFailure",
	}
	QoSFromString = map[string]QoSType{
		"QoSDefault": QoSDefault,
		"QoS1":       QoS1,
		"QoS2":       QoS2,
		"QoSError":   QoSError,
		"QoSFailure": QoSFailure,
	}
)
