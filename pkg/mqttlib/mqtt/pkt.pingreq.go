/*
 * Copyright © 2020 Hedzr Yeh.
 */

package mqtt

import (
	"io"
)

func NewPingreqBuilder() PacketBuilder {
	builder := &pingreqBuilder{
		data: []byte{0xc0, 0x00},
	}
	return builder
}

type pingreqBuilder struct {
	data []byte
}

func (b *pingreqBuilder) IsResending() bool {
	return false
}

func (b *pingreqBuilder) RequestResend() {
}

func (b *pingreqBuilder) NextPktId(ctx *StateContext) uint16 {
	return 0
}

func (b *pingreqBuilder) ReportType() ReportType {
	return PINGREQ
}

func (b *pingreqBuilder) WriteTo(w io.Writer) (n int, err error) {
	n, err = w.Write(b.Bytes())
	return
}

func (b *pingreqBuilder) Bytes() (bytes []byte) {
	return b.data
}

func (b *pingreqBuilder) AsString() (str string) {
	return
}

func (b *pingreqBuilder) Build(ctx *StateContext, pkg *Pkg) (err error) {
	return
}
