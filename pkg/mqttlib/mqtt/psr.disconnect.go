/*
 * Copyright © 2020 Hedzr Yeh.
 */

package mqtt

import (
	"gitlab.com/hedzr/mqttlib/pkg/mqttlib/errors"
)

// ....................................................

func NewDisconnectParser(to State) PacketParser {
	return &disconnectParser{base: newBase("disconnectParser"), to: to}
}

type disconnectParser struct {
	base
	to                   State
	workingProtocolLevel ProtocolLevel
}

func (p *disconnectParser) GetToState() State {
	return p.to
}

func (p *disconnectParser) CreateVH() VariantHeader {
	return newDisconnect50VH()
}

func (p *disconnectParser) CreatePayload() Payload {
	return newDisconnect50Payload()
}

func (p *disconnectParser) OnAction(ctx *StateContext, pkg *Pkg) (err error) {
	if vh50, ok := pkg.VH.(*disconnect50VH); ok {
		p.With2("VH", vh50, "PAYLOAD", pkg.Payload).Debug("       %s-V50 received: close context...", pkg.ReportType)
	} else {
		p.Debug("       %s: close context...", pkg.ReportType)
	}

	ctx.normalClose = true                        // 3.1.2-10, about cancelling any will msg
	err = ctx.CloseWithReason(errors.CloseNormal) // for all: 3.14.1-1, for client: 3.14.1-1, 3.14.4-2
	if err == nil {
		ctx.normalClose = true // for server: 3.14.4-3, about cancelling any will msg
	} else {
		ctx.normalClose = false
	}
	return
}

func (p *disconnectParser) OnError(ctx *StateContext, pkg *Pkg, reason error) (err error) {
	err = reason
	return
}

func newDisconnect50VH() *disconnect50VH {
	return &disconnect50VH{
		reason:  errors.CloseNormal,
		vh50ext: *newvh50ext(),
	}
}

type disconnect50VH struct {
	reason errors.CloseReason
	vh50ext
}

func (s *disconnect50VH) Apply(ctx *StateContext, pkg *Pkg, data []byte, pos int) (newPos int, err error) {
	if pos < len(data) {
		s.v50length = len(data) - pos + 1
		s.reason = errors.CloseReason(data[pos])
		pos++

		// if pos+4 >= len(data) {
		// 	err = errors.ErrOverflow
		// 	return
		// }

		if pos < len(data) && ctx.ConnectParam.ProtocolLevel >= ProtocolLevelForV50 {
			pos, err = s.vh50ext.Apply(ctx, pkg, data, pos)
		}

		// // s.propertiesLength, pos, err = codec.ReadBEInt32(data, pos)
		// var ret, eat int
		// ret, eat = codec.DecodeIntFrom(data, pos)
		// s.propertiesLength, pos = int32(ret), pos+eat
		//
		// if pos < len(data) && err == nil && s.propertiesLength > 0 {
		// 	for {
		// 		id := data[pos]
		// 		pos++
		// 		switch id {
		// 		case 0x11: // session expiry interval
		// 			if pos+4 >= len(data) {
		// 				err = errors.ErrOverflow
		// 				return
		// 			}
		// 			s.sessionExpiryInterval, pos, err = codec.ReadBEInt32(data, pos)
		// 		case 0x1c: // server reference string
		// 			s.serverReferenceId, pos, err = codec.ReadString(data, pos)
		// 		case 0x1f: // reason string
		// 			s.reasonString, pos, err = codec.ReadString(data, pos)
		// 		case 0x26: // user properties
		// 			var k, v string
		// 		retryReadKV:
		// 			k, pos, err = codec.ReadString(data, pos)
		// 			if err == nil {
		// 				v, pos, err = codec.ReadString(data, pos)
		// 				if err == nil {
		// 					s.userProperties[k] = v
		// 					if pos == len(data) {
		// 						break
		// 					}
		// 					goto retryReadKV
		// 				}
		// 			}
		// 		}
		// 		if err != nil {
		// 			return
		// 		}
		// 		if pos == len(data) {
		// 			break
		// 		}
		// 	}
		// }
	}

	newPos = pos // Apply() must return newPos to pointer the updated new position
	pkg.VH = s   // Apply() must assign itself into pkg.VH
	return
}

func newDisconnect50Payload() *disconnect50Payload {
	return &disconnect50Payload{}
}

type disconnect50Payload struct {
}

func (s *disconnect50Payload) Apply(ctx *StateContext, pkg *Pkg, data []byte, pos int) (newPos int, err error) {
	newPos = pos    // Apply() must return newPos to pointer the updated new position
	pkg.Payload = s // Apply() must be assign itself into pkg.Payload
	return
}
