/*
 * Copyright © 2020 Hedzr Yeh.
 */

package mqtt

import (
	"regexp"
	"testing"
)

func TestProtocolLevelConstants(t *testing.T) {
	for i := ProtocolLevel0; i <= ProtocolLevelForV50; i++ {
		str := i.String()
		if matched, err := regexp.Match(`ProtocolLevel\(\d+\)`, []byte(str)); err == nil && !matched {
			c := ProtocolLevel0.Parse(str)
			t.Logf("  - %x (%d) => %q", int(c), int(c), str)
		}
	}
}
