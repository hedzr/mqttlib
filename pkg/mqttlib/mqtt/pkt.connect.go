/*
 * Copyright © 2020 Hedzr Yeh.
 */

package mqtt

import (
	"bytes"
	"gitlab.com/hedzr/mqttlib/pkg/mqttlib/mqtt/cid"
	"gitlab.com/hedzr/mqttlib/pkg/mqttlib/mqtt/codec"
	"io"
)

type ConnectBuilderOpt func(*connectBuilder)

type connectBuilder struct {
	// BasePacketBuilder

	DUP    bool
	QoS    QoSType
	RETAIN bool

	cleanSessionSet bool
	keepAliveSet    bool

	ProtocolLevel     ProtocolLevel
	ConnectFlag       byte
	KeepAlivedSeconds uint16 // in seconds

	willTopic   string
	willMessage []byte
	Password    string
	Username    string

	ClientId string
	// PacketIdentifier uint16
	payload []byte

	b1   byte
	data []byte
	n    int
}

func (b *connectBuilder) IsResending() bool {
	return b.DUP
}

func (b *connectBuilder) RequestResend() {
	b.DUP = true
}

func (b *connectBuilder) NextPktId(ctx *StateContext) uint16 {
	return 0
}

func (b *connectBuilder) ReportType() ReportType {
	return CONNECT
}

func (b *connectBuilder) WriteTo(w io.Writer) (n int, err error) {
	n, err = w.Write(b.Bytes())
	return
}

func (b *connectBuilder) Bytes() (bytes []byte) {
	return b.data[:b.n]
}

func (b *connectBuilder) AsString() (str string) {
	return
}

func (b *connectBuilder) Build(ctx *StateContext, pkg *Pkg) (err error) {
	bb := bytes.NewBuffer([]byte{})

	b.b1 = byte(b.ReportType() << 4)
	if b.DUP {
		b.b1 |= 0x08
	}
	if b.RETAIN {
		b.b1 |= 0x01
	}
	b.b1 |= byte(b.QoS) << 1

	for i := 0; i < 5; i++ {
		err = bb.WriteByte(b.b1)
		if err != nil {
			return
		}
	}

	// vh

	err = codec.WriteString(bb, "MQTT")
	if err != nil {
		return
	}
	err = bb.WriteByte(byte(b.ProtocolLevel))
	if err != nil {
		return
	}
	err = bb.WriteByte(b.ConnectFlag)
	if err != nil {
		return
	}
	err = codec.WriteBEUint16(bb, b.KeepAlivedSeconds)
	if err != nil {
		return
	}

	// payload

	if ctx.ConnectParam == nil {
		ctx.ConnectParam = &ConnectParam{}
	}
	if len(b.ClientId) == 0 {
		b.ClientId = ctx.ConnectParam.ClientId
	}
	if len(b.ClientId) == 0 {
		b.ClientId = cid.CreateNewClientId()
		ctx.ConnectParam.ClientId = b.ClientId
	}
	err = codec.WriteString(bb, b.ClientId)
	if err != nil {
		return
	}

	if b.ConnectFlag&4 != 0 {
		if len(b.willTopic) > 0 {
			err = codec.WriteString(bb, b.willTopic)
			if err != nil {
				return
			}
		}
		if len(b.willMessage) >= 0 {
			_, err = bb.Write(b.willMessage)
			if err != nil {
				return
			}
		}
	}
	if b.ConnectFlag&0x40 != 0 {
		if len(b.Password) > 0 {
			err = codec.WriteString(bb, b.Password)
			if err != nil {
				return
			}
		}
	}
	if b.ConnectFlag&0x80 != 0 {
		if len(b.Username) > 0 {
			err = codec.WriteString(bb, b.Username)
			if err != nil {
				return
			}
		}
	}

	l := bb.Len() - 5
	b.data = bb.Bytes()
	bl := codec.EncodeIntToBuf(l, b.data, 1)
	copy(b.data[bl+1:], b.data[5:])
	b.n = l + 1 + bl
	return
}
