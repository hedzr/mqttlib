/*
 * Copyright © 2020 Hedzr Yeh.
 */

package mqtt

import (
	"gitlab.com/hedzr/mqttlib/pkg/mqttlib/errors"
	"gitlab.com/hedzr/mqttlib/pkg/mqttlib/mqtt/codec"
	"strings"
)

// ....................................................

func NewSubscribeParser(to State) PacketParser {
	return &subscribeParser{base: newBase("subscribeParser"), to: to}
}

type subscribeParser struct {
	base
	to State
}

func (p *subscribeParser) GetToState() State {
	return p.to
}

func (p *subscribeParser) CreateVH() VariantHeader {
	return newSubscribeVH()
}

func (p *subscribeParser) CreatePayload() Payload {
	return newSubscribePayload()
}

func (p *subscribeParser) OnAction(ctx *StateContext, pkg *Pkg) (err error) {
	if vh, ok := pkg.VH.(*subscribeVH); ok {
		if payload, ok := pkg.Payload.(*subscribePayload); ok {
			p.Debug("       %s: id=%v, payload.filters=%v, vh=%+v", pkg.ReportType, pkg.PacketIdentifier, payload.TopicFilters, vh)

			// var maxQoS = SubackOK
			var invoked QoSType
			// var retCodes []SubackRetcode
			// 3.8.4-4
			// 3.9.3-1
			for _, tf := range payload.TopicFilters {
				if invoked, err = ctx.sessionStore.Subscribe(ctx, ctx.ConnectParam.ClientId, tf, pkg.PacketIdentifier); err == nil {
					// retCodes = append(retCodes, SubackRetcode(invoked)) // 3.9.3-2
					if invoked < QoSType(tf.RequestedQoS) {
						// todo 3.8.4-6
					}
				} else if tf.RetCode == 0 {
					// retCodes = append(retCodes, subackFailure) // 3.8.4-5
					tf.RetCode = errors.CloseUnspecifiedReason // subackFailure, CloseUnspecifiedReason
				}
			}

			// subscribe: reply suback 3.8.4-1, 3.8.4-2
			var builder PacketBuilder = NewSubackBuilder(vh, payload, pkg.PacketIdentifier)
			if err = builder.Build(ctx, pkg); err != nil {
				p.Wrong(err, "can't build SUBACK packet")
				return
			}

			if _, err = ctx.Write(builder.Bytes()); err != nil {
				p.Wrong(err, "can't send package")
				return
			}

			p.Debug("    ▲ MQTT.%s % x (%v)", builder.ReportType(), builder.Bytes(), ctx.ConnectParam.ClientId)

		}
	}
	return
}

func (p *subscribeParser) OnError(ctx *StateContext, pkg *Pkg, reason error) (err error) {
	err = reason
	return
}

func newSubscribeVH() *subscribeVH {
	return &subscribeVH{}
}

type subscribeVH struct {
	vh50ext
}

func (s *subscribeVH) Apply(ctx *StateContext, pkg *Pkg, data []byte, pos int) (newPos int, err error) {
	if pkg.QoS != 1 {
		// 3.8.1-1
		err = errors.ErrCodePacketIllegal.New("in sub/unsub packet header, lo-byte must be 0,0,1,0")
		return
	}

	if pos < len(data) && ctx.ConnectParam.ProtocolLevel >= ProtocolLevelForV50 {
		// s.v50length =  len(data) - pos + 1
		pos, err = s.vh50ext.Apply(ctx, pkg, data, pos)
	}

	newPos = pos // Apply() must return newPos to pointer the updated new position
	pkg.VH = s   // Apply() must assign itself into pkg.VH
	return
}

type subscribePayload struct {
	TopicFilters []TopicFilter
}

func newSubscribePayload() *subscribePayload {
	return &subscribePayload{}
}

func (s *subscribePayload) Apply(ctx *StateContext, pkg *Pkg, data []byte, pos int) (newPos int, err error) {
	// var count uint16
	// count, pos, err = ReadBEUint16(data, pos)
	// if err != nil {
	// 	err = newErrorN(ErrCodePacketCorrupt, "can't read topic filter counter field")
	// 	return
	// }
	length := len(data)
	for pos < length {
		var tf TopicFilter
		tf.Filter, pos, err = codec.ReadString(data, pos) // 3.8.3-1
		if err != nil {
			err = errors.ErrCodePacketCorrupt.New("can't read topic filter string field")
			tf.RetCode = errors.CloseTopicFilterInvalid
			return
		}

		if ctx.noWildMatchFilter {
			if strings.ContainsAny(tf.Filter, "#+") {
				err = errors.ErrCodePacketCorrupt.New("wild-match chars ('#', '+') cannot be supported")
				tf.RetCode = errors.CloseUnsupportedWildcardSubscription
				// return
			}
		}

		// v5.0+
		if strings.HasPrefix(tf.Filter, "$shared/") {
			tf.Shared = true // $shared/SharedName/topic-filter
			a := strings.Split(tf.Filter[8:], "/")
			tf.SharedName = a[0]
			tf.Filter = strings.Join(a[1:], "/")
			if strings.ContainsAny(tf.Filter, "#+") {
				err = errors.ErrCodeBadSharedName.New("for shared name, wild-match chars ('#', '+') cannot be supported")
			}
		}

		if pos < length {
			var b = data[pos]
			if (b & 0xfc) != 0 {
				if ctx.ConnectParam.ProtocolLevel < ProtocolLevelForV50 {
					// 3.8.3-4, 3-8.3-4
					err = errors.ErrCodePacketCorrupt.New("requested QoS byte '%v' not ok: hi-order bit 7-2 must be 0", b)
					// return
				} else {
					// v5.0+
					tf.NoLocal = (b & 0x04) != 0 //
					if tf.Shared && tf.NoLocal {
						err = errors.ErrCodeProtocolError.New("no-local must be 0 in shared subscription mode")
					}
					tf.RetainAsPublished = (b & 0x08) != 0 //
					tf.RetainHandling = int(b>>4) & 0x03
					if (b & 0xc0) != 0 {
						err = errors.ErrCodePacketCorrupt.New("requested QoS byte '%v' not ok: hi-order bit 7-6 must be 0", b)
					}
				}
			}

			tf.RequestedQoS = QoSType(b & 0x03)
			if tf.RequestedQoS == 3 {
				// 3.8.3-4, 3-8.3-4
				err = errors.ErrCodePacketCorrupt.New("requested QoS byte '%v' not ok: QoS must be 0,1, or 2", tf.RequestedQoS)
				// return
			}

			pos++
		} else {
			err = errors.ErrCodePacketCorrupt.New("can't read requested QoS byte")
			return
		}

		s.TopicFilters = append(s.TopicFilters, tf)
	}

	if len(s.TopicFilters) == 0 {
		err = errors.ErrCodePacketIllegal.New("No topic filter, see 3.8.3-2") // 3.8.3-2
	}

	newPos = pos    // Apply() must return newPos to pointer the updated new position
	pkg.Payload = s // Apply() must be assign itself into pkg.Payload
	return
}
