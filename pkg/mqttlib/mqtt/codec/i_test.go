/*
 * Copyright © 2020 Hedzr Yeh.
 */

package codec

import (
	"bytes"
	"encoding/binary"
	"encoding/gob"
	"fmt"
	"testing"
	"time"
)

func TestA(t *testing.T) {
	buf := make([]byte, 10)
	ts := uint32(time.Now().Unix())
	binary.BigEndian.PutUint16(buf[0:], 0xa20c) // sensorID
	binary.BigEndian.PutUint16(buf[2:], 0x04af) // locationID
	binary.BigEndian.PutUint32(buf[4:], ts)     // timestamp
	binary.BigEndian.PutUint16(buf[8:], 479)    // temp
	t.Logf("buf: % x", buf)

	sensorID := binary.BigEndian.Uint16(buf[0:])
	locID := binary.BigEndian.Uint16(buf[2:])
	tstamp := binary.BigEndian.Uint32(buf[4:])
	temp := binary.BigEndian.Uint16(buf[8:])
	fmt.Printf("sid: %0#x, locID: %0#x, ts: %0#x, temp: %d\n",
		sensorID, locID, tstamp, temp)
}

type packet struct {
	Sensid uint32
	Locid  uint16
	Tstamp uint32
	Temp   int16
}

type packet2 struct {
	Sensid uint32
	Locid  uint16
}

func TestB(t *testing.T) {
	dataIn := packet{
		Sensid: 1, Locid: 1233, Tstamp: 123452123, Temp: 12,
	}
	buf := new(bytes.Buffer)
	err := binary.Write(buf, binary.BigEndian, dataIn)
	if err != nil {
		t.Error(err)
	}

	t.Logf("buf: % x", buf.Bytes())

	var dataOut packet
	err = binary.Read(buf, binary.BigEndian, &dataOut)
	if err != nil {
		t.Error(err)
	}
}

func TestC(t *testing.T) {
	dataOut := []packet{
		{Sensid: 1, Locid: 1233, Tstamp: 123452123, Temp: 12},
		{Sensid: 2, Locid: 4567, Tstamp: 133452124, Temp: 32},
		{Sensid: 7, Locid: 8910, Tstamp: 143452125, Temp: -12},
	}

	// encode a slice of packet
	buf := new(bytes.Buffer)
	err := binary.Write(buf, binary.LittleEndian, dataOut)
	if err != nil {
		t.Error(err)
	}

	t.Logf("buf: % x", buf.Bytes())

	// decode all items from slice
	dataIn := make([]packet, 3)
	err = binary.Read(buf, binary.LittleEndian, dataIn)
	if err != nil {
		t.Error(err)
	}

	t.Logf("dataIn: % d", dataIn)
}

func TestD(t *testing.T) {
	var xin = []string{"aa", "bb", "cc"}
	var err error

	b := new(bytes.Buffer)
	enc := gob.NewEncoder(b)
	dec := gob.NewDecoder(b)

	err = enc.Encode(xin)
	if err != nil {
		t.Error(err)
	}

	t.Logf("b: % x", b.Bytes())

	var xout []string
	err = dec.Decode(&xout)
	t.Logf("xout: %v", xout)
}

func TestE(t *testing.T) {
	var xin = map[string]string{"aa": "v", "bb": "x", "cc": "w"}
	var err error

	b := new(bytes.Buffer)
	enc := gob.NewEncoder(b)
	dec := gob.NewDecoder(b)

	err = enc.Encode(xin)
	if err != nil {
		t.Error(err)
	}

	t.Logf("b: % x", b.Bytes())

	var xout map[string]string
	err = dec.Decode(&xout)
	t.Logf("xout: %v", xout)
}

func TestF(t *testing.T) {
	var s = "string-fragment"
	var i = 9
	var err error

	b := new(bytes.Buffer)
	enc := gob.NewEncoder(b)
	dec := gob.NewDecoder(b)

	err = enc.Encode(s)
	if err != nil {
		t.Error(err)
	}
	err = enc.Encode(i)
	if err != nil {
		t.Error(err)
	}

	var v string
	err = dec.Decode(&v)
	if err != nil {
		t.Error(err)
	}
	t.Logf("v: %+v", v)
	var vi int
	err = dec.Decode(&vi)
	if err != nil {
		t.Error(err)
	}
	t.Logf("vi: %+v", vi)
}

func TestG(t *testing.T) {
	var xin = []packet2{{1, 2}, {3, 4}}
	var err error

	b := new(bytes.Buffer)
	enc := gob.NewEncoder(b)
	dec := gob.NewDecoder(b)

	err = enc.Encode(xin)
	if err != nil {
		t.Error(err)
	}

	t.Logf("b: % x", b.Bytes())

	var xout []packet2
	err = dec.Decode(&xout)
	t.Logf("xout: %v", xout)
}

// type P struct {
// 	X, Y, Z int
// 	Name    string
// }
//
// type Q struct {
// 	X, Y *int32
// 	Name string
// }

func a() {
	// var network bytes.Buffer
	// enc := gob.NewEncoder(&network)
	// dec := gob.NewDecoder(&network)
	// // Encode (send) the value.
	// err := enc.Encode(P{3, 4, 5, "Pythagoras"})
	// if err != nil {
	// 	log.Fatal("encode error:", err)
	// }
	// // Decode (receive) the value.
	// var q Q
	// err = dec.Decode(&q)
	// if err != nil {
	// 	log.Fatal("decode error:", err)
	// }
	// fmt.Println(q)
	// fmt.Printf("%q: {%d,%d}\n", q.Name, *q.X, *q.Y)
}

func b() {
	// var network bytes.Buffer
	// eeenc := gob.NewEncoder(&network)
	// eedec := gob.NewDecoder(&network)
	// var ss codec.Serializable
	// ss = s.(codec.Serializable)
	// ss.Load(eedec)
	// ss.Save(eeenc)
}
