/*
 * Copyright © 2020 Hedzr Yeh.
 */

package codec

import (
	"bufio"
	"bytes"
	"gitlab.com/hedzr/mqttlib/pkg/mqttlib/errors"
	"io"
)

// ReadBEUint16 reads a big-endian uint16 number
func ReadBEUint16(data []byte, pos int) (ret uint16, newPos int, err error) {
	newPos = pos
	if pos >= len(data) {
		return
	}

	var b byte
	b = data[pos]
	if pos >= len(data) {
		return
	}
	pos++
	ret = uint16(b) * 256
	b = data[pos]
	if pos >= len(data) {
		return
	}
	pos++
	ret += uint16(b)
	newPos = pos
	return
}

// ReadBEUint16 reads a big-endian uint16 number
func ReadBEUint16R(reader *bufio.Reader) (ret uint16, err error) {
	var b byte
	b, err = reader.ReadByte()
	if err != nil {
		return
	}
	ret = uint16(b) * 256
	b, err = reader.ReadByte()
	if err != nil {
		return
	}
	ret += uint16(b)
	return
}

// WriteBEUint16 writes a big-endian uint16 number
func WriteBEUint16(writer io.Writer, i uint16) (err error) {
	b := []byte{byte(i >> 8), byte(i & 0xff)}
	var n int
	n, err = writer.Write(b)
	if n != 2 && err == nil {
		err = errors.ErrCodeBadIO.New("can't write uint16 as |MSB|LSB| ... ")
	}
	return
}

// WriteBEUint16R writes a big-endian uint16 number
func WriteBEUint16R(writer bufio.Writer, i uint16) (err error) {
	b := byte(i >> 8)
	err = writer.WriteByte(b)
	if err != nil {
		return
	}
	b = byte(i & 0xff)
	err = writer.WriteByte(b)
	return
}

//

// ReadBEInt32 reads a big-endian four-byte-presented int32 number
func ReadBEInt32(data []byte, pos int) (ret int32, newPos int, err error) {
	var b byte
	b = data[pos]
	pos++
	ret += int32(b) * 16 * 16 * 16
	if pos >= len(data) {
		err = errors.ErrOverflow
		return
	}

	b = data[pos]
	pos++
	ret += int32(b) * 16 * 16
	if pos >= len(data) {
		err = errors.ErrOverflow
		return
	}

	b = data[pos]
	pos++
	ret += int32(b) * 16
	if pos >= len(data) {
		err = errors.ErrOverflow
		return
	}

	b = data[pos]
	pos++
	ret += int32(b)

	newPos = pos
	return
}

// WriteBEInt32 writes a big-endian four-byte-presented int32 number
func WriteBEInt32(buf io.Writer, i int32) (err error) {
	var bb = []byte{0}
	var b = &bb[0]
	*b = byte(i >> 24)
	_, err = buf.Write(bb)
	if err != nil {
		return
	}
	*b = byte(i >> 16)
	_, err = buf.Write(bb)
	if err != nil {
		return
	}
	*b = byte(i >> 8)
	_, err = buf.Write(bb)
	if err != nil {
		return
	}
	*b = byte(i)
	_, err = buf.Write(bb)
	return
}

// ReadBEInt32From reads a big-endian four-byte-presented int32 number
func ReadBEInt32From(reader *bytes.Buffer) (ret int32, err error) {
	var b byte
	b, err = reader.ReadByte()
	if err != nil {
		return
	}
	ret += int32(b) * 16 * 16 * 16
	b, err = reader.ReadByte()
	if err != nil {
		return
	}
	ret += int32(b) * 16 * 16
	b, err = reader.ReadByte()
	if err != nil {
		return
	}
	ret += int32(b) * 16
	b, err = reader.ReadByte()
	if err != nil {
		return
	}
	ret += int32(b)
	return
}

// WriteBEInt32To writes a big-endian four-byte-presented int32 number
func WriteBEInt32To(buf *bytes.Buffer, i int32) (err error) {
	var b byte
	b = byte(i >> 24)
	err = buf.WriteByte(b)
	if err != nil {
		return
	}
	b = byte(i >> 16)
	err = buf.WriteByte(b)
	if err != nil {
		return
	}
	b = byte(i >> 8)
	err = buf.WriteByte(b)
	if err != nil {
		return
	}
	b = byte(i)
	err = buf.WriteByte(b)
	return
}
