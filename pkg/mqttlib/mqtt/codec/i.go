/*
 * Copyright © 2020 Hedzr Yeh.
 */

package codec

type Encoder interface {
	Encode(v interface{}) (err error)
}

type Decoder interface {
	Decode(v interface{}) (err error)
}

type Serializable interface {
	Save(enc Encoder) (err error)
	Load(dec Decoder) (err error)
}
