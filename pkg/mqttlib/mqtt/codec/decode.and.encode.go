/*
 * Copyright © 2020 Hedzr Yeh.
 */

package codec

import (
	"bufio"
	"bytes"
	"io"
)

func DecodeInt(p []byte) (ret, eat int) {
	var v int
	var bound = int(mqttBound)
	ret = int(uint(p[eat]))
	for i := eat; i < len(p); i++ {
		if ret > bound-1 {
			ret -= bound
			eat++
			v = int(uint64(p[eat])) * bound
			ret += v
			bound *= int(mqttBound)
		} else {
			break
		}
	}
	// if ret > 16384 {
	// 	ret -= 16384
	// 	eat++
	// 	v = int(uint(p[eat])) * 128 * 128
	// 	ret += v
	// }
	// if ret > 2097152 {
	// 	ret -= 2097152
	// 	eat++
	// 	v = int(uint(p[eat])) * 128 * 128 * 128
	// 	ret += v
	// }
	eat++
	return
}

func DecodeIntFrom(p []byte, pos int) (ret int, eat int) {
	var v int
	var bound int = int(mqttBound)
	ret = int(uint(p[pos]))
	for i := eat; i < len(p); i++ {
		if ret > bound-1 {
			ret -= bound
			eat++
			pos++
			v = int(uint(p[pos])) * bound
			ret += v
			bound *= int(mqttBound)
		} else {
			break
		}
	}
	eat++
	return
}

func DecodeIntWithBuffer(b1 byte, reader *bytes.Buffer) (ret int, eat int, err error) {
	var v int
	var bound = int(mqttBound)
	var bb byte
	ret = int(b1)
	for i := 0; i < 4; i++ {
		if ret > bound-1 {
			ret -= bound
			bb, err = reader.ReadByte()
			eat++
			if err != nil {
				return
			}
			v = int(bb) * bound
			ret += v
			bound *= int(mqttBound)
		} else {
			break
		}
	}
	return
}

// DecodeIntAndCopyToWithReader decode a integer from `reader`, and copy it to `buf[pos:]` as is.
func DecodeIntAndCopyToWithReader(b1 byte, reader *bufio.Reader, buf []byte, pos int) (ret, eat int, err error) {
	var v int
	var bound = int(mqttBound)
	var bb byte
	ret = int(b1)
	for i := 0; i < 4; i++ {
		if ret > bound-1 {
			ret -= bound
			bb, err = reader.ReadByte()
			eat++
			if err != nil {
				return
			}
			buf[pos] = bb
			pos++
			v = int(bb) * bound
			ret += v
			bound *= int(mqttBound)
		} else {
			break
		}
	}
	return
}

func DecodeInt64(p []byte) (ret int64, eat int) {
	var v int64
	var bound int64 = int64(mqttBound)
	ret = int64(uint64(p[eat]))
	for i := eat; i < len(p); i++ {
		if ret > bound-1 {
			ret -= bound
			eat++
			v = int64(uint64(p[eat])) * bound
			ret += v
			bound *= int64(mqttBound)
		} else {
			break
		}
	}
	eat++
	return
}

func DecodeInt64From(p []byte, pos int) (ret int64, eat int) {
	var v int64
	var bound int64 = int64(mqttBound)
	ret = int64(uint64(p[pos]))
	for i := eat; i < len(p); i++ {
		if ret > bound-1 {
			ret -= bound
			eat++
			pos++
			v = int64(uint64(p[pos])) * bound
			ret += v
			bound *= int64(mqttBound)
		} else {
			break
		}
	}
	eat++
	return
}

func DecodeUint64(p []byte) (ret uint64, eat int) {
	var v uint64
	var bound uint64 = mqttBound
	ret = uint64(p[eat])
	for i := eat; i < len(p); i++ {
		if ret > bound-1 {
			ret -= bound
			eat++
			v = uint64(p[eat]) * bound
			ret += v
			bound *= uint64(mqttBound)
		} else {
			break
		}
	}
	eat++
	return
}

func DecodeUint64WithReader(b1 byte, reader *bufio.Reader) (ret uint64, err error) {
	var v uint64
	var bound uint64 = mqttBound
	var bb byte
	ret = uint64(b1)
	for i := 0; i < 4; i++ {
		if ret > bound-1 {
			ret -= bound
			bb, err = reader.ReadByte()
			if err != nil {
				return
			}
			v = uint64(bb) * bound
			ret += v
			bound *= uint64(mqttBound)
		} else {
			break
		}
	}
	return
}

func DecodeUint16WithReader(b1 byte, reader *bufio.Reader) (ret uint16, ate int, err error) {
	var v uint16
	var bound uint16 = uint16(mqttBound)
	var bb byte
	ret = uint16(b1)
	ate = 1
	for i := 0; i < 2; i++ {
		if ret > bound-1 {
			ret -= bound
			bb, err = reader.ReadByte()
			if err != nil {
				return
			}
			ate++
			v = uint16(bb) * bound
			ret += v
			bound *= uint16(mqttBound)
		} else {
			break
		}
	}
	return
}

// EncodeInt
// NOTE: for performance, use Assembly and native machine codes here.
func EncodeInt(value int) (ret []byte) {
	if value == 0 {
		return zeroValueBuffer
	}

	var pos = 0
	// var cf bool
	for ; value > 0; pos++ {
		b := value % int(mqttBound)
		value = value / int(mqttBound)
		if value > 0 {
			b = b | int(mqttBound)
		}
		staticBuffer[pos] = byte(b)
	}
	ret = staticBuffer[:pos]
	return
}

func EncodeIntTo(w bufio.Writer, value int) (n int, err error) {
	if value == 0 {
		if err = w.WriteByte(0); err == nil {
			n++
		}
		return
	}

	var pos = 0
	// var cf bool
	for ; value > 0; pos++ {
		b := value % int(mqttBound)
		value = value / int(mqttBound)
		if value > 0 {
			b = b | int(mqttBound)
		}
		if err = w.WriteByte(byte(b)); err == nil {
			n++
		}
	}
	return
}

func EncodeIntToW(w io.Writer, value int) (n int, err error) {
	if value == 0 {
		if _, err = w.Write([]byte{0}); err == nil {
			n++
		}
		return
	}

	var pos = 0
	// var cf bool
	for ; value > 0; pos++ {
		b := value % int(mqttBound)
		value = value / int(mqttBound)
		if value > 0 {
			b = b | int(mqttBound)
		}
		if _, err = w.Write([]byte{byte(b)}); err == nil {
			n++
		}
	}
	return
}

func EncodeIntToBuf(value int, data []byte, pos int) (n int) {
	if value == 0 {
		data[pos] = 0
		n++
		return
	}

	// var pos = 0
	// var cf bool
	for ; value > 0; pos++ {
		b := value % int(mqttBound)
		value = value / int(mqttBound)
		if value > 0 {
			b = b | int(mqttBound)
		}
		data[pos] = byte(b)
		n++
	}
	return
}

// func EncodeIntToBytesBuffer(value int, buf bytes.Buffer) (n int) {
// 	if value == 0 {
// 		buf.WriteByte(0)
// 		n++
// 		return
// 	}
//
// 	for ; value > 0; n++ {
// 		b := value % int(mqttBound)
// 		value = value / int(mqttBound)
// 		if value > 0 {
// 			b = b | int(mqttBound)
// 		}
// 		buf.WriteByte(byte(b))
// 	}
// 	return
// }

func EncodeInt64(value int64) (ret []byte) {
	if value == 0 {
		return zeroValueBuffer
	}

	var pos = 0
	// var cf bool
	for ; value > 0; pos++ {
		b := value % int64(mqttBound)
		value = value / int64(mqttBound)
		if value > 0 {
			b = b | int64(mqttBound)
		}
		staticBuffer[pos] = byte(b)
	}
	ret = staticBuffer[:pos]
	return
}

func EncodeUint64(value uint64) (ret []byte) {
	if value == 0 {
		return zeroValueBuffer
	}

	var pos = 0
	// var cf bool
	for ; value > 0; pos++ {
		b := value % uint64(mqttBound)
		value = value / uint64(mqttBound)
		if value > 0 {
			b = b | uint64(mqttBound)
		}
		staticBuffer[pos] = byte(b)
	}
	ret = staticBuffer[:pos]
	return
}

// todo thread-safe on staticBuffer
var staticBuffer = make([]byte, 16)

var zeroValueBuffer = []byte{0}

var zeroBuffer []byte

const mqttBound uint64 = 128
