/*
 * Copyright © 2020 Hedzr Yeh.
 */

package codec

import (
	"gitlab.com/hedzr/mqttlib/pkg/mqttlib/errors"
	"io"
)

func ReadString(data []byte, pos int) (ret string, newPos int, err error) {
	var l uint16
	l, pos, err = ReadBEUint16(data, pos)
	if err != nil {
		return
	}

	newPos = pos + int(l)
	if newPos <= len(data) {
		ret = string(data[pos:newPos])
	} else {
		err = errors.ErrCodePacketCorrupt.New("can't read string from: %v", data[pos-2:])
	}
	return
}

func WriteString(writer io.Writer, s string) (err error) {
	l := len(s)
	err = WriteBEUint16(writer, uint16(l))
	if err != nil {
		return
	}
	var n int
	n, err = writer.Write([]byte(s))
	if n != l && err == nil {
		err = errors.ErrCodeBadIO.New("can't write string")
	}
	return
}
