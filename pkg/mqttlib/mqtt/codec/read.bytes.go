/*
 * Copyright © 2020 Hedzr Yeh.
 */

package codec

import "gitlab.com/hedzr/mqttlib/pkg/mqttlib/errors"

func ReadBytes(data []byte, pos int) (ret []byte, newPos int, err error) {
	var l uint16
	l, pos, err = ReadBEUint16(data, pos)
	if err != nil {
		return
	}

	newPos = pos + int(l)
	if newPos <= len(data) {
		ret = data[pos:newPos]
	} else {
		err = errors.ErrCodePacketCorrupt.New("can't read bytes from: %v", data[pos-2:])
	}
	return
}
