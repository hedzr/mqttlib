/*
 * Copyright © 2020 Hedzr Yeh.
 */

package mqtt

func NewWrongPacketParser() PacketParser {
	return &WrongPacketParser{}
}

type WrongPacketParser struct{}

func (p *WrongPacketParser) GetToState() State {
	return WRONG_STATE
}

func (p *WrongPacketParser) CreateVH() VariantHeader {
	return nil
}

func (p *WrongPacketParser) CreatePayload() Payload {
	return nil
}

func (p *WrongPacketParser) OnAction(ctx *StateContext, pkg *Pkg) (err error) {
	return nil
}

func (p *WrongPacketParser) OnError(ctx *StateContext, pkg *Pkg, reason error) (err error) {
	err = reason
	return
}
