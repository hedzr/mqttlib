/*
 * Copyright © 2020 Hedzr Yeh.
 */

package mqtt

import (
	"bytes"
	"encoding/gob"
	"fmt"
	"github.com/sirupsen/logrus"
	"time"
)

type qp struct {
	qos                 QoSType
	packetIdentifier    uint16
	wantedProtocolLevel ProtocolLevel
}

func newQP(wantedProtocolLevel ProtocolLevel, qos QoSType, packetIdentifier uint16) *qp {
	return &qp{
		qos:                 qos,
		packetIdentifier:    packetIdentifier,
		wantedProtocolLevel: wantedProtocolLevel,
	}
}

func (s qp) String() string {
	return fmt.Sprintf("%v,%v,#%v", s.wantedProtocolLevel, s.qos, s.packetIdentifier)
}

func (s qp) MarshalBinary() (ret []byte, err error) {
	var b bytes.Buffer
	enc := gob.NewEncoder(&b)
	// _, err = fmt.Fprintln(&b, s.qos, s.packetIdentifier)
	err = enc.Encode(s.qos)
	if err == nil {
		err = enc.Encode(s.packetIdentifier)
	}
	ret = b.Bytes()
	return
}

func (s *qp) UnmarshalBinary(data []byte) (err error) {
	b := bytes.NewBuffer(data)
	dec := gob.NewDecoder(b)
	// _, err = fmt.Fscanln(b, &s.qos, &s.packetIdentifier)
	err = dec.Decode(&s.qos)
	if err == nil {
		err = dec.Decode(&s.packetIdentifier)
	}
	return
}

type Pubinfo struct {
	data             []byte
	publishingQoS    QoSType
	sentClients      map[string]*qp
	published        int32
	publisher        string
	packetIdentifier uint16 // pi when publisher published
	timestamp        time.Time
}

func (s Pubinfo) MarshalBinary() (ret []byte, err error) {
	var b bytes.Buffer
	enc := gob.NewEncoder(&b)
	// _, err = fmt.Fprintln(&b, s.qos, s.packetIdentifier)

	err = enc.Encode(s.data)
	if err == nil {
		err = enc.Encode(s.publishingQoS)
		if err == nil {
			err = enc.Encode(s.sentClients)
			if err == nil {
				err = enc.Encode(s.published)
				if err == nil {
					err = enc.Encode(s.publisher)
					if err == nil {
						err = enc.Encode(s.packetIdentifier)
						if err == nil {
							err = enc.Encode(s.timestamp)
						}
					}
				}
			}
		}
	}

	if err != nil {
		logrus.Debugf("error: %v", err)
	}
	ret = b.Bytes()
	return
}

func (s *Pubinfo) UnmarshalBinary(data []byte) (err error) {
	b := bytes.NewBuffer(data)
	dec := gob.NewDecoder(b)
	// _, err = fmt.Fscanln(b, &s.qos, &s.packetIdentifier)

	err = dec.Decode(&s.data)
	if err == nil {
		err = dec.Decode(&s.publishingQoS)
		if err == nil {
			err = dec.Decode(&s.sentClients)
			if err == nil {
				err = dec.Decode(&s.published)
				if err == nil {
					err = dec.Decode(&s.publisher)
					if err == nil {
						err = dec.Decode(&s.packetIdentifier)
						if err == nil {
							err = dec.Decode(&s.timestamp)
						}
					}
				}
			}
		}
	}
	if err != nil {
		logrus.Debugf("error: %v", err)
	}
	return
}

// func (s *Pubinfo) Save(enc codec.Encoder) (err error) {
// 	err = enc.Encode(s.data)
// 	if err != nil {
// 		logrus.Debugf("error: %v", err)
// 		return
// 	}
// 	err = enc.Encode(s.publishingQoS)
// 	if err != nil {
// 		logrus.Debugf("error: %v", err)
// 		return
// 	}
// 	err = enc.Encode(s.published)
// 	if err != nil {
// 		logrus.Debugf("error: %v", err)
// 		return
// 	}
// 	err = enc.Encode(s.publisher)
// 	if err != nil {
// 		logrus.Debugf("error: %v", err)
// 		return
// 	}
// 	err = enc.Encode(s.packetIdentifier)
// 	if err != nil {
// 		logrus.Debugf("error: %v", err)
// 		return
// 	}
// 	err = enc.Encode(s.timestamp)
// 	if err != nil {
// 		logrus.Debugf("error: %v", err)
// 		return
// 	}
// 	count := len(s.sentClients)
// 	err = enc.Encode(count)
// 	if err != nil {
// 		logrus.Debugf("error: %v", err)
// 		return
// 	}
// 	for k, v := range s.sentClients {
// 		err = enc.Encode(k)
// 		if err != nil {
// 			logrus.Debugf("error: %v", err)
// 			return
// 		}
// 		err = enc.Encode(v.packetIdentifier)
// 		if err != nil {
// 			logrus.Debugf("error: %v", err)
// 			return
// 		}
// 		err = enc.Encode(v.qos)
// 		if err != nil {
// 			logrus.Debugf("error: %v", err)
// 			return
// 		}
// 	}
// 	return
// }
//
// func (s *Pubinfo) Load(dec codec.Decoder) (err error) {
// 	// need restored:
// 	// tag,
// 	// var str string
// 	// err = dec.Decode(&str)
// 	// logrus.Debug(str)
// 	err = dec.Decode(&s.data)
// 	if err != nil {
// 		logrus.Debugf("error: %v", err)
// 		return
// 	}
// 	err = dec.Decode(&s.publishingQoS)
// 	if err != nil {
// 		logrus.Debugf("error: %v", err)
// 		return
// 	}
// 	err = dec.Decode(&s.published)
// 	if err != nil {
// 		logrus.Debugf("error: %v", err)
// 		return
// 	}
// 	err = dec.Decode(&s.publisher)
// 	if err != nil {
// 		logrus.Debugf("error: %v", err)
// 		return
// 	}
// 	err = dec.Decode(&s.packetIdentifier)
// 	if err != nil {
// 		logrus.Debugf("error: %v", err)
// 		return
// 	}
// 	err = dec.Decode(&s.timestamp)
// 	if err != nil {
// 		logrus.Debugf("error: %v", err)
// 		return
// 	}
//
// 	var count int
// 	err = dec.Decode(&count)
// 	if err != nil {
// 		logrus.Debugf("error: %v", err)
// 		return
// 	}
// 	if s.sentClients == nil {
// 		s.sentClients = make(map[string]*qp)
// 	}
// 	for i := 0; i < count; i++ {
// 		var key string
// 		var q = &qp{}
// 		err = dec.Decode(&key)
// 		if err != nil {
// 			logrus.Debugf("error: %v", err)
// 			return
// 		}
// 		err = dec.Decode(&q.packetIdentifier)
// 		if err != nil {
// 			logrus.Debugf("error: %v", err)
// 			return
// 		}
// 		err = dec.Decode(&q.qos)
// 		if err != nil {
// 			logrus.Debugf("error: %v", err)
// 			return
// 		}
// 		s.sentClients[key] = q
// 	}
// 	return
// }
