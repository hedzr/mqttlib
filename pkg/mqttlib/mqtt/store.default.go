/*
 * Copyright © 2020 Hedzr Yeh.
 */

package mqtt

type defaultStore struct {
	data map[string]interface{}
}

func (d *defaultStore) Has(key string) bool {
	_, ok := d.data[key]
	return ok
}

func (d *defaultStore) Get(key string, defaultValue ...interface{}) interface{} {
	if v, ok := d.data[key]; ok {
		return v
	}
	if len(defaultValue) > 0 {
		return defaultValue[0]
	}
	return nil
}

func (d *defaultStore) Set(key string, value interface{}) (err error) {
	d.data[key] = value
	return
}
