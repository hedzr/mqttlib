/*
 * Copyright © 2020 Hedzr Yeh.
 */

package mqtt

import (
	"bytes"
	"gitlab.com/hedzr/mqttlib/pkg/mqttlib/errors"
	"gitlab.com/hedzr/mqttlib/pkg/mqttlib/mqtt/codec"
	"io"
)

// ....................................................

func NewUnsubackBuilder(opts ...UnsubackBuilderOpt) PacketBuilder {
	builder := &unsubackBuilder{
		vh50ext: *newvh50ext(),
		// vh: vh, payload: payload,
	}
	for _, opt := range opts {
		opt(builder)
	}
	return builder
}

func WithUnsubackIdentifier(id uint16) UnsubackBuilderOpt {
	return func(builder *unsubackBuilder) {
		builder.identifier = id
	}
}

func WithUnsubackRetCodes(retCodes []UnsubackRetcode) UnsubackBuilderOpt {
	return func(builder *unsubackBuilder) {
		builder.retCode = retCodes
	}
}

func WithUnsuback50UserProperties(props map[string]string) UnsubackBuilderOpt {
	return func(builder *unsubackBuilder) {
		for k, v := range props {
			builder.userProperties[k] = v
		}
	}
}

type UnsubackBuilderOpt func(*unsubackBuilder)

type UnsubackRetcode int

const (
	UnsubackOK UnsubackRetcode = iota
	unsubackQoS1
	unsubackQoS2
	unsubackFailure = iota + 0x80
)

type unsubackBuilder struct {
	vh50ext

	// vh         *unsubscribeVH
	// payload    *unsubscribePayload
	identifier uint16
	retCode    []UnsubackRetcode

	b1   byte
	data []byte
	n    int
}

func (b *unsubackBuilder) IsResending() bool {
	return false
}

func (b *unsubackBuilder) RequestResend() {
}

func (b *unsubackBuilder) NextPktId(ctx *StateContext) uint16 {
	return 0
}

func (b *unsubackBuilder) ReportType() ReportType {
	return UNSUBACK
}

func (b *unsubackBuilder) Build(ctx *StateContext, pkg *Pkg) (err error) {
	// bb := bytes.NewBuffer([]byte{0xb0, 0x00})
	//
	// if err = codec.WriteBEUint16(bb, b.identifier); err != nil {
	// 	err =errors.ErrNumBadIO.New("can't write identifier field")
	// 	return
	// }
	//
	// for _, rc := range b.retCode {
	// 	if err = codec.WriteBEUint16(bb, uint16(rc)); err != nil {
	// 		err = errors.ErrNumBadIO.New("can't write retCode")
	// 		return
	// 	}
	// }

	bb := bytes.NewBuffer([]byte{})

	b.b1 = byte(b.ReportType() << 4)
	// if b.DUP {
	// 	b.b1 |= 0x08
	// }
	// if b.RETAIN {
	// 	b.b1 |= 0x01
	// }
	// b.b1 |= byte(b.QoS) << 1

	for i := 0; i < 5; i++ {
		err = bb.WriteByte(b.b1)
		if err != nil {
			return
		}
	}

	if err = codec.WriteBEUint16(bb, b.identifier); err != nil {
		err = errors.ErrCodeBadIO.New("can't write identifier field")
		return
	}

	for _, rc := range b.retCode {
		// if err = codec.WriteBEUint16(bb, uint16(rc)); err != nil {
		// 	err = errors.ErrCodeBadIO.New("can't write retCode")
		// 	return
		// }
		bb.WriteByte(byte(rc))
	}

	if ctx.ConnectParam.ProtocolLevel >= ProtocolLevelForV50 {
		var buf bytes.Buffer
		if buf, err = b.buildv5(ctx, pkg); err != nil {
			return
		}
		bb.Write(buf.Bytes())
	}

	l := bb.Len() - 5
	b.data = bb.Bytes()
	bl := codec.EncodeIntToBuf(l, b.data, 1)
	copy(b.data[bl+1:], b.data[5:])
	b.n = l + 1 + bl
	return
}

func (b *unsubackBuilder) buildv5(ctx *StateContext, pkg *Pkg) (buf bytes.Buffer, err error) {
	// for MQTT v5.0

	// construct the properties buffer at first

	var propertiesBuf bytes.Buffer
	propertiesBuf, err = b.WriteAs()
	if err != nil {
		return
	}

	var length int
	var buf8 []byte = make([]byte, 8)
	length = codec.EncodeIntToBuf(propertiesBuf.Len(), buf8, 0)

	// vh
	buf.Write(buf8[:length])         // properties length
	buf.Write(propertiesBuf.Bytes()) // properties buffer
	return
}

func (b *unsubackBuilder) WriteTo(w io.Writer) (n int, err error) {
	n, err = w.Write(b.Bytes())
	return
}

func (b *unsubackBuilder) Bytes() (bytes []byte) {
	return b.data[:b.n]
}

func (b *unsubackBuilder) AsString() (str string) {
	return
}
