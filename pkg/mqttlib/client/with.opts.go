/*
 * Copyright © 2020 Hedzr Yeh.
 */

package client

import (
	"bufio"
	"bytes"
	"github.com/hedzr/cmdr"
	"github.com/sirupsen/logrus"
	"gitlab.com/hedzr/mqttlib/pkg/logger"
	"gitlab.com/hedzr/mqttlib/pkg/mqttlib/mqtt"
	"gitlab.com/hedzr/mqttlib/pkg/tcp"
	"gitlab.com/hedzr/mqttlib/pkg/tcp/tls"
	"os"
	"time"
)

// NewClient returns a mqtt client object.
// NOTE that it might be nil sometimes, such as failed on connecting to server/broker
func NewClient(addr string, opts ...ClientOpt) *Client {
	s := &Client{
		Base:              logger.Base{Tag: "mqtt.client"},
		remains:           bytes.NewBuffer(nil),
		done:              make(chan struct{}),
		pkgGotCh:          make(chan *mqtt.Pkg),
		subCh:             make(chan *mqtt.SubBlock),
		pubCh:             make(chan *mqtt.PubBlock),
		sendQueue:         make(map[uint16]*mqtt.PubSubTask),
		verbose:           false,
		debugMode:         false,
		keepAliveSeconds:  60,
		pingRespTimeout:   0,
		connAckTimeout:    15 * time.Second,
		pubExpiredTimeout: 15 * time.Second,
		reconnectDuration: 5 * time.Second,
	}

	// s.remainsBuffer = bufio.NewReader(s.remains)
	s.remainsBuffer = bufio.NewReadWriter(bufio.NewReader(s.remains), bufio.NewWriter(s.remains))

	s.reconnectTimer = time.NewTimer(mqtt.InfiniteDuration)

	// in default, client don't serialize its session store onto disk
	opt := mqtt.WithDefaultSessionStoreSavingPeriod(time.Hour * 24 * 365 * 10)
	// WithStateContextOpts(mqtt.WithDefaultSessionStoreOpts(opt))(s)
	mqtt.NewDefaultSessionStore(opt)

	for _, opt := range opts {
		opt(s)
	}

	if s.sendCh == nil {
		s.sendCh = make(chan *mqtt.PubSubTask, 32)
	}

	s.addr = addr
	s.Client = tcp.NewClient(s.addr,
		append(s.clientOpts,
			tcp.WithClientOnProcessFunc(s.onTcpProcessRead),
			tcp.WithClientOnConnectedFunc(s.onTcpConnected),
			tcp.WithClientOnDisconnectedFunc(s.onTcpDisconnected),
			// tcp.WithReadBufferSize(128), // testing
		)...,
	)

	// connecting to addr failed
	if s.Client == nil {
		return nil
	}

	if s.DisconnectedPktReceivedCh == nil {
		s.DisconnectedPktReceivedCh = make(chan struct{})
	}

	go s.looper()
	return s
}

type CloseReason int

const (
	ClosedNormal CloseReason = iota
	ClosedConnAckTimeout
	ClosedPingRespTimeout
	ClosedReconnecting
	ClosedAppExiting
)

func WithMaxQoS(maxQoS mqtt.QoSType) ClientOpt {
	return func(client *Client) {
		opt := mqtt.WithMaxQoS(maxQoS)
		client.contextOpts = append(client.contextOpts, opt)
	}
}

func WithMaxProtocolLevel(level mqtt.ProtocolLevel) ClientOpt {
	return func(client *Client) {
		opt := mqtt.WithMaxProtocolLevel(level)
		client.contextOpts = append(client.contextOpts, opt)
	}
}

func WithMaxVersion(v string) ClientOpt {
	return func(client *Client) {
		opt := mqtt.WithMaxVersion(v)
		client.contextOpts = append(client.contextOpts, opt)
	}
}

func WithConnAckTimeout(d time.Duration) ClientOpt {
	return func(client *Client) {
		client.connAckTimeout = d
	}
}

func WithPublishExpiredTimeout(d time.Duration) ClientOpt {
	return func(client *Client) {
		client.pubExpiredTimeout = d
	}
}

func WithStateContextOpts(opts ...mqtt.StateContextOpt) ClientOpt {
	return func(client *Client) {
		client.contextOpts = append(client.contextOpts, opts...)
	}
}

func WithVerbose(verbose bool) ClientOpt {
	return func(client *Client) {
		client.verbose = verbose
	}
}

func WithDebugMode(debugMode bool) ClientOpt {
	return func(client *Client) {
		client.debugMode = debugMode
	}
}

func WithDumpSendingData(b bool) ClientOpt {
	return func(client *Client) {
		client.dumpSendingData = b
	}
}

func WithClientId(id string) ClientOpt {
	return func(client *Client) {
		client.clientId = id
	}
}

// WithKeepAliveSeconds sets a keep-alive seconds.
// Normally, it could be one or more minutes. The suggestion about
// the maximal keep-alive might be 18 hours 12 minutes 15 seconds.
// It means 65535 seconds.
// I have no idea about this maximal value but refer to 3.1.2.10 pls.
func WithKeepAliveSeconds(s uint16) ClientOpt {
	return func(client *Client) {
		client.keepAliveSeconds = s
	}
}

func WithSendQueueSize(size int) ClientOpt {
	return func(client *Client) {
		if size < 1 {
			size = 1
		}
		if size > 65535 {
			size = 65535
		}
		client.sendCh = make(chan *mqtt.PubSubTask, size)
	}
}

func WithConnectWillMsgFromCmdr(prefix string) ClientOpt {
	return func(client *Client) {
		if len(prefix) == 0 {
			prefix = "mqtt.client.sub"
		}
		var (
			willRetain bool         = cmdr.GetBoolRP(prefix, "will-retain")
			willQoS    mqtt.QoSType = mqtt.QoSType(cmdr.GetIntRP(prefix, "will-qos"))

			// 为了保留主题中可能的 $ 字符，关闭自动展开特性
			// NOTE 像 `$cid` 这样的主题带有专属含义，它将被 MQTTLIB 自动展开为客户端的 clientId 值
			willTopic   string = cmdr.GetStringNoExpandRP(prefix, "will-topic")
			willMessage []byte = []byte(cmdr.GetStringNoExpandRP(prefix, "will-payload"))
		)
		logrus.Debugf("os.Args: %v", os.Args)
		if len(willTopic) > 0 {
			opt := mqtt.WithConnectWillMsg(willRetain, willQoS, willTopic, willMessage)
			client.connectOpts = append(client.connectOpts, opt)
		}
	}
}

func WithConnectWillMsg(willRetain bool, willQoS mqtt.QoSType, willTopic string, willMessage []byte) ClientOpt {
	return func(client *Client) {
		if len(willTopic) > 0 {
			opt := mqtt.WithConnectWillMsg(willRetain, willQoS, willTopic, willMessage)
			client.connectOpts = append(client.connectOpts, opt)
		}
	}
}

func WithConnectOpts(opts ...mqtt.ConnectBuilderOpt) ClientOpt {
	return func(client *Client) {
		client.connectOpts = append(client.connectOpts, opts...)
	}
}

func WithReconnectEnable(b bool, afterBroken time.Duration) ClientOpt {
	return func(client *Client) {
		client.reconnect = b
		if client.reconnectDuration > 0 {
			client.reconnectDuration = afterBroken
		}
	}
}

func WithAlwaysReconnectEnable(b bool, afterBroken time.Duration) ClientOpt {
	return func(client *Client) {
		client.alwaysReconnect = b // reconnect even if client closed by others/peer
		client.reconnect = b
		if client.reconnectDuration > 0 {
			client.reconnectDuration = afterBroken
		}
	}
}

func WithOnTcpClientOpts(opts ...tcp.ClientOpt) ClientOpt {
	return func(client *Client) {
		client.clientOpts = append(client.clientOpts, opts...)
	}
}

func WithTcpTlsConfig(s *tls.CmdrTlsConfig) ClientOpt {
	return func(client *Client) {
		client.clientOpts = append(client.clientOpts, tcp.WithClientTlsConfig(s))
	}
}

func WithOnTcpConnected(connected tcp.OnTcpConnectedFunc) ClientOpt {
	return func(client *Client) {
		// client.clientOpts = append(client.clientOpts, tcp.WithClientOnConnectedFunc(connected))
		client.onTcpConnectedFunc = connected
	}
}

func WithOnTcpDisconnected(disconnected tcp.OnTcpDisconnectedFunc) ClientOpt {
	return func(client *Client) {
		// client.clientOpts = append(client.clientOpts, tcp.WithClientOnDisconnectedFunc(disconnected))
		client.onTcpDisconnectedFunc = disconnected
	}
}

func WithOnMqttConnected(connected OnMqttServerResp) ClientOpt {
	return func(client *Client) {
		client.onMqttConnected = connected
	}
}

func WithOnMqttDisconnected(disconnected OnMqttServerResp) ClientOpt {
	return func(client *Client) {
		client.onMqttDisconnected = disconnected
	}
}

func WithOnMqttSubscribed(fn OnMqttServerResp) ClientOpt {
	return func(client *Client) {
		client.onMqttSubscribed = fn
	}
}

func WithOnMqttUnsubscribed(fn OnMqttServerResp) ClientOpt {
	return func(client *Client) {
		client.onMqttUnsubscribed = fn
	}
}

func WithOnMqttPublishing(fn OnMqttServerResp) ClientOpt {
	return func(client *Client) {
		client.onMqttPublishing = fn
	}
}

func WithOnMqttPublished(fn OnMqttServerResp) ClientOpt {
	return func(client *Client) {
		client.onMqttPublished = fn
	}
}

func WithOnMqttPubRec(fn OnMqttServerResp) ClientOpt {
	return func(client *Client) {
		client.onMqttPubRec = fn
	}
}

func WithOnMqttPubComp(fn OnMqttServerResp) ClientOpt {
	return func(client *Client) {
		client.onMqttPubComp = fn
	}
}

func WithOnMqttPingResp(fn OnMqttServerResp) ClientOpt {
	return func(client *Client) {
		client.onMqttPingResp = fn
	}
}
