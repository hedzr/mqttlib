/*
 * Copyright © 2020 Hedzr Yeh.
 */

package client

import (
	"bytes"
	"fmt"
	"github.com/hedzr/cmdr"
	"github.com/sirupsen/logrus"
	"io"
	"net"
	"strings"
)

const (
	Parallel = 1
	Times    = 1
	// Broker   = "localhost:1883"
	// Broker = "test.mosquitto.org:1883"
	Broker = ""
	Topic  = ""
)

func ParseHostAddr(broker string, defaultPort ...string) (host, port, addr string, err error) {
	host, port, err = net.SplitHostPort(broker)
	if err != nil {
		if !strings.Contains(broker, ":") {
			port = cmdr.GetStringR("mqtt.ports.default", "1883")
			for _, p := range defaultPort {
				port = p
				break
			}
			broker += ":" + port
			host, port, err = net.SplitHostPort(broker)
		}
		if err != nil {
			logrus.Errorf("get broker address ('%v') failed: %v", broker, err)
			return
		}
	}

	addr = net.JoinHostPort(host, port)
	return
}

func shortCopy() (err error) {
	var n int
	var written int64
	var conn net.Conn
	conn, err = net.Dial("tcp", "google.com:80")
	if err != nil {
		fmt.Println("dial error:", err)
		return
	}
	defer conn.Close()
	n, err = fmt.Fprintf(conn, "GET / HTTP/1.0\r\n\r\n")
	var buf bytes.Buffer
	written, err = io.Copy(&buf, conn)
	fmt.Println("total size:", buf.Len(), " | ", n, written)
	return
}
