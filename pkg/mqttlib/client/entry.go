/*
 * Copyright © 2020 Hedzr Yeh.
 */

package client

import (
	"bufio"
	"github.com/hedzr/cmdr"
	"github.com/sirupsen/logrus"
	"gitlab.com/hedzr/mqttlib/pkg/tcp"
	"gitlab.com/hedzr/mqttlib/pkg/tcp/tls"
	"gitlab.com/hedzr/mqttlib/pkg/trace"
	"net"
	"runtime"
	"strconv"
	"time"
)

// 本文件基本未用，保留用作参考代码

func Run() {
	runtime.GOMAXPROCS(runtime.NumCPU())

	broker := cmdr.GetStringR("mqtt.client.broker")
	_, _, addr, err := ParseHostAddr(broker)
	if err != nil {
		logrus.Errorf("get broker address ('%v') failed: %v", broker, err)
		return
	}

	maxTimes := cmdr.GetIntR("mqtt.client.times")
	parallel := cmdr.GetIntR("mqtt.client.parallel")
	sleep := cmdr.GetDurationR("mqtt.client.sleep")

	if cmdr.InDebugging() || cmdr.GetBoolR("trace") {
		_ = trace.Start()
		defer trace.Stop()
	}

	client := NewClient(addr, WithTcpTlsConfig(tls.NewCmdrTlsConfig("tcp.client.tls", "tcp.client")))
	defer client.Close()

	run(client, maxTimes, parallel, sleep)

	// tcp.HandleSignals(func(s os.Signal) {
	// 	logrus.Debugf("signal[%v] caught and exiting this program", s)
	// })()
}

func run(client *Client, maxTimes, parallel int, sleep time.Duration) {
	for i := 0; i < maxTimes; i++ {
		str := "hello " + strconv.Itoa(i) + "\r\n"
		logrus.Debugf("sending: %v", str)
		client.Client.Send([]byte(str))
		if sleep != 0 {
			time.Sleep(sleep)
		}
	}
	time.Sleep(13 * time.Second)
	// }
}

func Run0() {
	runtime.GOMAXPROCS(runtime.NumCPU())

	broker := cmdr.GetStringR("mqtt.client.broker")
	host, port, err := net.SplitHostPort(broker)
	if err != nil {
		logrus.Errorf("get broker address failed: %v", err)
		return
	}

	dest := net.JoinHostPort(host, port)
	maxTimes := cmdr.GetIntR("mqtt.client.times")
	parallel := cmdr.GetIntR("mqtt.client.parallel")
	sleep := cmdr.GetDurationR("mqtt.client.sleep")

	client := tcp.NewClient(dest, tcp.WithClientOnProcessFunc(func(p []byte, in *bufio.Reader, out *bufio.Writer) (n int, err error) {
		logrus.Debugf("[x] read: %v (%v)", p, string(p))
		return len(p), nil
	}))
	defer client.Close()

	run0(client, maxTimes, parallel, sleep)

	// tcp.HandleSignals(func(s os.Signal) {
	// 	logrus.Debugf("signal[%v] caught and exiting this program", s)
	// })()
}

func run0(client *tcp.Client, maxTimes, parallel int, sleep time.Duration) {
	for i := 0; i < maxTimes; i++ {
		str := "hello " + strconv.Itoa(i) + "\r\n"
		logrus.Debugf("sending: %v", str)
		client.Send([]byte(str))
		if sleep != 0 {
			time.Sleep(sleep)
		}
	}
	time.Sleep(13 * time.Second)
	// }
}
