/*
 * Copyright © 2020 Hedzr Yeh.
 */

package client

import (
	"bufio"
	"bytes"
	"gitlab.com/hedzr/mqttlib/pkg/logger"
	"gitlab.com/hedzr/mqttlib/pkg/mqttlib/errors"
	"gitlab.com/hedzr/mqttlib/pkg/mqttlib/mqtt"
	"gitlab.com/hedzr/mqttlib/pkg/mqttlib/mqtt/cid"
	"gitlab.com/hedzr/mqttlib/pkg/mqttlib/mqtt/codec"
	"gitlab.com/hedzr/mqttlib/pkg/mqttlib/mqtt/math"
	"gitlab.com/hedzr/mqttlib/pkg/tcp"
	"net"
	"sync"
	"sync/atomic"
	"time"
)

type ClientOpt func(*Client)
type OnMqttServerResp func(c *Client, pkg *mqtt.Pkg)

type Client struct {
	logger.Base

	Client        *tcp.Client
	addr          string
	connected     int32
	closedReason  CloseReason
	remains       *bytes.Buffer
	remainsBuffer *bufio.ReadWriter

	context *mqtt.StateContext

	done         chan struct{}
	pkgGotCh     chan *mqtt.Pkg
	subCh        chan *mqtt.SubBlock
	pubCh        chan *mqtt.PubBlock
	sendCh       chan *mqtt.PubSubTask
	sendQueue    map[uint16]*mqtt.PubSubTask
	rwmSendQueue sync.RWMutex

	onMqttConnected       OnMqttServerResp
	onMqttDisconnected    OnMqttServerResp
	onMqttSubscribed      OnMqttServerResp
	onMqttUnsubscribed    OnMqttServerResp
	onMqttPingResp        OnMqttServerResp
	onMqttPublished       OnMqttServerResp
	onMqttPubRec          OnMqttServerResp
	onMqttPubComp         OnMqttServerResp
	onMqttPublishing      OnMqttServerResp
	onTcpConnectedFunc    tcp.OnTcpConnectedFunc
	onTcpDisconnectedFunc tcp.OnTcpDisconnectedFunc
	clientOpts            []tcp.ClientOpt

	verbose           bool
	debugMode         bool
	dumpSendingData   bool
	reconnect         bool
	alwaysReconnect   bool
	clientId          string
	connectOpts       []mqtt.ConnectBuilderOpt
	contextOpts       []mqtt.StateContextOpt
	timerWantPingResp *time.Timer
	timerWantConnAck  *time.Timer
	pingRespTimeout   time.Duration
	connAckTimeout    time.Duration
	pubExpiredTimeout time.Duration
	reconnectDuration time.Duration
	keepAliveSeconds  uint16
	reconnectTimer    *time.Timer

	DisconnectedPktReceivedCh chan struct{}
}

func (s *Client) IsMqttConnected() bool {
	cc := atomic.LoadInt32(&s.connected)
	return cc == 1
}

func (s *Client) setConnected() (ok bool) {
	ok = atomic.CompareAndSwapInt32(&s.connected, 0, 1)
	return
}

func (s *Client) setDisconnected() (ok bool) {
	ok = atomic.CompareAndSwapInt32(&s.connected, 1, 0)
	return
}

func (s *Client) IsClosed() bool {
	return s.Client.IsClosed()
}

func (s *Client) Close() {
	s.close(ClosedNormal)
}

func (s *Client) close(reason CloseReason) {
	s.Client.Close()
	if s.done != nil {
		close(s.done)
		s.done = nil
	}
	s.closedReason = reason
}

func (s *Client) runReconnectTask() {
	s.reconnectTimer.Reset(s.reconnectDuration)
	s.close(ClosedReconnecting)
	select {
	case <-s.reconnectTimer.C:
		s.Debug("➠ [mqtt][client] reconnecting to %v", s.addr)
		s.Client = tcp.NewClient(s.addr,
			tcp.WithClientOnProcessFunc(s.onTcpProcessRead),
			tcp.WithClientOnConnectedFunc(s.onTcpConnected),
			tcp.WithClientOnDisconnectedFunc(s.onTcpDisconnected),
			// tcp.WithReadBufferSize(128), // testing
		)

		if s.Client == nil {
			return
		}

		go s.looper()

	case <-s.done:
		s.reconnectTimer.Stop()
		return
	}
	// s.reconnectTimer.Reset(time.Duration(pktidgen.IntMAX) * time.Hour)
}

func (s *Client) looper() {
	if s.keepAliveSeconds == 0 {
		s.keepAliveSeconds = math.MaxUint16
		s.pingRespTimeout = time.Duration(s.keepAliveSeconds) * time.Second
	} else {
		s.pingRespTimeout = time.Duration(s.keepAliveSeconds*3/2+5) * time.Second
	}
	s.timerWantPingResp = time.NewTimer(s.pingRespTimeout)
	s.timerWantConnAck = time.NewTimer(mqtt.InfiniteDuration)
	timerDoPing := time.NewTimer(time.Duration(s.keepAliveSeconds) * time.Second)

	defer func() {
		timerDoPing.Stop()
		s.timerWantConnAck.Stop()
		s.timerWantPingResp.Stop()
		s.Trace("[mqtt][client] looper goroutine exited.")
		if s.reconnect {
			go s.runReconnectTask()
		}
	}()

	for {
		select {
		case task := <-s.sendCh:
			s.doSendTask(task)

		case pkg := <-s.pkgGotCh:
			s.doProcessPkg(pkg)

		case tick := <-timerDoPing.C:
			s.Trace("doPing timer tick at %v", tick)
			// 3.1.2-23
			if err := s.doPing(); err != nil {
				s.Wrong(err, "[mqtt][client] ping failed")
			}
		case tick := <-s.timerWantPingResp.C:
			s.Trace("❝ can't received PINGRESP from peer in %v, closing at %v", s.pingRespTimeout, tick)
			// 3.1.2-24
			if s.done != nil {
				s.close(ClosedPingRespTimeout)
			} else {
				go s.runReconnectTask()
			}
			s.Warn("❝ connection closed because pingresp not received in a short while.")
		case tick := <-s.timerWantConnAck.C:
			s.Trace("❝ can't received CONNACK from peer in %v, closing at %v", s.connAckTimeout, tick)
			if s.done != nil {
				s.close(ClosedConnAckTimeout)
			} else {
				go s.runReconnectTask()
			}
			s.Warn("❝ connection closed because connack not received in a short while.")

		case <-s.done:
			return
		}
	}
}

//
//
//

func (s *Client) onTcpConnected(c *tcp.Client, conn net.Conn) {
	if len(s.clientId) == 0 {
		s.clientId = cid.CreateNewClientId()
	}

	// 3.1.0-1
	s.context = mqtt.NewMqttClientContext(s.clientId, conn, s.contextOpts...)

	if err := s.SendConnectPkt(s.connectOpts...); err != nil {
		s.Wrong(err, "[mqtt][client] send connect packet failed")
	}

	if s.onTcpConnectedFunc != nil {
		s.onTcpConnectedFunc(c, conn)
	}
}

func (s *Client) onTcpDisconnected(c *tcp.Client) {
	s.Debug("➠ [mqtt][client] tcp closed")
	if s.setDisconnected() {
		c.Close()

		// cmdr.SignalTermSignal()

		if s.onTcpDisconnectedFunc != nil {
			s.onTcpDisconnectedFunc(c)
		}

		if s.reconnect && s.alwaysReconnect {
			go s.runReconnectTask()
		}
	}
}

func (s *Client) onTcpProcessRead(p []byte, in *bufio.Reader, out *bufio.Writer) (n int, err error) {
	// logrus.Debugf("    <- R: %v (%v)", p, string(p))
	// n, err = s.remainsBuffer.Write(p)
	// s.Debug("     recv %v bytes. remainsBuffer.cap=%v, len=%v", len(p), s.remainsBuffer.Reader.Size(), s.remainsBuffer.Reader.Buffered())
	// s.remainsBuffer.Reset(s.remains)
	nLen, nn, pos := len(p), 0, 0
	if s.remains.Len() > 0 {
		s.remains.Write(p)
		p = s.remains.Bytes()
		s.remains.Reset()
	}

keepGoing:
	if nLen >= 2 {
		nn, err = s.readPacket(p, pos, in)
		if err == nil {
			nLen -= nn
			pos += nn
			goto keepGoing
		} else if errors.Equal(err, errors.ErrCodePacketIncomplete) {
			// read packet failed, put back the read bytes to 'remains' and return
			s.remains.Write(p)
			err = nil
			// s.Warn("   --> put back the read bytes (nn=%v), remains.buffer.len=%v, remains.cap=%v", nn, s.remainsBuffer.Reader.Buffered(), s.remains.Cap())
		}
	} else {
		s.remains.Write(p[pos:])
	}

	return
}

func (s *Client) GetFlagsAs(flags uint8) (DUP, RETAIN bool, QoS mqtt.QoSType) {
	if (flags & 0x08) != 0 {
		DUP = true
	}
	if (flags & 0x01) != 0 {
		RETAIN = true
	}
	QoS = mqtt.QoSType((flags & 0x06) >> 1)
	return
}

func (s *Client) readPacket(p []byte, posFrom int, reader *bufio.Reader) (nn int, err error) {
	pos, eat := 0, 0

	var pkg mqtt.Pkg

	pkg.ReceivedTime = time.Now().UTC()

	// read header

	// fixed header read

	pkg.ReportType = mqtt.ReportType(p[posFrom] >> 4)
	pkg.Flags = p[posFrom] & 0x0f
	pkg.DUP, pkg.RETAIN, pkg.QoS = s.GetFlagsAs(pkg.Flags)
	// rw.Debug("length = %v, rptType = %v, DUP = %v, RETAIN = %v, QoS = %v", pkg.length, pkg.rptType, pkg.DUP, pkg.RETAIN, pkg.QoS)

	pkg.Length, eat = codec.DecodeInt(p[posFrom+1:])
	pos += 1 + eat

	if posFrom+pos+pkg.Length > len(p) {
		err = errors.ErrCodePacketIncomplete.New("expect len %d > actual len %d", posFrom+pos+pkg.Length, len(p))
		return
	}

	// vh + payload

	nn = pos + pkg.Length
	pkg.Data = p[posFrom+pos : posFrom+nn]

	if len(pkg.Data) == pkg.Length { // err == nil && eat == pkg.Length {

		// if trace.IsEnabled() {
		// 	rw.Trace(">>> ▿RECV %v bytes: %v", nn, p[:nn])
		// 	// nn = len(buf)
		// 	// rw.Debug("     - just for debugging: mqttReader.Process processed %v bytes (%v, '%v')", nn, buf[:nn], string(buf[:nn]))
		// }

		// 提前测试是否正确切分了报文
		if _, err = s.context.Parse(&pkg); err != nil {
			s.Wrong(err, "    %+v", pkg)
			return
		}

		if s.debugMode {
			if pkg.ReportType == mqtt.PINGRESP {
				s.Trace("      got pkt, %v", pkg.ReportType)
			}
		} else {
			s.Trace("      got pkt: %v, len=%v", pkg.ReportType, pkg.Length)
		}

		s.pkgGotCh <- &pkg

	} else { // if err != nil {
		s.Warn("read VH+Payload failed: nn=%v, wanted=%v, err: %+v", nn, pkg.Length, err)
	}
	return
}

func (s *Client) removeSendingTask(packetIdentifier uint16) {
	if packetIdentifier > 0 {
		s.rwmSendQueue.RLock()
		_, ok := s.sendQueue[packetIdentifier]
		s.rwmSendQueue.RUnlock()
		if ok {
			s.rwmSendQueue.Lock()
			delete(s.sendQueue, packetIdentifier)
			s.rwmSendQueue.Unlock()
		}
	}
}

func (s *Client) scheduleResendTask(packetIdentifiers ...uint16) {
	for _, packetIdentifier := range packetIdentifiers {
		if packetIdentifier > 0 {
			s.rwmSendQueue.RLock()
			v, ok := s.sendQueue[packetIdentifier]
			s.rwmSendQueue.RUnlock()
			if ok {
				s.sendCh <- v.RequestResend()
			}
		}
	}
}

func (s *Client) doSendTask(task *mqtt.PubSubTask) {
	if s.Client.IsClosed() {
		return
	}

	if err := task.Builder.Build(s.context, nil); err != nil {
		s.Wrong(err, "->")
		return
	}

	// s.Debug("-> builder: %+v", builder)
	data := task.Builder.Bytes()
	s.Client.Send(data)
	if s.dumpSendingData {
		s.Debug("▲ MQTT.%s: % x", task.Builder.ReportType(), data)
	} else {
		l := 4
		if len(data) < 4 {
			l = len(data)
		}
		s.Debug("▲ MQTT.%s: %v bytes (% x) \"%v\"", task.Builder.ReportType(), len(data), data[:l], task.Builder.AsString())
	}
	if task.OnSent != nil {
		task.OnSent(task, data)
	}

	if task.PacketIdentifier > 0 {
		s.rwmSendQueue.Lock()
		delete(s.sendQueue, task.PacketIdentifier)
		for _, v := range s.sendQueue {
			if time.Now().UTC().Sub(v.SentTime) > 0 && !v.IsResending() {
				// NOTE, TODO 可能导致死锁，当sendCh满，持续发送失败，且新的发送任务不断追加时，下句将会死锁
				s.sendCh <- v.RequestResend()
				// delete(s.sendQueue, pi)
			}
		}
		s.rwmSendQueue.Unlock()
	}

	// s.rwmSendQueue.RLock()
	// s.rwmSendQueue.RUnlock()
}

func (s *Client) sendPubSubPacket(builder mqtt.PacketBuilder, dumpData bool) (err error) {
	s.dumpSendingData = dumpData
	task := &mqtt.PubSubTask{
		PacketIdentifier: builder.NextPktId(s.context),
		SendingTask:      mqtt.SendingTask{Builder: builder},
	}

	s.rwmSendQueue.Lock()
	s.sendQueue[task.PacketIdentifier] = task
	s.rwmSendQueue.Unlock()

	s.sendCh <- task
	return
}

func (s *Client) sendPacket(builder mqtt.PacketBuilder, dumpData bool, onSent func(task *mqtt.PubSubTask, data []byte)) (err error) {
	s.dumpSendingData = dumpData
	s.sendCh <- &mqtt.PubSubTask{
		PacketIdentifier: 0,
		SendingTask:      mqtt.SendingTask{Builder: builder},
		OnSent:           onSent,
	}
	return
}

func (s *Client) doPing() (err error) {
	builder := mqtt.NewPingreqBuilder()
	err = s.sendPacket(builder, false, func(task *mqtt.PubSubTask, data []byte) {
		s.timerWantPingResp.Reset(s.pingRespTimeout)
	})
	// s.lastPingTime = time.Now().UTC()
	return
}

func (s *Client) SendDisconnectPkt(reason errors.CloseReason, reasonString string, opts ...mqtt.Disconnect50BuilderOpt) (err error) {
	// var opts []mqtt.Disconnect50BuilderOpt
	if s.context.MaxProtocolLevel() >= mqtt.ProtocolLevelForV50 {
		opts = append(opts,
			mqtt.WithDisconnect50SessionExpiryInterval(7200),
			mqtt.WithDisconnect50Reason(reason, reasonString),
		)
	}

	builder := mqtt.NewDisconnectBuilder(opts...)
	err = s.sendPacket(builder, false, nil)
	return
}

// DestroyConversation remove the current conversation without keep it.
// The right sequences should be:
// 1. client -> server, with connect pkt and clean-session flag 0 or 1;
// 2. client send disconnect pkt to close conversation with server;
// 3. client.DestroyConversation to reconnect to server with clean-session flag 1;
// 4. client send disconnect pkt to close connection. And now server will
//    erase all about the conversation and session.
// see also 3.1.2.4
func (s *Client) DestroyConversation() (err error) {
	builder := mqtt.NewConnectBuilder(
		mqtt.WithConnectCleanSessionIfNotSet(true),
		mqtt.WithConnectKeepAliveSecondsIfNotSet(uint16(s.keepAliveSeconds)),
	)
	err = s.sendPacket(builder, false, nil)
	s.Debug("  --- Client Id: %v ---", s.context.ConnectParam.ClientId)
	return
}

func (s *Client) SendConnectPkt(opts ...mqtt.ConnectBuilderOpt) (err error) {
	builder := mqtt.NewConnectBuilder(
		append(opts,
			mqtt.WithConnectCleanSessionIfNotSet(true),
			mqtt.WithConnectKeepAliveSecondsIfNotSet(uint16(s.keepAliveSeconds)),
		)...,
	)
	err = s.sendPacket(builder, false, func(task *mqtt.PubSubTask, data []byte) {
		s.timerWantConnAck.Reset(s.connAckTimeout)
	})
	s.Debug("  --- Client Id: %v ---", s.context.ConnectParam.ClientId)
	return
}

func (s *Client) SendPublishPkt(topicName string, msg []byte, opts ...mqtt.PublishBuilderOpt) (err error) {
	builder := mqtt.NewPublishBuilder(topicName, msg, opts...)
	// go func() {
	err = s.sendPubSubPacket(builder, false)
	// }()
	return
}

func (s *Client) SendSubscribePkt(topicFilters []string, qos mqtt.QoSType, verbose bool) (err error) {
	builder := mqtt.NewSubscribeBuilder(verbose,
		// mqtt.WithSubHolder(s.context.clientSubsHolder),
		mqtt.WithSubTopicFilters(topicFilters, qos),
	)
	err = s.sendPubSubPacket(builder, true)
	return
}

func (s *Client) SendUnsubscribePkt(topicFilter string) (err error) {
	builder := mqtt.NewUnsubscribeBuilder(
		mqtt.WithUnsubTopicFilter(topicFilter),
		// builders.WithUnsubPacketIdentifier(id),
	)
	err = s.sendPubSubPacket(builder, false)
	return
}

func (s *Client) onReceivedMsg(topicName string, payload []byte) {
	s.Debug("recv msg: %v, %v", topicName, payload)
}

func (s *Client) scheduleRecent(packetIdentifier uint16) {

}

func (s *Client) doProcessPkg(pkg *mqtt.Pkg) {
	if _, err := s.context.Parse(pkg); err != nil {
		s.Wrong(err, "%+v", pkg)
		return
	}

	switch pkg.ReportType {
	case mqtt.CONNACK:
		if !s.IsMqttConnected() {
			if vh, ok := pkg.VH.(*mqtt.ConnackVH); ok {
				switch vh.ReasonCode {
				case mqtt.ConnackOK:
					ok := s.setConnected()
					if ok {
						s.timerWantConnAck.Reset(mqtt.InfiniteDuration)
						s.timerWantPingResp.Reset(mqtt.InfiniteDuration)

						s.Debug("▿ MQTT.%s: %+v", pkg.ReportType, pkg)

						if s.onMqttConnected != nil {
							s.onMqttConnected(s, pkg)
						}
					}
				default:
					s.Warn(" cannot connect to mqtt server, ret code = %v", vh.ReasonCode)
					// 3.1.0-2
				}
			}
		} else {
			s.Warn("received mqtt connected ack again")
			s.timerWantConnAck.Reset(mqtt.InfiniteDuration)
			s.timerWantPingResp.Reset(mqtt.InfiniteDuration)
		}
	case mqtt.PUBLISH:
		if s.IsMqttConnected() {
			s.timerWantPingResp.Reset(s.pingRespTimeout)
			// s.Debug("▿ MQTT.%s: %+v", pkg.ReportType, pkg)
			if s.onMqttPublishing != nil {
				s.onMqttPublishing(s, pkg)
			}
		}
	case mqtt.PUBACK: // if QoS1, resp of publish
		s.Debug("▿ MQTT.%s: %+v", pkg.ReportType, pkg)
		s.removeSendingTask(pkg.VH.(*mqtt.PubackVH).PacketIdentifier)
		if s.onMqttPublished != nil {
			s.onMqttPublished(s, pkg)
		}
	case mqtt.PUBREC: // if QoS2, resp of publish
		s.Debug("▿ MQTT.%s: %+v", pkg.ReportType, pkg)
		if s.onMqttPubRec != nil {
			s.onMqttPubRec(s, pkg)
		}
	case mqtt.PUBCOMP: // if QoS2, resp of pubrel
		s.Debug("▿ MQTT.%s: %+v", pkg.ReportType, pkg)
		if s.onMqttPubComp != nil {
			s.onMqttPubComp(s, pkg)
		}
	case mqtt.SUBACK:
		s.Debug("▿ MQTT.%s: %+v", pkg.ReportType, pkg)
		if s.onMqttSubscribed != nil {
			s.onMqttSubscribed(s, pkg)
		}
	case mqtt.UNSUBACK:
		s.Debug("▿ MQTT.%s: %+v", pkg.ReportType, pkg)
		if s.onMqttUnsubscribed != nil {
			s.onMqttUnsubscribed(s, pkg)
		}
	case mqtt.PINGRESP:
		s.Debug("▿ MQTT.%s: %+v", pkg.ReportType, pkg)
		s.timerWantPingResp.Reset(mqtt.InfiniteDuration)
		if s.onMqttPingResp != nil {
			s.onMqttPingResp(s, pkg)
		}

	case mqtt.DISCONNECT:
		s.Debug("▿ MQTT.%s: %+v", pkg.ReportType, pkg)
		s.timerWantPingResp.Reset(mqtt.InfiniteDuration)
		if s.setDisconnected() {
			if s.onMqttDisconnected != nil {
				s.onMqttDisconnected(s, pkg)
			}
			s.DisconnectedPktReceivedCh <- struct{}{}
		}

	default:
		s.Debug("▿ MQTT.%s: %+v", pkg.ReportType, pkg)
	}
}
