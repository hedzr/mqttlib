/*
 * Copyright © 2020 Hedzr Yeh.
 */

package client

import (
	"bufio"
	"fmt"
	"github.com/hedzr/cmdr"
	"gitlab.com/hedzr/mqttlib/pkg/logger"
	"gitlab.com/hedzr/mqttlib/pkg/mqttlib/errors"
	"gitlab.com/hedzr/mqttlib/pkg/mqttlib/mqtt"
	"gitlab.com/hedzr/mqttlib/pkg/mqttlib/mqtt/cid"
	"gitlab.com/hedzr/mqttlib/pkg/tcp"
	"gitlab.com/hedzr/mqttlib/pkg/tcp/tls"
	"gitlab.com/hedzr/mqttlib/pkg/tool"
	"gitlab.com/hedzr/mqttlib/pkg/trace"
	"os"
	"runtime"
	"strings"
	"time"
)

type pub struct {
	logger.Base
}

func PubRun(level mqtt.ProtocolLevel) {
	runtime.GOMAXPROCS(runtime.NumCPU())

	app := &pub{Base: logger.Base{Tag: "pub"}}
	_ = app.run(level)
}

func (s *pub) run(level mqtt.ProtocolLevel) (err error) {
	tlscfg := tls.NewCmdrTlsConfig("mqtt.client.tls", "mqtt.client")

	persistClientId := cmdr.GetStringR("mqtt.client.pub.persist-client-id", cid.CreateNewClientId())
	clientId := cmdr.GetStringR("mqtt.client.pub.client-id", persistClientId)
	verbose := cmdr.GetBoolR("verbose")
	broker := cmdr.GetStringR("mqtt.client.host", cmdr.GetStringR("mqtt.client.broker"))
	if len(broker) == 0 {
		s.Wrong(nil, "Please specify broker address via '-b'/'-h', such as: '-b test.mosquitto.org:1883', or '-h localhost'")
		return errors.New("Please specify broker address via '-b'/'-h', such as: '-b test.mosquitto.org:1883', or '-h localhost'")
	}
	var addr, dp string
	dp = cmdr.GetStringR("mqtt.ports.default", "1883")
	if tlscfg.IsServerCertValid() {
		dp = cmdr.GetStringR("mqtt.ports.tls", "8883")
	}
	_, _, addr, err = ParseHostAddr(broker, dp)
	if err != nil {
		// logrus.Errorf("%v", err)
		s.Wrong(err, "get broker address ('%v') failed: %v", broker, err)
		return
	}

	s.Debug("got broker addr: %v", addr)
	if cmdr.InDebugging() || cmdr.GetBoolR("trace") {
		_ = trace.Start()
		defer trace.Stop()
	}

	client := NewClient(addr,
		WithConnectOpts(
			mqtt.WithConnectCleanSession(cmdr.GetBoolR("mqtt.client.pub.clean-session")), // keep the session
			mqtt.WithConnectClientId(clientId),
		),
		WithOnMqttConnected(s.pubOnConnected),
		WithOnMqttPubComp(s.pubOnPubComp),
		// WithOnDisconnected(s.pubOnDisconnected),
		WithOnTcpDisconnected(s.pubOnTcpDisconnected),
		// WithOnPublishing(s.pubOnPublishing),
		WithTcpTlsConfig(tlscfg),

		WithVerbose(verbose),
		WithDebugMode(cmdr.InDebugging()),
		WithConnectWillMsgFromCmdr("mqtt.client.pub"),
		// WithAlwaysReconnectEnable(true, 0),

		WithMaxProtocolLevel(level),
	)
	defer client.Close()

	tcp.HandleSignals(func(sig os.Signal) {
		s.Debug("signal[%v] caught and exiting this program", sig)
	})()

	return
}

func (s *pub) pubOnDisconnected(c *Client, pkg *mqtt.Pkg) {

}

var tcpClosedCh = make(chan struct{})

func (s *pub) pubOnTcpDisconnected(c *tcp.Client) {
	go func() {
		// time.Sleep(3 * time.Second)
		// cmdr.SignalQuitSignal()
		tcpClosedCh <- struct{}{}
	}()
}

func (s *pub) pubOnConnected(c *Client, pkg *mqtt.Pkg) {
	go s.pubOnConnectedImpl(c, pkg)
}

func (s *pub) pubOnPubComp(c *Client, pkg *mqtt.Pkg) {
	if vh, ok := pkg.VH.(*mqtt.PubCompVH); ok {
		fmt.Printf("qos 2: publish completed. pi=%v\n", vh.Id)
	}
}

func (s *pub) pubOnConnectedImpl(c *Client, pkg *mqtt.Pkg) {
	// 为了保留主题中可能的 $ 字符，关闭自动展开特性
	// NOTE 像 `$cid` 这样的主题带有专属含义，它将被 MQTTLIB 自动展开为客户端的 clientId 值
	topicName := cmdr.GetStringNoExpandR("mqtt.client.pub.topic")
	msg := cmdr.GetStringNoExpandR("mqtt.client.pub.payload")

	filename := cmdr.GetStringR("mqtt.client.pub.file")
	repeat := cmdr.GetIntR("mqtt.client.pub.repeat")
	startNumber := cmdr.GetIntR("mqtt.client.pub.start-number")
	randomString := cmdr.GetBoolR("mqtt.client.pub.rand")
	minLength := cmdr.GetIntR("mqtt.client.pub.min")
	maxLength := cmdr.GetIntR("mqtt.client.pub.min")
	retain := cmdr.GetBoolR("mqtt.client.pub.retain")

	qos := cmdr.GetIntR("mqtt.client.qos")

	if len(topicName) == 0 {
		s.Wrong(cmdr.ErrBadArg, "Topic-name must be specified! (use -t topic-name)")
		os.Exit(-1)
	}
	if len(msg) == 0 && len(filename) == 0 {
		s.Wrong(cmdr.ErrBadArg, "Message or filename should be specified! (use -m msg-content or -f filepath)")
		os.Exit(-1)
	}
	// verbose := cmdr.GetBoolR("verbose")
	s.Debug("connected, do publish: %v; %v, %v", topicName, msg, c.verbose)

	if len(filename) > 0 {
		inFile, err := os.Open(filename)
		if err != nil {
			s.Wrong(err, "can't read %v. error: %+v", filename, err)
			return
		}
		defer inFile.Close()

		scanner := bufio.NewScanner(inFile)
		for scanner.Scan() {
			// fmt.Println(scanner.Text()) // the line
			text := scanner.Text()
			arr := strings.SplitN(text, " ", 2)
			topic := topicName
			if len(arr) > 1 {
				text = fmt.Sprintf("%5d: %v", startNumber, arr[1])
				topic = arr[0]
				if strings.ContainsAny(topic, "#+") {
					topic = strings.ReplaceAll(topic, "#", "-")
					topic = strings.ReplaceAll(topic, "+", "-")
				}
			} else {
				text = fmt.Sprintf("%5d: %v", startNumber, text)
			}
			if err := c.SendPublishPkt(topic, []byte(text), mqtt.WithPubQoS(false, mqtt.QoSType(qos), retain)); err != nil {
				s.Wrong(err, "CAN'T send subscribe packet: %+v", err)
				return
			}
			startNumber++
		}
	} else {
		for i := startNumber; i < startNumber+repeat; i++ {
			var text string
			if randomString {
				text = fmt.Sprintf("[#%d] %v", i, msg)
				if minLength == maxLength && minLength > 1 {
					text = fmt.Sprintf("%v %v", text, tool.String(minLength))
				} else {
					text = fmt.Sprintf("%v %v", text, tool.StringVariantLength(minLength, maxLength))
				}
			} else if repeat > 1 {
				text = fmt.Sprintf("%v %v", msg, i)
				// s.Info("sending %v: %v", i, text)
			} else {
				text = msg
			}

			// fmt.Printf("-> %v: %v\n", topicName, text)
			if err := c.SendPublishPkt(topicName, []byte(text), mqtt.WithPubPacketIdentifier(uint16(i)), mqtt.WithPubQoS(false, mqtt.QoSType(qos), retain)); err != nil {
				s.Wrong(err, "CAN'T send publish packet: %+v", err)
			} else {
				time.Sleep(time.Millisecond)
			}
		}
		fmt.Printf("All %v messages sent.\n", repeat)
	}

	// if repeat < 3 {
	// 	time.Sleep(time.Second)
	// }

	// publish完成之后，客户端应该明确地结束 MQTT 通讯，
	if err := c.SendDisconnectPkt(errors.CloseNormal, "normal-close"); err != nil {
		s.Wrong(err, "CAN'T send disconnect packet: %+v", err)
	}

	if !cmdr.InDebugging() {
		tool.PressAnyKeyToContinue()
	}

	if !c.IsClosed() {
		c.Close()
	}

	<-tcpClosedCh           // 直到tcp连接关闭，而且用户任意按键之后
	cmdr.SignalQuitSignal() // 发出结束信号以便退出os信号监听循环
}
