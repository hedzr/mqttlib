/*
 * Copyright © 2020 Hedzr Yeh.
 */

package client

import (
	"fmt"
	"github.com/hedzr/cmdr"
	"gitlab.com/hedzr/mqttlib/pkg/logger"
	"gitlab.com/hedzr/mqttlib/pkg/mqttlib/errors"
	"gitlab.com/hedzr/mqttlib/pkg/mqttlib/mqtt"
	"gitlab.com/hedzr/mqttlib/pkg/tcp"
	"gitlab.com/hedzr/mqttlib/pkg/tcp/tls"
	"gitlab.com/hedzr/mqttlib/pkg/trace"
	"os"
	"runtime"
)

type sub struct {
	logger.Base
}

func SubRun(level mqtt.ProtocolLevel) {
	runtime.GOMAXPROCS(runtime.NumCPU())

	app := &sub{Base: logger.Base{Tag: "sub"}}

	_ = app.run(level)
}

func (s *sub) run(level mqtt.ProtocolLevel) (err error) {
	verbose := cmdr.GetBoolR("verbose")
	tlscfg := tls.NewCmdrTlsConfig("mqtt.client.tls", "mqtt.client")
	// broker := cmdr.GetStringR("mqtt.client.broker")
	broker := cmdr.GetStringR("mqtt.client.host", cmdr.GetStringR("mqtt.client.broker"))
	if len(broker) == 0 {
		s.Wrong(nil, "Please specify broker address via '-b'/'-h', such as: '-b test.mosquitto.org:1883', or '-h localhost'")
		return errors.New("Please specify broker address via '-b'/'-h', such as: '-b test.mosquitto.org:1883', or '-h localhost'")
	}
	var addr, dp string
	dp = cmdr.GetStringR("mqtt.ports.default", "1883")
	if tlscfg.IsServerCertValid() {
		dp = cmdr.GetStringR("mqtt.ports.tls", "8883")
	}
	_, _, addr, err = ParseHostAddr(broker, dp)
	if err != nil {
		// logrus.Errorf("%v", err)
		s.Wrong(err, "get broker address ('%v') failed: %v", addr, err)
		return
	}

	if cmdr.InDebugging() || cmdr.GetBoolR("trace") {
		_ = trace.Start()
		defer trace.Stop()
	}

	client := NewClient(addr,
		WithOnMqttConnected(s.subOnConnected),
		WithOnMqttPublishing(s.subOnPublishing),

		WithTcpTlsConfig(tlscfg),

		WithVerbose(verbose),
		WithDebugMode(cmdr.InDebugging()),
		WithConnectWillMsgFromCmdr("mqtt.client.sub"),
		WithAlwaysReconnectEnable(true, 0),

		WithMaxProtocolLevel(level),
	)
	defer client.Close()

	tcp.HandleSignals(func(sig os.Signal) {
		s.Debug("signal[%v] caught and exiting this program", sig)
	})()

	return
}

func (s *sub) subOnPublishing(c *Client, pkg *mqtt.Pkg) {
	if vh, ok := pkg.VH.(*mqtt.PublishVH); ok {
		if payload, ok := pkg.Payload.(*mqtt.PublishPayload); ok {
			if c.verbose {
				fmt.Printf("  - %s %v\n", vh.Topic, string(payload.Data))
			} else {
				fmt.Printf("  - %v\n", string(payload.Data))
			}
		}
	}
}

func (s *sub) subOnConnected(c *Client, pkg *mqtt.Pkg) {
	// 为了保留主题中可能的 $ 字符，关闭自动展开特性
	// NOTE 像 `$cid` 这样的主题带有专属含义，它将被 MQTTLIB 自动展开为客户端的 clientId 值
	// NOTE 在 TopicFilter 中，暂未解释诸如 `$cid` 等特定参数标记
	// NOTE 在 TopicFilter 中，按照 MQTT 标准进行阐述，包括：
	// - '$SYS/#'
	// - '#'
	// - 'a/b/+/d'
	// 等等通配符格式。
	topicFilters := cmdr.GetStringSliceR("mqtt.client.sub.topic-filter")

	qos := mqtt.QoSType(cmdr.GetIntR("mqtt.client.qos", int(mqtt.QoSDefault)))

	// verbose := cmdr.GetBoolR("verbose")
	s.Debug("connected, do subscribe: %v; %v, %v", topicFilters, qos, c.verbose)

	go func() {
		if err := c.SendSubscribePkt(topicFilters, qos, c.verbose); err != nil {
			s.Wrong(err, "CAN'T send subscribe packet: %+v", err)
		}
	}()
}
