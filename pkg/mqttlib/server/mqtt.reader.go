/*
 * Copyright © 2020 Hedzr Yeh.
 */

package server

import (
	"bufio"
	"bytes"
	"gitlab.com/hedzr/mqttlib/pkg/logger"
	"gitlab.com/hedzr/mqttlib/pkg/mqttlib/errors"
	"gitlab.com/hedzr/mqttlib/pkg/mqttlib/mqtt"
	"gitlab.com/hedzr/mqttlib/pkg/mqttlib/mqtt/codec"
	"gitlab.com/hedzr/mqttlib/pkg/trace"
	"io"
	"net"
	"time"
)

// mqtt(s)://[username[:password]@]host[:port]/topic

type mqttReader struct {
	logger.Base

	reader *bufio.Reader
	// writer      *bufio.Writer
	largeBuffer *bytes.Buffer

	// header []byte
	// buffer []byte
	// cur    int

	context *mqtt.StateContext
}

func (rw *mqttReader) GetContext() *mqtt.StateContext {
	return rw.context
}

func (rw *mqttReader) GetClientId() string {
	return rw.context.ConnectParam.ClientId
}

func newMqttReader(conn net.Conn, opts ...mqtt.StateContextOpt) *mqttReader {

	reader := bufio.NewReader(conn)
	writer := bufio.NewWriter(conn)
	// writer := conn

	var closer io.Closer = conn

	ctx := mqtt.NewMqttServerContext("", writer, closer, opts...)

	this := &mqttReader{
		Base: logger.Base{Tag: "mqtt.reader"},
		// header: make([]byte, 2),
		// buffer: make([]byte, tcp.DefaultBufferSize),
		reader:      reader, // writer:      writer,
		largeBuffer: bytes.NewBuffer([]byte{}),
		context:     ctx,
	}

	// ctx.writer = writer

	return this
}

// Process implements Processor interface to announce that i will process the incoming data in Read().
func (rw *mqttReader) Process(buf []byte, in io.Reader, out io.Writer) (nn int, err error) {
	// nn = len(buf)
	// rw.Debug("     - just for debugging: mqttReader.Process processed %v bytes (%v, '%v')", nn, buf[:nn], string(buf[:nn]))
	return
}

// // Write to write bytes back to remote client.
// // mqttReader implements Writer interface too.
// func (rw *mqttReader) Write(p []byte) (nn int, err error) {
// 	nn, err = rw.writer.Write(p)
// 	if err == nil {
// 		err = rw.writer.Flush()
// 	}
// 	return
// }

// func (rw *mqttReader) GetFixedHeader() (rptType mqtt.ReportType, DUP, RETAIN bool, QoS int) {
// 	rptType = mqtt.ReportType(rw.header[0] >> 4)
// 	flags := rw.header[0] & 0x0f
// 	DUP, RETAIN, QoS = rw.GetFlagsAs(flags)
// 	return
// }

func (rw *mqttReader) GetFlagsAs(flags uint8) (DUP, RETAIN bool, QoS mqtt.QoSType) {
	if (flags & 0x08) != 0 {
		DUP = true
	}
	if (flags & 0x01) != 0 {
		RETAIN = true
	}
	QoS = mqtt.QoSType((flags & 0x06) >> 1)
	return
}

// Read reads remote side data from rw.reader and returns the dummy data to caller.
// Read do process the read data right away itself, caller must not try to process
// the data returned from the callee again. For this purpose, mqttReader implements
// Processor interface to announce it's a finisher about the remote data read.
func (rw *mqttReader) Read(p []byte) (nn int, err error) {
	// if rw.remains.Len() > 0 {
	// 	rw.remains.Write(p)
	// 	p = rw.remains.Bytes()
	// }

	var pos, eat = 0, 0
	var b1, b2 byte
	var pkg mqtt.Pkg
	// var ate int

	pkg.ReceivedTime = time.Now().UTC()

	// read header
	b1, err = rw.reader.ReadByte()
	if err != nil {
		return
	}
	b2, err = rw.reader.ReadByte()
	if err != nil {
		return
	}

	// fixed header read

	// rptType, DUP, RETAIN, QoS := rw.GetFixedHeader()
	pkg.ReportType = mqtt.ReportType(b1 >> 4)
	pkg.Flags = b1 & 0x0f
	pkg.DUP, pkg.RETAIN, pkg.QoS = rw.GetFlagsAs(pkg.Flags)
	if pkg.QoS == 3 {
		err = errors.ErrCodePacketCorrupt.New("unexpect qos value %v", pkg.QoS)
		return // 3.3.1.4
	}
	// rw.Debug("length = %v, rptType = %v, DUP = %v, RETAIN = %v, QoS = %v", pkg.length, pkg.rptType, pkg.DUP, pkg.RETAIN, pkg.QoS)

	p[0], p[1] = b1, b2
	// var length int
	pkg.Length, eat, err = codec.DecodeIntAndCopyToWithReader(b2, rw.reader, p, 2)
	if err != nil {
		rw.Wrong(err, "can't read length bytes (eat=%v)", eat)
		return
	}

	pkg.HdrLen = 2 + eat
	pos += 2 + eat

	rw.Trace("       mqtt-reader reading new pkt %s with length %v+%v (%v)", pkg.ReportType, pos, pkg.Length, p[0:pos])

	// payload
	if pos+pkg.Length < len(p) {
		slice := p[pos : pos+pkg.Length]
	READ_MORE:
		nn, err = rw.reader.Read(slice)
		if err != nil || nn != len(slice) {
			// rw.Wrong(err, "can't read mqtt payload (expect %v bytes, but %v bytes read)", pkg.length, nn)
			slice = p[pos+nn : pos+pkg.Length]
			goto READ_MORE
		} else {
			// p = rw.buffer[:length]
			// copy(p, rw.buffer[:length])
			// rw.Debug("payload read %v bytes: %v", nn, p[:nn])

			// slice := p[pos : pos+pkg.length]
			nn = pos + pkg.Length
			pkg.Data = p[pos:nn]
			// rw.remains.Reset()

			if trace.IsEnabled() {
				rw.Trace(">>> ▿RECV %v bytes: %v", nn, p[:nn])
				// nn = len(buf)
				// rw.Debug("     - just for debugging: mqttReader.Process processed %v bytes (%v, '%v')", nn, buf[:nn], string(buf[:nn]))
			}

			err = rw.doProcessPkg(&pkg)
			if err != nil {
				rw.Wrong(err, "mqtt doProcessPkg has error, in mqttReader.Read()")
			}
			// if trace.IsEnabled() {
			// 	nn, err = rw.Process(p[:nn], rw.conn, rw.wr)
			// }
		}
	} else {
		// rw.Debug("TO-DO  read the large block if payload.length > 4KB (or pos+pkg.length >= len(p)): pos=%v,pkg.length=%v,p.len=%v", pos, pkg.Length, len(p))

		var written int64
		fullLen := pos + pkg.Length
		rw.largeBuffer.Grow(pkg.Length)
		written, err = io.CopyN(rw.largeBuffer, rw.reader, int64(pkg.Length))
		if err != nil || written != int64(pkg.Length) {
			rw.Wrong(err, "can't read mqtt vh+payload (expect %v bytes, but %v bytes read)", pkg.Length, written)
		} else {
			nn = fullLen
			pkg.Data = rw.largeBuffer.Bytes()
			rw.largeBuffer.Reset()

			if trace.IsEnabled() {
				rw.Trace(">>> ▿RECV %v bytes: %v + %v", nn, p[0:pos], pkg.Data)
				// nn = len(buf)
				// rw.Debug("     - just for debugging: mqttReader.Process processed %v bytes (%v, '%v')", nn, buf[:nn], string(buf[:nn]))
			}

			err = rw.doProcessPkg(&pkg)
			if err != nil {
				rw.Wrong(err, "mqtt doProcessPkg has error, in mqttReader.Read()")
			}
		}
	}

	// } else {
	// 	rw.wrong(err, "can't read mqtt fixed header")
	// }
	return
}

// 中文是不是太不正常？这取决于内存余量够不够多。三个goland加上两个调试一对tcp server+client的话，在goland中输入中文会出现不可忍受的延迟，浮动窗口不弹出或者不消失。哎呀有趣有趣，搜狗不会出现这样de问题；但随即我已彻底卸载了搜狗输入法，这家伙搜集我的各种键入信息，代码、密码、不可描述的搜索关键词之类的，我还是不能交给他啊，起码从现在开始不能再交给它了，搜狗现在是和腾讯打得火热，重点是国内的互联网"大咖"，有一个算一个，没有一个值得我尊敬，他们聚敛财富的方式太脏了。
func (rw *mqttReader) doProcessPkg(pkg *mqtt.Pkg) (err error) {
	if trace.IsEnabled() {
		rw.Debug("    ▽ INCOMING MQTT-pkg: %+v", pkg)
	}
	_, err = rw.context.Advance(pkg)
	return
}
