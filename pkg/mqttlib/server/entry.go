/*
 * Copyright © 2020 Hedzr Yeh.
 */

package server

import (
	"fmt"
	"github.com/hedzr/cmdr"
	"github.com/sirupsen/logrus"
	"io"
	"net"
	"os"
	"strings"
	"time"

	"gitlab.com/hedzr/mqttlib/pkg/logger"
	"gitlab.com/hedzr/mqttlib/pkg/mqttlib/errors"
	"gitlab.com/hedzr/mqttlib/pkg/mqttlib/mqtt"
	"gitlab.com/hedzr/mqttlib/pkg/tcp"
	"gitlab.com/hedzr/mqttlib/pkg/tcp/tls"
	"gitlab.com/hedzr/mqttlib/pkg/tool/vxconf"
	"gitlab.com/hedzr/mqttlib/pkg/trace"
)

func GetServer() ServerCntr {
	return svr
}

type ServerCntr interface {
	GetTcpServer() *tcp.Server
	EnableSysStats(enabled bool)
	EnableSysStatsLog(enabled bool)
}

// Run runs a mqtt server and block it on os signals
func Run(opts ...ServerOpt) (err error) {
	if svr != nil {
		return
	}

	var (
		broker, host, port string
		addr               string
		wsport             string
		what               string
		storageEnabled     bool
	)

	broker = cmdr.GetStringR("mqtt.server.addr") // for cmdline opt '--addr'
	host, port, err = net.SplitHostPort(broker)
	if err != nil {
		logrus.Errorf("get broker address failed: %v", err)
		return
	}

	if port == "" {
		port = cmdr.GetStringR("mqtt.ports.default", "1883") // extract from config file
	}

	addr = net.JoinHostPort(host, port)

	bdr := newbdr()
	svr = bdr

	what = cmdr.GetStringR("mqtt.server.storage.what", "gob")
	storageEnabled = cmdr.GetBoolR("mqtt.server.storage.enabled", true)
	if storageEnabled {
		switch what {
		case "gob":
			what = fmt.Sprintf("mqtt.server.storage.%s.%s", what, vxconf.RunModeExt())
			dataDir := cmdr.GetStringRP(what, "data-dir", "/var/lib/$APPNAME/gob-data")
			// filepath := fmt.Sprintf("%s/session.store.ser", dataDir)
			// // logrus.Debugf("reading %v...", filepath)
			// WithSessionStoreFile(filepath)(bdr)

			savingPeriod := cmdr.GetDurationRP(what, "saving-period", 5*time.Minute)
			WithSessionStoreSavingPeriod(savingPeriod)(bdr)

			savingEnabled := cmdr.GetBoolRP(what, "enabled", true)
			WithSessionStoreSavingEnabled(savingEnabled)(bdr)

			rw := mqtt.NewPathRWGob(mqtt.WithPathRWGobDataDir(dataDir))
			rw.Start()
			WithSessionStoreDefaultOpts(mqtt.WithDefaultSessionStorePathRW(rw))(bdr)

		case "redis":
			what = fmt.Sprintf("mqtt.server.storage.%s.%s", what, vxconf.RunModeExt())
			rw := mqtt.NewPathRWRedis(mqtt.WithPathRWRedisPrefix(what))
			rw.Start()
			WithSessionStoreDefaultOpts(mqtt.WithDefaultSessionStorePathRW(rw))(bdr)
		}
	}

	for _, opt := range opts {
		opt(bdr)
	}

	if bdr.enableTracing {
		err = trace.Start()
	}

	if bdr.mqttOverWebSocketEnabled {
		wsport = "80"
	}
	if bdr.CmdrTlsConfig.IsCertValid() {
		// MQTT over TLS
		port = cmdr.GetStringR("mqtt.ports.tls", "8883") // "443", 8883
		if bdr.mqttOverWebSocketEnabled {
			wsport = cmdr.GetStringR("mqtt.ports.websocket", "443")
		}
		addr = net.JoinHostPort(host, port)
	}

	bdr.sessionStore = mqtt.NewDefaultSessionStore(bdr.defaultSessionStoreOpts...)
	bdr.sessionStore.EnableResetStorage(false)

	if cmdr.GetBoolR("mqtt.server.dry-run") == false {
		// make the internal mqtt server
		bdr.server = tcp.StartServer(addr,
			tcp.WithServerReadWriter(bdr.makeBuilder()),
			tcp.WithServerConnectedWithClient(bdr.onTcpConnected),
			tcp.WithServerDisconnectedWithClient(bdr.onTcpDisconnected),
			tcp.WithServerListening(bdr.onTcpServerListening),
			tcp.WithTlsConfig(bdr.CmdrTlsConfig),
		)

		// and bind the WS endpoint to this internal server, so that the
		// packets from WS can be put into the internal server.
		if bdr.mqttOverWebSocketEnabled {
			err = newWsRouter(bdr, net.JoinHostPort(host, wsport), bdr.CmdrTlsConfig)
		}
	}

	defer func() {
		if bdr.mqttOverWebSocketEnabled {
			bdr.wsRouter.Stop()
		}
		if bdr.server != nil {
			bdr.server.Stop()
		}
		bdr.sessionStore.Close()
		if bdr.enableTracing {
			trace.Stop()
		}
	}()

	if cmdr.GetBoolR("mqtt.server.dry-run") == false {
		tcp.HandleSignals(func(s os.Signal) {
			logrus.Debugf("signal[%v] caught and exiting this program", s)
		})()
	}

	return
}

func newbdr() *bdr {
	bdr := &bdr{
		upTime: time.Now().UTC(),
		// strictClientId: true,
	}
	// bdr.CmdrTlsConfig.InitTlsConfigFromConfigFile()
	return bdr
}

// svr MUST BE singleton, here is just fast-coding shortcut but not gracefully.
var svr *bdr

type bdr struct {
	// authenticator     mqtt.Authenticator
	// stateRegistry     mqtt.StateRegistry
	// sessionStore      mqtt.SessionStore
	// enableRiskControl bool
	// enableAuth        bool
	// maxQoS            mqtt.QoSType
	upTime        time.Time
	enableTracing bool
	// strictClientId    bool
	// strictProtcolName bool
	// resetStorage            bool
	sessionStore            mqtt.SessionStore
	contextOpts             []mqtt.StateContextOpt
	defaultSessionStoreOpts []mqtt.DefaultSessionStoreOpt
	// redisOpOpts             []redis.RedisOpOpt

	CmdrTlsConfig *tls.CmdrTlsConfig

	mqttsnMode               bool
	mqttOverWebSocketEnabled bool
	server                   *tcp.Server
	wsRouter                 *wsRouter
}

func (bdr *bdr) GetTcpServer() *tcp.Server {
	return bdr.server
}

func (bdr *bdr) EnableSysStats(enabled bool) {
	// if cmdr.GetBoolR("mqtt.server.no-sys-stats") != !enabled {
	// 	cmdr.Set("mqtt.server.no-sys-stats", !enabled)
	// }
	bdr.sessionStore.EnableSysStats(enabled)
}

func (bdr *bdr) EnableSysStatsLog(enabled bool) {
	bdr.sessionStore.EnableSysStatsLog(enabled)
}

func (bdr *bdr) onTcpServerListening(ss *tcp.Server, l net.Listener) {
	bdr.sessionStore.BeforeServerListening()
}

func (bdr *bdr) onTcpConnected(ss *tcp.Server, conn net.Conn) {
	logger.WithField("client", conn.RemoteAddr()).
		Infof("♦︎ server.onTcpConnected: A client connected")
}

func (bdr *bdr) onTcpDisconnected(ss *tcp.Server, conn net.Conn, reader io.Reader) {
	if mqr, ok := reader.(*mqttReader); ok {
		if err := mqr.context.CloseWithReason(errors.CloseByPeer); err != nil {
			if !strings.Contains(err.Error(), "use of closed network connection") {
				logger.WithField("client", conn.RemoteAddr()).
					Errorf("♦︎ server.onTcpDisconnected: <mqttReader> context.close failed: %+v", err)
				return
			}
		}
		logger.WithField("normal-close", mqr.context.IsNormalClosed()).
			WithField("client", conn.RemoteAddr()).
			Infof("♦︎ server.onTcpDisconnected: <mqttReader> context.close closed.")
		return
	}
	logger.WithField("client", conn.RemoteAddr()).
		Warnf("♦︎ <unknown reader> A client closed")
}

func (bdr *bdr) makeBuilder() func(ss *tcp.Server, conn net.Conn, tsConnected time.Time) (in io.Reader, out io.Writer) {
	return func(ss *tcp.Server, conn net.Conn, tsConnected time.Time) (in io.Reader, out io.Writer) {
		v := newMqttReader(conn, bdr.contextOpts...)
		in, out = v, v.context
		return
	}
}
