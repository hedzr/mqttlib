/*
 * Copyright © 2020 Hedzr Yeh.
 */

package server

import (
	"gitlab.com/hedzr/mqttlib/pkg/mqttlib/mqtt"
	"gitlab.com/hedzr/mqttlib/pkg/tcp/tls"
	"time"
)

type ServerOpt func(*bdr)

func WithMaxVersion(v string) ServerOpt {
	return func(i *bdr) {
		i.contextOpts = append(i.contextOpts, mqtt.WithMaxVersion(v))
	}
}

func WithMaxQoS(maxQoS mqtt.QoSType) ServerOpt {
	return func(i *bdr) {
		i.contextOpts = append(i.contextOpts, mqtt.WithMaxQoS(maxQoS))
	}
}

func WithMaxProtocolLevel(lvl mqtt.ProtocolLevel) ServerOpt {
	return func(i *bdr) {
		i.contextOpts = append(i.contextOpts, mqtt.WithMaxProtocolLevel(lvl))
	}
}

func WithStateContextOpts(opts ...mqtt.StateContextOpt) ServerOpt {
	return func(i *bdr) {
		i.contextOpts = append(i.contextOpts, opts...)
	}
}

func WithTLSConfig(prefixInConfigFile, prefixInCommandline string) ServerOpt {
	return func(i *bdr) {
		ss := tls.NewCmdrTlsConfig(prefixInConfigFile, prefixInCommandline)
		i.CmdrTlsConfig = ss
	}
}

func WithMQTTSNMode(enabled bool) ServerOpt {
	return func(i *bdr) {
		i.mqttsnMode = enabled
	}
}

func WithMQTTOverWebSocket(enabled bool) ServerOpt {
	return func(i *bdr) {
		i.mqttOverWebSocketEnabled = enabled
	}
}

// func WithDefaultSessionStoreOpts(opts ...mqtt.DefaultSessionStoreOpt) ServerOpt {
// 	return func(i *bdr) {
// 		i.contextOpts = append(i.contextOpts, mqtt.WithDefaultSessionStoreOpts(opts...))
// 	}
// }

func WithTracingEnabled(enableTracing bool) ServerOpt {
	return func(i *bdr) {
		i.enableTracing = enableTracing
	}
}

func WithStrictClientId(strict bool) ServerOpt {
	return func(i *bdr) {
		// i.strictClientId = strict
		i.contextOpts = append(i.contextOpts, mqtt.WithStrictClientId(strict))
	}
}

func WithStrictProtocolName(strict bool) ServerOpt {
	return func(i *bdr) {
		// i.strictProtcolName = strict
		i.contextOpts = append(i.contextOpts, mqtt.WithStrictProtocolName(strict))
	}
}

// func WithResetStorage(bb bool) ServerOpt {
// 	return func(i *bdr) {
// 		i.resetStorage = bb
// 	}
// }

// func WithSessionStoreRedisOpts(opts ...redis.RedisOpOpt) ServerOpt {
// 	return func(i *bdr) {
// 		// opt := mqtt.WithPathRWRedisOpts(opts...)
// 		i.redisOpOpts = append(i.redisOpOpts, opts...)
// 	}
// }

func WithSessionStoreDefaultOpts(opts ...mqtt.DefaultSessionStoreOpt) ServerOpt {
	return func(i *bdr) {
		i.defaultSessionStoreOpts = append(i.defaultSessionStoreOpts, opts...)
	}
}

// func WithSessionStoreFile(filepath string) ServerOpt {
// 	return func(i *bdr) {
// 		opt := mqtt.WithDefaultSessionStoreSavingFile(filepath)
// 		// i.contextOpts = append(i.contextOpts, mqtt.WithDefaultSessionStoreOpts(opt))
// 		i.defaultSessionStoreOpts = append(i.defaultSessionStoreOpts, opt)
// 	}
// }

func WithSessionStoreSavingPeriod(d time.Duration) ServerOpt {
	return func(i *bdr) {
		opt := mqtt.WithDefaultSessionStoreSavingPeriod(d)
		// i.contextOpts = append(i.contextOpts, mqtt.WithDefaultSessionStoreOpts(opt))
		i.defaultSessionStoreOpts = append(i.defaultSessionStoreOpts, opt)
	}
}

// WithSessionStoreSavingEnabled the saving routine should be run
func WithSessionStoreSavingEnabled(persist bool) ServerOpt {
	return func(i *bdr) {
		opt := mqtt.WithDefaultSessionStoreSavingEnabled(persist)
		i.defaultSessionStoreOpts = append(i.defaultSessionStoreOpts, opt)
	}
}

func WithSessionStoreNoSysStats(b bool) ServerOpt {
	return func(i *bdr) {
		opt := mqtt.WithDefaultSessionStoreNoSysStats(b)
		i.defaultSessionStoreOpts = append(i.defaultSessionStoreOpts, opt)
	}
}

func WithSessionStoreNoSysStatsLog(b bool) ServerOpt {
	return func(i *bdr) {
		opt := mqtt.WithDefaultSessionStoreNoSysStatsLog(b)
		i.defaultSessionStoreOpts = append(i.defaultSessionStoreOpts, opt)
	}
}

func WithSessionStoreResetStorage(b bool) ServerOpt {
	return func(i *bdr) {
		opt := mqtt.WithDefaultSessionStoreResetStorage(b)
		i.defaultSessionStoreOpts = append(i.defaultSessionStoreOpts, opt)
	}
}

// func WithSessionStore(sessionStore mqtt.SessionStore) ServerOpt {
// 	return func(i *bdr) {
// 		i.sessionStore = sessionStore
// 	}
// }
//
// func WithStateRegistry(registry mqtt.StateRegistry) ServerOpt {
// 	return func(i *bdr) {
// 		i.stateRegistry = registry
// 	}
// }
//
// func WithAuthenticator(authenticator mqtt.Authenticator) ServerOpt {
// 	return func(i *bdr) {
// 		i.authenticator = authenticator
// 	}
// }
//
// func WithAuthEnabled(enabled bool) ServerOpt {
// 	return func(i *bdr) {
// 		i.enableAuth = enabled
// 	}
// }
//
// func WithRiskControlEnabled(enabled bool) ServerOpt {
// 	return func(i *bdr) {
// 		i.enableRiskControl = enabled
// 	}
// }
