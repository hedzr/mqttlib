/*
 * Copyright © 2020 Hedzr Yeh.
 */

package server

import (
	"gitlab.com/hedzr/mqttlib/pkg/logger"
	"gitlab.com/hedzr/mqttlib/pkg/mqttlib/errors"
	"gitlab.com/hedzr/mqttlib/pkg/mqttlib/mqtt"
	"gitlab.com/hedzr/mqttlib/pkg/mqttlib/mqtt/codec"
	"gitlab.com/hedzr/mqttlib/pkg/mqttlib/ws/melody"
	"io"
	"strconv"
	"sync/atomic"
	"time"
)

func newMqttConnection(s *melody.Session, counter int, opts ...mqtt.StateContextOpt) *wsMqttConnection {
	wsmc := &wsMqttConnection{
		strconv.Itoa(int(counter)), "0", "0",
		nil, s, 0,
		s.Request.Header.Get("Sec-Websocket-Protocol"), // == "mqttv3.1"
	}
	writer := wsmc
	closer := wsmc
	wsmc.context = mqtt.NewMqttServerContext("", writer, closer, opts...)
	mqtt.WithMaxVersion(wsmc.reqSubProtocol)(wsmc.context)
	mqtt.WithStrictProtocolName(false)(wsmc.context)
	return wsmc
}

type wsMqttConnection struct {
	ID, X, Y       string
	context        *mqtt.StateContext
	session        *melody.Session
	closed         int32
	reqSubProtocol string // == "mqttv3.1", "mqttv5.0", ...
}

func (sc *wsMqttConnection) Close() error {
	if atomic.CompareAndSwapInt32(&sc.closed, 0, 1) {
		return sc.session.Close()
	}
	return nil
}

func (sc *wsMqttConnection) Write(p []byte) (n int, err error) {
	if atomic.CompareAndSwapInt32(&sc.closed, 0, 0) {
		err = sc.session.WriteBinary(p)
		if err == nil {
			n = len(p)
		}
	}
	return
}

func (sc *wsMqttConnection) getFlagsAs(flags uint8) (DUP, RETAIN bool, QoS mqtt.QoSType) {
	if (flags & 0x08) != 0 {
		DUP = true
	}
	if (flags & 0x01) != 0 {
		RETAIN = true
	}
	QoS = mqtt.QoSType((flags & 0x06) >> 1)
	return
}

func (sc *wsMqttConnection) TryParseMqttPkg(msg []byte) (pkg *mqtt.Pkg, err error) {

	if len(msg) < 2 {
		err = io.ErrShortBuffer
		return
	}

	var pos, eat = 0, 0
	var b1 byte

	pkg = new(mqtt.Pkg)
	pkg.ReceivedTime = time.Now().UTC()

	// read header
	b1 = msg[pos]

	// fixed header read

	pkg.ReportType = mqtt.ReportType(b1 >> 4)
	pkg.Flags = b1 & 0x0f
	pkg.DUP, pkg.RETAIN, pkg.QoS = sc.getFlagsAs(pkg.Flags)
	if pkg.QoS == 3 {
		err = errors.ErrCodePacketCorrupt.New("unexpect qos value %v", pkg.QoS)
		pkg = nil
		return // 3.3.1.4
	}

	pkg.Length, eat = codec.DecodeIntFrom(msg, 1)
	pkg.HdrLen = 1 + eat
	pos += 1 + eat

	if pos+pkg.Length < len(msg) {
		err = io.ErrShortBuffer
		pkg = nil
	} else if pos+pkg.Length == len(msg) {
		pkg.Data = msg[pos:]
		logger.Debugf("[WS] mqtt packet incoming: %v", *pkg)
	} else {
		err = io.ErrUnexpectedEOF
		pkg = nil
	}

	return
}

func (sc *wsMqttConnection) ParseMqttPkg(pkg *mqtt.Pkg) (err error) {
	_, err = sc.context.Advance(pkg)
	return
}
