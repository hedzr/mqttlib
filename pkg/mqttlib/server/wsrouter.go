/*
 * Copyright © 2020 Hedzr Yeh.
 */

package server

import (
	"context"
	"github.com/gin-contrib/static"
	"github.com/gin-gonic/gin"
	"github.com/hedzr/cmdr"
	"github.com/sirupsen/logrus"
	"gitlab.com/hedzr/mqttlib/pkg/logger"
	"gitlab.com/hedzr/mqttlib/pkg/mqttlib/ws/melody"
	"gitlab.com/hedzr/mqttlib/pkg/tcp/tls"
	"net/http"
	"path"
	"strings"
	"sync"
	"sync/atomic"
	"time"
)

func newWsRouter(bdr *bdr, addr string, config *tls.CmdrTlsConfig) (err error) {
	if !bdr.mqttOverWebSocketEnabled {
		return
	}

	wsr := &wsRouter{lock: new(sync.Mutex), bdr: bdr}
	err = wsr.init(addr, config)
	bdr.wsRouter = wsr
	return
}

type wsRouter struct {
	bdr         *bdr
	server      *http.Server
	router      *gin.Engine
	mrouter     *melody.Melody
	connections map[*melody.Session]*wsMqttConnection
	lock        *sync.Mutex
	counter     int32
	actived     int32
	publicDir   string
}

func (wsr *wsRouter) Stop() {
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	if err := wsr.server.Shutdown(ctx); err != nil {
		logrus.Fatal("Server Shutdown:", err)
	}
	logrus.Println("Server exiting")
}

func (wsr *wsRouter) init(addr string, config *tls.CmdrTlsConfig) (err error) {
	wsr.router = gin.Default()
	wsr.mrouter = melody.New()
	wsr.mrouter.Config.MessageBufferSize = int(cmdr.GetKibibytesR("mqtt.server.websocket.message-buffer-size", 4096))
	wsr.mrouter.Config.MaxMessageSize = int64(cmdr.GetKibibytesR("mqtt.server.websocket.max-message-size", 8192))
	wsr.connections = make(map[*melody.Session]*wsMqttConnection)

	logger.Tracef("[WS] max-msg-size <= %v", wsr.mrouter.Config.MaxMessageSize)

	for _, d := range []string{"./public",
		path.Join(cmdr.GetExecutableDir(), "./public"),
		path.Join(cmdr.GetExecutableDir(), "../public"),
		path.Join(cmdr.GetUsedConfigSubDir(), "../public"),
	} {
		file := path.Join(d, "index.html")
		if cmdr.FileExists(file) {
			wsr.publicDir = d
			break
		}
	}

	// wsr.router.GET("/", func(c *gin.Context) {
	// 	http.ServeFile(c.Writer, c.Request, path.Join(wsr.publicDir, "index.html"))
	// })
	wsr.router.Use(static.Serve("/", static.LocalFile(wsr.publicDir, false)))
	wsr.router.GET("/ping", func(c *gin.Context) {
		c.String(200, "pong")
	})

	wsr.router.GET("/ws", wsr.wsEntry)
	wsr.router.GET("/mqtt", wsr.wsEntry)

	wsr.mrouter.HandleConnect(wsr.handleWsConnect)
	wsr.mrouter.HandleDisconnect(wsr.handleWsDisconnect)
	wsr.mrouter.HandleMessage(wsr.handleWsTextPacket)
	wsr.mrouter.HandleMessageBinary(wsr.handleWsBinaryPacket)

	if config.IsServerCertValid() {
		wsr.server = &http.Server{
			Addr:      addr,
			Handler:   wsr.router,
			TLSConfig: config.ToServerTlsConfig(),
		}
	}

	go func() {
		if config.IsServerCertValid() {
			// err = wsr.router.RunTLS(addr, config.Cert, config.Key)
			logrus.Infof("MQTT over WebSocket listening and serving HTTPS on %s\n", addr)
			if err = wsr.server.ListenAndServeTLS(config.Cert, config.Key); err != nil && err != http.ErrServerClosed {
				logrus.Fatalf("listen: %s\n", err)
			}
		} else {
			// err = wsr.router.Run(addr)
			logrus.Infof("MQTT over WebSocket listening and serving HTTP on %s\n", addr)
			if err = wsr.server.ListenAndServe(); err != nil && err != http.ErrServerClosed {
				logrus.Fatalf("listen: %s\n", err)
			}
		}
	}()
	return
}

func (wsr *wsRouter) wsEntry(c *gin.Context) {
	_ = wsr.mrouter.HandleRequest(c.Writer, c.Request)
}

func (wsr *wsRouter) handleWsConnect(s *melody.Session) {
	var ok bool
	wsr.lock.Lock()
	if _, ok = wsr.connections[s]; !ok {
		wsmc := newMqttConnection(s, int(wsr.counter), wsr.bdr.contextOpts...)
		wsr.connections[s] = wsmc
	}
	// for _, info := range wsr.connections {
	// 	s.Write([]byte("set " + info.ID + " " + info.X + " " + info.Y))
	// }
	// s.Write([]byte("iam " + wsr.connections[s].ID))
	wsr.lock.Unlock()
	if ok {
		atomic.AddInt32(&wsr.counter, 1)
		atomic.AddInt32(&wsr.actived, 1)
		logger.Debugf("[WS][%v] +1 %v", wsr.actived, s.Request.UserAgent())
		s.Write([]byte("ok " + wsr.connections[s].ID))
	} else {
		logger.Debugf("[WS][%v] +DUP %v", wsr.actived, s.Request.UserAgent())
	}
}

func (wsr *wsRouter) handleWsDisconnect(s *melody.Session) {
	wsr.lock.Lock()
	// wsr.mrouter.BroadcastOthers([]byte("dis "+wsr.connections[s].ID), s)
	delete(wsr.connections, s)
	wsr.lock.Unlock()
	atomic.AddInt32(&wsr.actived, -1)
	logger.Debugf("[WS][%v] -1 %v", wsr.actived, s.Request.UserAgent())
}

func (wsr *wsRouter) handleWsTextPacket(s *melody.Session, msg []byte) {
	p := strings.Split(string(msg), " ")
	wsr.lock.Lock()
	info := wsr.connections[s]
	if len(p) == 2 {
		info.X = p[0]
		info.Y = p[1]
		wsr.mrouter.BroadcastOthers([]byte("set "+info.ID+" "+info.X+" "+info.Y), s)
	}
	wsr.lock.Unlock()
	logger.Debugf("[WS][%v] TXT %v", wsr.actived, s.Request.UserAgent())
}

func (wsr *wsRouter) handleWsBinaryPacket(s *melody.Session, msg []byte) {
	wsr.lock.Lock()
	info := wsr.connections[s]
	wsr.lock.Unlock()
	logger.Debugf("[WS][%v] BIN %v", wsr.actived, s.Request.UserAgent())
	logger.Debugf("[WS]     % x %q", msg, string(msg[4:10]))
	// logger.Debugf("%v: ws binary packet in. headers: %v", info.ID, s.Request.Header)

	// wsr.bdr.PutMqttPacket(msg)
	// pkg := &mqtt.Pkg{
	// 	ReceivedTime:     time.Now().UTC(),
	// 	HdrLen:           2,
	// 	Length:           0,
	// 	ReportType:       0,
	// 	DUP:              false,
	// 	RETAIN:           false,
	// 	QoS:              0,
	// 	Flags:            0,
	// 	PacketIdentifier: 0,
	// 	VH:               nil,
	// 	Payload:          nil,
	// 	Data:             nil,
	// }

	pkg, err := info.TryParseMqttPkg(msg)
	if err == nil {
		err = info.ParseMqttPkg(pkg)
	}
	if err != nil {
		logger.Warnf("NOT MQTT Packet: %v", err)
	}
}
