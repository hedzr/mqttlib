/*
 * Copyright © 2020 Hedzr Yeh.
 */

package auth

func NewMySQLAuthenticator() Authenticator {
	return &mysqlAuth{}
}

type mysqlAuth struct {
}

func (s *mysqlAuth) Authenticate(clientId, username, password string) (passed bool) {
	return
}
