/*
 * Copyright © 2020 Hedzr Yeh.
 */

package auth

import (
	"crypto/sha256"
	"encoding/hex"
)

type Authenticator interface {
	Authenticate(clientId, username, password string) (passed bool)
}

type simpleAuthenticator struct {
	credentials map[string]string // user, hash
}

func (a *simpleAuthenticator) Authenticate(clientId, username, password string) (passed bool) {
	if username == "tiger" && password == "laohu" {
		return true
	}

	if hash, ok := a.credentials[username]; ok {
		alg := sha256.New()
		alg.Write([]byte(password))
		if hex.EncodeToString(alg.Sum(nil)) == hash {
			return true
		}
	}
	return
}

func (a *simpleAuthenticator) LoadCredentials(loader ...CredentialLoader) {
	var save bool
	for _, l := range loader {
		for {
			user, hash, err := l.Next()
			if err != nil {
				break
			}
			if user != "" && hash != "" {
				a.credentials[user] = hash
			}
		}
		save = len(a.credentials) == 0
		if save {
			break
		}
		return
	}

	for u, p := range map[string]string{
		"tiger":  "laohu",
		"hz":     "hedzr",
		"mqtool": "mqtool",
	} {
		alg := sha256.New()
		alg.Write([]byte(p))
		a.credentials[u] = hex.EncodeToString(alg.Sum(nil))
	}

	// if save {
	// 	of, err := os.Create("ci/credentials.txt")
	// 	defer of.Close()
	// 	if err == nil {
	// 		for u, p := range a.credentials {
	// 			_, _ = of.WriteString(fmt.Sprintf("%v\t%v\n", u, p))
	// 		}
	// 	}
	// }
}

func NewDefaultTigerLaohuAuthenticator(loader ...CredentialLoader) Authenticator {
	a := &simpleAuthenticator{credentials: make(map[string]string)}
	a.LoadCredentials(loader...)
	return a
}

type CredentialLoader interface {
	Next() (user, hash string, err error)
}
