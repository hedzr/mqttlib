/*
 * Copyright © 2020 Hedzr Yeh.
 */

package vxconf

import (
	"encoding/json"
	"gopkg.in/yaml.v2"
	"io/ioutil"
)

// JSON -----------------------------------------------------------------------

// ParseJSON reads a JSON configuration from the given string.
func ParseJSON(cfg string) (*AppConfig, error) {
	return parseJSON([]byte(cfg))
}

// ParseJSONFile reads a JSON configuration from the given filename.
func ParseJSONFile(filename string) (*AppConfig, error) {
	cfg, err := ioutil.ReadFile(filename)
	if err != nil {
		return nil, err
	}
	return parseJSON(cfg)
}

// parseJSON performs the real JSON parsing.
func parseJSON(cfg []byte) (*AppConfig, error) {
	var out interface{}
	var err error
	if err = json.Unmarshal(cfg, &out); err != nil {
		return nil, err
	}
	if out, err = normalizeValue(out); err != nil {
		return nil, err
	}
	return &AppConfig{Root: out}, nil
}

// RenderJSON renders a JSON configuration.
func RenderJSON(cfg interface{}) (string, error) {
	b, err := json.Marshal(cfg)
	if err != nil {
		return "", err
	}
	return string(b), nil
}

// YAML -----------------------------------------------------------------------

// ParseYaml reads a YAML configuration from the given string.
func ParseYaml(cfg string) (*AppConfig, error) {
	return parseYaml([]byte(cfg))
}

// ParseYamlFile reads a YAML configuration from the given filename.
func ParseYamlFile(filename string) (*AppConfig, error) {
	cfg, err := ioutil.ReadFile(filename)
	if err != nil {
		return nil, err
	}
	return parseYaml(cfg)
}

// parseYaml performs the real YAML parsing.
func parseYaml(cfg []byte) (*AppConfig, error) {
	var out interface{}
	var err error
	if err = yaml.Unmarshal(cfg, &out); err != nil {
		return nil, err
	}
	if out, err = normalizeValue(out); err != nil {
		return nil, err
	}
	return &AppConfig{Root: out}, nil
}

// RenderYaml renders a YAML configuration.
func RenderYaml(cfg interface{}) (string, error) {
	b, err := yaml.Marshal(cfg)
	if err != nil {
		return "", err
	}
	return UnescapeUnicode(b), nil
}
