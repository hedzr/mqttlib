/*
 * Copyright © 2020 Hedzr Yeh.
 */

package vxconf

func IfStr(condition bool, valueIfTrue, valueIfFalse string) string {
	if condition {
		return valueIfTrue
	}
	return valueIfFalse
}

func IfInt(condition bool, valueIfTrue, valueIfFalse int) int {
	if condition {
		return valueIfTrue
	}
	return valueIfFalse
}
