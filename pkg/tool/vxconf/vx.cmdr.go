/*
 * Copyright © 2020 Hedzr Yeh.
 */

package vxconf

import (
	"fmt"
	"github.com/hedzr/cmdr"
	"github.com/hedzr/cmdr/conf"
	"gopkg.in/yaml.v2"
	"os"
	"strconv"
	"strings"
	"time"
)

//

//
// ----------------------------------------------------------------
//

//

// ToBool parses the string to bool type.
// these strings will be scanned as true: "1", "y", "t", "yes", "true", "ok", "on"
func ToBool(s interface{}) (ret bool) {
	switch v := s.(type) {
	case bool:
		ret = v
	case string:
		ret = StringToBool(v)
	default:
		vs := fmt.Sprint(s)
		ret = StringToBool(vs)
	}
	return
}

// StringToBool parses the string to bool type.
// these strings will be scanned as true: "1", "y", "t", "yes", "true", "ok", "on"
func StringToBool(s string) (ret bool) {
	switch strings.ToLower(s) {
	case "1", "y", "t", "yes", "true", "ok", "on":
		ret = true
	}
	return
}

// func GetConfigValue(key, defaultValue string) (ret string) {
// 	ret = vxconf.GetString(key)
// 	if len(ret) == 0 {
// 		ret = defaultValue
// 	}
// 	return
// }

// GetBoolR returns the cmdr value or defaultValue
// Deprecated
func GetBoolR(key string, defaultValue bool) (ret bool) {
	s := GetStringR(key, fmt.Sprintf("%v", defaultValue))
	ret = ToBool(s)
	return
}

// GetBoolRP returns the cmdr value
// Deprecated
func GetBoolRP(prefix, key string, defaultValue bool) (ret bool) {
	s := GetStringRP(prefix, key, fmt.Sprintf("%v", defaultValue))
	ret = ToBool(s)
	return
}

// GetIntR returns the cmdr value or defaultValue
// Deprecated
func GetIntR(key string, defaultValue int) (ret int) {
	s := GetStringR(key, fmt.Sprintf("%v", defaultValue))
	var e error
	if ret, e = strconv.Atoi(s); e != nil {
		ret = defaultValue
	}
	return
}

// GetDurationR returns the cmdr value or defaultValue
// Deprecated
func GetDurationR(key string, defaultValue time.Duration) (ret time.Duration) {
	s := GetStringR(key, fmt.Sprintf("%v", defaultValue))
	var e error
	if ret, e = time.ParseDuration(s); e != nil {
		ret = defaultValue
	}
	return
}

// GetMapR returns the cmdr value or defaultValue
// Deprecated
func GetMapR(key string, defaultValue map[string]interface{}) (ret map[string]interface{}) {
	ret = cmdr.GetMapR(key)
	if len(ret) == 0 {
		ret = defaultValue
	}
	return
}

// GetStringSliceR returns the cmdr value or defaultValue
// Deprecated
func GetStringSliceR(key string, defaultValue []string) (ret []string) {
	ret = cmdr.GetStringSliceR(key)
	if len(ret) == 0 {
		ret = defaultValue
	}
	return
}

// GetStringRP returns the cmdr value or defaultValue
// Deprecated
func GetStringRP(prefix, key, defaultValue string) (ret string) {
	ret = cmdr.GetStringRP(prefix, key)
	if len(ret) == 0 {
		ret = defaultValue
	}
	return
}

// GetStringR returns the cmdr value or defaultValue
// Deprecated
func GetStringR(key, defaultValue string) (ret string) {
	ret = cmdr.GetStringR(key)
	if len(ret) == 0 {
		ret = defaultValue
	}
	if strings.Contains(ret, "$") {
		// logrus.Debug("RPC_ADDR = ", os.Getenv("RPC_ADDR"))
		ret = os.ExpandEnv(ret)
	}
	return
}

// GetR returns the cmdr value
// Deprecated
func GetR(key string) (ret interface{}) {
	ret = cmdr.GetR(key)
	return
}

// RunModeExt returns the running mode: devel, prod, ...
// While env-var 'ENT_POS' exists, it looks like: devel-local, devel-lan, devel, ...
func RunModeExt() string {
	s := RunMode()
	if pos, ok := os.LookupEnv(fmt.Sprintf("%v_POS", conf.AppName)); ok && len(pos) > 0 {
		return fmt.Sprintf("%v-%v", s, pos)
	}
	return s
}

// RunMode returns the running mode: devel, prod, ...
func RunMode() string {
	s := cmdr.GetStringR("runmode", "devel")

	// FOR RUNMODE:
	// 不考虑配置中心刷新后的可能性，总是检查本机的环境变量设置并当做第一优先级
	if v, ok := os.LookupEnv(fmt.Sprintf("%v_RUNMODE", strings.ToUpper(conf.AppName))); ok {
		s = v
	}
	return s
}

// IsProd return true if app is in production mode.
func IsProd() bool {
	switch RunMode() {
	case "prod", "production":
		return true
	}
	return false
}

// LoadSectionTo returns error while cannot yaml Marshal and Unmarshal
func LoadSectionTo(sectionKeyPath string, configHolder interface{}) (err error) {
	var b []byte

	runMode := RunMode()

	aKey := fmt.Sprintf("%s.%s", sectionKeyPath, runMode)
	fObj := cmdr.GetMapR(aKey)
	if fObj == nil {
		fObj = cmdr.GetMapR(sectionKeyPath)
	}

	b, err = yaml.Marshal(fObj)
	if err != nil {
		return
	}

	err = yaml.Unmarshal(b, configHolder)
	if err != nil {
		return
	}

	// logrus.Debugf("configuration section got: %v", configHolder)
	return
}
