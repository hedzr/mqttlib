/*
 * Copyright © 2020 Hedzr Yeh.
 */

package tool

import (
	"fmt"
	"github.com/hedzr/cmdr"
	"github.com/sirupsen/logrus"
	"gitlab.com/hedzr/mqttlib/pkg/tool/vxconf"
	"strconv"
	"strings"
)

// LoadGRPCListenConfig loads config info of MAIN grpc service
// Generally, it loads a map from `prefix` (such as "server.grpc.<service-name>") and extract sub-keys: "listen", "id", "disabled"
func LoadGRPCListenConfig(prefix string) (listen string, id string, disabled bool, port int) {
	listen = vxconf.GetStringR(fmt.Sprintf("%v.listen", prefix), "")
	if len(listen) == 0 {
		logrus.Fatalf("Wrong configurations (wrong or stale data at remote config-center?), CANNOT read `%v` from config options.", prefix)
	}

	id = vxconf.GetStringR(fmt.Sprintf("%v.id", prefix), "")
	disabled = vxconf.GetBoolR(fmt.Sprintf("%v.disabled", prefix), false)
	parts := strings.Split(listen, ":")
	port, _ = strconv.Atoi(parts[1])
	return
}

// LoadGRPCListenConfig loads config info from `prefix`.`serviceName`
func LoadGRPCListenSpecialConfig(prefix, serviceName string) (listen string, id string, disabled bool, port int) {
	listen = vxconf.GetStringR(fmt.Sprintf("%v.%v.listen", prefix, serviceName), "")
	if len(listen) == 0 {
		logrus.Fatalf("Wrong configurations (wrong or stale data at remote config-center?), CANNOT read `%v` from config options.", prefix)
	}

	id = vxconf.GetStringR(fmt.Sprintf("%v.%v.id", prefix, serviceName), "")
	disabled = vxconf.GetBoolR(fmt.Sprintf("%v.%v.disabled", prefix, serviceName), false)
	parts := strings.Split(listen, ":")
	port, _ = strconv.Atoi(parts[1])
	return
}

// IncGrpcListen for grpc server
func IncGrpcListen(prefix string) (listen string, port int) {
	listen = vxconf.GetStringR(fmt.Sprintf("%v.listen", prefix), "")
	if len(listen) == 0 {
		logrus.Fatalf("Wrong configurations (wrong or stale data at remote config-center?), CANNOT read `%v` from config options.", prefix)
	}

	parts := strings.Split(listen, ":")
	p, _ := strconv.Atoi(parts[1])
	p++
	port = p
	listen = fmt.Sprintf("%s:%d", parts[0], p)
	cmdr.Set(fmt.Sprintf("%s.listen", prefix), listen)
	return
}

//
// what: "addr", "grpc", "health"
//
func FindService(what string) {
	// /
}
