/*
 * Copyright © 2020 Hedzr Yeh.
 */

/*
 */

package tool

import (
	"context"
	"fmt"
	"github.com/hedzr/cmdr"
	"gitlab.com/hedzr/mqttlib/pkg/tool/vxconf"
)

var port int
var host string

func Port() int {
	return port
}

func IncPort() int {
	port++
	return port
}

func IncPortAndAddr(prefix string) (addr string) {
	addr = fmt.Sprintf("%s:%d", host, IncPort())
	cmdr.Set(fmt.Sprintf("%s.port", prefix), Port())
	return
}

// LoadHostDefinition loads the dependence info from config entry `server.deps.xxx`
// LoadHostDefinition("server.deps.apply")
func LoadHostDefinition(prefix string) (addr string) {
	addr = vxconf.GetStringR(fmt.Sprintf("%s.addr", prefix), "")
	host = vxconf.GetStringR(fmt.Sprintf("%s.host", prefix), "0.0.0.0")
	port = vxconf.GetIntR(fmt.Sprintf("%s.port", prefix), 2300)
	if len(host) > 0 && len(addr) == 0 {
		addr = fmt.Sprintf("%s:%d", host, port)
		// } else {
		// logrus.Debugf("prefix = %v, addr = %v", prefix, addr)
		// parts := strings.Split(addr, ":")
		// host = parts[0]
		// port, _ = strconv.Atoi(parts[1])
	}
	return
}

//
//
//

// func Open(r *forwarder.Registrar) store.KVStore {
// 	switch s.Source {
// 	case "etcd":
// 		r.Client = etcd.New(&s.Etcd[s.Env])
// 	case "consul":
// 	}
// 	return r.Client
// }
//
// func Close(r *forwarder.Registrar) {
// 	r.Close()
// }

// // var reFind = regexp.MustCompile(`^\s*[^\s\:]+\:\s*["']?.*\\u.*["']?\s*$`)
// var reFind = regexp.MustCompile(`[^\s\:]+\:\s*["']?.*\\u.*["']?`)
// var reFindU = regexp.MustCompile(`\\u[0-9a-fA-F]{4}`)
//
// func expandUnicodeInYamlLine(line []byte) []byte {
// 	// TODO: restrict this to the quoted string value
// 	return reFindU.ReplaceAllFunc(line, expandUnicodeRune)
// }
//
// func expandUnicodeRune(esc []byte) []byte {
// 	ri, _ := strconv.ParseInt(string(esc[2:]), 16, 32)
// 	r := rune(ri)
// 	repr := make([]byte, utf8.RuneLen(r))
// 	utf8.EncodeRune(repr, r)
// 	return repr
// }
//
// // UnescapeUnicode 解码 \uxxxx 为 unicode 字符; 但是输入的 b 应该是 yaml 格式
// func UnescapeUnicode(b []byte) string {
// 	b = reFind.ReplaceAllFunc(b, expandUnicodeInYamlLine)
// 	return string(b)
// }

func IsCancelled(err error) (ret bool) {
	ret = err == context.Canceled
	return
}

func IsDeadline(err error) (ret bool) {
	ret = err == context.DeadlineExceeded
	return
}

func IsCancelledOrDeadline(err error) (ret bool) {
	ret = err == context.DeadlineExceeded || err == context.Canceled
	return
}
