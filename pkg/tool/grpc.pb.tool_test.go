/*
 * Copyright © 2020 Hedzr Yeh.
 */

package tool_test

import (
	"gitlab.com/hedzr/mqttlib/pkg/tool"
	"strconv"
	"testing"
)

func inthex(i int64) string {
	return strconv.FormatUint(uint64(i), 16)
}

func uinthex(i uint64) string {
	return strconv.FormatUint(i, 16)
}

// ref: https://blog.csdn.net/fullsail/article/details/42686699
//
// tools_test.go:35: in, r, ate = [136 1], 136/0x88, 2
// tools_test.go:41: in, r, ate = [136 145 2], 34952/0x8888, 3
// tools_test.go:47: in, r, ate = [232 209 163 199 14], 3907578088/0xe8e8e8e8, 5
// tools_test.go:54: in, r, ate = [232 209 163 199 142 157 186 244 232 1], 16782920098433788136/0xe8e8e8e8e8e8e8e8, 10
//
func TestInt64(t *testing.T) {
	// var buf = proto.Buffer{}
	// buf.SetBuf([]byte{123})
	// buf.EncodeZigzag32(256)
	// buf.DebugPrint("", nil)

	in := []byte{0x88, 0x01}
	r, ate := tool.DecodeZigZagInt(in) // 0x88
	t.Logf("in, r, ate = %v, %v/0x%v, %v", in, r, inthex(r), ate)
	if r != 0x88 || ate != 2 {
		t.Errorf("BAD decoder")
	}
	in = []byte{0x88, 0x91, 0x02}
	r, ate = tool.DecodeZigZagInt(in) // 0x8888
	t.Logf("in, r, ate = %v, %v/0x%v, %v", in, r, inthex(r), ate)
	if r != 0x8888 || ate != 3 {
		t.Errorf("BAD decoder")
	}
	in = []byte{0xe8, 0xd1, 0xa3, 0xc7, 0x0e}
	r, ate = tool.DecodeZigZagInt(in) // 0xE8E8E8E8
	t.Logf("in, r, ate = %v, %v/0x%v, %v", in, r, inthex(r), ate)
	if r != 0xe8e8e8e8 || ate != 5 {
		t.Errorf("BAD decoder")
	}

	in = []byte{0xe8, 0xd1, 0xa3, 0xc7, 0x8e, 0x9d, 0xba, 0xf4, 0xe8, 0x01}
	ur, ate2 := tool.DecodeZigZagUint(in)
	t.Logf("in, r, ate = %v, %v/0x%v, %v", in, ur, uinthex(ur), ate2)
	if ur != 0xe8e8e8e8e8e8e8e8 || ate2 != 10 {
		t.Errorf("BAD decoder")
	}
}
