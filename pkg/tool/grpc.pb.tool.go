/*
 * Copyright © 2020 Hedzr Yeh.
 */

package tool

import (
	"github.com/golang/protobuf/ptypes"
	"github.com/golang/protobuf/ptypes/timestamp"
	"github.com/sirupsen/logrus"
	"reflect"
	"time"
)

const (
	DefaultNilTimeNano = -6795364578871345152 // =DefaultNilTime
)

func NullIf(in, defaultValue interface{}) (out interface{}) {
	var (
		tIn, tDef reflect.Type
		vtIn      reflect.Value
	)
	vtIn = reflect.ValueOf(in)
	tIn = reflect.TypeOf(in)
	tDef = reflect.TypeOf(defaultValue)
	if tDef.AssignableTo(tIn) {
		if vtIn.IsNil() {
			out = defaultValue
			return
		}
	}
	out = in
	return
}

func BlankIf(in, defaultValue string) (out string) {
	if len(in) == 0 {
		return defaultValue
	}
	return in
}

func ZeroIf(in, defaultValue uint64) (out uint64) {
	if in == 0 {
		return defaultValue
	}
	return in
}

func ZeroIfAny(in, defaultValue interface{}) (out interface{}) {
	var (
		tIn, tDef reflect.Type
		vtIn      reflect.Value
	)
	vtIn = reflect.ValueOf(in)
	tIn = reflect.TypeOf(in)
	tDef = reflect.TypeOf(defaultValue)
	if tDef.AssignableTo(tIn) {
		switch vtIn.Kind() {
		case reflect.Int:
		case reflect.Int8:
		case reflect.Int16:
		case reflect.Int32:
		case reflect.Int64:
		case reflect.Uint:
		case reflect.Uint8:
		case reflect.Uint16:
		case reflect.Uint32:
		case reflect.Uint64:
		default:
			return in
		}
		if vtIn.Int() == 0 {
			return defaultValue
		}
	}
	return in
}

func TimestampToTime(ts *timestamp.Timestamp) (tm time.Time) {
	var err error
	tm, err = ptypes.Timestamp(ts)
	if err != nil {
		logrus.Warnf("CAN'T extract pb ptypes.timestamp to time: %v", err)
	}
	return
}

func TimeToTimestamp(tm time.Time) (ts *timestamp.Timestamp) {
	var err error
	ts, err = ptypes.TimestampProto(tm)
	if err != nil {
		logrus.Warnf("CAN'T convert time to pb ptypes.timestamp: %v", err)
	}
	return
}

// utcTime is a nanoseconds
func Int64ToTime(utcTime int64) (tm time.Time) {
	// if utcTime == DefaultNilTimeNano {
	// 	return time.Date(1, 1, 1, 0, 0, 0, 0, time.UTC)
	// }
	return time.Unix(0, utcTime)
}

func Int64SecondsToTime(utcTimeSeconds int64) (tm time.Time) {
	return time.Unix(utcTimeSeconds, 0)
}

func Int64ToTimestamp(utcTime int64) (ts *timestamp.Timestamp) {
	tm := Int64ToTime(utcTime)
	ts = &timestamp.Timestamp{Seconds: int64(tm.Unix()), Nanos: int32(tm.Nanosecond())}
	return
}

// DecodeZigZagInt 解码 protobuffer 的变长整数（int8,int16,int32,int64）
func DecodeZigZagInt(b []byte) (r int64, ate int) {
	var b1 byte
	var sh uint
	for i := 0; i < len(b); i++ {
		b1 = b[i]
		if b1&0x80 == 0 {
			r += int64(uint64(b1)) << sh
			break
		} else {
			r += int64(b1&0x7f) << sh
			sh += 7
			ate++
		}
	}
	ate++
	return
}

func DecodeZigZagUint(b []byte) (r uint64, ate int) {
	var b1 byte
	var sh uint
	for i := 0; i < len(b); i++ {
		b1 = b[i]
		if b1&0x80 == 0 {
			r += uint64(uint64(b1)) << sh
			break
		} else {
			r += uint64(b1&0x7f) << sh
			sh += 7
			ate++
		}
	}
	ate++
	return
}
