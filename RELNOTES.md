



### v1.1.0 - Planned





### v1.0.9

- [x] 共享订阅
- [x] --null-message


### v1.0.7

Added:

- [x] mqtt over websocket 部分
  - 添加用于测试的网页
  - 修改 websocket 库，解决存在的兼容性问题
  - 实现 MQTT over WebSocket 功能，支持 ws, wss 协议




### v1.0.5

Added:

- [x] certs，TLS/SSL 部分
  - 添加证书
  - 实现 MQTT over TLSv2+
  - 实现双向鉴权
- [x] auth 部分
  - 实现标准的 SHA256 user+hash 鉴权算法: 从 `ci/credentials.txt` 中装载
  - 可以自行提供诸如 mysql，redis 等不同等鉴权算法
  - 可以利用 MQTT over TLS 的双向鉴权来进行客户端鉴权



### v1.0.3

本版本将被作为临时发布版本，为全面测试提供一个标定。

作为完整支持MQTT全部版本的最终版本，将会发布到 v1.1.x 。

- 支持 mqttv3, mqttv3.1.1, mqttv5
- 支持 qos 0, 1, 2
- 支持 retained messages，retained will messages
- 支持 `$SYS/`
- 支持通配符订阅
- [x] certs，TLS/SSL 部分
  - 添加证书
  - 实现 MQTT over TLSv2+
  - 实现双向鉴权
- [x] auth 部分
  - 实现标准的 SHA256 user+hash 鉴权算法: 从 `ci/credentials.txt` 中装载
  - 可以自行提供诸如 mysql，redis 等不同等鉴权算法
  - 可以利用 MQTT over TLS 的双向鉴权来进行客户端鉴权
- 

NOT YET:

- [ ] redis 持久化和集群支持
  - 一半代码：初始化、卸载；操作模型; 定时flush；
  - [ ] read/write
- [ ] 共享订阅
- [ ] 完整的 `$SYS`
  - [ ] o
- [ ] --null-message
- [ ] mqttsn 部分 - 不计划实施支持
- 其他的协议的各项细节



### v1.0.2

- mqttv3.1.1 就绪待测试




### v1.0.1

- 基本MQTT功能



### v1.0.0

- basic release
- MQTT框架结构成型
