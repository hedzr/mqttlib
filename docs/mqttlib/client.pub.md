# client publish - 发布主题

## Usage

详细参数可以参阅 `--help` 输出。

```bash

$ bin/triot mqtt client publish --help

Usages:
    triot mqtt client pub [tail args...] [Options] [Parent/Global Options]

Description:
    test mqtt client

Options:
  -r,  --retain                                   retain flag (default=false)
  -t,  --topic, --topic-name                      topic (default=)
  [Payload]
  -f,  --file, --input-file                       treat the file content as msg body line by line (default=)
  -p,  --payload, --msg                           message payload (default=)
  [Auto Repeat]
       --max=MAX, --max-length                    random string maximal length, eg. 8192 (default MAX=32768)
       --min=MIN, --min-length                    random string minimal length, eg. 4096 (default MIN=0)
  -rnd, --rand, --random                          append random string at the end of message text (default=false)
  -rpt, --repeat                                  repeat times. (default=1)
  -sn, --start-number                             the start number of publishing messages. (default=1)
  [Session]
  -cs, --clean-session                            clean or keep session, default is keep session (default=false)
  -cid, --client-id                               the client id. default is $mqtt.client.pub.persist-client-id (default=)
  [Will Msg]
  -wp, --will-payload, --will-msg                 will message (default=)
  -wq, --will-qos                                 will qos (= 0,1,2) (default=0)
  -wr, --will-retain                              will retain flag (default=false)
  -wt, --will-topic, --will-topic-name            will topic (default=)

Parent (`client`) Options:
  -b,  --broker=BROKER                            broker address, = -h (default BROKER=)
  -h,  --host=HOST[:PORT]                         broker address, = -b (default HOST[:PORT]=)
  [MQTT QoS]
  -qos, --qos                                     qos (= 0,1,2) (default=0)
  [Test]
  -p,  --parallel=PARALLEL                        publish how many goroutines (default PARALLEL=1)
       --sleep                                    sleep time between each sending (default=0s)

Parent (`mqtt`) Options:
  [MQTT Version]
  -v3                                             enable MQTT V3.x (default=false)
  -v311                                           enable MQTT V3.1.1 (default=true)
  -v5                                             enable MQTT V5.x (default=true)

Global Options:
  [Misc]
       --config=[Locations of config files]       load config files from where you specified (default [Locations of config files]=)
  -q,  --quiet                                    No more screen output. (default=false)
  -tr, --trace, --enable-trace                    enable trace mode for tcp/mqtt send/recv data dump (default=false)
  -v,  --verbose                                  Show this help screen (default=false)

Type '-h'/'-?' or '--help' to get command help screen.

```


## 释例

简单的发布信息

```bash
bin/triot mq c pub -b localhost -t 'a/b/c' -p 'hello world' --qos 0
bin/triot mq c pub -b localhost -t 'a/b/c' -p 'hello world' --qos 1 --retain
```

可以使用一个文本文件作为 payload 进行逐行文本的连续不断的发布：

```bash
bin/triot mq c pub -b localhost -t 'a/b/c' -f ./line-by-line.log.source
```

可以以一个文本为基准，重复发送多遍（自动追加计数器后缀）：

```bash
bin/triot mq c pub -b localhost -t 'a/b/c' -p 'hello' -rpt 100
```

可以使用随机文本发生器进行持续性发布：

```bash
bin/triot mq c pub -b localhost -t 'a/b/c' -p 'hello world' -rpt 1 --rand --min 4206 --max 4206
bin/triot mq c pub -b localhost -t 'a/b/c' -p 'hello world' -rpt 100 --rand --min 8 --max 80
```

- `-rpt 1` 表示发送一次; 等价于 `--repeat`
- `-rpt 12345678` 表示发送 12345678 次，这一尺度可被用于压测目的
- `--rand` 表示启用随机文本发生器，随机文本将被追加在 `-p` 指定的 Payload 文本之后
- `--min` 和 `--max` 指定 payload 的总长度区间，这在 `--rand` 被启用的情况下才是有意义的。


由于内置的TCP/MQTT解码层使用一个 4KB 大小的缓冲区，因此：

- ` --min 4206 --max 4206` 允许发送定长payload，且长度超出内部缓冲区尺寸，从而可以印证解码算法处理 MQTT 协议分包的正确性。
- ` --max 10000` 允许发送随机长度的payload，但上限为10000字节，从而可以进一步模拟真实场所下解码算法的正确性。



##### 发布主题时支持 '$cid'

```bash
bin/triot mq c pub -b localhost -t 'a/b/c/$cid' -p 'hello world' -rpt 10
```

但并不支持 payload 中的 `$cid` 替换。



## MQTT Version

注意到命令行参数：

```bash
Parent (`mqtt`) Options:
  [MQTT Version]
  -v3                                             enable MQTT V3.x (default=false)
  -v311                                           enable MQTT V3.1.1 (default=true)
  -v5                                             enable MQTT V5.x (default=true)

```

这意味着可以支持不同的版本。

