# mqtt server FAQ

## 持久化层

### gob 

一般情况下，gob持久化层使用 `/var/lib/triot/gob-data/session.store.ser` 进行持久化存储和加载。

因此测试、调试期间，可能需要频繁用到：

```bash
rm /var/lib/triot/gob-data/session.store.ser
```

此外，在开发机或最终部署时，都要注意 `/var/lib` 下需要root权限，所以在 server 首次启动之前，通常需要：

```bash
sudo mkdir /var/lib/triot
sudo chown -R $USER: /var/lib/triot
```










## 启停服务

```bash
bin/triot mqtt server
bin/triot mqtt server -a localhost:1883
```

CTRL-C 可以终止服务

今后的版本将会将这个入口进一步拓展，包含daemon化参数，启动、停止专用子命令等。


