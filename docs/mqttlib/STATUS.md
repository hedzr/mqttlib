# STATUS

- [x] 支持 mqtt3.1 / mqtt3.1.1协议（可选 mqtt5.0）
- [x] 支持 QoS 0, 1, 2
- [x] 支持 遗嘱
  client: 支持连接时指定遗嘱主题包含 '$cid' 字串，在 server 端
  将会将其展开为 clientId 的具体值。例如：
  ```bash
  $ mosquitto_sub -t a/b/c --will-topic '/devices/status/bad/$cid' --will-payload xxid-4 --will-qos 0 --will-retain
  ```
  client的命令行处理程序也可以考虑识别payload body文字中的 '$cid'
  并完成相似的逻辑，但mqttlib并不直接支持消息内容中的参数化替换。
  
- [x] '$cid' 字串展开
  在发布主题时，自动展开 '$cid' 标记
  
- [x] 支持 持久化
  - [x] RETAINED 消息
  - [x] QoS 1 或 2 消息（重试） 
  - [x] Sessions
  - [x] gob persistent layer
  - [ ] redis persistent layer, WIP
- [ ] 连接方式：
  - [x] over TCP 
  - [ ] over WebSocket 
  - [ ] SSL 
- [x] RETAINED 消息
- [x] 消息重试机制
- [x] 完全的通配符订阅支持
- [x] 消息重试机制
- [ ] 集群 
- [x] 自定义鉴权
- [ ] 共享订阅
- [ ] 吞吐量要求：
- [ ] $SYS 统计信息
- [ ] 管理界面，监控界面 
- [ ] 转接到：Redis，Kafka，……
- [ ] 流量控制
- [ ] 
- [ ] after server restarted
  - [x] 第三方 clients 将能够自动重连，相应的session也会正确地被重新构造  
  - [x] 内置的 Client 需要实现同样的机制 
  - [x] 支持 持久化
    - [ ] RETAINED 消息
    - [ ] QoS 1 或 2 消息（重试） 
    - [x] Sessions
  - [ ] 
  - [ ] 
  - [ ] 
- [ ] 
- [ ] server 
  - [x] 收到 pingreq 时 keepalive
- [ ] 
- [ ] 
- [ ] 
- [ ] 
- [ ] 
- [ ] 
- [ ] client
  - [x] 收到 pingresp 时 keepalive
- [ ] 
- [ ] 

## BUGS FOUND

- [x] [1] 多个客户端订阅到主题下时，pub一条消息会让客户端收到重复消息
- [?] [2] 客户端频繁获得 `received mqtt connected ack again` 警告
  在debug模式中挂起任何一端，较易收到警告，可能的原因有多种：
  1. server端检测到client keepalive ping超时，故关闭连接
  1. server发出的 SUBACK 被识别为无效，导致 `remains payload: ` 警告日志出现
  1. 其它异常情况
  
  客户端发生上述状况或者类似情况时，将进入错误的工作状态。
  
  随后一定的时间之内，客户端将不能正确响应和接收通知。
  
  然后服务端将会检测到错误应答的客户端并切断它，或者是因为无法从客户端连接上读取和写入导致i/o错误或 eof 错误，
  无论那种错误，该客户端将被切断，随后自动重连，重新进入正确的工作状态。

- [ ] retained msg sending logic for `$SYS`
- [x] `$SYS` 不应被发送给普通订阅者
- [x] `a/b/c` 的随机发送3次，无法被任何人收到
  > **REASON**:  
  > `func (s *pub) pubOnConnected(c *Client, pkg *mqtt.Pkg)` 锁住了 session.store 的 runloop 循环，导致 publish 信号无法被处理
- [x] `$SYS` 应该总是被 retained
- [x] QoS 2 server 不工作
  源于 ReadUint16 算法错误
- [x] QoS 2 client 不工作
  源于 pubrec 应答到 server 没有正确工作。











