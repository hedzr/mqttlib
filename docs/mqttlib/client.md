# introduction of client side

```bash
$ bin/triot mqtt client

Usages:
    triot mqtt client [Sub-Commands] [tail args...] [Options] [Parent/Global Options]

Description:
    test mqtt client

Sub-Commands:
  [Test]
  p, pub, publish                                 test mqtt client
  s, sub, subscribe                               test mqtt client

Options:
  -b,  --broker=BROKER                            broker (default BROKER='test.mosquitto.org:1883')
  -p,  --parallel=PARALLEL                        publish how many goroutines (default PARALLEL=1)
  -sp, --subpub=PUBSUB                            must sub or pub? (default PUBSUB=1)
  -t,  --times=TIMES                              publish times (default TIMES=1)
  -tpc, --topic=TOPIC                             topic should be made (default TOPIC=)
  [Test]
       --sleep                                    sleep time between each sending (default=0s)

Global Options:
  -tr, --trace, --enable-trace                    enable trace mode for tcp/mqtt send/recv data dump (default=false)
  -v,  --verbose                                  Show this help screen (default=false)
  [Misc]
       --config=[Locations of config files]       load config files from where you specified (default [Locations of config files]=)
  -q,  --quiet                                    No more screen output. (default=false)
  -v,  --verbose                                  Show this help screen (default=false)
  -V,  --version, --ver                           Show the version of this app. (default=false)

Type '-h' or '--help' to get command help screen.

```

#### 订阅所有主题

`-v` 使得收到推送信息时也同时打印出主题路径

```bash
bin/triot mq c sub -b localhost --tf '#' -v
```

#### 订阅所有主题，且登记遗嘱信息

```bash
bin/triot mq c sub -b localhost --tf '#' -v --will-topic 'STATUS/$cid' -wp 'offline' -wr true -wq 1
```

##### 登记遗嘱主题时支持 '$cid'

```bash
bin/triot mq c sub -b localhost --tf '#' -v --will-topic 'STATUS/$cid' -wp 'offline' -wr true -wq 1
```


#### 订阅特定主题

```bash
bin/triot mq c sub -b localhost --tf 'a/b/c' -v
```




