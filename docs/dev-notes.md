# Developing Notes

## For Developers

### 在 Goland 中调试

为了在 Goland 中调试 triot 应用程序，有必要打开日志的 Debug 级别。

这可以通过如下方式达成：

- 在 'Run/Debug Configurations' 菜单中，选择你的调试配置 A
- 在 A 配置中，'Go tool arguments:'选项中，输入 `-tags=delve`
- 以调试方式启动 A 配置，则 triot 能够识别到调试器的存在，并在 triot 配置的日志级别未达到 DebugLevel 的情况下自动调高日志输出级别。
- 详细代码请参考 `func EarlierInitLogger()`


### 提前准备

需要提前准备一些命令行工具以完成编译等相关工作。

```bash
go get -u golang.org/x/tools/cmd/stringer
go mod tidy
ls -la $GOPATH/bin/stringer
```

### 完整的 build

我们需要 `go generate` 机制来完成一些固定代码的自动生成。
因此，完整的构建过程依赖于如下的步骤：

```bash
go generate -n ./...
go build -v cli/main.go
```

`Makefile` 中的 targets 中，`compile` 已经包装了上述的构建过程，然而要注意的是，`build-linux` 和 `build-ci` 可能并未完成响应的包装，需要进一步测试和调试。


### Makefile Common Help

获取 Makefile 用法：

```bash
make help
make info
```

### `triot` 专用的 `make` 指令

```bash
# 编译为本机版本
make compile
# 编译为 linux 版本
make build-linux
# 为 CI 专用
make build-ci
```

### `triot` 的特定参数

1. `--version-sim 1.3.1`
   
   可以指定要模仿的版本号，而不是内置的编译发布版本号。
   
   > 此版本号在服务注册中心可见可用，也会影响到负载均衡器的选择算法。

