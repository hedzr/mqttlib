#!/bin/bash

#cat $filename | while read line
#do
#    #echo "Processing new line" >/dev/tty
#    #for word in $line
#    #do
#    #    echo $word
#    #done
#done

while read -r line; do
  topic="${line[0]}"
  payload="${line[@]:1}"
  mosquitto_pub -t "$topic" -m "$payload"
done <log.v.log
