/*
 * Copyright © 2020 Hedzr Yeh.
 */

// Package mqttlib is full-features MQTTv5/v3 server/client golang library.
package mqttlib

const (
	// AppName const
	AppName = "mqtool" // main app-name
	// Version const
	Version = "1.1.1" // version name
	// VersionInt const
	VersionInt = 0x010101 // using as

	// AppDesc const
	AppDesc = "go mqttlib tool"
	// AppDescLong const
	AppDescLong = `go mqttlib tool long`
	// AppExamples const
	AppExamples = `
$ {{.AppName}} gen shell [--bash|--zsh|--auto]
  generate bash/shell completion scripts
$ {{.AppName}} gen man
  generate linux man page 1
$ {{.AppName}} --help
  show help screen.
`
	// AppCopyright const
	AppCopyright = `go mqttlib (mqtool) tool`
	// AppAuthor const
	AppAuthor = `Hedzr`

	DefaultEtcdPrefix = "/mqtool.test"
)

const DefaultPort = 8883

// const DefaultPortUnsecure = 1_883

// func GetApiPrefix() string {
// 	return "/api/v1"
// }
//
// func GetApiVersion() string {
// 	return "v1"
// }

// func GetAppExitCh() chan bool {
// 	return appExitCh
// }
//
// var appExitCh = make(chan bool)

// func IsProd() bool {
// 	return vxconf.IsProd()
// }
