module gitlab.com/hedzr/mqttlib

go 1.13

// replace github.com/hedzr/errors => ../../../../golang-dev/src/github.com/hedzr/errors

// replace github.com/hedzr/cmdr => ../../../../golang-dev/src/github.com/hedzr/cmdr

// replace github.com/hedzr/logex v1.1.0 => ../../../../golang-dev/src/github.com/hedzr/logex

require (
	github.com/gin-contrib/static v0.0.0-20191128031702-f81c604d8ac2
	github.com/gin-gonic/gin v1.5.0
	github.com/go-redis/redis v6.15.6+incompatible
	github.com/go-swagger/go-swagger v0.22.0 // indirect
	github.com/golang/protobuf v1.3.3
	github.com/gorilla/websocket v1.4.1
	github.com/hedzr/cmdr v1.6.26
	github.com/hedzr/errors v1.1.18
	github.com/hedzr/logex v1.1.5
	github.com/kr/pretty v0.2.0 // indirect
	github.com/mediocregopher/radix/v3 v3.4.2
	github.com/onsi/ginkgo v1.10.3 // indirect
	github.com/onsi/gomega v1.7.1 // indirect
	github.com/pkg/errors v0.9.1 // indirect
	github.com/sirupsen/logrus v1.4.2
	golang.org/x/crypto v0.0.0-20200128174031-69ecbb4d6d5d // indirect
	golang.org/x/net v0.0.0-20200114155413-6afb5195e5aa // indirect
	golang.org/x/sys v0.0.0-20200124204421-9fbb57f87de9 // indirect
	golang.org/x/text v0.3.2 // indirect
	golang.org/x/tools v0.0.0-20200131211209-ecb101ed6550
	gopkg.in/yaml.v2 v2.2.8
)
