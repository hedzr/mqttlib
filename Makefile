include .env
-include .env.local
# ref: https://kodfabrik.com/journal/a-good-makefile-for-go/

PROJECTNAME=$(shell basename "$(PWD)")
APPNAME=$(patsubst "%",%,$(shell grep -E "AppName[ \t]+=[ \t]+" doc.go|grep -Eo "\\\".+\\\""))
VERSION=$(shell grep -E "Version[ \t]+=[ \t]+" doc.go|grep -Eo "[0-9.]+")
APPTITLE=$(shell grep -E "AppTitle[ \t]+=[ \t]+" doc.go|grep -Eo "\\\".+\\\"")

# Go related variables.
GOBASE = $(shell pwd)
#GOPATH="$(GOBASE)/vendor:$(GOBASE)"
#GOPATH=$(GOBASE)/vendor:$(GOBASE):$(shell dirname $(GOBASE))
GOPATH2= $(shell dirname $(shell dirname $(shell dirname $(GOBASE))))
GOPATH1= $(shell dirname $(GOPATH2))
ifneq ($(wildcard $(GOPATH2)/src),)
	GOPATH = $(GOPATH2)
else
	GOPATH = $(HOME)/go
endif
GOBIN  = $(GOBASE)/bin
GOFILES= $(wildcard *.go)
BIN    = $(GOPATH)/bin
GOLINT = $(BIN)/golint
GOCYCLO= $(BIN)/gocyclo
GOYOLO = $(BIN)/yolo

GO111MODULE   = on
GOPROXY_FINAL = $(or $(GOPROXY),https://athens.azurefd.net)

# Redirect error output to a file, so we can show it in development mode.
STDERR ?= /tmp/.$(PROJECTNAME)-stderr.txt

# PID file will keep the process id of the server
PID ?= /tmp/.$(PROJECTNAME).pid

# Make is verbose in Linux. Make it silent.
# MAKEFLAGS += --silent

#
LDFLAGS=
M = $(shell printf "\033[34;1m▶\033[0m")
ADDR = ":5q5q"
SERVER_START_ARG=server run
SERVER_STOP_ARG=server stop


goarch=amd64
W_PKG=github.com/hedzr/cmdr/conf
TIMESTAMP=$(shell date -u '+%Y-%m-%d_%I:%M:%S%p')
GITHASH=$(shell git rev-parse HEAD)
GOVERSION=$(shell go version)
LDFLAGS=-s -w -X '$(W_PKG).Buildstamp=$(TIMESTAMP)' -X '$(W_PKG).Githash=$(GITHASH)' -X '$(W_PKG).GoVersion=$(GOVERSION)' -X '$(W_PKG).Version=$(VERSION)' -X '$(W_PKG).AppName=$(APPNAME)'





.PHONY: install start stop restart start-server stop-server stop-server-std watch restart-server
.PHONY: tidy mod-init
.PHONY: compile exec clean fo-compile run 
.PHONY: build build-linux docker-build docker-upload build-ci
.PHONY: go-build go-generate go-mod-download go-get go-install go-clean



## install: Install missing dependencies. Runs `go mod download` internally.
install: info go-mod-download

## start: Start in development mode. Auto-starts when code changes.
start:
	bash -c "trap 'make stop' EXIT; $(MAKE) compile start-server watch run='make compile start-server'"

## stop: Stop development mode.
stop: stop-server

## restart: Restart development mode.
restart: restart-server




start-server: stop-server stop-server-std | $(GOBASE) $(GOYOLO)
	@echo "  >  starting... $(PROJECTNAME) is available at $(ADDR)"
	@-$(GOBIN)/$(PROJECTNAME) $(SERVER_START_ARG) 2>&1 & echo $$! > $(PID)
	@cat $(PID) | sed "/^/s/^/  \>  PID: /"

stop-server:
	@echo "  >  stopping $(PROJECTNAME) at $(ADDR)"
	@-$(GOBIN)/$(PROJECTNAME) $(SERVER_STOP_ARG)

stop-server-std:
	@echo "  >  stopping $(PROJECTNAME) at $(ADDR)"
	@-touch $(PID)
	@-kill `cat $(PID)` 2> /dev/null || true
	@-rm $(PID)

## watch: Run given command when code changes. eg; make watch run="echo 'hey'"
watch:  | $(GOBASE) $(GOYOLO)
	# @GOPATH=$(GOPATH) GOBIN=$(GOBIN) yolo -i . -e vendor -e bin -c "$(run)"
	@GOPATH=$(GOPATH) GOBIN=$(GOBIN) GO111MODULE=$(GO111MODULE) GOPROXY=$(GOPROXY_FINAL) \
	$(GOYOLO) -i . -e vendor -e bin -c "$(run)"

restart-server: stop-server start-server





## tidy: Go Module Tidy
tidy:
	@GOPATH=$(GOPATH) GOBIN=$(BIN) GO111MODULE=$(GO111MODULE) GOPROXY=$(GOPROXY_FINAL) \
	go mod tidy

## mod-init: Go Module init
mod-init:
	@GOPATH=$(GOPATH) GOBIN=$(BIN) GO111MODULE=$(GO111MODULE) GOPROXY=$(GOPROXY_FINAL) \
	go mod init

## compile: Compile the binary.
compile:
	@-touch $(STDERR)
	@-rm $(STDERR)
	@-$(MAKE) go-compile 2> $(STDERR)
	@cat $(STDERR) | sed -e '1s/.*/\nError:\n/'  | sed 's/make\[.*/ /' | sed "/^/s/^/     /" 1>&2

## exec: Run given cmd, wrapped with custom GOPATH. eg; make exec run="go test ./..."
exec:
	@GOPATH=$(GOPATH) GOBIN=$(BIN) GO111MODULE=$(GO111MODULE) GOPROXY=$(GOPROXY_FINAL) \
	$(run)

## clean: Clean build files. Runs `go clean` internally.
clean:
	@(MAKEFILE) go-clean

go-compile: go-clean go-generate go-build

## run: go run xxx
run:
	@GOPATH=$(GOPATH) GOBIN=$(GOBIN) GO111MODULE=$(GO111MODULE) GOPROXY=$(GOPROXY_FINAL) \
	go run -ldflags "$(LDFLAGS)" $(GOBASE)/cli/main.go

build: compile

## build-linux: build to linux executable, for LAN deploy manually.
build-linux:
	@echo "  >  Building linux binary..."
	@echo "  >  LDFLAGS = $(LDFLAGS)"
	$(foreach os, linux, \
	  echo "     Building $(GOBIN)/$(APPNAME)_$(os)_$(goarch)...$(os)"; \
	      GOARCH="$(goarch)" GOOS="$(os)" GOPATH="$(GOPATH)" GOBIN="$(GOBIN)" GO111MODULE="$(GO111MODULE)" GOPROXY="$(GOPROXY_FINAL)" \
	        go build -ldflags "$(LDFLAGS)" -o $(GOBIN)/$(APPNAME)_$(os)_$(goarch) $(GOBASE)/cli/main.go; \
		chmod +x $(GOBIN)/$(APPNAME)_$(os)_$(goarch); \
		ls -laG $(GOBIN)/$(APPNAME)_$(os)_$(goarch); \
		#echo "  >  Uploading to tc01 (LAN Server)..."; \
		#scp $(GOBIN)/$(PROJECTNAME)_$(os)_$(goarch) $(TC01_LAN):~/; \
		#rsync -avrztopg --progress --delete $(GOBASE)/ci/etc/$(APPNAME)/* $(TC01_LAN):~/.$(APPNAME)/; \
		#rsync -avrztopg --progress --delete $(GOBASE)/public $(TC01_LAN):~/; \
		## rsync -avrztopg --progress --delete $(GOBASE)/ci/certs/* $(TC01_LAN):~/.$(APPNAME)/certs/; \
	)
	#@ls -laG $(GOBIN)/*linux*

.PHONY: ./ci/alpine-base/
./ci/alpine-base/:
	$(MAKE) -C $@ # $(MAKECMDGOALS)

build-alpine: ./ci/alpine-base/
	@echo "done"

## docker-build: build docker container for this cli app
docker-build: build-alpine
	$(foreach os, linux, \
		echo "  >  copy to pack/, preparing for docker builder"; \
		rsync -avrztopg --delete $(GOBASE)/ci/etc $(GOBASE)/pack/; \
		rsync -avrztopg --delete $(GOBASE)/public $(GOBASE)/pack/; \
		rsync -avrztopg --delete $(GOBIN)/$(PROJECTNAME)_$(os)_$(goarch) $(GOBASE)/pack/bin/; \
		echo "  >  docker building 'tricentech/${APPTITLE}'..."; \
		docker build -t tricentech/${APPTITLE} -t tricentech/${APPTITLE}:$(VERSION) .; \
	)

## docker-upload: upload the docker container to CETCIOT and unpack it
docker-upload:
	ci/deploy-ent-to-lan.sh

## build-ci: run build-ci task. just for CI tools
build-ci:
	@echo "  >  Building binaries in CI flow..."
	@echo "  >  LDFLAGS = $(LDFLAGS)"
	$(foreach os, darwin linux windows, \
	  echo "     Building $(GOBIN)/$(PROJECTNAME)_$(os)_$(goarch)...$(os)"; \
	      GOARCH="$(goarch)" GOOS="$(os)" GOPATH="$(GOPATH)" GOBIN="$(GOBIN)" GO111MODULE="$(GO111MODULE)" GOPROXY="$(GOPROXY_FINAL)" \
	        go build -x -v -ldflags "$(LDFLAGS)" -o $(GOBIN)/$(APPNAME)_$(os)_$(goarch) $(GOBASE)/cli/main.go; \
	        gzip -f $(GOBIN)/$(APPNAME)_$(os)_$(goarch); \
	)
	@ls -la $(GOBIN)/*

go-build:
	@echo "  >  Building binary..."
	@echo "  >  LDFLAGS = $(LDFLAGS)"
	GOPATH=$(GOPATH) GOBIN=$(GOBIN) GO111MODULE=$(GO111MODULE) GOPROXY=$(GOPROXY_FINAL) \
	  go build -ldflags "$(LDFLAGS)" -o $(GOBIN)/$(APPNAME) $(GOBASE)/cli
	# go build -o $(GOBIN)/$(PROJECTNAME) $(GOFILES)
	# chmod +x $(GOBIN)/*
	ls -la $(GOBIN)/$(APPNAME)

go-generate: | $(BIN)/swagger $(BIN)/stringer
	@echo "  >  Generating dependency files..."
	@GOPATH=$(GOPATH) GOBIN=$(GOBIN) GO111MODULE=$(GO111MODULE) GOPROXY=$(GOPROXY_FINAL) \
	go generate $(generate)
	@echo "  >  Generating swagger files..."
	# GOROOT=/usr/local/opt/go/libexec
	# @GOPATH=$(GOPATH) GO111MODULE=$(GO111MODULE) GOPROXY=$(GOPROXY_FINAL)
	# @echo "     $(BIN)/swagger generate -o $(GOBIN)/sw.log spec -o $(GOBIN)/$(APPNAME).swagger.json -i cli/server/swagger.demo.1.go"
	#
	#@GOPATH=$(GOPATH) GOBIN=$(GOBIN) GO111MODULE=$(GO111MODULE) GOPROXY=$(GOPROXY_FINAL) \
	#$(BIN)/swagger generate -o $(GOBIN)/sw.log spec -o $(GOBIN)/$(APPNAME).swagger.json -i cli/server/swagger.demo.1.go || echo $? && echo "     ok"
	#@echo "  >  done"

go-mod-download:
	@GOPATH=$(GOPATH) GOBIN=$(GOBIN) GO111MODULE=$(GO111MODULE) GOPROXY=$(GOPROXY_FINAL) \
	go mod download

go-get:
	# Runs `go get` internally. e.g; make install get=github.com/foo/bar
	@echo "  >  Checking if there is any missing dependencies...$(get)"
	@GOPATH=$(GOPATH) GOBIN=$(GOBIN) GO111MODULE=$(GO111MODULE) GOPROXY=$(GOPROXY_FINAL) \
	go get $(get)

go-install:
	@GOPATH=$(GOPATH) GOBIN=$(GOBIN) GO111MODULE=$(GO111MODULE) GOPROXY=$(GOPROXY_FINAL) \
	go install $(GOFILES)

go-clean:
	@echo "  >  Cleaning build cache"
	@GOPATH=$(GOPATH) GOBIN=$(GOBIN) GO111MODULE=$(GO111MODULE) GOPROXY=$(GOPROXY_FINAL) \
	go clean

## test: run go test
test:
	@echo "  >  running go test"
	@GOPATH=$(GOPATH) GOBIN=$(GOBIN) GO111MODULE=$(GO111MODULE) GOPROXY=$(GOPROXY_FINAL) \
	go test ./...



$(BIN)/golint: | $(GOBASE)   # # # ❶
	@echo "  >  installing golint ..."
	#@-mkdir -p $(GOPATH)/src/golang.org/x/lint/golint
	#@cd $(GOPATH)/src/golang.org/x/lint/golint
	#@pwd
	@GOPATH=$(GOPATH) GO111MODULE=$(GO111MODULE) GOPROXY=$(GOPROXY_FINAL) \
	go get -v golang.org/x/lint/golint
	@echo "  >  installing golint ..."
	@GOPATH=$(GOPATH) GO111MODULE=$(GO111MODULE) GOPROXY=$(GOPROXY_FINAL) \
	go install golang.org/x/lint/golint
	@cd $(GOBASE)

$(BIN)/gocyclo: | $(GOBASE)  # # # ❶
	@echo "  >  installing gocyclo ..."
	@GOPATH=$(GOPATH) GO111MODULE=$(GO111MODULE) GOPROXY=$(GOPROXY_FINAL) \
	go install github.com/fzipp/gocyclo

$(BIN)/yolo: | $(GOBASE)     # # # ❶
	@echo "  >  installing yolo ..."
	@GOPATH=$(GOPATH) GO111MODULE=$(GO111MODULE) GOPROXY=$(GOPROXY_FINAL) \
	go install github.com/azer/yolo

$(BIN)/swagger: | $(GOBASE)     # # # ❶
	@echo "  >  installing go-swagger ..."
	#@GOPATH=$(GOPATH) GO111MODULE=$(GO111MODULE) GOPROXY=$(GOPROXY_FINAL) \
	#go get -v github.com/go-swagger/go-swagger
	@GOPATH=$(GOPATH) GO111MODULE=$(GO111MODULE) GOPROXY=$(GOPROXY_FINAL) \
	go install github.com/go-swagger/go-swagger/cmd/swagger
	@ls -la $(BIN)

$(BIN)/stringer: | $(GOBASE)
	@echo "  > installing stringer ..."
	@GOPATH=$(GOPATH) GO111MODULE=$(GO111MODULE) GOPROXY=$(GOPROXY_FINAL) \
	go get -u golang.org/x/tools/cmd/stringer
	@GOPATH=$(GOPATH) GO111MODULE=$(GO111MODULE) GOPROXY=$(GOPROXY_FINAL) \
	go mod tidy
	@ls -la $(BIN)/stringer


$(BASE):
	# @mkdir -p $(dir $@)
	# @ln -sf $(CURDIR) $@


.PHONY: format fmt gofmt cov gocov
## format: run gofmt tool
format: | $(GOBASE)
	@echo "  >  gofmt ..."
	@GOPATH=$(GOPATH) GOBIN=$(BIN) GO111MODULE=$(GO111MODULE) GOPROXY=$(GOPROXY_FINAL) \
	gofmt -l -w -s .

## fmt: run gofmt tool. = format
fmt: format

## gofmt: run gofmt tool. = format
gofmt: format

.PHONY: lint
## lint: run golint tool
lint: | $(GOBASE) $(GOLINT)
	@echo "  >  golint ..."
	@GOPATH=$(GOPATH) GOBIN=$(BIN) GO111MODULE=$(GO111MODULE) GOPROXY=$(GOPROXY_FINAL) \
	$(GOLINT) ./...

## cov: run go coverage test. = coverage
cov: coverage

## gocov: run go coverage test. = coverage
gocov: coverage

## coverage: run go coverage test
coverage: | $(GOBASE)
	@echo "  >  gocov ..."
	@GOPATH=$(GOPATH) GOBIN=$(BIN) GO111MODULE=$(GO111MODULE) GOPROXY=$(GOPROXY_FINAL) \
	go test -race -covermode=atomic -coverprofile coverage.txt ./...
	@GOPATH=$(GOPATH) GOBIN=$(BIN) GO111MODULE=$(GO111MODULE) GOPROXY=$(GOPROXY_FINAL) \
	go tool cover -html=coverage.txt -o cover.html
	@open cover.html

## codecov: run go test for codecov; (codecov.io)
codecov: | $(GOBASE)
	@echo "  >  codecov ..."
	@GOPATH=$(GOPATH) GOBIN=$(BIN) GO111MODULE=$(GO111MODULE) GOPROXY=$(GOPROXY_FINAL) \
	go test -race -coverprofile=coverage.txt -covermode=atomic ./...
	@bash <(curl -s https://codecov.io/bash) -t $(CODECOV_TOKEN)

## cyclo: run gocyclo tool
cyclo: | $(GOBASE) $(GOCYCLO)
	@echo "  >  gocyclo ..."
	@GOPATH=$(GOPATH) GO111MODULE=$(GO111MODULE) GOPROXY=$(GOPROXY_FINAL) \
	$(GOCYCLO) -top 20 ./...



info:
	@echo "     GOBASE: $(GOBASE)"
	@echo "      GOBIN: $(GOBIN)"
	@echo "     GOPATH: $(GOPATH)"
	@echo "GO111MODULE: $(GO111MODULE)"
	@echo "        BIN: $(BIN)"
	@echo "       PATH: $(PATH)"
	@echo "    GOPROXY: $(GOPROXY)"
	@echo "    APPNAME: $(APPNAME)"
	@echo "    VERSION: $(VERSION)"
	@echo

.PHONY: help
all: help
help: Makefile
	@echo
	@echo " Choose a command run in "$(PROJECTNAME)":"
	@echo
	@sed -n 's/^##//p' $< | column -t -s ':' |  sed -e 's/^/ /'
	@echo