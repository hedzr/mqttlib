# `mqtool` (go-mqttlib)

`mqtool` is a full-features MQTT v5/v3 Server and Client Library.

> LIMITED GPLv3. CAN'T BE USED FOR COMMERCIAL PURPOSE.


## Usage

To get all (sub-)commands：

```bash
bin/mqtool --tree
```







## LICENSE

GPLv3.

For Commercial Release/Purpose.

### NOTE

These codes were migrated, archived from an internal private sources.

DON'T USE IT FOR COMMERCIAL PURPOSE. ALL RIGHTS RESERVED.
