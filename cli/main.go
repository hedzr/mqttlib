/*
 * Copyright © 2020 Hedzr Yeh.
 */

package main

import "gitlab.com/hedzr/mqttlib/cli/cmd"

func main() {
	cmd.Entry()
}
