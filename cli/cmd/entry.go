/*
 * Copyright © 2020 Hedzr Yeh.
 */

package cmd

import (
	"fmt"
	"github.com/hedzr/cmdr"
	"github.com/sirupsen/logrus"
	mqtool "gitlab.com/hedzr/mqttlib"
	"gitlab.com/hedzr/mqttlib/pkg/mqttlib"
	"gitlab.com/hedzr/mqttlib/pkg/tcp/client"
	"gitlab.com/hedzr/mqttlib/pkg/tcp/server"
	"gitlab.com/hedzr/mqttlib/pkg/tool/vxconf"
	"strings"
)

// Entry is app main entry
func Entry() {

	// cmdr.AddOnConfigLoadedListener()
	// if cmdr.InDebugging() {
	// 	logrus.SetLevel(logrus.TraceLevel)
	// }

	if err := cmdr.Exec(buildRootCmd(),

		cmdr.WithLogex(cmdr.DebugLevel),
		cmdr.WithLogexPrefix("mqtool.logger"),

		cmdr.WithXrefBuildingHooks(func(root *cmdr.RootCommand, args []string) {
			cmdr.NewBool(false).
				Titles("tr", "trace", "enable-trace").
				Description("enable trace mode for tcp/mqtt send/recv data dump", "Totally enable tracing level for more logging output").
				Group(cmdr.SysMgmtGroup).
				AttachToRoot(root)
		}, nil),
		cmdr.WithUnknownOptionHandler(onUnknownOptionHandler),
		cmdr.WithWatchMainConfigFileToo(true),
		cmdr.WithNoWatchConfigFiles(false),
		cmdr.WithOptionMergeModifying(func(keyPath string, value, oldVal interface{}) {
			logrus.Debugf("%%-> -> %q: %v -> %v", keyPath, oldVal, value)
			if strings.HasSuffix(keyPath, ".mqtt.server.no-sys-stats") {
				mqttlib.FindServer().EnableSysStats(!vxconf.ToBool(value))
			}
			if strings.HasSuffix(keyPath, ".mqtt.server.no-sys-stats-log") {
				mqttlib.FindServer().EnableSysStatsLog(!vxconf.ToBool(value))
			}
		}),
	); err != nil {
		logrus.Errorf("Error: %v", err)
	}

}

func buildRootCmd() (rootCmd *cmdr.RootCommand) {

	root := cmdr.Root(mqtool.AppName, mqtool.Version).
		Copyright(mqtool.AppCopyright, mqtool.AppAuthor).
		Description(mqtool.AppDesc, mqtool.AppDescLong).
		Examples(mqtool.AppExamples).
		PreAction(onAppStart).
		PostAction(onAppExit)
	rootCmd = root.RootCommand()

	// mqtt client

	mqttOptCmd := root.NewSubCommand("mqtt", "mq").
		Description("MQTT server/client operations...").
		Group("Test")
	mqttlib.AttachToCmdr(mqttOptCmd)

	// tcp server & client

	tcp := root.NewSubCommand("tcp", "t").
		Description("TCP server/client operations...").
		Group("Test")
	server.AttachToCmdr(tcp)
	client.AttachToCmdr(tcp)

	// xy-print

	root.NewSubCommand("xy-print", "xy").
		Description("test terminal control sequences", "test terminal control sequences,\nverbose long descriptions here.").
		Group("Test").
		Action(func(cmd *cmdr.Command, args []string) (err error) {
			//
			// https://en.wikipedia.org/wiki/ANSI_escape_code
			// https://zh.wikipedia.org/wiki/ANSI%E8%BD%AC%E4%B9%89%E5%BA%8F%E5%88%97
			// https://en.wikipedia.org/wiki/POSIX_terminal_interface
			//

			fmt.Println("\x1b[2J") // clear screen

			for i, s := range args {
				fmt.Printf("\x1b[s\x1b[%d;%dH%s\x1b[u", 15+i, 30, s)
			}

			return
		})

	// mx-test

	mx := root.NewSubCommand("mx-test", "mx").
		Description("test new features", "test new features,\nverbose long descriptions here.").
		Group("Test").
		Action(func(cmd *cmdr.Command, args []string) (err error) {
			fmt.Printf("*** Got pp: %s\n", cmdr.GetStringR("mx-test.password"))
			fmt.Printf("*** Got msg: %s\n", cmdr.GetStringR("mx-test.message"))
			return
		})
	mx.NewFlagV("", "password", "pp").
		Description("the password requesting.", "").
		Group("").
		Placeholder("PASSWORD").
		ExternalTool(cmdr.ExternalToolPasswordInput)
	mx.NewFlagV("", "message", "m", "msg").
		Description("the message requesting.", "").
		Group("").
		Placeholder("MESG").
		ExternalTool(cmdr.ExternalToolEditor)

	// kafka-test, kfk

	// kfk := root.NewSubCommand().
	// 	Titles("kfk", "kafka-test", "kafka").
	// 	Description("test new features", "test new features,\nverbose long descriptions here.").
	// 	Group("Test")
	//
	// kfk.NewFlag(cmdr.OptFlagTypeString).
	// 	Titles("a", "addr", "brokers").
	// 	Description("kafka server addresses", "").
	// 	Group("").
	// 	DefaultValue([]string{}, "ADDR")
	//
	// kfk.NewSubCommand().
	// 	Titles("s", "server", "svr").
	// 	Description("test new features", "test new features,\nverbose long descriptions here.").
	// 	Group("Test").
	// 	Action(func(cmd *cmdr.Command, args []string) (err error) {
	// 		err = kafka.ServerEntry()
	// 		return
	// 	})
	//
	// kfk.NewSubCommand().
	// 	Titles("c", "client").
	// 	Description("test new features", "test new features,\nverbose long descriptions here.").
	// 	Group("Test").
	// 	Action(func(cmd *cmdr.Command, args []string) (err error) {
	// 		err = kafka.ClientEntry()
	// 		return
	// 	})

	// // m2mClient
	// m2mCmd := root.NewSubCommand().
	// 	Titles("m2m", "m2m-test").
	// 	Description("startHttp", "startHttp").
	// 	Group("").
	// 	Action(func(cmd *cmdr.Command, args []string) (err error) {
	// 		// xserver.StartRestFul();
	// 		service.M2M()
	// 		return
	// 	})
	//
	// m2mCmd.NewFlag(cmdr.OptFlagTypeString).
	// 	Titles("nrp", "notifyReceiverPort").
	// 	Description("NotifyReceiverPort?", "").
	// 	DefaultValue("", "NotifyReceiverPort")
	//
	// m2mCmd.NewFlag(cmdr.OptFlagTypeString).
	// 	Titles("addr", "address").
	// 	Description("Address?", "").
	// 	DefaultValue("", "Address")
	//
	// m2mCmd.NewFlag(cmdr.OptFlagTypeString).
	// 	Titles("curdn", "curdn").
	// 	Description("CURDN?", "").
	// 	DefaultValue("", "CURDN")
	//
	// m2mCmd.NewFlag(cmdr.OptFlagTypeString).
	// 	Titles("ty", "type").
	// 	Description("Type?", "").
	// 	DefaultValue("", "Type")
	//
	// m2mCmd.NewFlag(cmdr.OptFlagTypeString).
	// 	Titles("ori", "origin").
	// 	Description("Origin?", "").
	// 	DefaultValue("", "Origin")
	//
	// m2mCmd.NewFlag(cmdr.OptFlagTypeString).
	// 	Titles("tgt", "target").
	// 	Description("Target?", "").
	// 	DefaultValue("", "Target")
	//
	// m2mCmd.NewFlag(cmdr.OptFlagTypeString).
	// 	Titles("rn", "resourceName").
	// 	Description("ResourceName?", "").
	// 	DefaultValue("", "ResourceName")
	//
	// m2mCmd.NewFlag(cmdr.OptFlagTypeString).
	// 	Titles("lbl", "labels").
	// 	Description("Labels?", "").
	// 	DefaultValue("", "Labels")
	//
	// m2mCmd.NewFlag(cmdr.OptFlagTypeString).
	// 	Titles("acpi", "accessPolicyIds").
	// 	Description("accessPolicyIds?", "").
	// 	DefaultValue("", "accessPolicyIds")
	//
	// m2mCmd.NewFlag(cmdr.OptFlagTypeString).
	// 	Titles("api", "appId").
	// 	Description("AppId?", "").
	// 	DefaultValue("", "AppId")
	//
	// m2mCmd.NewFlag(cmdr.OptFlagTypeString).
	// 	Titles("poa", "pointOfAccess").
	// 	Description("PointOfAccess?", "").
	// 	DefaultValue("", "PointOfAccess")
	//
	// m2mCmd.NewFlag(cmdr.OptFlagTypeString).
	// 	Titles("mni", "maxNumOfInstances").
	// 	Description("MaxNumOfInstances?", "").
	// 	DefaultValue("", "MaxNumOfInstances")
	//
	// m2mCmd.NewFlag(cmdr.OptFlagTypeString).
	// 	Titles("con", "content").
	// 	Description("Content?", "").
	// 	DefaultValue("", "Content")
	//
	// m2mCmd.NewFlag(cmdr.OptFlagTypeString).
	// 	Titles("conf", "contentInfo").
	// 	Description("ContentInfo?", "").
	// 	DefaultValue("", "ContentInfo")
	//
	// m2mCmd.NewFlag(cmdr.OptFlagTypeString).
	// 	Titles("pvacor", "pvacors").
	// 	Description("Pvacors?", "").
	// 	DefaultValue("", "Pvacors")
	//
	// m2mCmd.NewFlag(cmdr.OptFlagTypeString).
	// 	Titles("pvsacor", "pvsacors").
	// 	Description("Pvsacors?", "").
	// 	DefaultValue("", "Pvsacors")
	//
	// m2mCmd.NewFlag(cmdr.OptFlagTypeInt).
	// 	Titles("pvaco", "pvaco").
	// 	Description("Pvaco?", "").
	// 	DefaultValue("", "Pvaco")
	//
	// m2mCmd.NewFlag(cmdr.OptFlagTypeInt).
	// 	Titles("pvsaco", "pvsaco").
	// 	Description("Pvsaco?", "").
	// 	DefaultValue("", "Pvsaco")
	//
	// m2mCmd.NewFlag(cmdr.OptFlagTypeString).
	// 	Titles("nu", "notifyUrls").
	// 	Description("NotifyUrls?", "").
	// 	DefaultValue("", "NotifyUrls")
	//
	// m2mCmd.NewFlag(cmdr.OptFlagTypeString).
	// 	Titles("su", "subscriptionUrl").
	// 	Description("SubscriptionUrl?", "").
	// 	DefaultValue("", "SubscriptionUrl")
	//
	// m2mCmd.NewFlag(cmdr.OptFlagTypeInt).
	// 	Titles("net", "notifyEventType").
	// 	Description("NotifyEventType?", "").
	// 	DefaultValue("", "NotifyEventType")
	//
	// m2mCmd.NewFlag(cmdr.OptFlagTypeString).
	// 	Titles("ri", "resourceId").
	// 	Description("ResourceId?", "").
	// 	DefaultValue("", "ResourceId")

	// // http server
	// root.NewSubCommand().
	// 	Titles("h1s", "httpServer").
	// 	Description("shs", "startHttpServer").
	// 	Group("").
	// 	Action(func(cmd *cmdr.Command, args []string) (err error) {
	// 		err = service.StartRestFul()
	// 		return
	// 	})
	//
	// // http 2 client
	// root.NewSubCommand().
	// 	Titles("h2", "h2-test").
	// 	Description("test http 2 client", "test http 2 client,\nverbose long descriptions here.").
	// 	Group("Test").
	// 	Action(func(cmd *cmdr.Command, args []string) (err error) {
	// 		server.RunClient()
	// 		return
	// 	})

	// // grpc client: grpc-test
	//
	// grpcCmd := root.NewSubCommand().
	// 	Titles("gr", "grpc-test", "grpc").
	// 	Description("test grpc client", "test grpc client,\nverbose long descriptions here.").
	// 	Group("Test")
	//
	// grpcCmd.NewFlag(cmdr.OptFlagTypeInt).
	// 	Titles("t", "times").
	// 	Description("requests should be made in each connection", "").
	// 	DefaultValue(service.Times, "TIMES").
	// 	Group("Z tests")
	//
	// grpcCmd.NewFlag(cmdr.OptFlagTypeInt).
	// 	Titles("p", "parallel").
	// 	Description("connections should be made", "").
	// 	DefaultValue(service.Parallel, "PARALLEL").
	// 	Group("Z tests")
	//
	// grpcCmd.NewFlag(cmdr.OptFlagTypeString).
	// 	Titles("rr", "registry", "reg-addr").
	// 	Description("service registry address", "").
	// 	DefaultValue(service.DefaultLocalRegistryAddress, "ADDR").
	// 	Group("Z tests")
	//
	// grpcCmd.NewFlag(cmdr.OptFlagTypeString).
	// 	Titles("a", "address", "addr").
	// 	Description("gRPC server address", "").
	// 	DefaultValue(service.DefaultAddress, "ADDR").
	// 	Group("Z tests")
	//
	// grpcCmd.NewFlag(cmdr.OptFlagTypeString).
	// 	Titles("ar", "address-remote", "addr-remote").
	// 	Description("2nd gRPC server address", "").
	// 	DefaultValue(service.DefaultRemoteAddress, "ADDR").
	// 	Group("Z tests")
	//
	// grpcCmd.NewFlag(cmdr.OptFlagTypeDuration).
	// 	Titles("", "sleep", "sleep-time").
	// 	Description("sleep time after one message sent", "").
	// 	DefaultValue(100*time.Millisecond, "MS").
	// 	Group("Z tests")
	//
	// grpcCmd.NewSubCommand().
	// 	Titles("r", "route-guide", "route").
	// 	Description("test grpc client", "test grpc client,\nverbose long descriptions here.").
	// 	Action(func(cmd *cmdr.Command, args []string) (err error) {
	// 		service.RunGrpcRouteClient(cmd)
	// 		return
	// 	})
	// grpcCmd.NewSubCommand().
	// 	Titles("g", "greeter").
	// 	Description("test grpc client", "test grpc client,\nverbose long descriptions here.").
	// 	Action(func(cmd *cmdr.Command, args []string) (err error) {
	// 		service.RunGrpcGreeterClient()
	// 		return
	// 	})
	// grpcCmd.NewSubCommand().
	// 	Titles("rnd", "random", "rand").
	// 	Description("random test grpc client", "random test grpc client,\nverbose long descriptions here.").
	// 	Action(func(cmd *cmdr.Command, args []string) (err error) {
	// 		service.RunRandGrpcRouteClient()
	// 		return
	// 	})
	//
	//
	// // server.OnBuildCmd(rootCmd)
	// //redis-test
	// redis := root.NewSubCommand().
	// 	Titles("redis", "redis-test").
	// 	Description("test new features", "test new features,\nverbose long descriptions here.").
	// 	Group("Test")
	//
	// redis.NewSubCommand().
	// 	Titles("set", "set").
	// 	Description("test new features", "redis test new features,\nverbose long descriptions here.").
	// 	Group("Test").
	// 	Action(func(cmd *cmdr.Command, args []string) (err error) {
	// 		service.StartRedis()
	// 		errr := redis_tool.SaveData("test-mqtool", "test")
	// 		logrus.Printf("redis-test-set(%v)", errr)
	// 		return
	// 	})
	//
	// redis.NewSubCommand().
	// 	Titles("get", "get").
	// 	Description("test new features", "redis test new features,\nverbose long descriptions here.").
	// 	Group("Test").
	// 	Action(func(cmd *cmdr.Command, args []string) (err error) {
	// 		service.StartRedis()
	// 		res, err := redis_tool.GetData("test-mqtool")
	// 		logrus.Printf("redis-test-get(%v)(%v)", res, err)
	// 		return
	// 	})
	//
	// redis.NewSubCommand().
	// 	Titles("del", "del").
	// 	Description("test new features", "redis test new features,\nverbose long descriptions here.").
	// 	Group("Test").
	// 	Action(func(cmd *cmdr.Command, args []string) (err error) {
	// 		service.StartRedis()
	// 		res, err := redis_tool.DelData("test-mqtool")
	// 		logrus.Printf("redis-test-get(%v)(%v)", res, err)
	// 		return
	// 	})

	return
}
