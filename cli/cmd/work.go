/*
 * Copyright © 2020 Hedzr Yeh.
 */

package cmd

import (
	"fmt"
	"github.com/hedzr/cmdr"
	"github.com/sirupsen/logrus"
	"os"
	"strings"

	mqtool "gitlab.com/hedzr/mqttlib"
)

// ESC CSI escape code
const ESC = 27

var clear = fmt.Sprintf("%c[%dA%c[2K", ESC, 1, ESC)

func clearLines(lineCount int) {
	_, _ = fmt.Fprint(os.Stdout, strings.Repeat(clear, lineCount))
}

func modifier(daemonServerCommand *cmdr.Command) *cmdr.Command {
	// rootCmd := daemonServerCommand.GetRoot()
	// rootCmd.PreAction = func(cmd *cmdr.Command, args []string) (err error) {
	// 	earlierInitLogger()
	// 	return
	// }

	if startCmd := cmdr.FindSubCommand("start", daemonServerCommand); startCmd != nil {
		startCmd.PreAction = onServerPreStart
		startCmd.PostAction = onServerPostStop
		if flg := cmdr.FindFlag("port", daemonServerCommand); flg != nil {
			flg.DefaultValue = mqtool.DefaultPort
		} else {
			logrus.Warnf("CAN'T found server cmd")
		}
	}

	// cmdOpt := cmdr.NewCmdFrom(daemonServerCommand)
	//
	// cmdOpt.NewFlag(cmdr.OptFlagTypeBool).
	// 	Titles("", "update-cc").
	// 	Description("tranform local app-config to config-center", ``).
	// 	DefaultValue(false, "").
	// 	Group("service spec").
	// 	Action(func(cmd *cmdr.Command, args []string) (err error) {
	// 		logrus.Info("--update-cc FOUND, local config will be transforming to remote config center.")
	// 		cmdr.Set(cc.ForceUpdateKeyPath, true)
	// 		return
	// 	})
	//
	// cmdOpt.NewFlag(cmdr.OptFlagTypeBool).
	// 	Titles("", "update-sc").
	// 	Description("tranform local service-config to config-center", ``).
	// 	DefaultValue(false, "").
	// 	Group("service spec").
	// 	Action(func(cmd *cmdr.Command, args []string) (err error) {
	// 		logrus.Info("--update-sc FOUND, local service-config will be transforming to remote config center.")
	// 		cmdr.Set(registry.ForceUpdateServiceConfigKeyPath, true)
	// 		return
	// 	})

	return daemonServerCommand
}

func onAppStart(cmd *cmdr.Command, args []string) (err error) {
	// logrus.Debug("onAppStart")
	// logger.EarlierInitLogger()
	return
}

func onAppExit(cmd *cmdr.Command, args []string) {
	// logrus.Debug("onAppExit")
}

func onUnknownOptionHandler(isFlag bool, title string, cmd *cmdr.Command, args []string) (fallbackToDefaultDetector bool) {
	return true
}
